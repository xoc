// If we make attribute inline, we get better profile graphs
// out of pprof.  But then gdb shows you attribute calling
// attribute calling attribute instead of the name of the
// function that inlined the copy of attribute.  Sigh.

#undef ATTRINLINE
#ifdef ATTRINLINE
static inline Zpoly
attribute(Ast *ast, AttrInfo *info) __attribute__((always_inline));
inline
#endif

static Zpoly
attribute(Ast *ast, AttrInfo *info)
{
	Attr *a;
	AttrLink link;
	Zfn *f;
	Zpoly val;

	a = get_attribute(ast, info);
	switch(a->state){
	case AttrInit:
		return a->data;
	case AttrUninit:
		a->state = AttrComputing;
		link.info = info;
		link.next = attrlinks;
		link.ast = ast;
		attrlinks = &link;
		f = *info->fn;
		if(f == nil){
			fprint(2, "no implementation of %s, declared at %s\n",
				info->name, info->pos);
			abort();
		}
		val = ((Zpoly(*)(void*, Ast*))f->fn)(f->escf, ast);
		a->data = val;
		a->state = AttrInit;
		attrlinks = link.next;
		info->ncompute++;
		return a->data;
	case AttrComputing:
		attrloop(info);
		abort();
	}
	fprint(2, "bad state %d in attribute", a->state);
	abort();
	return 0;
}

