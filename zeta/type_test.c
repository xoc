#include "a.h"
#include "type.h"

int
main(int argc, char **argv)
{
	Type *tbool, *tint, *tfloat, *tstring, *tvoid, *tfmt;
	Type *tlint, *tlstring, *tintstring, *tastring;
	
	tbool = typebool();
	tint = typeint();
	tfloat = typefloat();
	tstring = typestring();
	tvoid = typevoid();
	tfmt = typewrapper(nameof("Fmt"));
	
	assert(tbool);
	assert(tint);
	assert(tfloat);
	assert(tstring);
	assert(tvoid);
	assert(tfmt);
	
	assert(tbool == typebool());
	assert(tint == typeint());
	assert(tfloat == typefloat());
	assert(tstring == typestring());
	assert(tvoid == typevoid());
	assert(tvoid == typetuple(nil));
	assert(tvoid == typetuplel(nil));
	assert(tfmt == typewrapper(nameof("Fmt")));
	
	tlint = typelist(tint);
	tlstring = typelist(tstring);
	tintstring = typetuplel(tint, tstring, nil);
	tastring = typearray(tstring);

	assert(tlint);
	assert(tlstring);
	assert(tintstring);
	assert(tastring);
	
	assert(tlint == typelist(tint));
	assert(tlstring == typelist(tstring));
	assert(tintstring == typetuplel(tint, tstring, nil));
	assert(tastring == typearray(tstring));

	return 0;
}
