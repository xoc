#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "a.h"
#include "type.h"
#include "runtime.h"
#include "compile.h"

void
writemodule(Name zeta)
{
	int fd;
	Name zm, zo;
	VarL *vl;
	VarL *topvarl;
	IncludeL *il;
	ModuleL *ml;
	struct stat st;
	char buf[1024];
	Fmt fmt;

	zo = zeta2zo(zeta);
	if(stat(nametostr(zo), &st) < 0)
		panic("stat %s: %r", zo);

	zm = zeta2zm(zeta);
	if((fd = creat(nametostr(zm), 0666)) < 0)
		panic("create %s: %r", zm);
	fmtfdinit(&fmt, fd, buf, sizeof buf);

	fmtprint(&fmt, "zeta7\n");
	fmtprint(&fmt, "mtime\t%lu\n", (ulong)st.st_mtime);

	// List input files. 
	// (Reverse list so that it goes out in right order.)
	includel = revIncludeL(includel);
	for(il=includel; il; il=il->tl){
		if(strchr(nametostr(il->hd->file), '\t'))
			panic("bugger off, tabby");
		fmtprint(&fmt, "include\t%s\t%lu\t%lu\n",
			il->hd->file, (ulong)il->hd->size, (ulong)il->hd->mtime);
	}
	includel = revIncludeL(includel);

	// List imported modules.
	importl = revModuleL(importl);
	for(ml=importl; ml; ml=ml->tl)
		if(!ml->hd->internal)
			fmtprint(&fmt, "import\t%s\t%lu\n",
				fullpath(nametostr(ml->hd->zeta)), (ulong)ml->hd->mtime);
	importl = revModuleL(importl);

	// List top-level variables, but not modules.
	topvarl = revVarL(topscope->varl);
	for(vl=topvarl; vl; vl=vl->tl){
		Var *v;
		
		v = vl->hd;
		if(v->type->left == typemodule())
			continue;
		if(v->flags&VarBuiltin)
			continue;
		if(v->type->left->id < 0)
			panic("wmodule %s %T", v->name, v->type->left);
		fmtprint(&fmt, "var\t%s\t%s\t%d\t%d\t%d\n",
			v->name, v->cname,
			v->type->left->id,
			v->realtype ? v->realtype->id : -1,
			v->flags);
	}
	revVarL(topvarl);

	fmtfdflush(&fmt);
	close(fd);	
}
