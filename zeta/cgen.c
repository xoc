#include "a.h"
#include "type.h"
#include "runtime.h"
#include "compile.h"
#include "grammar.h"

int linenumbers = 1;
void cgenmatch(Cbuf *cb, Expr *pattern, Name val, int lfail);
static int gtmp;

Name
cbtmp(Cbuf *cb)
{
	return nameprint("_%d", gtmp++);
}

Name cgenlexpr(Cbuf*, Expr*);
Name cgenexpr(Cbuf*, Expr*);

static char* binfmt[MaxBinOp] = {
	[BinEqEq]	"(P(%s) == P(%s))",
	[BinNotEq]	"(P(%s) != P(%s))",

	[BinAddInt]	"(%s + %s)",
	[BinSubInt]	"(%s - %s)",
	[BinMulInt]	"(%s * %s)",
	[BinDivInt]	"(%s / %s)",
	[BinModInt]	"(%s %% %s)",
	[BinLshInt]	"(%s << %s)",
	[BinRshInt]	"(%s >> %s)",
	[BinOrInt]	"(%s | %s)",
	[BinAndInt]	"(%s & %s)",
	[BinXorInt]	"(%s ^ %s)",
	[BinLtInt]	"(%s < %s)",
	[BinLeInt]	"(%s <= %s)",
	[BinGtInt]	"(%s > %s)",
	[BinGeInt]	"(%s >= %s)",

	[BinAddFlt]	"F2P(P2F(%s) + P2F(%s))",
	[BinSubFlt]	"F2P(P2F(%s) - P2F(%s))",
	[BinMulFlt]	"F2P(P2F(%s) * P2F(%s))",
	[BinDivFlt]	"F2P(P2F(%s) / P2F(%s))",
	[BinModFlt]	"F2P(fmodf(P2F(%s), P2F(%s)))",
	
	[BinAddStr]	"addZstring(%s, %s)",
	[BinLtStr]	"(cmpZstring(%s, %s) < 0)",
	[BinGtStr]	"(cmpZstring(%s, %s) > 0)",
	[BinLeStr]	"(cmpZstring(%s, %s) <= 0)",
	[BinGeStr]	"(cmpZstring(%s, %s) >= 0)",
	
	[BinIndexStr]	"indexZstring(%s, %s)", 

	[BinConsList]	"mkZlist(P(%s), %s)",
	[BinIndexList]	"indexZlist(%s, %s)",
	[BinAssocList]	"assocZlist(%s, P(%s))",
	[BinAddList]	"addZlist(%s, %s)",

	[BinIndexArr]	"*indexZarray(%s, %s)",
	
	[BinIndexMap]	"*indexZmap(%s, P(%s))",
	[BinIndexFmap] "indexZfmap(%s, P(%s))",

	[BinIndexTuple] "indexZtuple(%s, %s)",
	
	[BinJoinAst]	"astjoin(%s, %s)",
	[BinSameAst]	"astsame(%s, %s)",
};

// We could just always insert casts, but I like having
// the C compiler check as much as it can.
static int bincast[MaxBinOp] = {
	[BinIndexList] 1,
	[BinAssocList] 1,
	[BinIndexArr] 1,
	[BinIndexMap]	1,
	[BinIndexTuple] 1,
};
static int bincastargs[MaxBinOp] = {
	[BinEqEq] 1,
	[BinNotEq] 1,
};

static char* unfmt[MaxUnOp] = {
	[UnNegInt]	"(-%s)",
	[UnPosInt]	"(+%s)",
	[UnTwidInt]	"(~%s)",
	[UnBoolAny]	"(%s != 0)",
	
	[UnLenList]	"lenZlist(%s)",
	[UnLenArr]	"lenZarray(%s)",
	[UnLenStr]	"lenZstring(%s)",
	
	[UnKeysMap]	"keysZmap(%s)",
	
	[UnStringBool]	"(%s ? N(\"true\") : N(\"false\"))",
	[UnStringInt]	"nameprint(\"%%d\", %s)",
	[UnStringFlt]	"nameprint(\"%%g\", (double)P2F(%s))",
	[UnStringStr]	"%s",

	[UnArrayList]	"arrayZlist(%s)",
	[UnArrayInt]	"mkZarray(%s)",
	
	[UnListArr]	"listZarray(%s)",
	[UnListAst]	"asttolist(%s)",
	
	[UnSplitAst]	"astsplit(%s)",
	[UnCopyAst]	"astcopy(%s)",
	[UnLastkidAst]	"astlastkid(%s)",
	
	[UnMergedAst]	"astmerged(%s)",
	[UnSlottedAst]	"astslotted(%s)",
	
	[UnLispAst]	"ZN(astlisp(%s))",
	[UnLongAst]	"ZN(astlong(%s))",
	[UnStringAst]	"ZN(asttostring(%s))",
	
	[UnIntFlt]	"((Zint)P2F(%s))",
	[UnIntString]	"intZstring(%s)",
	[UnIntBool]	"(%s)",

	[UnNegFlt]	"F2P(-P2F(%s))",
	
	[UnFloatStr]	"F2P(atof(nametostr(%s)))",
	
	[UnPtr]	"nameprint(\"%%p\", %s)",
	
	[UnRmcopyAst]	"astremovecopiedfrom(%s)",
};

void
cgenassign(Cbuf *cb, Expr *dst, Name src)
{
	int i;
	ExprL *el;
	TypeL *tl;
	Name n1;

	switch(dst->op){
	case ExprName:
		if(dst->var == nil && dst->name == N("_")){
			cbprint(cb, dst->pos, "%s;", src);
			break;
		}
		goto Simple;
	case ExprDot:
		if(dst->e1->type->op == TypeGram){
			cbprint(cb, dst->pos, "set_attribute(%s, &%s_info, P(%s));",
				cgenexpr(cb, dst->e1),
				dst->var->cname,
				src);
			break;
		}
		// fall through
	case ExprBinary:
	case ExprDeclType:
	Simple:
		cbprint(cb, dst->pos, "%s = %s;", cgenlexpr(cb, dst), src);
		break;
	case ExprTuple:
		n1 = cbtmp(cb);
		cbprint(cb, dst->pos, "Zpoly *%s = %s;", n1, src);
		if(dst->type->op != TypeTuple)
			panic("expected tuple - %T", dst->type);
		for(el = dst->el, tl = dst->type->typel, i = 0; el && tl; el=el->tl, tl=tl->tl, i++)
			cgenassign(cb, el->hd, nameprint("((%hT)%s[%d])", tl->hd, n1, i));
		break;
	default:
		panic("cgenassign");
		break;
	}
}

Name
cgenlexpr(Cbuf *cb, Expr *expr)
{
	Name n1, n2, n3;
	switch(expr->op){
	default:
		panic("cannot leval %#P", expr->pos);
	case ExprName:
	case ExprDot:
	case ExprDeclType:
		return cgenexpr(cb, expr);
	case ExprBinary:
		n1 = cgenexpr(cb, expr->e1);
		n2 = cgenexpr(cb, expr->e2);
		assert(binfmt[expr->binop]);
		if(bincastargs[expr->binop]){
			if(expr->e1->type != expr->e2->type)
				n2 = nameprint("((%hT)%s)", expr->e1->type, n2);
		}
		n3 = nameprint(binfmt[expr->binop], n1, n2);
		if(bincast[expr->binop]){
			assert(nametostr(n3)[0] == '*');
			n3 = nameprint("*((%hT*)%s)", expr->type, nametostr(n3)+1);
		}
		return n3;
	}
}

Name
cgenfn(Cbuf *cb, Fn *fn)
{
	Name name;
	Fmt fmt;
	Fmt *oldfmt;
	FnargL *al;
	VarL *vl;
	Var *v;
	StmtL *l;
	Fn *oldfn;
	
	oldfn = cb->fn;
	cb->fn = fn;

	name = cbtmp(cb);
	if(fn->name)
		name = nameprint("%s%s", csanitize(fn->name), name);
	else
		name = nameprint("fn%s", name);

	// Declare function prototype.
	fmtstrinit(&fmt);
	oldfmt = cb->fmt;
	cb->fmt = &fmt;
	fmtprint(&fmt, "static %hT %s(void *outf", fn->returntype, name);
	for(al=fn->args; al; al=al->tl)
		fmtprint(&fmt, ", %hT a_%s", al->hd->var->type->left, al->hd->name);
	fmtprint(&fmt, ")\n{\n");
	
	// Declare local variables.
	for(vl=fn->localframe->varl; vl; vl=vl->tl){
		v = vl->hd;
		if(!v->escape && !(v->flags&VarRedeclare) && !v->redirect)
			fmtprint(&fmt, "\t%hT %s;\n", v->type->left, v->cname);
	}
	
	// Declare local escape frame if needed.
	// First element in frame is pointer to outer frame.
	if(fn->nescape){
		fmtprint(&fmt, "\tZpoly *escf = emalloc(%d*sizeof(void*));\n", fn->nescape);
		fmtprint(&fmt, "\tescf[0] = outf;\n");
		
		// Copy any arguments that need to be copied.
		for(al=fn->args; al; al=al->tl){
			v = al->hd->var;
			if(v->escape)
				fmtprint(&fmt, "\tescf[%d] = P(a_%s);\n", v->escape, al->hd->name);
		}
	}

	// Function body
	fmtprint(&fmt, "\n");
	for(l=fn->body; l; l=l->tl)
		cgenstmt(cb, l->hd);
	if(fn->returntype == typevoid())
		fmtprint(&fmt, "\treturn 0;\n");
	fmtprint(&fmt, "}\n");
	cb->fmt = oldfmt;
	fmtprint(&cb->topfmt, "%s\n", fmtstrflush(&fmt));

	cb->fn = oldfn;
	return name;
}

Name
cgenvar(Cbuf *cb, Var *v)
{
	Fn *f;

	if(!v->escape){
		if(v->flags&VarRedeclare)
			return nameprint("((%hT)%s)", v->type->left, v->cname);
		return v->cname;
	}

	assert(v->scope->frame->fn);
	assert(cb->fn);
	if(v->scope->frame->fn != cb->fn){
		// *(Zint*)((Zpoly*)outf+1)	x in outer frame
		// *(Zint*)(*(Zpoly**)outf+1)	x two frames up
		// *(Zint*)(**(Zpoly***)outf+1)	x three frames up
		Name stars;
		
		stars = N("");
		for(f=cb->fn->outer; f != v->scope->frame->fn; f=f->outer)
			stars = nameprint("*%s", stars);
		return nameprint("(*(%hT*)(%s(Zpoly*%s)outf+%d))",
			v->type->left, stars, stars, v->escape);
	}else{
		// *(Zint*)(escf+1)	x in current frame
		return nameprint("(*(%hT*)(escf+%d))",
			v->type->left, v->escape);
	}
}

Name
cgenexpr(Cbuf *cb, Expr *expr)
{
	Name n1, n2, n3, nval, nloc, ntmp;
	NameL *nl;
	ExprL *l;
	Fmt fmt;
	Var *v;
	int i, postinc;
	Type *t, *xtype;

	switch(expr->op){
	case ExprDots:
		panic("cgenexpr dots");

	case ExprBool:
	case ExprInteger:
		return nameprint("%d", expr->i);
	
	case ExprFloat:
		return nameprint("F2P(%g)", expr->f);

	case ExprString:
		return nameprint("%N", expr->str);
	
	case ExprNil:
		return nameprint("0");

	case ExprName:
		v = expr->var;
		while(v->redirect)
			v = v->redirect;
		return cgenvar(cb, v);

	case ExprBinary:
		n1 = cgenexpr(cb, expr->e1);
		n2 = cgenexpr(cb, expr->e2);
		assert(binfmt[expr->binop]);
		n3 = nameprint(binfmt[expr->binop], n1, n2);
		if(bincast[expr->binop])
			n3 = nameprint("((%hT)%s)", expr->type, n3);
		return n3;

	case ExprBinaryEq:
		postinc = 0;
	BinaryEq:
		n1 = cgenlexpr(cb, expr->e1);
		n2 = cgenexpr(cb, expr->e2);
		assert(binfmt[expr->binop]);
		nloc = cbtmp(cb);
		cbprint(cb, expr->pos, "%hT *%s = &%s;",
			expr->e1->type, nloc, n1);
		nval = cbtmp(cb);
		cbprint(cb, expr->pos, "%hT %s = *%s;", expr->e1->type, nval, nloc);
		n3 = nameprint(binfmt[expr->binop], nval, n2);
		if(bincast[expr->binop])
			n3 = nameprint("((%hT)%s)", expr->e1->type, n3);
		ntmp = cbtmp(cb);
		cbprint(cb, expr->pos, "%hT %s = %s;", expr->type, ntmp, n3);
		cbprint(cb, expr->pos, "*%s = %s;", nloc, ntmp);
		if(postinc)
			return nval;
		return ntmp;

	case ExprPreInc:
	case ExprPreDec:
		postinc = 0;
		expr = expr->e3;
		goto BinaryEq;

	case ExprPostInc:
	case ExprPostDec:
		postinc = 1;
		expr = expr->e3;
		goto BinaryEq;

	case ExprUnary:
		n1 = cgenexpr(cb, expr->e1);
		if(expr->unop == UnParentAst)
			return nameprint("astparent(%s, %#T)", n1, expr->type);
		if(expr->unop == UnSlotAst)
			return nameprint("astcvtslot(%s, %#T)", n1, expr->type);
		assert(unfmt[expr->unop]);
		return nameprint(unfmt[expr->unop], n1);

	case ExprAssign:
	case ExprDeclAssign:
		n1 = cbtmp(cb);
		n2 = cgenexpr(cb, expr->e2);
		// might have to cast structs, for example
		if(expr->type != expr->e2->type)
			n2 = nameprint("((%hT)%s)", expr->type, n2);
		cbprint(cb, expr->pos, "%hT %s = %s;", expr->type, n1, n2);
		cgenassign(cb, expr->e1, n1);
		return n1;
	
	case ExprDeclType:
		v = expr->var;
		while(v->redirect)
			v = v->redirect;
		Name n1 = cgenvar(cb, v);
		cbprint(cb, expr->pos, "%s = 0;", n1);
		return n1;
	
	case ExprCall:
		n1 = cbtmp(cb);
		cbprint(cb, expr->e1->pos, "Zfn* %s = %s;",
			n1, cgenexpr(cb, expr->e1));
		fmtstrinit(&fmt);
		fmtprint(&fmt, "((%hT(*)(void*", expr->type);
		for(l=expr->el; l; l=l->tl)
			fmtprint(&fmt, ", %hT", l->hd->type);
		fmtprint(&fmt, "))%s->fn)(%s->escf", n1, n1);
		for(l=expr->el; l; l=l->tl)
			fmtprint(&fmt, ", %s", cgenexpr(cb, l->hd));
		fmtprint(&fmt, ")");
		return nameof(fmtstrflush(&fmt));
	
	case ExprMkStruct:
		n1 = cbtmp(cb);
		cbprint(cb, expr->pos, "%hT %s = emalloc(sizeof *%s);", expr->type, n1, n1);
		if(expr->type->tags || expr->type->super)
			cbprint(cb, expr->pos, "%s->tag = %#T;", n1, expr->type);
		for(l=expr->el, nl=expr->namel; l; l=l->tl, nl=nl->tl){
			if(l->hd->op == ExprDots)
				break;
			cbprint(cb, expr->pos, "%s->%s = (%hT)%s;",
				n1, nl->hd, structdot(expr->type, nl->hd, &xtype), cgenexpr(cb, l->hd));
		}
		return n1;

	case ExprMkList:
		nval = cbtmp(cb);
		nloc = cbtmp(cb);
		cbprint(cb, expr->pos, "Zlist* %s = 0;", nval);
		cbprint(cb, expr->pos, "Zlist** %s = &%s;", nloc, nval);
		for(l=expr->el; l; l=l->tl){
			n1 = cgenexpr(cb, l->hd);
			cbprint(cb, expr->pos, "*%s = mkZlist(P(%s), 0);", nloc, n1);
			cbprint(cb, expr->pos, "%s = &(*%s)->tl;", nloc, nloc);
		}
		return nval;

	case ExprMkArray:
		nval = cbtmp(cb);
		cbprint(cb, expr->pos, "Zarray *%s = mkZarray(%d);", nval,
			lenExprL(expr->el));
		for(l=expr->el, i=0; l; l=l->tl, i++)
			cbprint(cb, expr->pos, "%s->a[%d] = P(%s);", nval, i, cgenexpr(cb, l->hd));
		return nval;

	case ExprTuple:
		nval = cbtmp(cb);
		cbprint(cb, expr->pos, "Zpoly *%s;", nval);
		cbprint(cb, expr->pos, "%s = emalloc(%d*sizeof %s[0]);",
			nval, lenExprL(expr->el), nval);
		for(l=expr->el, i=0; l; l=l->tl, i++)
			cbprint(cb, expr->pos, "%s[%d] = P(%s);",
				nval, i, cgenexpr(cb, l->hd));
		return nval;

	case ExprMap:
		panic("cgenexpr map");
	case ExprMkMap:
		nval = cbtmp(cb);
		cbprint(cb, expr->pos, "Zmap *%s;", nval);
		cbprint(cb, expr->pos, "%s = mkZmap();", nval);
		for(l=expr->el; l; l=l->tl){
			n1 = cgenexpr(cb, l->hd->e1);
			n2 = cgenexpr(cb, l->hd->e2);
			cbprint(cb, expr->pos, "*indexZmap(%s, P(%s)) = P(%s);",
				nval, n1, n2);
		}
		return nval;
	
	case ExprMkFmap:
		nval = cbtmp(cb);
		cbprint(cb, expr->pos, "Zfmap *%s;", nval);
		l = expr->el;
		if(l && l->hd->op != ExprMap){
			assert(l->hd->type->op == TypeFmap);
			cbprint(cb, expr->pos, "%s = %s;", nval, cgenexpr(cb, l->hd));
			l = l->tl;
		}else
			cbprint(cb, expr->pos, "%s = 0;", nval);
		for(; l; l=l->tl){
			n1 = cgenexpr(cb, l->hd->e1);
			n2 = cgenexpr(cb, l->hd->e2);
			cbprint(cb, expr->pos, "%s = insertZfmap(%s, P(%s), P(%s));",
				nval, nval, n1, n2);
		}
		return nval;

	case ExprDot:
		if(expr->e1->type->op == TypeGram){
			n1 = cbtmp(cb);
			cbprint(cb, expr->pos, "Ast *%s = %s;", n1, cgenexpr(cb, expr->e1));
			if(expr->var->flags&VarBuiltin){
				if(expr->var->flags&VarExtensible){
					if(expr->name == N("canonical")){
						// Don't call the canonicalizer directly,
						// because we need the recursion provided
						// by astcanonicalize.  This makes the implementation
						// of a.canonical kind of a lie, but it's a little white lie.
						cbprint(cb, expr->pos, "%s = astcanonicalize(%s);", n1, n1);
						return n1;
					}
					n2 = cbtmp(cb);
					cbprint(cb, expr->pos, "Zfn *%s = %s;", n2, expr->var->cname);
					return nameprint("((%hT)((Zpoly(*)(void*, Ast*))%s->fn)(%s->escf, %s))",
						expr->type, n2, n2, n1);
				}
				return nameprint("%s(%s)", expr->var->cname, n1);
			}
			return nameprint("((%hT)attribute(%s, &%s_info))", expr->type, n1, expr->var->cname);
		}
		n1 = cgenexpr(cb, expr->e1);
		if(expr->e1->type->op == TypeStruct && expr->realtype){
			n1 = nameprint("((%hT)xget(&%s->extend, %#T))",
				expr->realtype, n1, expr->realtype);
		}
		return nameprint("(%s->%s)", n1, expr->name);
	
	case ExprNot:
		return nameprint("(!%s)", cgenexpr(cb, expr->e1));
	
	case ExprAndAnd:
		nval = cbtmp(cb);
		cbprint(cb, expr->pos, "Zbool %s;", nval);
		cbprint(cb, expr->pos, "if(!%s) %s = 0; else{", cgenexpr(cb, expr->e1), nval);
		cbprint(cb, expr->pos, "%s = %s;", nval, cgenexpr(cb, expr->e2));
		cbprint(cb, expr->pos, "}");
		return nval;

	case ExprOrOr:
		nval = cbtmp(cb);
		cbprint(cb, expr->pos, "Zbool %s;", nval);
		cbprint(cb, expr->pos, "if(%s) %s = 1; else{", cgenexpr(cb, expr->e1), nval);
		cbprint(cb, expr->pos, "%s = %s;", nval, cgenexpr(cb, expr->e2));
		cbprint(cb, expr->pos, "}");
		return nval;
	
	case ExprSlice:
		n1 = cgenexpr(cb, expr->e1);
		n2 = cgenexpr(cb, expr->e2);
		if(expr->e3)
			n3 = cgenexpr(cb, expr->e3);
		else
			n3 = N("NO_UPPER");
		switch(expr->e1->type->op){
		default:
			panic("cgenexpr slice %T", expr->e1->type);
		case TypeArray:
			return nameprint("sliceZarray(%s, %s, %s)",
				n1, n2, n3);
		case TypeString:
			return nameprint("sliceZstring(%s, %s, %s)",
				n1, n2, n3);
		}
	
	case ExprFn:
		n1 = cgenfn(cb, expr->fn);
		n2 = cbtmp(cb);
		if(cb->fn && cb->fn->nescape)
			n3 = N("escf");
		else
			n3 = N("0");
		// Initialize function pointer here.
		cbprint(cb, expr->pos, "Zfn *%s = mkZfn(%s, %s);", n2, n1, n3);
		return n2;
	
	case ExprMatch:
		n1 = cbtmp(cb);
		cbprint(cb, expr->pos, "%hT %s = %s;", expr->e1->type, n1, cgenexpr(cb, expr->e1));
		n2 = cbtmp(cb);
		cbprint(cb, expr->pos, "int %s;", n2);
		cgenmatch(cb, expr->e2, n1, expr->l1);
		cbprint(cb, expr->pos, "%s = 1;", n2);
		cbprint(cb, expr->pos, "if(0){ L%d: %s = 0; }", expr->l1, n2);
		return n2;
	
	case ExprSubtype:
		t = expr->realtype;
		n1 = cbtmp(cb);
		cbprint(cb, expr->pos, "%hT %s = %s;", expr->e1->type, n1, cgenexpr(cb, expr->e1));
		return nameprint("(%s != 0 && issubtype(%s->tag, %#T))",
			n1, n1, expr->realtype);

	case ExprTag:
		n1 = cbtmp(cb);
		cbprint(cb, expr->pos, "%hT %s = %s;", expr->e1->type, n1, cgenexpr(cb, expr->e1));
		return nameprint("nameprint(\"%%T\", %s ? %s->tag : 0)", n1, n1);
	
	case ExprParse:
		n1 = cgenexpr(cb, expr->e1);
		return nameprint("parsetext(%#T, %s)", expr->type, n1);
	
	case ExprBacktick:
	case ExprBacktick2:
		return cgenpexpr(cb, expr);
	}
	panic("cgenexpr %#P", expr->pos);
	return nil;
}

void
cgenmatch(Cbuf *cb, Expr *expr, Name val, int lfail)
{
	Name n1, n2;
	ExprL *el;
	NameL *nl;
	int i;

	switch(expr->op){
	default:
		panic("cgenmatch %#P", expr->pos);

	case ExprName:
		cgenassign(cb, expr, val);
		break;

	case ExprDeclAssign:
		n1 = cbtmp(cb);
		cbprint(cb, expr->pos, "%hT %s = (%hT)%s;", expr->type, n1, expr->type, val);
		cgenmatch(cb, expr->e1, n1, lfail);
		cgenmatch(cb, expr->e2, n1, lfail);
		break;
		
	case ExprBinary:
		if(expr->name == N("::")){
			cbprint(cb, expr->pos, "if(%s == 0) goto L%d;", val, lfail);
			cgenmatch(cb, expr->e1, nameprint("((%hT)%s->hd)", expr->e1->type, val), lfail);
			cgenmatch(cb, expr->e2, nameprint("(%s->tl)", val), lfail);
			break;
		}
		panic("cgenmatch binary %s", expr->name);

	case ExprMkList:
		n1 = cbtmp(cb);
		n2 = nameprint("((%hT)%s->hd)", expr->type->left, n1);
		cbprint(cb, expr->pos, "Zlist *%s = %s;", n1, val);
		for(el=expr->el; el; el=el->tl){
			if(el->hd->op == ExprDots)
				break;
			cbprint(cb, expr->pos, "if(%s == 0) goto L%d;", n1, lfail);
			cgenmatch(cb, el->hd, n2, lfail);
			cbprint(cb, expr->pos, "%s = %s->tl;", n1, n1);
		}
		if(el == nil)
			cbprint(cb, expr->pos, "if(%s != 0) goto L%d;", n1, lfail);
		break;
	case ExprMkArray:
		n1 = cbtmp(cb);
		cbprint(cb, expr->pos, "Zarray *%s = %s;", n1, val);
		i = 0;
		for(el=expr->el; el; el=el->tl){
			if(el->hd->op == ExprDots)
				break;
			cbprint(cb, expr->pos, "if(%s->n <= %d) goto L%d;", n1, i, lfail);
			cgenmatch(cb, el->hd, nameprint("((%hT)%s->a[%d])", el->hd->type, n1, i++), lfail);
		}
		if(el == nil)
			cbprint(cb, expr->pos, "if(%s->n != %d) goto L%d;",
				n1, lenExprL(expr->el), lfail);
		break;
	case ExprBool:
	case ExprInteger:
		cbprint(cb, expr->pos, "if(%s != %lld) goto L%d;", val, expr->i, lfail);
		break;
	case ExprFloat:
		cbprint(cb, expr->pos, "if(P2F(%s) != %g) goto L%d;", val, expr->f, lfail);
		break;
	case ExprString:
		cbprint(cb, expr->pos, "if(%s != %N) goto L%d;", val, expr->str, lfail);
		break;
	case ExprTuple:
		n1 = cbtmp(cb);
		cbprint(cb, expr->pos, "Zpoly *%s = %s;", n1, val);
		i = 0;
		for(el=expr->el; el; el=el->tl)
			cgenmatch(cb, el->hd, nameprint("((%hT)%s[%d])", el->hd->type, n1, i++), lfail);
		break;
	case ExprAndAnd:
		n1 = cbtmp(cb);
		cbprint(cb, expr->pos, "%hT %s = %s;", expr->type, n1, val);
		cgenmatch(cb, expr->e1, n1, lfail);
		cgenmatch(cb, expr->e2, n1, lfail);
		break;
	case ExprOrOr:
		n1 = cbtmp(cb);
		cbprint(cb, expr->pos, "%hT %s = %s;", expr->type, n1, val);
		cgenmatch(cb, expr->e1, n1, expr->l1);
		cbprint(cb, expr->pos, "goto L%d;", expr->l2);
		cbprint(cb, expr->pos, "L%d:;", expr->l1);
		cgenmatch(cb, expr->e2, n1, lfail);
		cbprint(cb, expr->pos, "L%d:;", expr->l2);
		break;
	
	/*
	case ExprTypeCase:
		n1 = cbtmp(cb);
		cbprint(cb, expr->pos, "%hT %s = %s;", expr->type, n1, val);
		cbprint(cb, expr->pos, "if(%s == 0 || !issubtype(%s->tag, %#T)) goto L%d;",
			n1, n1, expr->ptype, lfail);
		cgenassign(cb, expr->e3, n1);
		break;
	*/

	case ExprBacktick:
	case ExprBacktick2:
		cgenpmatch(cb, expr, val, lfail);
		break;
	
	case ExprCall:
		n1 = cbtmp(cb);
		cbprint(cb, expr->pos, "%hT %s = (%hT)%s;", expr->type, n1, expr->type, val);
		cbprint(cb, expr->pos, "if(%s == 0) goto L%d;", n1, lfail);
		if(expr->type->tags || expr->type->super)
			cbprint(cb, expr->pos, "if(!issubtype(%s->tag, %#T)) goto L%d;",
				n1, expr->type, lfail);
		structmktypes(expr->type, &nl, 1);
		for(el=expr->el; el; el=el->tl, nl=nl->tl){
			if(el->hd->op == ExprDots)
				break;
			cgenmatch(cb, el->hd, nameprint("%s->%s", n1, nl->hd), lfail);
		}
		break;
	}
}

void
cgenstmt(Cbuf *cb, Stmt *stmt)
{
	Name n1;
	StmtL *l;
	Type *t;

	if(stmt == nil)
		return;

	if(stmt->fn && stmt->fn->var && (stmt->fn->var->flags&VarCanonical)){
		t = stmt->fn->returntype;
		if(t->sym->used_canonical){
			warn(stmt->pos, "extending attribute canonical(%T) "
				"will not affect `%T patterns in this file",
				t, t);
		}
	}

	switch(stmt->op){
	case StmtBlock:
		for(l=stmt->stmtl; l; l=l->tl)
			cgenstmt(cb, l->hd);
		break;
	case StmtExpr:
		if(stmt->e1)
			cbprint(cb, stmt->pos, "%s;", cgenexpr(cb, stmt->e1));
		break;
	case StmtPrint:
		cbprint(cb, stmt->pos, "zprint(%#T, P(%s));",
			typegeneralize(stmt->e1->type)->left,
			cgenexpr(cb, stmt->e1));
		break;
	case StmtWhile:
		cbprint(cb, stmt->pos, "L%d:;", stmt->lcontinue);
		cbprint(cb, stmt->pos, "if(!%s) goto L%d;",
			cgenexpr(cb, stmt->e1),
			stmt->lbreak);
		cgenstmt(cb, stmt->s1);
		cbprint(cb, stmt->pos, "goto L%d;", stmt->lcontinue);
		cbprint(cb, stmt->pos, "L%d: ;", stmt->lbreak);
		break;
	
	case StmtFor:
		if(stmt->e1)
			cgenexpr(cb, stmt->e1);
		cbprint(cb, stmt->pos, "L%d:;", stmt->label);
		if(stmt->e2){
			cbprint(cb, stmt->pos, "if(!%s) goto L%d;",
				cgenexpr(cb, stmt->e2),
				stmt->lbreak);
		}
		cgenstmt(cb, stmt->s1);
		if(stmt->continued)
			cbprint(cb, stmt->pos, "L%d:;", stmt->lcontinue);
		if(stmt->e3)
			cgenexpr(cb, stmt->e3);
		cbprint(cb, stmt->pos, "goto L%d;", stmt->label);
		if(stmt->broke || stmt->e2)
			cbprint(cb, stmt->pos, "L%d:;", stmt->lbreak);
		break;
	
	case StmtForIn:
		switch(stmt->e2->type->op){
		Name _a, _i, _l, _m;	// thanks, td!

		default:
			panic("for-in %T", stmt->e2->type);

		case TypeList:
			_l = cbtmp(cb);
			cbprint(cb, stmt->pos, "Zlist *%s = %s;",
				_l, cgenexpr(cb, stmt->e2));
			cbprint(cb, stmt->pos, "L%d: if(%s == 0) goto L%d;",
				stmt->label, _l, stmt->lbreak);
			cgenassign(cb, stmt->e1, nameprint("((%hT)%s->hd)",
				stmt->e2->type->left, _l));
			cgenstmt(cb, stmt->s1);
			if(stmt->continued)
				cbprint(cb, stmt->pos, "L%d:;", stmt->lcontinue);
			cbprint(cb, stmt->pos, "%s = %s->tl;", _l, _l);
			cbprint(cb, stmt->pos, "goto L%d;", stmt->label);
			cbprint(cb, stmt->pos, "L%d:;", stmt->lbreak);
			break;
		
		case TypeArray:
			_a = cbtmp(cb);
			_i = cbtmp(cb);
			cbprint(cb, stmt->pos, "Zarray *%s = %s;",
				_a, cgenexpr(cb, stmt->e2));
			cbprint(cb, stmt->pos, "int %s = 0;", _i);
			cbprint(cb, stmt->pos, "L%d: if(%s >= %s->n) goto L%d;",
				stmt->label, _i, _a, stmt->lbreak);
			cgenassign(cb, stmt->e1, nameprint("((%hT)%s->a[%s])",
				stmt->e2->type->left, _a, _i));
			cgenstmt(cb, stmt->s1);
			if(stmt->continued)
				cbprint(cb, stmt->pos, "L%d:;", stmt->lcontinue);
			cbprint(cb, stmt->pos, "%s++;", _i);
			cbprint(cb, stmt->pos, "goto L%d;", stmt->label);
			cbprint(cb, stmt->pos, "L%d:;", stmt->lbreak);
			break;
	
		case TypeMap:
			_a = cbtmp(cb);
			_i = cbtmp(cb);
			_m = cbtmp(cb);
			cbprint(cb, stmt->pos, "Zmap *%s = %s;", _m, cgenexpr(cb, stmt->e2));
			cbprint(cb, stmt->pos, "Hashiter %s; Hashkv %s;", _a, _i);
			cbprint(cb, stmt->pos, "if(!%s || !hashiterstart(&%s, %s->h)) goto L%d;",
				_m, _a, _m, stmt->lbreak);
			cbprint(cb, stmt->pos, "L%d: if(!hashiternextkv(&%s, &%s)) goto L%d;",
				stmt->lcontinue, _a, _i, stmt->lbreak);
			cgenassign(cb, stmt->e1, nameprint("((Zpoly*)&%s)", _i));
			cgenstmt(cb, stmt->s1);
			cbprint(cb, stmt->pos, "goto L%d;", stmt->lcontinue);
			cbprint(cb, stmt->pos, "L%d:;", stmt->lbreak);
			break;

		case TypeFmap:
			_a = cbtmp(cb);
			_i = cbtmp(cb);
			_m = cbtmp(cb);
			cbprint(cb, stmt->pos, "Zfmap *%s = %s;", _m, cgenexpr(cb, stmt->e2));
			cbprint(cb, stmt->pos, "Fmapiter %s; Fmapkv %s;", _a, _i);
			cbprint(cb, stmt->pos, "if(!%s || !fmapiterstart(&%s, %s)) goto L%d;",
				_m, _a, _m, stmt->lbreak);
			cbprint(cb, stmt->pos, "L%d: if(!fmapiternextkv(&%s, &%s)) goto L%d;",
				stmt->lcontinue, _a, _i, stmt->lbreak);
			cgenassign(cb, stmt->e1, nameprint("((Zpoly*)&%s)", _i));
			cgenstmt(cb, stmt->s1);
			cbprint(cb, stmt->pos, "goto L%d;", stmt->lcontinue);
			cbprint(cb, stmt->pos, "L%d:;", stmt->lbreak);
			cbprint(cb, stmt->pos, "fmapiterend(&%s);", _a);
			break;
		}
		break;

	case StmtDoWhile:
		cbprint(cb, stmt->pos, "L%d:;", stmt->label);
		cgenstmt(cb, stmt->s1);
		if(stmt->continued)
			cbprint(cb, stmt->pos, "L%d:;", stmt->lcontinue);
		cbprint(cb, stmt->e1->pos, "if(%s) goto L%d;",
			cgenexpr(cb, stmt->e1),
			stmt->label);
		if(stmt->broke)
			cbprint(cb, stmt->pos, "L%d:;",
				stmt->lbreak);
		break;

	case StmtBreak:
	case StmtContinue:
		cbprint(cb, stmt->pos, "goto L%d;", stmt->label);
		break;
	
	case StmtIf:
		cbprint(cb, stmt->pos, "if(!%s) goto L%d;",
			cgenexpr(cb, stmt->e1),
			stmt->lfalse);
		cgenstmt(cb, stmt->s1);
		if(stmt->s2){
			cbprint(cb, stmt->pos, "goto L%d;", stmt->label);
			cbprint(cb, stmt->pos, "L%d:;", stmt->lfalse);
			cgenstmt(cb, stmt->s2);
		}
		cbprint(cb, stmt->pos, "L%d:;", stmt->label);
		break;
	
	case StmtAssert:
		cbprint(cb, stmt->pos, "if(!%s) panic(\"%%s\", %N);",
			cgenexpr(cb, stmt->e1),
			nameprint("%P: assert failed: %#P", stmt->pos, stmt->e1->pos));
		break;

	case StmtReturn:
		if(stmt->e1 == nil)
			cbprint(cb, stmt->pos, "return 0;");
		else{
			n1 = cgenexpr(cb, stmt->e1);
			if(stmt->e1->type != stmt->fn->returntype)
				n1 = nameprint("(%hT)%s", stmt->fn->returntype, n1);
			cbprint(cb, stmt->pos, "return %s;", n1);
		}
		break;
	
	case StmtFn:
		// Initialize function pointer here.
		// XXX Should check that function pointer is 0 when initializing.
		if(cb->fn && cb->fn->nescape)
			n1 = N("escf");
		else
			n1 = N("0");
		cgenassign(cb, stmt->e1, 
			nameprint("mkZfn(%s, %s);",
				cgenfn(cb, stmt->fn), n1));
		break;
	
	case StmtGoto:
		if(stmt->label == -1)
			error(stmt->pos, "undefined label %s", stmt->name);
		cbprint(cb, stmt->pos, "goto L%d;", stmt->label);
		break;
	
	case StmtLabel:
		// The unnecessary goto silences a warning about
		// unused labels if there are no other gotos.
		cbprint(cb, stmt->pos, "goto L%d; L%d:;", stmt->label, stmt->label);
		break;

	// Has no run-time effect.
	case StmtTypedef:
	case StmtStruct:
	case StmtGrammar:	// handled in __init__
	case StmtImport:
	case StmtAttrDecl:
	case StmtType:
		cbprint(cb, stmt->pos, "/* cheshire grin */");
		break;
	
	// Has been rewritten.
	case StmtSwitch:
	case StmtCase:
		panic("cgen");
	}	
}

void
cbprint(Cbuf *cb, Pos pos, char *fmt, ...)
{
	va_list arg;
	
	va_start(arg, fmt);
	if(linenumbers)
		fmtprint(cb->fmt, "#line %lP\n", pos);
	fmtprint(cb->fmt, "\t%V\n", fmt, arg);
	va_end(arg);
}

int
namefmt(Fmt *f)
{
	Name x;
	Rune r;
	char *p;
	int i, n;
	
	x = va_arg(f->args, Name);
	if(x == nil){
		fmtprint(f, "0");
		return 0;
	}
	fmtprint(f, "ZN(N(\"");
	for(p=nametostr(x); *p; p+=n){
		n = chartorune(&r, p);
		// Can't print ? because of ANSI trigraph braindamage.
		if(r == '\\' || r == '\"' || r < 0x20 || r >= Runeself || r == '?')
			for(i=0; i<n; i++)
				fmtprint(f, "\\%03o", (uchar)p[i]);
		else
			fmtrune(f, r);
	}
	fmtprint(f, "\"))");
	return 0;
}

void
cgentypes0(Cbuf *cb)
{
	fmtprint(&cb->topfmt, "\t__types__ = emalloc(%d*sizeof __types__[0]);\n", nalltype);
}

void
cgentypes(Cbuf *cb)
{
	int i;
	Type *t;
	TypeL *tl;
	NameL *nl;

	for(i=0; i<nalltype; i++){
		t = alltype[i];
		fmtprint(&cb->topfmt, "\t%#T = %#lT;\n", t, t);
	}
	for(i=0; i<nalltype; i++){
		t = alltype[i];
		if(t->op == TypeStruct){
			fmtprint(&cb->topfmt, "\tif(!%#T->structinit){\n", t);
			fmtprint(&cb->topfmt, "\t\t%#T->structinit = 1;\n", t);
			tl = structmktypes(t, &nl, 0);
			for(; tl; tl=tl->tl, nl=nl->tl)
				fmtprint(&cb->topfmt, "\t\ttypestructmember(%#T, %N, %#T);\n",
					t, nl->hd, tl->hd);
			fmtprint(&cb->topfmt, "\t}\n");
		}
	}
}

void
cgenimportdecls(Cbuf *cb)
{
	ModuleL *ml;
	MVarL *vl;
	Module *m;
	MVar *v;

	for(ml=importl; ml; ml=ml->tl){
		m = ml->hd;
		for(vl=m->mvarl; vl; vl=vl->tl){
			v = vl->hd;
			switch(v->type->left->op){
			case TypeType:
			case TypeModule:
				continue;
			default:
				break;
			}
			fmtprint(&cb->topfmt, "extern %hT %s;\n", v->type->left, v->cname);
			if(v->flags&VarAttribute)
				fmtprint(&cb->topfmt, "extern AttrInfo %s_info;\n", v->cname);
		}
	}
}

void cgenstructmembers(Cbuf*, Type*);

void
cgenstructdecls(Cbuf *cb)
{
	int i;
	Type *t;
	
	for(i=0; i<nalltype; i++){
		t = alltype[i];
		if(t->op != TypeStruct)
			continue;
		fmtprint(&cb->topfmt, "typedef struct %s %s;\n", t->cname, t->cname);
	}
	for(i=0; i<nalltype; i++){
		t = alltype[i];
		if(t->op != TypeStruct)
			continue;
		fmtprint(&cb->topfmt, "struct %s {\n", t->cname);
		cgenstructmembers(cb, t);
		fmtprint(&cb->topfmt, "};\n");
	}
}

void
cgenstructmembers(Cbuf *cb, Type *t)
{	
	TypeL *tl;
	NameL *nl;

	if(t->super)
		cgenstructmembers(cb, t->super);
	else{
		if(t->tags)
			fmtprint(&cb->topfmt, "\tType *tag;\n");
		if(t->extensible)
			fmtprint(&cb->topfmt, "\tHash *extend;\n");
	}
	tl = structmktypes(t, &nl, 0);
	for(; tl; tl=tl->tl, nl=nl->tl)
		fmtprint(&cb->topfmt, "\t%hT %s;\n", tl->hd, nl->hd);
}
