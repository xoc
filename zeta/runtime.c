#include "a.h"
#include "type.h"
#include "runtime.h"

int printtypes;

// Constructors

Zfn*
mkZfn(void *fn, void *escf)
{
	Zfn *z;
	
	z = emalloc(sizeof *z);
	z->fn = fn;
	z->escf = escf;
	return z;
}

Zlist*
mkZlist(Zpoly hd, Zlist *tl)
{
	Zlist *l;
	
	l = emalloc(sizeof *l);
	l->hd = hd;
	l->tl = tl;
	return l;
}

Zarray*
mkZarray(int n)
{
	Zarray *a;
	
	a = emalloc(sizeof *a + n*sizeof a->a[0]);
	a->a = (Zpoly*)(a+1);
	a->n = n;
	return a;
}

Zmap*
mkZmap(void)
{
	Zmap *m;
	
	m = emalloc(sizeof *m);
	m->h = mkhash();
	return m;
}


// Accessors

// Slice semantics: indices 0..n mean the usual.
// Indices -n..-1 mean 0..n-1.
// Indices below -n mean 0.
// Indices above n mean n.
#define SLICEFIX(lo, hi, n) \
	if(hi == NO_UPPER)	\
		hi = n;	\
	if(lo < 0)	\
		lo += n;	\
	if(lo < 0)	\
		lo = 0;	\
	if(hi < 0)	\
		hi += n;	\
	if(hi > n)	\
		hi = n;	\
	if(lo > hi)	\
		lo = hi = 0;	\

int
lenZarray(Zarray *a)
{
	// XXX Should we just crash if a == nil?
	if(a == nil)
		return 0;
	return a->n;
}

Zpoly*
indexZarray(Zarray *a, int i)
{
	if(i < 0 || i >= a->n)
		panic("array index out of bounds");
	return &a->a[i];
}

Zarray*
sliceZarray(Zarray *a, int lo, int hi)
{
	Zarray *b;

	SLICEFIX(lo, hi, a->n);
	b = emalloc(sizeof *b);
	b->a = a->a + lo;
	b->n = hi - lo;
	return b;
}

Zlist*
listZarray(Zarray *a)
{
	Zlist *l;
	int i;
	
	if(a == nil)
		return nil;
	l = nil;
	for(i=a->n-1; i>=0; i--)
		l = mkZlist(a->a[i], l);
	return l;
}

int
lenZlist(Zlist *l)
{
	int i;
	
	i = 0;
	for(; l; l=l->tl)
		i++;
	return i;
}

static Zlist*
revinplaceZlist(Zlist *l)
{
	Zlist *last, *next;
	
	last = nil;
	for(; l; l=next){
		next = l->tl;
		l->tl = last;
		last = l;
	}
	return last;
}

Zlist*
revZlist(Zlist *l)
{
	Zlist *ll;
	
	ll = nil;
	for(; l; l=l->tl)
		ll = mkZlist(l->hd, ll);
	return ll;
}

Zlist*
copyZlist(Zlist *l)
{
	return revinplaceZlist(revZlist(l));
}

Zlist*
sliceZlist(Zlist *l, int lo, int hi)
{
	int n, nn;
	Zlist *l1;
	
	n = lenZlist(l);
	SLICEFIX(lo, hi, n);
	nn = hi - lo;

	while(lo-- > 0)
		l = l->tl;
	if(hi == n)
		return l;
	l1 = nil;
	while(nn-- > 0){
		l1 = mkZlist(l->hd, l1);
		l = l->tl;
	}
	return revinplaceZlist(l1);
}

Zpoly
assocZlist(Zlist *l, Zpoly p)
{
	Zpoly *t;
	
	for(; l; l=l->tl){
		t = (Zpoly*)l->hd;
		if(t[0] == p)
			return t[1];
	}
	return 0;
}

Zlist*
addZlist(Zlist *l, Zlist *ll)
{
	if(l == nil)
		return ll;
	if(ll == nil)
		return l;
	return mkZlist(l->hd, addZlist(l->tl, ll));
}

Zpoly
indexZlist(Zlist *l, int i)
{
	while(l && i-- > 0)
		l = l->tl;
	if(l)
		return l->hd;
	return 0;
}

Zarray*
arrayZlist(Zlist *l)
{
	int i;
	Zarray *a;

	a = mkZarray(lenZlist(l));
	i = 0;
	for(; l; l=l->tl)
		a->a[i++] = l->hd;
	return a;
}

int
lenZstring(Zstring s)
{
	if(s == nil)
		return 0;
	return strlen(nametostr(s));
}

int
intZstring(Zstring s)
{
	if(s == nil)
		return 0;
	return atoi(nametostr(s));
}

int
cmpZstring(Zstring s, Zstring t)
{
	if(s == nil)
		s = N("");
	if(t == nil)
		t = N("");
	return strcmp(nametostr(s), nametostr(t));
}

Zstring
addZstring(Zstring s, Zstring t)
{
	if(s == nil)
		s = N("");
	if(t == nil)
		t = N("");
	return ZN(nameprint("%s%s", s, t));
}

Zstring
sliceZstring(Zstring s, int lo, int hi)
{
	int n;
	
	if(s == nil)
		s = N("");
	n = strlen(nametostr(s));
	SLICEFIX(lo, hi, n);
	return ZN(nameofn(nametostr(s)+lo, hi-lo));
}

Zstring
indexZstring(Zstring s, int lo)
{
	return sliceZstring(s, lo, lo+1);
}

Zpoly*
indexZmap(Zmap *m, Zpoly p)
{
	return hashgetp(m->h, p);
}

Zpoly
indexZtuple(Zpoly *t, int i)
{
	return t[i];
}


// Printing

// Fmt.width is used as the current indentation level.
// This print preserves the .width parameter.
int
pprintf(Fmt *f, char *fmt, ...)
{
	va_list arg;
	int n, w;

	w = f->width;
	va_start(arg, fmt);
	n = fmtvprint(f, fmt, arg);
	va_end(arg);
	f->width = w;
	return n;
}

void
pprintindent(Fmt *f)
{
	int i;
	for(i=0; i < f->width; i++)
		pprintf(f, "  ");
}

Hash *zprinthash;	// what's being printed right now

void
zprint1(Type *t, Zpoly p)
{
	int i;
	TypeL *tl;
	NameL *nl;
	Zlist *l;
	Zunion u;
	Hash *h;
	Hashiter hi;
	Hashkv kv;

	u.p = p;
	switch(t->op){
	case TypeType:
		print("type");
		break;
	case TypeBool:
		print("%s", u.b ? "true" : "false");
		break;
	case TypeInt:
		print("%d", u.i);
		break;
	case TypeFloat:
		print("%g", u.fl);
		break;
	case TypeString:
		if(u.s)
			print("%s", u.s);
		break;
	case TypeNil:
		print("nil");
		break;
	case TypeAny:
		print("any");
		break;
	case TypeVarFrozen:
		print("poly(%p)", p);
		break;
	case TypeList:
		print("list [");
		for(l=u.l; l; l=l->tl){
			zprint1(t->left, l->hd);
			if(l->tl)
				print(", ");
		}
		print("]");
		break;
	case TypeArray:
		print("array [");
		for(i=0; i<u.a->n; i++){
			if(i > 0)
				print(", ");
			zprint1(t->left, u.a->a[i]);
		}
		print("]");
		break;
	case TypeTuple:
		print("(");
		for(tl=t->typel, i=0; tl; tl=tl->tl, i++){
			if(i > 0)
				print(", ");
			zprint1(tl->hd, u.t[i]);
		}
		print(")");
		break;
	case TypeMap:
		print("map");
		break;
	case TypeFmap:
		print("fmap");
		break;
	case TypeFn:
		print("fn");
		break;
	case TypeStruct:
		i = 0;
		if(u.t == nil){
			print("%T(<nil>)", t);
			break;
		}
		if(t->super || t->tags)
			t = u.t[i++];
		h = nil;
		if(t->extensible)
			h = u.t[i++];
		print("%T(", t);
		if(hashget(zprinthash, p)){
			print("...)");
			break;
		}
		hashput(zprinthash, p, p);
		tl = structmktypes(t, &nl, 1);
		for(; tl; tl=tl->tl, nl=nl->tl, i++){
			print("%s=", nl->hd);
			zprint1(tl->hd, u.t[i]);
			if(tl->tl)
				print(", ");
		}
		if(hashiterstart(&hi, h)){
			while(hashiternextkv(&hi, &kv)){
				t = kv.key;
				u.t = kv.value;
				i = 0;
				for(tl=t->typel, nl=t->namel; tl; tl=tl->tl, nl=nl->tl){
					print(", %s=", nl->hd);
					zprint1(tl->hd, u.t[i]);
				}
			}
		}
		print(")");
		hashput(zprinthash, p, nil);
		break;
	case TypeWrapper:
		print("%s(%p)", t->name, p);
		break;
	case TypeVarInst:
		if(t->constraint->min)
			zprint1(t->constraint->min, p);
		else if(t->constraint->max)
			zprint1(t->constraint->max, p);
		else
			print("???");
		break;
	case TypeVarNum:
	case TypeVarName:
		print("?");
		break;
	case TypeModule:
		print("module");
		break;
	case TypeGram:
		print("%#A", p);
		break;
	}
}

void
zprint(Type *t, Zpoly p)
{
	zprinthash = mkhash();
	if(printtypes)
		print("[%p] %T\n", p, t);
	zprint1(t, p);
	print("\n");
}

void*
xget(Hash **h, Type *t)
{
	void *v;

	if(*h == nil)
		*h = mkhash();
	v = hashget(*h, t);
	if(v)
		return v;
	v = emalloc(sizeof(Zpoly)*lenTypeL(t->typel));
	hashput(*h, t, v);
	return v;
}

Zlist*
keysZmap(Zmap *m)
{
	Zlist *l;
	Hashiter h;
	void *k;

	l = nil;
	if(m && hashiterstart(&h, m->h)){
		while(hashiternextkey(&h, &k))
			l = mkZlist(k, l);
		hashiterend(&h);
	}
	return l;
}
