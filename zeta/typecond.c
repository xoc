#include "a.h"
#include "type.h"
#include "runtime.h"
#include "compile.h"

Expr*
tobool(Typecheck *tc, Expr *expr)
{
	Expr *e;

	if(expr->type == typebool())
		return expr;
	e = mkExpr(ExprUnary);
	e->name = N("bool");
	e->e1 = expr;
	e->pos = expr->pos;
	typecheckexpr(tc, e, Value);
	return e;
}

void
dumpscope(Scope *scope, char *msg)
{
	VarL *vl;
	
	print("== %s\n", msg);
	for(; scope; scope=scope->outer){
		print("--scope %p -> %p\n", scope, scope->outer);
		for(vl=scope->varl; vl; vl=vl->tl)
			print("%s: %T\n", vl->hd->name, vl->hd->type->left);
	}
}

// Type-check an expression used as a branch condition.
// Names introduced only if it evaluates true go in tscope.
// Names introduced only if it evalutes false go in fscope.
// Names introduced no matter what go in tc->scope.
void
typecheckcond(Typecheck *tc, Expr *expr, Scope *tscope, Scope *fscope)
{
	Scope *f1, *f2, *t1, *t2, *oscope;

	switch(expr->op){
	case ExprNot:
		typecheckcond(tc, expr->e1, fscope, tscope);
		expr->e1 = tobool(tc, expr->e1);
		expr->type = typebool();
		expr->l1 = tc->nlabel++;
		break;
	
	case ExprAndAnd:
		f1 = copyscope(fscope);
		typecheckcond(tc, expr->e1, tscope, f1);
		f2 = copyscope(fscope);
		oscope = tc->scope;
		tc->scope = tscope;
		typecheckcond(tc, expr->e2, tscope, f2);
		tc->scope = oscope;
		mergescopes(f1, f2, fscope);
		expr->e1 = tobool(tc, expr->e1);
		expr->e2 = tobool(tc, expr->e2);
		expr->type = typebool();
		break;

	case ExprOrOr:
		t1 = copyscope(tscope);
		typecheckcond(tc, expr->e1, t1, fscope);
		t2 = copyscope(tscope);
		oscope = tc->scope;
		tc->scope = fscope;
		typecheckcond(tc, expr->e2, t2, fscope);
		tc->scope = oscope;
		mergescopes(t1, t2, tscope);
		expr->e1 = tobool(tc, expr->e1);
		expr->e2 = tobool(tc, expr->e2);
		expr->l1 = tc->nlabel++;
		expr->l2 = tc->nlabel++;
		expr->type = typebool();
		break;
	
	case ExprSubtype:
		typecheckexpr(tc, expr, Value);
		redeclare(tscope, expr->e1, expr->realtype);
		expr->type = typebool();
		break;

	case ExprMatch:
		if(typecheckexpr(tc, expr->e1, Value) >= 0){
			oscope = tc->scope;
			tc->scope = tscope;
			typecheckmatch(tc, expr->e2, expr->e1->type);
			if(expr->e2->realtype && expr->e1->type != expr->e2->realtype)
				redeclare(tscope, expr->e1, expr->e2->realtype);
			tc->scope = oscope;
		}
		expr->type = typebool();
		expr->l1 = tc->nlabel++;
		break;
		
	default:
		typecheckexpr(tc, expr, Value);
		break;
	}
}

Type*
typecheckcallname(Typecheck *tc, Expr *expr, Type *context)
{
	Module *m;
	Type *t, *tt;
	NameL *nl;
	Name mname;
	Var *v;
	
	// Build name list in most significant -> least significant order.
	nl = nil;
	while(expr->op == ExprDot){
		nl = mkNameL(expr->name, nl);
		expr = expr->e1;
	}
	if(expr->op != ExprName){
		error(expr->pos, "cannot use %#P as a constructor name", expr->pos);
		return nil;
	}
	nl = mkNameL(expr->name, nl);
	
	m = nil;
	t = nil;
	mname = nil;

	// First name is special: it can be relative to the context type.
	if(context && (t = typestructdot(context, nl->hd)) != nil)
		nl = nl->tl;

	// Now loop over names, digging deeper.
	// After each step, exactly one of t or m is non-nil.
	for(; nl; nl=nl->tl){
		if(m){
			if((v = mlookupvar(m, nl->hd)) == nil){
				error(expr->pos, "no %s.%s", mname, nl->hd);
				return nil;
			}
			if(v->type->left->op == TypeModule){
				m = v->module;
				mname = v->name;
			}
			else if(v->type->left->op == TypeType){
				m = nil;
				t = v->realtype;
			}else{
				error(expr->pos, "%s.%s is not a type", mname, nl->hd);
				return nil;
			}
		}else if(t){
			if((tt = typestructdot(t, nl->hd)) != nil)
				t = tt;
			else{
				error(expr->pos, "no %T.%s", t, nl->hd);
				return nil;
			}
		}else{
			if((v = lookupvar(tc->scope, nl->hd)) == nil){
				error(expr->pos, "no %s", nl->hd);
				return nil;
			}
			if(v->type->left->op == TypeModule){
				m = v->module;
				mname = v->name;
			}
			else if(v->type->left->op == TypeType)
				t = v->realtype;
			else{
				error(expr->pos, "%s is not a type", nl->hd);
				return nil;
			}
		}
	}
	assert(m || t);

	if(m){
		error(expr->pos, "%s is not a struct type", mname);
		return nil;
	}
	if(t->op != TypeStruct){
		error(expr->pos, "%T is not a struct type", t);
		return nil;
	}
	return t;
}

void
typecheckmatch(Typecheck *tc, Expr *expr, Type *type)
{
	Var *v;
	ExprL *el;
	NameL *nl;
	TypeL *tl;
	Scope *s1, *s2, *oscope;
	Type *t;

	switch(expr->op){
	default:
		error(expr->pos, "cannot use %#P as pattern", expr->pos);
		return;

	case ExprName:
		v = lookupvar(tc->scope, expr->name);
		if(v && expr->name != N("_"))
			warn(expr->pos, "shadowing %s in match [%T]", expr->name, v->type->left);
		v = declarevar(expr->pos, tc->scope, expr->name, type);
		expr->var = v;
		break;
	
	case ExprDeclAssign:
		typecheckmatch(tc, expr->e2, type);
		if(expr->e2->type)
			type = expr->e2->type;
		typecheckmatch(tc, expr->e1, type);
		break;
	
	case ExprTuple:
		if(type->op != TypeTuple){
			error(expr->pos, "match %T against tuple", type);
			return;
		}
		for(el=expr->el, tl=type->typel; el && tl; el=el->tl, tl=tl->tl){
			if(el->hd->op == ExprDots){
				if(el->tl)
					error(expr->pos, "dots ... not at end of list");
				el = nil;
				tl = nil;
				break;
			}
			typecheckmatch(tc, el->hd, tl->hd);
		}
		if(tl)
			error(expr->pos, "too few elements in tuple pattern");
		if(el)
			error(expr->pos, "too many elements in tuple pattern");
		break;

	case ExprMkList:
		if(type->op != TypeList){
			error(expr->pos, "match %T against list", type);
			return;
		}
		for(el=expr->el; el; el=el->tl){
			if(el->hd->op == ExprDots){
				if(el->tl)
					error(expr->pos, "dots ... not at end of list");
				continue;
			}
			typecheckmatch(tc, el->hd, type->left);
		}
		break;
	
	case ExprMkArray:
		if(type->op != TypeArray){
			error(expr->pos, "match %T against array", type);
			return;
		}
		for(el=expr->el; el; el=el->tl){
			if(el->hd->op == ExprDots){
				if(el->tl)
					error(expr->pos, "dots ... not at end of array");
				continue;
			}
			typecheckmatch(tc, el->hd, type->left);
		}
		break;
	
	case ExprBinary:
		if(expr->name == N("::")){
			if(type->op != TypeList){
				error(expr->pos, "match %T against ::", type);
				return;
			}
			typecheckmatch(tc, expr->e1, type->left);
			typecheckmatch(tc, expr->e2, type);
			break;
		}
		error(expr->pos, "match against %s", expr->name);
		return;
	
	case ExprInteger:
		if(type != typeint()){
			error(expr->pos, "match %T against int", type);
			return;
		}
		break;

	case ExprFloat:
		if(type != typefloat()){
			error(expr->pos, "match %T against float", type);
			return;
		}
		break;

	case ExprString:
		if(type != typestring()){
			error(expr->pos, "match %T against string", type);
			return;
		}
		break;
	
	case ExprOrOr:
		oscope = tc->scope;
		s1 = copyscope(oscope);
		tc->scope = s1;
		typecheckmatch(tc, expr->e1, type);
		tc->scope = oscope;
		
		s2 = copyscope(tc->scope);
		tc->scope = s2;
		typecheckmatch(tc, expr->e2, type);
		tc->scope = oscope;
		
		mergescopes(s1, s2, tc->scope);
		expr->l1 = tc->nlabel++;
		expr->l2 = tc->nlabel++;
		break;
	
	case ExprBacktick:
	case ExprBacktick2:
		if(typecheckpmatch(tc, expr, type) < 0)
			return;
		break;

	case ExprCall:
		if(type->op != TypeStruct){
			error(expr->pos, "cannot match %T against constructor", type);
			return;
		}
		if((t = typecheckcallname(tc, expr->e1, type)) == nil)
			return;
		if(!issubtype(t, type)){
			error(expr->pos, "%T cannot match %T", t, type);
			return;
		}
		tl = structmktypes(t, &nl, 1);
		for(el=expr->el; el; el=el->tl, tl=tl->tl){
			if(el->hd->op == ExprDots){
				if(el->tl)
					error(el->hd->pos, "dots ... not at end of constructor");
				tl = nil;
				break;
			}
			if(tl == nil){
				error(el->hd->pos, "too many constructor arguments");
				break;
			}
			typecheckmatch(tc, el->hd, tl->hd);
		}
		if(tl)
			error(expr->pos, "not enough constructor arguments");
		type = t;
		break;
	}
	
	expr->type = type;
}

