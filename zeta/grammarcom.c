#include "a.h"
#include "type.h"
#include "runtime.h"
#include "grammar.h"
#include "compile.h"
#include "grammarcom.h"
#include "listimpl.h"

static NameL *graminit;

void typecheckgstmt(Typecheck*, Type*, GStmt*);
void typecheckgrule(Typecheck *tc, Type*, CfgSym *left, GRule *rule);
CfgSym* typecheckgterm(Type*, GTerm *term);

// Grammars are a bit different from the usual compiler stuff,
// because there is a lot of state that has to get recreated
// when the module gets loaded, and it's not easy to just 
// traverse the state itself (the grammar and its rules) to 
// generate C code that will reconstruct the state.  Instead,
// we just write down everything we've done as we do it.
// Remember, a compiler is just an interpreter with the actions
// turned into print statements.
static Fmt *cfmt;

static void
line(Pos pos)
{
	if(linenumbers)
		fmtprint(cfmt, "#line %lP\n", pos);
}

void
typecheckgram(Typecheck *tc, GTop *gtop)
{
	Var *v;
	GStmtL *l;
	Gram *g;
	Type *tg;
	Fmt fmt;
	char *s;
	NameL *nl;
	Name x;

	fmtstrinit(&fmt);
	cfmt = &fmt;
	v = nil;
	line(gtop->pos);
	fmtprint(&fmt, "\t{\n\t\tType *tg;\n", tg);
	if(gtop->xflag != N("extend") && gtop->extendl == nil){
		v = lookupvar(tc->scope, gtop->name);
		if(v){
			error(gtop->pos, "%s is already declared", gtop->name);
			return;
		}
		g = mkGram(gtop->name/*, gtop->xflag == N("extensible")*/);
		g->type = typegram1(gtop->name, g);
		v = declarevar(gtop->pos, tc->scope, gtop->name, typetype());
		v->realtype = g->type;
		v->cname = nil;
		tg = g->type;
		fmtprint(&fmt, "\t\ttg = typegram1(%N, mkGram(%N));\n", gtop->name, gtop->name);
	}else if(gtop->xflag == N("extend")){
		static int anon;
		v = lookupvar(tc->scope, gtop->name);
		if(v == nil){
			error(gtop->pos, "%s is not declared", gtop->name);
			return;
		}
		// TODO: better name generation (include module name)
		x = nameprint("%s_xG_%d", topscope->frame->prefix, anon++);
		tg = typegramxl(v->realtype, x, nil);
		fmtprint(&fmt, "\t\ttg = typegramxl(%#lT, %N, nil);\n", v->realtype, x);
		g = v->realtype->gram;
		v = declarevar(gtop->pos, tc->scope, x, typetype());
		v->realtype = tg;
		v->cname = nil;
	}else if(gtop->extendl){
		tg = nil;
		for(nl=gtop->extendl; nl; nl=nl->tl){
			v = lookupvar(tc->scope, nl->hd);
			if(v == nil){
				error(gtop->pos, "%s is not declared", nl->hd);
				return;
			}
			if(v->realtype == nil || v->realtype->op != TypeGram){
				error(gtop->pos, "%s is not a grammar", nl->hd);
				return;
			}
			if(tg == nil)
				tg = v->realtype;
			else
				if(typemax(tg, v->realtype, &tg) < 0){
					error(gtop->pos, "cannot extend %T and %T", tg, v->realtype);
					return;
				}
		}
		v = lookupvar(tc->scope, gtop->name);
		if(v){
			error(gtop->pos, "%s is already declared", gtop->name);
			return;
		}
		tg = typegramsym(tg, nil, nil);
		tg = typegramx(tg, nil);
		fmtprint(&fmt, "\t\t%#T = %#lT;\n", tg, tg);
		tg = typegramx(tg, mkNameL(gtop->name, tg->gramx));
		v = declarevar(gtop->pos, tc->scope, gtop->name, typetype());
		v->cname = nil;
		v->realtype = tg;
		fmtprint(&fmt, "\t\ttg = %#lT;\n", tg);
	}else{
		panic("typecheckgram");
		return;
	}

	// The __types__ array isn't yet set up, but we'd like to be able
	// to use %#T to refer to tg, so set up tg's entry.
	line(gtop->pos);
	fmtprint(&fmt, "\t\t__types__[%d] = tg;\n", tg->id);

	line(gtop->pos);
	fmtprint(&fmt, "\t\tGram *g = tg->gram;\n");
	for(l=gtop->stmtl; l; l=l->tl)
		typecheckgstmt(tc, tg, l->hd);
	fmtprint(&fmt, "\t}\n");
	s = fmtstrflush(&fmt);
	graminit = mkNameL(nameof(s), graminit);
	free(s);
}

void
typecheckgstmt(Typecheck *tc, Type *tg, GStmt *stmt)
{
	GRuleL *rl;
	CfgSym *left;
	NameL *nl;
	CfgPrecKind pkind;
	GTerm *term;
	GTermL *tl;

	switch(stmt->op){
	case GStmtRules:
		left = gramsym(tg, stmt->name);
		line(stmt->pos);
		fmtprint(cfmt, "\t\t{\n\t\t\tCfgSym *left = gramsym(tg, %N);\n", stmt->name);
		for(rl=stmt->rulel; rl; rl=rl->tl)
			typecheckgrule(tc, tg, left, rl->hd);
		fmtprint(cfmt, "\t\t}\n");
		break;
	
	case GStmtIgnore:
		for(tl=stmt->terml; tl; tl=tl->tl){
			term = tl->hd;
			switch(term->op){
			case GTermName:
				error(term->pos, "can only ignore strings and regexps");
				break;
			case GTermString:
				gramignore(tg->gram, term->text, RegexpLiteral, 0);
				line(term->pos);
				fmtprint(cfmt, "\t\tgramignore(g, %N, %d, 0);\n",
					term->text, RegexpLiteral);
				break;
			case GTermRegexp:
				if(gramignore(tg->gram, term->text, RegexpEgrep|term->flags, 1) == nil){
					error(term->pos, "invalid regexp in ignore");
					break;
				}
				fmtprint(cfmt, "\t\tgramignore(g, %N, %d, 0);\n",
					term->text, RegexpEgrep|term->flags);
				break;
			}
		}
		break;
	
	case GStmtNoCopy:
		for(tl=stmt->terml; tl; tl=tl->tl){
			term = tl->hd;
			switch(term->op){
			case GTermName:
				gramsym(tg, term->text)->nocopy = 1;
				line(stmt->pos);
				fmtprint(cfmt, "\t\tgramsym(tg, %N)->nocopy = 1;\n", term->text);
				break;
			case GTermRegexp:
			case GTermString:
				error(term->pos, "can only nocopy names");
				break;
			}
		}
		break;
	
	case GStmtAssoc:
		line(stmt->pos);
		for(nl=stmt->namel; nl; nl=nl->tl){
			gramprec(tg->gram, nl->hd, stmt->assoc, CfgPrecPriority, 1);
			line(stmt->pos);
			fmtprint(cfmt, "\t\tgramprec(g, %N, %d, %d, 1);\n", nl->hd, stmt->assoc, CfgPrecPriority);
		}
		break;
	
	case GStmtPriority:
		if(lenNameL(stmt->namel) < 2){
			error(stmt->pos, "meaningless precedence");
			break;
		}
		pkind = CfgPrecPriority;
		if(stmt->prefer)
			pkind = CfgPrecPrefer;
		for(nl=stmt->namel; nl->tl; nl=nl->tl){
			if(grampriority(tg->gram, nl->hd, nl->tl->hd, stmt->prefer) < 0)
				error(stmt->pos, "grampriority: %r");
			line(stmt->pos);
			fmtprint(cfmt, "\t\tif(grampriority(g, %N, %N, %d) < 0) panic(\"grampriority: %%r\");\n",
				nl->hd, nl->tl->hd, stmt->prefer);
		}
		break;
	}			
}

void
typecheckgrule(Typecheck *tc, Type *tg, CfgSym *left, GRule *rule)
{
	GTermL *tl;
	CfgSymL *sl;
	GramPrec *p;
	CfgSym *s;

	sl = nil;
	line(rule->pos);
	fmtprint(cfmt, "\t\t\t{\n\t\t\t\tCfgSymL *sl = 0;  CfgSym *s;\n");
	for(tl=rule->terml; tl; tl=tl->tl){
		if((s = typecheckgterm(tg, tl->hd)) == nil)
			return;
		sl = mkCfgSymL(s, sl);
		line(tl->hd->pos);
		fmtprint(cfmt, "\t\t\t\tsl = mkCfgSymL(s, sl);\n");
	}
	sl = revCfgSymL(sl);
	p = nil;
	line(rule->pos);
	fmtprint(cfmt, "\t\t\t\tsl = revCfgSymL(sl);\n");
	fmtprint(cfmt, "\t\t\t\tGramPrec *p = 0;\n");
	if(rule->prec){
		p = gramprec(tg->gram, rule->prec, 0, 0, 0);
		if(p == nil){
			error(rule->pos, "unknown precedence %s", rule->prec);
			return;
		}
		line(rule->pos);
		fmtprint(cfmt, "\t\t\t\tp = gramprec(g, %N, 0, 0, 0);\n", rule->prec);
		line(rule->pos);
		fmtprint(cfmt, "\t\t\t\tif(p == 0) panic(\"gramprec %s\", %N);\n", rule->prec);
	}
	gramaddrule(tg, p, left, sl);
	line(rule->pos);
	fmtprint(cfmt, "\t\t\t\tgramaddrule(tg, p, left, sl);\n");
	fmtprint(cfmt, "\t\t\t\tfreeCfgSymL(sl);\n");
	fmtprint(cfmt, "\t\t\t}\n");
}

CfgSym*
typecheckgterm(Type *tg, GTerm *term)
{
	CfgSym *s;

	line(term->pos);
	switch(term->op){
	case GTermRegexp:
		if((s = gramregexp(tg, term->text, term->flags, 1)) == nil){
			error(term->pos, "invalid regexp");
			return nil;
		}
		fmtprint(cfmt, "\t\t\t\ts = gramregexp(tg, %N, %d, 0);\n", term->text, term->flags);
		return s;
	
	case GTermName:
		s = gramsym(tg, term->text);
		fmtprint(cfmt, "\t\t\t\ts = gramsym(tg, %N);\n", term->text);
		return s;

	case GTermString:
		term->str = nameof(cunescapestring(nametostr(term->text), -1));
		s = gramliteral(tg, term->str);
		fmtprint(cfmt, "\t\t\t\ts = gramliteral(tg, %N);\n", term->str);
		return s;
	}
	panic("typecheckgterm");
	return nil;
}

int
typecheckgexpr(Typecheck *tc, Expr *expr, int kind)
{
	Type *t;

	switch(expr->op){
	default:
		panic("typecheckgexpr %#P", expr->pos);
	
	case ExprParse:
		noassign(expr, kind);
		nosideeffect(expr, kind);
		if(typecheckexpr(tc, expr->e1, Value) < 0)
			return -1;
		t = typechecktype(tc, expr->typex);
		if(t == nil)
			return -1;
		expr->type = t;
		break;
	}
	return 0;
}

Type*
typegramdot(Type *t, Name name)
{
	CfgSym *s;
	
	s = gramlooksym(t->gram, name);
	if(s == nil)
		return nil;
	return typegramsym(t, name, s);
}

void
cgengraminit(Cbuf *cb)
{
	NameL *nl;
	
	for(nl=revNameL(graminit); nl; nl=nl->tl)
		fmtprint(&cb->topfmt, "%s", nl->hd);
}

GTop*
mkGTop(Name xflag, Name name, NameL *extendl, GStmtL *stmtl)
{
	GTop *t;
	
	t = emalloc(sizeof *t);
	t->pos = yylastpos;
	t->xflag = xflag;
	t->name = name;
	t->extendl = extendl;
	t->stmtl = stmtl;
	return t;
}

GRule*
mkGRule(void)
{
	GRule *r;
	
	r = emalloc(sizeof *r);
	r->pos = yylastpos;
	return r;
}

GStmt*
mkGStmt(GStmtOp op)
{
	GStmt *s;
	
	s = emalloc(sizeof *s);
	s->op = op;
	s->pos = yylastpos;
	return s;
}

GTerm*
mkGTerm(GTermOp op)
{
	GTerm *t;
	
	t = emalloc(sizeof *t);
	t->op = op;
	t->pos = yylastpos;
	return t;
}

LISTIMPL(GStmt)
LISTIMPL(GRule)
LISTIMPL(GTerm)
