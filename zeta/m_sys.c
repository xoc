#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <stdio.h>  // for remove
#include "regexp9.h"
#include "a.h"
#include "type.h"
#include "runtime.h"
#include "grammar.h"

// Functions provided by sys module.

Zfn *sys_exit;
void*
sys_exit_(void *escf, int code)
{
	exit(code);
	return 0;
}

static int 
readn(int fd, char *buf, int n)
{
	int tot, m;
	
	for(tot=0; tot<n; tot+=m){
		m = read(fd, buf+tot, n-tot);
		if(m <= 0)
			break;
	}
	return tot;
}

static int
doreadpipe(char **argv, Name *output)
{
	int p[2], pid, fd, tot, m, n, status;
	char *buf;

	if(pipe(p) < 0)
		panic("pipe");
	pid = fork();
	if(pid == 0){
		close(p[0]);
		dup2(p[1], 1);
		execvp(argv[0], argv);
		fprint(2, "exec %s: %r", argv[0]);
		_exit(1);
	}
	close(p[1]);
	fd = p[0];

	tot = 0;
	m = 2048;
	buf = emalloc(m);
	for(;;){
		m *= 2;
		buf = erealloc(buf, m);
		n = readn(fd, buf + tot, m - tot);
		if(n <= 0)
			break;
		tot += n;
		if(tot < m)
			break;
	}
	buf[tot] = 0;
	*output = nameof(buf);
	free(buf);
	close(fd);
	
	if(waitpid(pid, &status, 0) < 0 || !WIFEXITED(status))
		return -1;
	return WEXITSTATUS(status);
}

Zfn *sys_readpipe;
Zpoly
sys_readpipe_(void *escf, Zlist *args)
{
	int i, n;
	char **argv;

	struct {
		Zstring output;
		int exitstatus;
	} *ret;
	
	ret = emalloc(sizeof *ret);
	ret->output = ZN(N(""));
	ret->exitstatus = 0;
	
	n = lenZlist(args);
	if(n == 0){
		fprint(2, "readpipe: no command\n");
		ret->exitstatus = -1;
		return ret;
	}
	
	argv = emalloc((n+1)*sizeof argv[0]);
	i = 0;
	for(; args; args=args->tl)
		argv[i++] = args->hd;
	argv[i] = nil;
	
	ret->exitstatus = doreadpipe(argv, &ret->output);
	ret->output = ZN(ret->output);
	free(argv);
	return ret;
}

Zfn *sys_readfile;
Zstring
sys_readfile_(void *escf, Zstring file)
{
	int fd;
	char *p;
	struct stat st;
	Name x;
	
	if(file == nil)
		file = N("");

	fd = open(nametostr(file), O_RDONLY);
	if(fd < 0)
		return ZN(N(""));
	if(fstat(fd, &st) < 0){
		close(fd);
		return ZN(N(""));
	}
	if(st.st_size == 0)
		return ZN(N(""));
	p = emalloc(st.st_size);
	if(readn(fd, p, st.st_size) != st.st_size){
		close(fd);
		free(p);
		return ZN(N(""));
	}
	close(fd);
	x = nameofn(p, st.st_size);
	free(p);
	return ZN(x);
}


Zfn *sys_writefile;
Zint
sys_writefile_(void *escf, Zstring file, Zstring data)
{
	int fd, n, m;
	char *p;

	if(file == nil)
		file = N("");
	if(data == nil)
		data = N("");

	// Pick off /dev/stderr.  Yes, it exists, but if you're redirecting
	// stderr to a file, opening /dev/stderr O_TRUNC will truncate
	// the output file!  Not what we want.
	if(file == N("/dev/stderr"))
		fd = dup(2);
	else{
		fd = open(nametostr(file), O_WRONLY|O_CREAT|O_TRUNC, 0666);
		if(fd < 0){
			fprint(2, "writefile %s: %r\n", file);
			return -1;
		}
	}
	p = nametostr(data);
	n = strlen(p);
	while(n > 0){
		m = write(fd, p, n);
		if(m <= 0)
			break;
		p += m;
		n -= m;
	}
	close(fd);
	if(n > 0)
		return -1;
	return 0;
}


Zfn *sys_abort;
Zpoly*
sys_abort_(void *escf)
{
	abort();
	return 0;
}

Zfn *sys_time;
Zfloat
sys_time_(void *escf)
{
	static int64 time0;
	int64 v;

	struct timeval tv;
	gettimeofday(&tv, nil);
	v = tv.tv_sec * 1000000LL + tv.tv_usec;
	if(time0 == 0)
		time0 = v;
	return F2P((v-time0) * 1e-6);
}

Zarray *sys_argv;

Zfn *sys_fmtstart;
Fmt*
sys_fmtstart_(void *escf)
{
	Fmt *f;
	
	f = emalloc(sizeof *f);
	fmtstrinit(f);
	return f;
}

Zfn *sys_fmt;
void*
sys_fmt_(void *escf, Fmt *f, char *text)
{
	if(text)
		fmtprint(f, "%s", text);
	return 0;
}

Zfn *sys_fmtflush;
Zstring
sys_fmtflush_(void *escf, Fmt *f)
{
	char *p;
	Name x;

	p = fmtstrflush(f);
	memset(f, 0, sizeof *f);
	if(p == nil)
		return ZN(N(""));
	x = ZN(nameof(p));
	free(p);
	return x;
}

Zfn* sys_sleep;
void*
sys_sleep_(void *escf, Zfloat f)
{
	struct timeval tv;
	int usec;
	
	usec = P2F(f)*1e6;
	tv.tv_sec = usec/1000000;
	tv.tv_usec = usec%1000000;
	select(0, NULL, NULL, NULL, &tv);
	return 0;
}

Zfn* sys_aststats;
Zint*
sys_aststats_(void *escf)
{
	Zint *p;
	
	p = emalloc(3*sizeof p[0]);
	p[0] = nmkast;
	p[1] = nfreeast;
	p[2] = nleakast;
	return p;
}

Zstring sys_zetadir;

Zfn* sys_loadmodule;
Zbool
sys_loadmodule_(void *escf, Zstring path)
{
	Module *m;

	if(path == nil)
		path = N("");
	m = loadmodule(path, nil, 1);
	return m != nil;
}

Zfn *sys_strstr;
Zint
sys_strstr_(void *escf, Zstring a, Zstring b)
{
	char *p;

	if(a == nil)
		a = N("");
	if(b == nil)
		b = N("");
	
	p = strstr(nametostr(a), nametostr(b));
	if(p == nil)
		return -1;
	return p - nametostr(a);
}

Zfn *sys_remove;
Zint
sys_remove_(void *escf, Zstring a)
{
	if(a == nil)
		a = N("");
	return remove(nametostr(a));
}

Zfn *sys_getpid;
Zint
sys_getpid_(void *escf)
{
	return getpid();
}

Zfn* sys_werrstr;
Zpoly
sys_werrstr_(void *escf, Zstring s)
{
	if(s == nil)
		s = N("");
	werrstr("%s", s);
	return 0;
}

Zfn* sys_errstr;
Zstring
sys_errstr_(void *escf)
{
	return ZN(nameprint("%r"));
}

Zfn* sys_fork;
Zint
sys_fork_(void *escf)
{
	return fork();
}

Zfn* sys_system;
Zint
sys_system_(void *escf, Zstring cmd)
{
	if(cmd == nil)
		cmd = N("");
	return system(nametostr(cmd));
}

Zfn* sys_gsub;
Zstring
sys_gsub_(void *escf, Zstring zin, Zstring zpattern, Zstring zrepl)
{
	Fmt fmt;
	char *in, *p, *lastmatch, *repl, *pattern;
	char buf[1024];
	Resub m[10];
	Reprog *prog;
	Zstring out;

	if(zin == nil)
		zin = N("");
	if(zpattern == nil)
		zpattern = N("");
	if(zrepl == nil)
		zrepl = N("");
	
	repl = nametostr(zrepl);
	pattern = nametostr(zpattern);
	in = nametostr(zin);

	prog = regcomp(pattern);

	fmtstrinit(&fmt);
	p = in;
	lastmatch = nil;
	for(;;){
		memset(m, 0, sizeof m);
		if(!regexec(prog, p, m, nelem(m)))
			break;
		if(m[0].s.sp == lastmatch){
			if(*p == 0)
				break;
			memset(m, 0, sizeof m);
			if(!regexec(prog, p+1, m, nelem(m)))
				break;
		}
		if(m[0].s.sp > p)
			fmtprint(&fmt, "%.*s", utfnlen(p, m[0].s.sp - p), p);
		buf[0] = 0;
		regsub(repl, buf, sizeof buf, m, nelem(m));
		fmtstrcpy(&fmt, buf);
		lastmatch = m[0].s.sp;
		p = m[0].e.ep;
	}
	fmtstrcpy(&fmt, p);
	p = fmtstrflush(&fmt);
	out = ZN(nameof(p));
	free(p);
	free(prog);
	return out;
}
	

// Mechanism for implementing a built-in module.
// If we add another module besides sys, will have to 
// move to another file.

Module *msys;

MVar*
addvar(char *name, char *cname, Type *type)
{
	MVar *mv;
	
	mv = emalloc(sizeof *mv);
	mv->name = nameof(name);
	mv->cname = nameof(cname);
	mv->type = typegeneralize(type);
	msys->mvarl = mkMVarL(mv, msys->mvarl);
	return mv;
}

void
addfn(Zfn **zfn, void *fn, char *name, char *cname, Type *type)
{
	*zfn = mkZfn(fn, nil);
	addvar(name, cname, type);
}

#define FN(x)	&sys_##x, sys_##x##_, #x, "sys_" #x
#define V(x) #x, "sys_" #x

NameL *sys_modpath;

Zint sys_astchatty;


void
m_sys_init(int argc, char **argv)
{
	int i;
	Type *fmt;
	static int did;
	MVar *v;
	
	if(did)
		abort();
	did++;

	sys_time_(0);
	msys = emalloc(sizeof *msys);
	msys->zeta = N("$Sys");
	
	addfn(FN(exit), typefnl(typeint(), typevoid(), nil));
	
	addfn(FN(readpipe), 
		typefnl(typelist(typestring()), typetuplel(typestring(), typeint(), nil), nil));

	addfn(FN(abort), typefnl(typevoid(), nil));
	addfn(FN(time), typefnl(typefloat(), nil));
	addfn(FN(readfile), typefnl(typestring(), typestring(), nil));
	addfn(FN(writefile), typefnl(typestring(), typestring(), typeint(), nil));
	addfn(FN(sleep), typefnl(typefloat(), typevoid(), nil));

	fmt = typewrapper(N("Fmt"));
	v = addvar("Fmt", "Fmt", typetype());
	v->realtype = fmt;
	
	addfn(FN(fmtstart), typefnl(fmt, nil));
	addfn(FN(fmtflush), typefnl(fmt, typestring(), nil));
	addfn(FN(fmt), typefnl(fmt, typestring(), typevoid(), nil));

	addfn(FN(aststats), typefnl(typetuplel(typeint(), typeint(), typeint(), nil), nil));

	sys_argv = mkZarray(argc);
	for(i=0; i<argc; i++)
		sys_argv->a[i] = ZN(nameof(argv[i]));
	addvar(V(argv), typearray(typestring()));
	
	sys_zetadir = N(PWD);
	addvar(V(zetadir), typestring());
	addvar(V(modpath), typelist(typestring()));
	addvar(V(astchatty), typeint());

	addfn(FN(loadmodule), typefnl(typestring(), typebool(), nil));
	addfn(FN(strstr), typefnl(typestring(), typestring(), typeint(), nil));
	addfn(FN(remove), typefnl(typestring(), typeint(), nil));
	addfn(FN(getpid), typefnl(typeint(), nil));
	
	addfn(FN(werrstr), typefnl(typestring(), typevoid(), nil));
	addfn(FN(errstr), typefnl(typestring(), nil));
	addfn(FN(fork), typefnl(typeint(), nil));
	addfn(FN(system), typefnl(typestring(), typeint(), nil));
	addfn(FN(gsub), typefnl(typestring(), typestring(), typestring(), typestring(), nil));

	registermodule(N("sys"), msys);
}

