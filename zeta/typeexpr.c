#include "a.h"
#include "type.h"
#include "runtime.h"
#include "compile.h"
#include "grammar.h"

void
noassign(Expr *expr, int kind)
{
	if(kind&LValue)
		error(expr->pos, "cannot assign to %#P", expr->pos);
	if(kind&BindValue)
		error(expr->pos, "cannot bind to %#P", expr->pos);
}

void
nosideeffect(Expr *expr, int kind)
{
	if(!(kind&(LValue|Value)))
		error(expr->pos, "%#P evaluated but not used", expr->pos);
}


typedef struct Binary Binary;
struct Binary
{
	BinOp op;
	char *namestr;
	char *typestr;
	GType *type;
	Name name;
};

static Binary binaries[] = {
	{ BinEqEq,	"==",	"fn('a, 'a): bool" },
	{ BinNotEq,	"!=",	"fn('a, 'a): bool" },

	{ BinAddInt,	"+",	"fn(int, int): int" },
	{ BinSubInt,	"-",	"fn(int, int): int" },
	{ BinMulInt,	"*",	"fn(int, int): int" },
	{ BinDivInt,	"/",	"fn(int, int): int" },
	{ BinModInt,	"%",	"fn(int, int): int" },
	{ BinLshInt,	"<<",	"fn(int, int): int" },
	{ BinRshInt,	">>",	"fn(int, int): int" },
	{ BinOrInt,	"|",	"fn(int, int): int" },
	{ BinAndInt,	"&",	"fn(int, int): int" },
	{ BinXorInt,	"^",	"fn(int, int): int" },
	{ BinLtInt,	"<",	"fn(int, int): bool" },
	{ BinGtInt,	">",	"fn(int, int): bool" },
	{ BinLeInt,	"<=",	"fn(int, int): bool" },
	{ BinGeInt,	">=",	"fn(int, int): bool" },

	{ BinAddFlt,	"+",	"fn(float, float): float" },
	{ BinSubFlt,	"-",	"fn(float, float): float" },
	{ BinMulFlt,	"*",	"fn(float, float): float" },
	{ BinDivFlt,	"/",	"fn(float, float): float" },
	{ BinModFlt,	"%",	"fn(float, float): float" },
	
	{ BinAddStr,	"+",	"fn(string, string): string" },
	{ BinLtStr,	"<",	"fn(string, string): bool" },
	{ BinLeStr,	"<=",	"fn(string, string): bool" },
	{ BinGtStr,	">",	"fn(string, string): bool" },
	{ BinGeStr,	">=",	"fn(string, string): bool" },
	{ BinIndexStr,	"[]",	"fn(string, int): string" },
	
	{ BinConsList,	"::",	"fn('a, list 'a): list 'a" },
	{ BinAssocList,	"[]",	"fn(list ('a, 'b), 'a): 'b" },	// overrides indexlist
	{ BinIndexList,	"[]",	"fn(list 'a, int): 'a" },
	{ BinAddList,	"+",	"fn(list 'a, list 'a): list 'a" },
	
	{ BinIndexArr,	"[]",	"fn(array 'a, int): 'a" },
	
	{ BinIndexMap,	"[]",	"fn(map ('a, 'b), 'a): 'b" },
	{ BinIndexFmap, "[]", "fn(fmap ('a, 'b), 'a): 'b" },
	
	// Have to use array 'a because array G is not a subtype of array Ast.
	// Will check actual type below.
	{ BinJoinAst,	"astjoin",	"fn(Ast, array 'a): Ast" },
	
	{ BinSameAst,	"astsame",	"fn(Ast, Ast): bool" },
};

static int binassign[MaxBinOp] = {
	[BinIndexArr] 1,
	[BinIndexMap]	1,
};

typedef struct Unary Unary;
struct Unary
{
	UnOp op;
	char *namestr;
	char *typestr;
	GType *type;
	Name name;
};

static Unary unaries[] = {
	{ UnNegInt,	"-",	"fn(int): int" },
	{ UnTwidInt,	"~",	"fn(int): int" },
	{ UnPosInt,	"+",	"fn(int): int" },
	{ UnNegFlt,	"-",	"fn(float): float" },
	{ UnLenList,	"length",	"fn(list 'a): int" },
	{ UnLenArr,	"length",	"fn(array 'a): int" },
	{ UnLenStr,	"length",	"fn(string): int" },
	{ UnBoolAny,	"bool",	"fn('a): bool" },
	{ UnStringInt,	"string",	"fn(int): string" },
	{ UnStringBool,	"string",	"fn(bool): string" },
	{ UnStringFlt,	"string",	"fn(float): string" },
	{ UnStringStr,	"string",	"fn(string): string" },	// stupid
	{ UnStringAst,	"string",	"fn(Ast): string" },
	{ UnIntFlt,	"int",	"fn(float): int" },
	{ UnIntString,	"int",	"fn(string): int" },
	{ UnIntBool,	"int",	"fn(bool): int" },
	{ UnArrayList,	"array",	"fn(list 'a): array 'a" },
	{ UnListArr,	"list",	"fn(array 'a): list 'a" },
	{ UnListAst,	"list",	"fn(Ast): list Ast" },
	{ UnArrayInt,	"array",	"fn(int): array __any__" },
	{ UnSplitAst,	"astsplit",	"fn(Ast): array Ast" },
	{ UnCopyAst,	"astcopy",	"fn(Ast): Ast" },
	{ UnLastkidAst,	"astlastkid",	"fn(Ast): Ast" },
	{ UnMergedAst,	"astmerged",	"fn(Ast): bool" },
	{ UnLispAst,	"astlisp",	"fn(Ast): string" },
	{ UnLongAst,	"astlong",	"fn(Ast): string" },
	{ UnFloatStr,	"float",	"fn(string): float" },
	{ UnParentAst,	"astparent",	"fn(Ast): Ast" },
	{ UnSlottedAst,	"astslotted",	"fn(Ast): bool" },
	{ UnSlotAst,	"astslot",	"fn(Ast): Ast" },
	{ UnKeysMap,	"keys",	"fn(map('a, 'b)): list 'a" },
	{ UnPtr,	"ptr",	"fn('a): string" },
	{ UnRmcopyAst,	"astuncopy",	"fn(Ast)" },
};

void
typecheckinit(void)
{
	int i;
	
	if(binaries[0].type)
		return;
	for(i=0; i<nelem(binaries); i++){
		binaries[i].name = nameof(binaries[i].namestr);
		binaries[i].type = parsetype(binaries[i].typestr);
	}
	for(i=0; i<nelem(unaries); i++){
		unaries[i].name = nameof(unaries[i].namestr);
		unaries[i].type = parsetype(unaries[i].typestr);
	}
}

static int
typecheckbinary(Expr *expr, Name name)
{
	TypeL *l;
	Type *t;
	int i;
	
	l = mkTypeL(expr->e1->type, mkTypeL(expr->e2->type, nil));
	for(i=0; i<nelem(binaries); i++){
		if(binaries[i].name != name)
			continue;
		t = typeinstantiate(binaries[i].type);
		if(!canunifyl(t->typel, l, 1))
			continue;
		expr->binop = binaries[i].op;
		expr->type = typeunified(t->left);
		return 0;
	}
	error(expr->pos, "cannot typecheck %T %s %T",
		expr->e1->type, expr->name, expr->e2->type);
	return -1;
}

static int
typecheckunary(Expr *expr)
{
	TypeL *l;
	Type *t;
	int i;
	
	l = mkTypeL(expr->e1->type, nil);
	for(i=0; i<nelem(unaries); i++){
		if(unaries[i].name != expr->name)
			continue;
		t = typeinstantiate(unaries[i].type);
		if(!canunifyl(t->typel, l, 1))
			continue;
		expr->unop = unaries[i].op;
		expr->type = t->left;
		return 0;
	}
	error(expr->pos, "cannot typecheck %s %T",
		expr->name, expr->e1->type);
	return -1;
}

void
escapev(Var *v)
{
	Fn *fn;

	if(v->escape)
		return;
	fn = v->scope->frame->fn;
	fn->stolen = mkVarL(v, fn->stolen);
	if(fn->nescape == 0)
		fn->nescape = 1;
	v->escape = fn->nescape++;
	v->cname = nil;
}

void
escapeto(Fn *inner, Fn *outer)
{
	Fn *f;
	
	f = inner;
	do{
		f = f->outer;
		if(f->nescape == 0)
			f->nescape = 1;
	}while(f != outer);
}

Var*
lookupvar(Scope *scope, Name name)
{
	VarL *l;
	Scope *s;
	Var *v;
	
	for(s=scope; s; s=s->outer)
		for(l=s->varl; l; l=l->tl){
			v = l->hd;
			if(v->name == name){
				if(s->frame && scope->frame && 
				   s->frame->fn && s->frame->fn != scope->frame->fn){
					escapeto(scope->frame->fn, s->frame->fn);
					escapev(v);
				}
				return v;
			}
		}
	return nil;
}

VarL*
lookupvarl(Scope *scope, Name name)
{
	VarL *l, *ret;
	Scope *s;
	Var *v;
	
	ret = nil;
	for(s=scope; s; s=s->outer)
		for(l=s->varl; l; l=l->tl){
			v = l->hd;
			if(v->name == name){
				if(s->frame && scope->frame && 
				   s->frame->fn && s->frame->fn != scope->frame->fn){
					escapeto(scope->frame->fn, s->frame->fn);
					escapev(v);
				}
				ret = mkVarL(v, ret);
			}
		}
	return revVarL(ret);
}

static int
isattribute(Var *v, Type *type)
{
	return canunify(v->type->left->typel->hd, type, 1);
}

Var*
lookupattribute(Scope *scope, Name name, Type *type)
{
	VarL *l;
	Scope *s;
	Var *v;
	char *prefix;

	// Built-in attributes.
	// It would be nice if this were more table-driven,
	// but there are lots of special cases.
	if(name == N("fileline")){
		// Not a special case: attribute line(Ast): (string, int).
		v = emalloc(sizeof *v);
		v->type = typegeneralize(typefnl(typeast(), typetuplel(typestring(), typeint(), nil), nil));
		v->flags |= VarAttribute|VarBuiltin;
		v->cname = N("astline");
		v->name = nameprint("attribute astline()", type);
		return v;
	}
	if(name == N("parent") || name == N("prev") || name == N("next")){
		// Invent attribute(G): G for any grammar type G.
		v = emalloc(sizeof *v);
		type = typegramsym(type, nil, nil);
		v->type = typegeneralize(typefnl(type, type, nil));
		v->flags |= VarAttribute|VarBuiltin;
		v->cname = nameprint("ast%s_attr", name);
		v->name = nameprint("attribute ast%s(%T)", name, type);
		return v;
	}
	if(name == N("canonical")){
		// Invent attribute(G.sym): G.sym for any grammar type G.sym.
		// We use the specific type signature to enforce that canonicalization doesn't
		// change the kind of symbol (that would make for some malformed trees).
		if(type->sym == nil){
fprint(2, "canonical? %T\n", type);
			return nil;
		}
		v = emalloc(sizeof *v);
		v->type = typegeneralize(typefnl(type, type, nil));
		v->flags |= VarAttribute|VarBuiltin|VarExtensible|VarCanonical;
		v->cname = nameprint("%#T->sym->canonical", type);
		v->name = nameprint("attribute canonical(%T)", type);
		return v;
	}
	if(name == N("copiedfrom")){
		// Invent attribute(G.sym): G.sym for any grammar type G.sym.
		// Or just attribute(G): G for any grammar type G.
		v = emalloc(sizeof *v);
		v->type = typegeneralize(typefnl(type, type, nil));
		v->flags |= VarAttribute|VarBuiltin;
		v->cname = nameprint("ast%s_attr", name);
		v->name = nameprint("attribute ast%s(%T)", name, type);
		return v;
	}
	prefix = smprint("attribute %s(", name);
	for(s=scope; s; s=s->outer)
		for(l=s->varl; l; l=l->tl){
			v = l->hd;
			if((v->flags&VarAttribute) &&
			   memcmp(nametostr(v->name), prefix, strlen(prefix)) == 0 &&
			   isattribute(v, type)){
				if(s->frame && scope->frame && 
				   s->frame->fn && s->frame->fn != scope->frame->fn){
					escapeto(scope->frame->fn, s->frame->fn);
					escapev(v);
				}
				free(prefix);
				return v;
			}
		}
	free(prefix);
	return nil;
}

static Hash *mscopes;

Var*
mlookupvar(Module *m, Name name)
{
	Scope *s;
	VarL *vl;
	MVarL *mvl;
	Var *v;
	MVar *mv;

	if(mscopes == nil)
		mscopes = mkhash();
	s = hashget(mscopes, m);
	if(s == nil){
		s = emalloc(sizeof *s);
		vl = nil;
		for(mvl=m->mvarl; mvl; mvl=mvl->tl){
			mv = mvl->hd;
			v = emalloc(sizeof *v);
			v->name = mv->name;
			v->cname = mv->cname;
			v->type = mv->type;
			v->realtype = mv->realtype;
			v->flags = mv->flags;
			vl = mkVarL(v, vl);
		}
		vl = revVarL(vl);
		s->varl = vl;
		hashput(mscopes, m, s);
	}
	return lookupvar(s, name);
}

Var*
lookupframe(Frame *f, Name cname)
{
	VarL *l;

	for(l=f->varl; l; l=l->tl)
		if(l->hd->cname == cname)
			return l->hd;
	return nil;
}

Var*
_declarevar(Pos pos, Scope *scope, Name name, Type *type, int check, Name newcname)
{
	int n;
	VarL *l;
	Var *v;
	Name cname;
	
	if(scope == nil)
		panic("declarevar");

	if(check){
		for(l=scope->varl; l; l=l->tl){
			if(l->hd->name == name && name != N("_")){
				if(newcname && l->hd->cname == newcname && l->hd->scope == scope)
					return l->hd;
				if(newcname == nil || l->hd->cname != newcname)
				if(strncmp(nametostr(name), "restructure("/*)*/, 12) != 0
				&& strncmp(nametostr(name), "nodestructure("/*)*/, 14) != 0)
					warn(pos, "redeclaring %s [%s %s]", name, newcname, l->hd->cname);
				break;
			}
		}
	}

	v = emalloc(sizeof *v);
	v->pos = pos;
	v->name = name;
	v->type = typegeneralize(type);
	v->scope = scope;
	if(name != N("_"))
		scope->varl = mkVarL(v, scope->varl);
	
	if(newcname)
		v->cname = newcname;
	else{
		cname = csanitize(name);
		if(scope->frame->prefix)
			cname = nameprint("%s%s", scope->frame->prefix, cname);

		n = 0;
		v->cname = cname;
		while(lookupframe(scope->frame, v->cname))
			v->cname = nameprint("%s__%d", cname, ++n);
	}
	scope->frame->varl = mkVarL(v, scope->frame->varl);
	return v;
}

Var*
declarevar(Pos pos, Scope *scope, Name name, Type *type)
{
	return _declarevar(pos, scope, name, type, 1, nil);
}

Var*
declarevarmany(Pos pos, Scope *scope, Name name, Type *type)
{
	return _declarevar(pos, scope, name, type, 0, nil);
}

// Redeclare expr to have type.
// If expr is too complex, just forget it.
void
redeclare(Scope *scope, Expr *expr, Type *type)
{
	Var *v;
	ExprL *el;
	TypeL *tl;
	
if(issubtype(expr->type, type)) fprint(2, "redeclare %s %T %T\n", expr->name, expr->type, type);

	switch(expr->op){
	case ExprName:
		v = _declarevar(expr->pos, scope, expr->name, type, 0, nil);
		v->flags |= VarRedeclare;
		// XXX Redeclare doesn't work right if variable
		// escapes after the fact.
		v->cname = expr->var->cname;
		return;
	case ExprTuple:
		if(type->op != TypeTuple){
			error(expr->pos, "redeclare tuple := non-tuple");
			return;
		}
		for(el=expr->el, tl=type->typel; el && tl; el=el->tl, tl=tl->tl)
			redeclare(scope, el->hd, tl->hd);
		if(el || tl)
			error(expr->pos, "argument count mismatch");
		return;
	default:
		return;
	}
}

// Declare new variable expr to have type.
void
declaretype(Scope *scope, Expr *expr, Type *type)
{
	Var *v;
	ExprL *el;
	TypeL *tl;

	expr->type = type;
	switch(expr->op){
	case ExprName:
		if(expr->name == N("_")){
			expr->var = nil;
			expr->type = type;
			return;
		}
		v = declarevar(expr->pos, scope, expr->name, type);
		expr->var = v;
		return;

	case ExprTuple:
		if(type->op != TypeTuple){
			error(expr->pos, "tuple := non-tuple");
			return;
		}
		for(el=expr->el, tl=type->typel; el && tl; el=el->tl, tl=tl->tl)
			declaretype(scope, el->hd, tl->hd);
		if(el || tl)
			error(expr->pos, "argument count mismatch");
		return;
	default:
		error(expr->pos, "cannot declare %#P", expr->pos);
	}
}

int
typecheckexprl(Typecheck *tc, ExprL *l, int kind)
{
	int r;
	
	r = 0;
	for(; l; l=l->tl)
		if(typecheckexpr(tc, l->hd, kind) < 0)
			r = -1;
	return r;
}

static TypeL*
exprtypes(ExprL *l)
{
	TypeL *tl;
	
	tl = nil;
	for(; l; l=l->tl)
		tl = mkTypeL(l->hd->type, tl);
	return revTypeL(tl);
}

static int
typemaxl(TypeL *l, Type **max)
{
	Type *t;

	t = nil;
	for(; l; l=l->tl)
		if(typemax(t, l->hd, &t) < 0)
			return -1;
	*max = t;
	return 0;
}

static Type*
typemapt(Type *t)
{
	if(t->op == TypeAny)
		return typemap(typeany(), typeany());
	assert(t->op == TypeTuple && lenTypeL(t->typel) == 2);
	return typemap(t->typel->hd, t->typel->tl->hd);
}

static Type*
typefmapt(Type *t)
{
	if(t->op == TypeAny)
		return typefmap(typeany(), typeany());
	assert(t->op == TypeTuple && lenTypeL(t->typel) == 2);
	return typefmap(t->typel->hd, t->typel->tl->hd);
}

int
typecheckexpr(Typecheck *tc, Expr *expr, int kind)
{
	Var *v;
	Type *t, *tt, *xtype;
	Expr *e;
	ExprL *el;
	char *what;
	Type *(*typecon)(Type*);
	TypeL *tl;
	Scope *oldscope, *tscope;
	
	if(expr->type){
		// Already typechecked- don't repeat.
		return 0;
	}

	oldscope = tc->scope;
	
	switch(expr->op){
	case ExprDots:
		error(expr->pos, "dots ... out of place");

	case ExprBool:
		nosideeffect(expr, kind);
		noassign(expr, kind);
		expr->type = typebool();
		break;

	case ExprInteger:
		nosideeffect(expr, kind);
		noassign(expr, kind);
		expr->type = typeint();
		break;
	
	case ExprFloat:
		nosideeffect(expr, kind);
		noassign(expr, kind);
		expr->type = typefloat();
		break;

	case ExprString:
		nosideeffect(expr, kind);
		noassign(expr, kind);
		expr->type = typestring();
		expr->str = nameof(cunescapestring(nametostr(expr->name), -1));
		break;
	
	case ExprNil:
		nosideeffect(expr, kind);
		noassign(expr, kind);
		expr->type = typenil();
		break;

	case ExprName:
		nosideeffect(expr, kind);
		// Internally-generated ExprNames can have pre-associated variables.
		if(expr->var)
			v = expr->var;
		else{
			if(expr->name == N("_")){
				if(kind != LValue){
					error(expr->pos, "cannot read _");
					return -1;
				}
				expr->var = nil;
				expr->type = typeany();
				break;
			}
			if(tc->scope == nil || (v = lookupvar(tc->scope, expr->name)) == nil){
				error(expr->pos, "unknown name %s", expr->name);
				return -1;
			}
			expr->var = v;
		}
		if(v->flags&VarRedeclare)
			noassign(expr, kind);
		expr->type = typeinstantiate(v->type);
		if(expr->type->op == TypeModule)
			expr->module = v->module;
		if(expr->type->op == TypeType)
			expr->realtype = v->realtype;
		break;
	
	case ExprAssign:
		noassign(expr, kind);
		if(typecheckexpr(tc, expr->e1, LValue) +
		   typecheckexpr(tc, expr->e2, Value) < 0)
			return -1;
		t = typeinstantiate(typegeneralize(expr->e1->type));
		if(!canunify(t, expr->e2->type, 1)){
			error(expr->pos, "cannot assign %T = %T [%r]",
				expr->e1->type, expr->e2->type);
			return -1;
		}
		expr->type = typeunified(expr->e1->type);
		break;

	case ExprDeclType:
		if((t = typechecktype(tc, expr->typex)) == nil)
			return -1;
		expr->type = t;
		v = declarevar(expr->pos, tc->scope, expr->name, expr->type);
		expr->var = v;
		break;
	
	case ExprDeclAssign:
		if(typecheckexpr(tc, expr->e2, Value) < 0)
			return -1;
		expr->type = expr->e2->type;
		declaretype(tc->scope, expr->e1, expr->type);
		break;

	case ExprUnary:
		noassign(expr, kind);
		if(typecheckexpr(tc, expr->e1, Value) < 0)
			return -1;
		if(typecheckunary(expr) < 0)
			return -1;
		switch(expr->unop){
		default:
			nosideeffect(expr, kind);
			break;
		case UnListAst:
			nosideeffect(expr, kind);
			t = expr->e1->type;
			if(t->sym){
				CfgSym *sym;
				
				sym = gramunlistsym(t->gram, t->sym);
				if(sym == nil){
					error(expr->pos, "cannot convert %T to list", t);
					return -1;
				}
				expr->type = typelist(typegramsym(t, sym->name, nil));
			}else{
				// XXX Should it be an error to do list(ast)?
				expr->type = typelist(t);
			}
			break;
		
		case UnSplitAst:
			nosideeffect(expr, kind);
			t = expr->e1->type;
			expr->type = typearray(typegramsym(t, nil, nil));
			break;
		
		case UnCopyAst:
			nosideeffect(expr, kind);
			expr->type = expr->e1->type;
			break;
		
		case UnLastkidAst:
			nosideeffect(expr, kind);
			expr->type = typegramsym(expr->e1->type, nil, nil);
			break;
		
		case UnParentAst:
			nosideeffect(expr, kind);
			if((t = typechecktype(tc, expr->typex)) == nil)
				return -1;
			tt = typegramsym(expr->e1->type, nil, nil);
			if(!issubtype(t, tt)){
				error(expr->pos, "parent of %T cannot have type %T", tt, t);
				return -1;
			}
			expr->type = t;
			break;
		
		case UnSlotAst:
			nosideeffect(expr, kind);
			if((t = typechecktype(tc, expr->typex)) == nil)
				return -1;
			tt = typegramsym(expr->e1->type, nil, nil);
			if(!issubtype(t, tt) || t->sym == nil){
				error(expr->pos, "cannot convert %T slot to %T",
					expr->e1->type, t);
				return -1;
			}
			expr->type = t;
			break;
		
		case UnPtr:
			nosideeffect(expr, kind);
			if(!issubtype(typenil(), expr->e1->type)){
				error(expr->pos, "cannot get pointer for %T",
					expr->e1->type);
				return -1;
			}
			break;
		
		case UnRmcopyAst:
			// NO nosideeffect
			break;
		}
		break;
	
	case ExprBinary:
		nosideeffect(expr, kind);
		if(typecheckexpr(tc, expr->e1, Value) +
		   typecheckexpr(tc, expr->e2, Value) < 0)
			return -1;
		if(expr->e1->type->op == TypeTuple && expr->e2->type == typeint()){
			if(expr->e2->op != ExprInteger){
				error(expr->pos, "tuple index must be integer literal");
				return -1;
			}
			if(expr->e2->i < 0 || expr->e2->i >= lenTypeL(expr->e1->type->typel)){
				error(expr->pos, "tuple index out of range");
				return -1;
			}
			expr->type = nthTypeL(expr->e1->type->typel, expr->e2->i);
			expr->binop = BinIndexTuple;
			noassign(expr, kind);
			break;
		}
		if(typecheckbinary(expr, expr->name) < 0)
			return -1;
		if(!binassign[expr->binop])
			noassign(expr, kind);
		switch(expr->binop){
		default:
			break;
		case BinJoinAst:
			tt = expr->e2->type->left;
			if(tt->op != TypeGram || typemax(expr->e1->type, tt, &t) < 0){
				error(expr->pos, "cannot astjoin(%T, %T)", expr->e1->type, expr->e2->type);
				return -1;
			}
			if(t->gram == nil){
				error(expr->pos, "cannot determine grammar for astjoin");
				return -1;
			}
			expr->type = typegramsym(t, expr->e1->type->symname, expr->e1->type->sym);
			break;
		}
		break;

	case ExprBinaryEq:
		noassign(expr, kind);
		if(typecheckexpr(tc, expr->e1, LValue) +
		   typecheckexpr(tc, expr->e2, Value) < 0)
			return -1;
		if(typecheckbinary(expr, 
				nameofn(nametostr(expr->name), 
					strlen(nametostr(expr->name))-1)) < 0)
			return -1;
		break;

	case ExprPostInc:
	case ExprPostDec:
	case ExprPreInc:
	case ExprPreDec:
		if(expr->op == ExprPostInc && !(kind&Value))
			expr->op = ExprPreInc;
		if(expr->op == ExprPostDec && !(kind&Value))
			expr->op = ExprPreDec;
		noassign(expr, kind);
		// Typecheck e1 += 1
		e = mkExpr(ExprBinaryEq);
		e->e1 = expr->e1;
		e->e2 = mkExpr(ExprInteger);
		e->e2->i = 1;
		if(expr->op == ExprPreInc || expr->op == ExprPostInc)
			e->name = N("+=");
		else
			e->name = N("-=");
		if(typecheckexpr(tc, e, 0) < 0)
			return -1;
		expr->type = expr->e1->type;
		// Squirrel it away for later.
		expr->e3 = e;
		break;

	case ExprMkStruct:
		// can't happen - not produced by parser.
		panic("typecheck exprconstruct");

	case ExprCall:
		noassign(expr, kind);
		if(typecheckexpr(tc, expr->e1, Value) < 0)
			return -1;
		t = expr->e1->type;
		if(t->op == TypeType){
			if(expr->e1->realtype == nil){
				error(expr->pos, "type constructor with no type [can't happen]");
				return -1;
			}
			t = expr->e1->realtype;
			if(t == nil || t->op != TypeStruct){
				error(expr->pos, "cannot construct %T", t);
				return -1;
			}
			expr->type = t;
			expr->op = ExprMkStruct;
			if(expr->el == nil){
				// Okay - will zero.
				break;
			}
			tl = structmktypes(t, &expr->namel, 1);
			for(el=expr->el; el; el=el->tl){
				if(el->hd->op == ExprDots){
					if(el->tl){
						error(el->hd->pos, "dots ... not last argument");
						return -1;
					}
					tl = nil;
					break;
				}
				if(typecheckexpr(tc, el->hd, Value) < 0)
					return -1;
				if(tl == nil){
					error(el->hd->pos, "too many constructor arguments");
					return -1;
				}
				if(!canunify(tl->hd, el->hd->type, 1)){
					error(el->hd->pos, "cannot use %#P as %T [%r]",
						el->hd->pos, tl->hd);
					return -1;
				}
				tl = tl->tl;
			}
			if(tl){
				error(expr->pos, "not enough constructor arguments");
				return -1;
			}
			break;
		}
		if(typecheckexprl(tc, expr->el, Value) < 0)
			return -1;
		if(t->op != TypeFn){
			error(expr->pos, "cannot call %T [%#P]", expr->e1->type, expr->e1->pos);
			return -1;
		}
		t = typeinstantiate(typegeneralize(t));
		if(!canunifyl(t->typel, exprtypes(expr->el), 1)){
			// XXX better error
			error(expr->pos, "bad call %T [%r]", t);
			return -1;
		}
		expr->type = typeunified(t->left);
		break;
	
	case ExprDot:
		if(typecheckexpr(tc, expr->e1, Value) < 0)
			return -1;
		switch(expr->e1->type->op){
		case TypeModule:
			nosideeffect(expr, kind);
			v = mlookupvar(expr->e1->var->module, expr->name);
			if(v == nil){
				error(expr->pos, "no %#P", expr->pos);
				return -1;
			}
			// Behave like named variable reference.
			expr->var = v;
			expr->op = ExprName;
			expr->type = typeinstantiate(v->type);
			expr->realtype = v->realtype;
			expr->module = v->module;
			break;
		
		case TypeList:
			noassign(expr, kind);
			nosideeffect(expr, kind);
			if(expr->name == N("hd")){
				expr->type = expr->e1->type->left;
				break;
			}
			if(expr->name == N("tl")){
				expr->type = expr->e1->type;
				break;
			}
			error(expr->pos, "no member %s", expr->name);
			return -1;
		
		case TypeType:
			noassign(expr, kind);
			nosideeffect(expr, kind);
			if(expr->e1->realtype == nil){
				error(expr->pos, "no type for %#P ?", expr->e1->pos);
				return -1;
			}
			if(expr->e1->realtype->op == TypeGram)
				t = typegramdot(expr->e1->realtype, expr->name);
			else
				t = typestructdot(expr->e1->realtype, expr->name);
			if(t == nil){
				error(expr->pos, "no type %#P", expr->pos);
				return -1;
			}
			expr->type = typetype();
			expr->realtype = t;
			break;
		
		case TypeStruct:
			nosideeffect(expr, kind);
			t = structdot(expr->e1->type, expr->name, &xtype);
			if(t == nil){
				error(expr->pos, "no field %s in %T", expr->name, expr->e1->type);
				return -1;
			}
			expr->type = t;
			expr->realtype = xtype;
			break;
		
		case TypeGram:
			if(expr->name == N("canonical"))
				warn(expr->pos, "almost always wrong to use ast.canonical directly");
			v = lookupattribute(tc->scope, expr->name, expr->e1->type);
			if(v == nil){
				error(expr->pos, "no attribute %s(%T)", expr->name, expr->e1->type);
				return -1;
			}
			expr->type = typeinstantiate(v->type)->left;
			expr->var = v;
			break;

		default:
			error(expr->pos, "cannot dot %T", expr->e1->type);
			return -1;
		}
		break;

	case ExprMkList:
		what = "list";
		typecon = typelist;
	Construct:
		noassign(expr, kind);
		nosideeffect(expr, kind);
		if(typecheckexprl(tc, expr->el, Value) , 0)
			return -1;
		tl = exprtypes(expr->el);
		if(typecon == typefmapt && tl){
			t = tl->hd;
			if(t->op == TypeFmap)
				tl = mkTypeL(typetuplel(t->left, t->right, nil), tl->tl);
		}
		if(typemaxl(tl, &t) < 0){
			error(expr->pos, "mixed types in %s", what);
			return -1;
		}
		if(t == nil)
			t = typeany();
		expr->type = typecon(t);
		break;
	
	case ExprMkArray:
		what = "array";
		typecon = typearray;
		goto Construct;
	
	case ExprMkMap:
		what = "map";
		typecon = typemapt;
		goto Construct;

	case ExprMkFmap:
		what = "fmap";
		typecon = typefmapt;
		goto Construct;

	case ExprMap:
		noassign(expr, kind);
		nosideeffect(expr, kind);
		if(typecheckexpr(tc, expr->e1, Value) +
		   typecheckexpr(tc, expr->e2, Value) < 0)
			return -1;
		expr->type = typetuplel(expr->e1->type, expr->e2->type, nil);
		break;

	case ExprMatch:
		noassign(expr, kind);
		nosideeffect(expr, kind);	// not true, but still a mistake not to look at result
		if(typecheckexpr(tc, expr->e1, Value) >=0){
			tscope = pushscope(tc);
			typecheckmatch(tc, expr->e2, expr->e1->type);
			popscope(tc, tscope);
		}
		expr->type = typebool();
		expr->l1 = tc->nlabel++;
		break;

	case ExprNot:
		noassign(expr, kind);
		nosideeffect(expr, kind);
		typecheckexpr(tc, expr->e1, Value);
		expr->e1 = tobool(tc, expr->e1);
		expr->type = typebool();
		break;
	
	case ExprAndAnd:
	case ExprOrOr:
		noassign(expr, kind);
		typecheckexpr(tc, expr->e1, Value);
		typecheckexpr(tc, expr->e2, kind);
		expr->e1 = tobool(tc, expr->e1);
		expr->e2 = tobool(tc, expr->e2);
		expr->type = typebool();
		break;
	
	case ExprSlice:
		nosideeffect(expr, kind);
		if(typecheckexpr(tc, expr->e1, Value) +
		   typecheckexpr(tc, expr->e2, Value) +
		   (expr->e3 ? typecheckexpr(tc, expr->e3, Value) : 0) < 0)
			return -1;
		if(expr->e2->type != typeint() ||
		   (expr->e3 && expr->e3->type != typeint())){
			error(expr->pos, "need integer indices in slice");
			return -1;
		}
		if(expr->e1->type->op == TypeArray || expr->e1->type == typestring())
			expr->type = expr->e1->type;
		else{
			error(expr->pos, "cannot slice %T", expr->e1->type);
			return -1;
		}
		break;
	
	case ExprFn:
		nosideeffect(expr, kind);
		noassign(expr, kind);
		if(typecheckfn(tc, expr->fn) < 0)
			return -1;
		expr->type = typeinstantiate(expr->fn->type);
		break;
	
	case ExprTuple:
		nosideeffect(expr, kind);
		if(typecheckexprl(tc, expr->el, kind) < 0)
			return -1;
		expr->type = typetuple(exprtypes(expr->el));
		break;
	
	case ExprSubtype:
		nosideeffect(expr, kind);
		noassign(expr, kind);
		if(typecheckexpr(tc, expr->e1, Value) < 0)
			return -1;
		t = typechecktype(tc, expr->typex);
		if(t == nil)
			return -1;
		if(issubtype(expr->e1->type, t)){
			error(expr->pos, "subtype check is always true");
			return -1;
		}
		// XXX explain this
		if(typemax(t, expr->e1->type, &tt) < 0){
			error(expr->pos, "subtype check is always false [%T <: %T]", t, expr->e1->type);
			return -1;
		}
		expr->realtype = t;
		expr->type = typebool();
		break;
	
	case ExprTag:
		nosideeffect(expr, kind);
		noassign(expr, kind);
		if(typecheckexpr(tc, expr->e1, Value) < 0)
			return -1;
		if(!istagtype(expr->e1->type)){
			error(expr->pos, "cannot tag(%T)", expr->e1->type);
			return -1;
		}
		expr->type = typestring();
		break;
	
	case ExprParse:
		if(typecheckgexpr(tc, expr, kind) < 0)
			return -1;
		break;
	
	case ExprBacktick:
	case ExprBacktick2:
		if(typecheckpexpr(tc, expr, kind) < 0)
			return -1;
		break;
	}
	if(expr->type == nil)
		panic("%P: missed type for %#P", expr->pos, expr->pos);
	// XXX maybe check that there are no unbound type variables?
	
	assert(tc->scope == oldscope);
	return 0;
}

static Expr*
typex2expr(Typex *x)
{
	Expr *e;

	switch(x->op){
	default:
		panic("typex2expr %#P", x->pos);
	case TypexName:
		e = mkExpr(ExprName);
		e->name = x->name;
		e->pos = x->pos;
		return e;
	case TypexDot:
		e = mkExpr(ExprDot);
		e->e1 = typex2expr(x->left);
		e->name = x->name;
		e->pos = x->pos;
		return e;
	}
}

TypeL* typechecktypel(Typecheck*, TypexL*);

Type*
typechecktype(Typecheck *tc, Typex *x)
{
	Type *t, *t1, *t2;
	TypeL *tl;
	Var *v;
	Expr *e;
	
	switch(x->op){
	case TypexEmpty:
		error(x->pos, "unexpected missing type");
		return nil;

	case TypexType:
		return x->type;
	
	case TypexOne:
		if((t = typechecktype(tc, x->left)) == nil)
			return nil;
		return x->mk.one(t);
	
	case TypexTwo:
		if((t1 = typechecktype(tc, x->left)) == nil)
			return nil;
		if((t2 = typechecktype(tc, x->right)) == nil)
			return nil;
		return x->mk.two(t1, t2);
	
	case TypexListOne:
		if((tl = typechecktypel(tc, x->typexl)) == nil && x->typexl != nil)
			return nil;
		if((t = typechecktype(tc, x->left)) == nil)
			return nil;
		return x->mk.listone(tl, t);
	
	case TypexList:
		if((tl = typechecktypel(tc, x->typexl)) == nil && x->typexl != nil)
			return nil;
		return x->mk.list(tl);

	case TypexVar:
		// XXX Can we get rid of TypeVarName?
		return typevarname(x->name);
	
		if(tc == nil){
			error(x->pos, "unknown type %s", x->name);
			return nil;
		}
		v = lookupvar(tc->scope, x->name);
		if(v == nil || v->type != typegeneralize(typetype())){
			error(x->pos, "%s is not a type", x->name);
			return nil;
		}
		return v->realtype;
	
	case TypexName:
	case TypexDot:
		// Convert to an expr.
		e = typex2expr(x);
		if(typecheckexpr(tc, e, Value) < 0)
			return nil;
		if(e->type != typetype()){
			error(x->pos, "%#P is not a type", x->pos);
			return nil;
		}
		if(e->realtype == nil){
			error(x->pos, "%#P has no realtype [can't happen]", x->pos);
			return nil;
		}
		return e->realtype;
	}
	panic("typechecktype %#P", x->pos);
	return nil;
}

TypeL*
typechecktypel(Typecheck *tc, TypexL *l)
{
	TypeL *tl;
	Type *t;

	if(l == nil)
		return nil;

	t = typechecktype(tc, l->hd);
	if(t == nil)
		return nil;
	tl = typechecktypel(tc, l->tl);
	if(l->tl && tl == nil)
		return nil;
	return mkTypeL(t, tl);
}

