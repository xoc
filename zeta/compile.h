// Data structures used only in the zeta compiler (not at run time).

typedef struct Expr		Expr;
typedef struct Fnarg		Fnarg;
typedef struct Fn		Fn;
typedef struct Frame		Frame;
typedef struct Include		Include;
typedef struct Pos		Pos;
typedef struct Scope		Scope;
typedef struct Stmt		Stmt;
typedef struct Structelem	Structelem;
typedef struct Typecheck	Typecheck;
typedef struct Var		Var;
typedef struct Yystype		Yystype;
typedef struct Typex Typex;

LIST(Expr)
LIST(Fn)
LIST(Fnarg)
LIST(Include)
LIST(Stmt)
LIST(Var)
LIST(Typex)
LIST(Structelem)

//////////////////////////////////////////////////////////////////////////
//
// Input, lexing, parsing

// Position of text in input (byte offsets).
struct Pos {
	int start;
	int end;
};
int	pos2fileline(Pos, char**);

// %P	file:line
// %#P	actual text at given location
// %lP	line "file"
int	posfmt(Fmt*);

// Error messages - error and warn are in the GNU C library, sigh.
#define	error	zeta_error
#define	warn	zeta_warn

void	error(Pos, char*, ...);
void	warn(Pos, char*, ...);
extern int	nerrors;	// counts calls to error


// Convert zeta file name into zm, zo, prefix
Name	zeta2zm(Name);
Name	zeta2zo(Name);
Name	zeta2prefix(Name);

typedef enum TypexOp {
	TypexType = 1,
	TypexOne,
	TypexListOne,
	TypexTwo,
	TypexVar,
	TypexList,
	TypexName,
	TypexDot,
	TypexEmpty,
} TypexOp;
struct Typex 
{
	Pos pos;
	TypexOp op;
	struct /*union, but safer*/ {
		Type *(*one)(Type*);
		Type *(*two)(Type*, Type*);
		Type *(*listone)(TypeL*, Type*);
		Type *(*list)(TypeL*);
	} mk;
	Name name;
	Typex *left;
	Typex *right;
	TypexL *typexl;
	Type *type;
};

Typex *mkTypex(TypexOp);
Typex *mkTypex1(Type*);

// Binary operators.
typedef enum BinOp {
	BinEqEq = 1,
	BinNotEq,

	BinAddInt,
	BinSubInt,
	BinMulInt,
	BinDivInt,
	BinModInt,
	BinLshInt,
	BinRshInt,
	BinOrInt,
	BinAndInt,
	BinXorInt,
	BinLtInt,
	BinGtInt,
	BinLeInt,
	BinGeInt,

	BinAddFlt,
	BinSubFlt,
	BinMulFlt,
	BinDivFlt,
	BinModFlt,
	
	BinAddStr,
	BinLtStr,
	BinGtStr,
	BinLeStr,
	BinGeStr,
	BinIndexStr,
	
	BinConsList,
	BinIndexList,
	BinAssocList,
	BinAddList,
	
	BinIndexArr,
	
	BinIndexMap,
	BinIndexFmap,
	
	BinIndexTuple,
	
	BinJoinAst,
	BinSameAst,
	
	MaxBinOp,
} BinOp;

// Unary operators
typedef enum UnOp {
	UnNegInt = 1,
	UnPosInt,
	UnTwidInt,
	UnLenList,
	UnLenArr,
	UnLenStr,
	UnBoolAny,
	UnStringBool,
	UnStringInt,
	UnStringFlt,
	UnStringStr,
	UnArrayList,
	UnListArr,
	UnListAst,
	UnArrayInt,
	UnSplitAst,
	UnCopyAst,
	UnLastkidAst,
	UnMergedAst,
	UnLispAst,
	UnLongAst,
	UnStringAst,
	UnIntFlt,
	UnIntString,
	UnIntBool,
	UnNegFlt,
	UnFloatStr,
	UnParentAst,
	UnSlotAst,
	UnSlottedAst,
	UnKeysMap,
	UnPtr,
	UnRmcopyAst,
	MaxUnOp,
} UnOp;

struct ParsePattern;

// Expressions
typedef enum ExprOp
{
	ExprName,
	ExprDeclType,
	ExprDeclAssign,
	ExprAssign,
	ExprInteger,
	ExprString,
	ExprUnary,
	ExprBinary,
	ExprBinaryEq,
	ExprCall,
	ExprDot,
	ExprPreInc,
	ExprPreDec,
	ExprPostInc,
	ExprPostDec,
	ExprNot,
	ExprMkList,
	ExprMkArray,
	ExprTuple,
	ExprMatch,
	ExprAndAnd,
	ExprOrOr,
	ExprFloat,
	ExprBool,
	ExprNil,
	ExprSlice,
	ExprFn,
	ExprMap,
	ExprMkMap,
	ExprMkFmap,
	ExprMkStruct,
	ExprSubtype,
	ExprTag,
	ExprParse,
	ExprBacktick,
	ExprBacktick2,
	ExprDots,
} ExprOp;
struct Expr
{
	ExprOp op;
	BinOp binop;
	UnOp unop;
	Name name;
	NameL *namel;
	Pos pos;
	Expr *e1;
	Expr *e2;
	Expr *e3;
	ExprL *el;
	int64 i;
	double f;
	Type *type;
	Typex *typex;
	Var *var;
	int l1;
	int l2;
	Fn *fn;
	Type *realtype;
	Module *module;
	Name str;
	struct ParsePattern *pattern;
};
Expr*	mkExpr(ExprOp op);

struct GTop;

// Statements
typedef enum StmtOp
{
	StmtExpr = 1,
	StmtPrint,
	StmtBlock,
	StmtWhile,
	StmtDoWhile,
	StmtBreak,
	StmtContinue,
	StmtIf,
	StmtFn,
	StmtReturn,
	StmtImport,
	StmtAssert,
	StmtFor,
	StmtGoto,
	StmtLabel,
	StmtForIn,
	StmtTypedef,
	StmtSwitch,
	StmtCase,
	StmtStruct,
	StmtGrammar,
	StmtAttrDecl,
	StmtType,
} StmtOp;
struct Stmt
{
	StmtOp op;
	Pos pos;
	Expr *e1;
	Expr *e2;
	Expr *e3;
	Stmt *s1;
	Stmt *s2;
	StmtL *stmtl;
	int lcontinue;
	int lbreak;
	int label;
	int lfalse;
	int broke;
	int continued;
	Fn *fn;
	Name name;
	Typex *typex;
	StructelemL *sel;
	int structinit;
	Name xflag;
	NameL *namel;
	struct GTop *gtop;
};
Stmt*	mkStmt(StmtOp op);

// Struct element
struct Structelem
{
	Pos pos;
	int tag;
	Name name;
	Typex *typex;
	StructelemL *sel;
};
Structelem *mkStructelem(void);

struct GTop;
struct GStmt;
struct GStmtL;
struct GRule;
struct GRuleL;
struct GTerm;
struct GTermL;
struct ParsePattern;
struct PatToken;
struct PatTokenL;

// Parse stack type.
struct Yystype
{
	int64 i;
	double f;
	Name name;
	int len;
	Expr *expr;
	ExprL *exprl;
	Stmt *stmt;
	Typex *typex;
	TypexL *typexl;
	StmtL *stmtl;
	FnargL *fnargl;
	Fnarg *fnarg;
	Structelem *structelem;
	StructelemL *structeleml;
	NameL *namel;
	struct GTop *gtop;
	struct GStmt *gstmt;
	struct GStmtL *gstmtl;
	struct GRule *grule;
	struct GRuleL *grulel;
	struct GTerm *gterm;
	struct GTermL *gterml;
	struct ParsePattern *parsepattern;
	struct PatToken *pattoken;
	struct PatTokenL *pattokenl;
	int flags;
};

// Functions
struct Fn
{
	Name name;
	FnargL *args;
	Type *returntype;
	Typex *xreturntype;
	StmtL *body;
	Pos pos;
	Frame *argframe;
	Frame *localframe;
	GType *type;
	Var *var;
	Fn *outer;
	int nescape;
	VarL *stolen;
	int extensible;
	int extend;
	int attribute;
	Var *vdefault;
	Expr *extender;
};
Fn*	mkFn(Name, FnargL*, Typex*, StmtL*);

// Function argument
struct Fnarg
{
	Pos pos;
	Name name;
	Typex *typex;
	Var *var;
};
Fnarg*	mkFnarg(Name, Typex*);


// Input reader.
void	pushinputfile(char*);
void	pushinputstring(char*);
int	yylex(Yystype*, Pos*);
Name	fullpath(char*);

extern int ingrammar;
extern int intermname;
extern int inbacktick;

// List of input files read.
struct Include
{
	Name file;
	uint32 mtime;
	uint32 size;	
};
extern IncludeL*	includel;

// Parse constants.
char*		cescapestring(char *s, int len, int q);
char*		cunescapestring(char *s, int len);
int		parsechar(char *s, uint64 *value, char **end);
int		parsefloat(char *s, long double *value);
int		parseinteger(char *s, uint64 *value);

// Parser
int	yyparse(void);
GType*	parsetype(char*);
void	topstmt(Stmt*);
extern Pos	yylastpos;	// Pos of most recent yacc action


//////////////////////////////////////////////////////////////////////
//
// Type checking; more generally, semantic analysis

// A variable: has a name, a type, and a cname (used in C).
struct Var
{
	Name name;
	Name cname;
	GType *type;
	Scope *scope;	// scope containing variable
	Pos pos;
	int escape;
	Var *redirect;	// use this one instead
	Module *module;	// when type == typemodule()
	Type *realtype;	// when type == typetype()
	int flags;
};

// Variables currently visible.
struct Scope
{
	Scope *outer;
	VarL *varl;
	Frame *frame;
};

// Variables in an allocation frame (e.g.,
// all the top-level variables, or all the function
// arguments, or all the stack-allocated variables
// in this function.)
struct Frame
{
	Name prefix;
	VarL *varl;
	Fn *fn;		// function containing frame
};

// Type-checking state.
struct Typecheck
{
	int reachable;		// next statement is reachable
	Scope *scope;		// current variable scope
	StmtL *jumpl;	// all gotos and labels
	int nlabel;		// labels used so far
	StmtL *breakl;		// stack of break-able statements
	StmtL *continuel;	// stack of continue-able statements
	Fn *fn;			// current function
	FnL *fnl;		// stack of fns containing fn.
};

// Expression contexts
enum
{
	Value = 1<<0,		// Expr being used for its value
	LValue = 1<<1,		// Expr being used for its lvalue
	BindValue = 1<<2,	// Expr must be a name
};

// Start/end new variable scope.
Scope*	pushscope(Typecheck*);
void	popscope(Typecheck*, Scope*);

// Copy an existing scope.
Scope*	copyscope(Scope*);

// S1 and s2 were copied from scope
// and then changed.  Merge common changes into scope.
void	mergescopes(Scope *s1, Scope *s2, Scope *scope);


// Look for, declare a variable in a scope.
Var*	lookupvar(Scope *scope, Name name);
VarL*	lookupvarl(Scope *scope, Name name);
Var*	declarevar(Pos pos, Scope *scope, Name name, Type *type);
Var*	declarevarmany(Pos pos, Scope *scope, Name name, Type *type);
Var*	mlookupvar(Module *m, Name name);
Var*	lookupattribute(Scope*, Name, Type*);
Var*	_declarevar(Pos, Scope*, Name, Type*, int, Name);

Name	attributename(Name, Type*);
Type*	attributeargtype(Type*);

// Start/end new variable frame.
// Also starts/end new scope for the new frame.
Frame*	pushframe(Typecheck*);
void	popframe(Typecheck*, Frame*);

// Start/end new function to type check.
void	pushfn(Typecheck*, Fn*);
void	popfn(Typecheck*, Fn*);

// Type check a statement or expression.
void	typecheckinit(void);
void	typecheckstmt(Typecheck*, Stmt*);
int	typecheckexpr(Typecheck*, Expr*, int);
void	typecheckmatch(Typecheck*, Expr*, Type*);
void	typecheckcond(Typecheck*, Expr*, Scope*, Scope*);
int	typecheckfn(Typecheck*, Fn*);
void	typecheckgram(Typecheck*, struct GTop*);
int typecheckpexpr(Typecheck*, Expr*, int);
int typecheckgexpr(Typecheck*, Expr*, int);

Type *typechecktype(Typecheck*, Typex*);

int	typecheckpmatch(Typecheck*, Expr*, Type*);

Type *typedot(Pos, Type*, Name);
Type	*typegramdot(Type*, Name);

void	declaretype(Scope*, Expr*, Type*);
void	redeclare(Scope*, Expr*, Type*);

Expr*	tobool(Typecheck*, Expr*);
void	noassign(Expr*, int);
void	nosideeffect(Expr*, int);

// List of all imported modules
extern ModuleL*	importl;

// Top-level scope in module.
extern Scope* topscope;


///////////////////////////////////////////////////////////////////
//
// C output

typedef struct Cbuf Cbuf;
struct Cbuf
{
	Fmt *fmt;	// default output target for cgenstmt etc.
	Fmt runfmt;	// output inside run() function
	Fmt topfmt;	// output at top level
	Fn *fn;		// function being compiled
};

// %N  ZN(N("string"))
int	namefmt(Fmt*);

void	cbprint(Cbuf*, Pos, char*, ...);
void	cgenstmt(Cbuf*, Stmt*);
void	cgentypes(Cbuf*);
void cgentypes0(Cbuf*);
void	cgenimportdecls(Cbuf*);
void	cgenstructdecls(Cbuf*);
void cgengraminit(Cbuf*);
Name cgenexpr(Cbuf*, Expr*);
Name cbtmp(Cbuf*);
Name cgenpexpr(Cbuf*, Expr*);
void cgenassign(Cbuf *cb, Expr *dst, Name src);
void		cgenpmatch(Cbuf *cb, Expr *expr, Name val, int lfail);
Name	csanitize(Name);

void	writemodule(Name);
extern int	linenumbers;

Name	findinclude(Pos, Name);

