#define _LISTIMPL(X, Z)			\
					\
X##L*					\
mk##X##L(Z hd, X##L *tl)		\
{					\
	X##L *l;			\
					\
	l = emallocnz(sizeof *l);		\
	l->hd = hd;			\
	l->tl = tl;			\
	return l;			\
}					\
					\
X##L*					\
copy##X##L(X##L *l)			\
{					\
	if(l == nil)			\
		return nil;		\
	return mk##X##L(l->hd, copy##X##L(l->tl));	\
}					\
					\
X##L*					\
rev##X##L(X##L *l)			\
{					\
	X##L *next, *prev;		\
					\
	prev = nil;			\
	for(; l; l=next){		\
		next = l->tl;		\
		l->tl = prev;		\
		prev = l;		\
	}				\
	return prev;			\
}					\
					\
void					\
free##X##L(X##L *l)			\
{					\
	if(l == nil)			\
		return;			\
	free##X##L(l->tl);		\
	free(l);			\
}					\
					\
int					\
len##X##L(X##L *l)			\
{					\
	int i;				\
					\
	i = 0;				\
	for(; l; l=l->tl)		\
		i++;			\
	return i;			\
}					\
					\
Z					\
nth##X##L(X##L *l, int i)	\
{					\
	while(i-- > 0)		\
		l = l->tl;		\
	return l->hd;		\
}					\
					\
X##L*				\
add##X##L(X##L *l, X##L *ll) \
{					\
	X##L *l1;			\
	if(l == nil)			\
		return ll;		\
	if(ll == nil)			\
		return l;		\
	l1 = l;			\
	while(l1->tl)		\
		l1 = l1->tl;	\
	l1->tl = ll;			\
	return l;			\
}					\
					\
X##L*				\
sort##X##L(X##L *l, int (*cmp)(typeof(((X##L*)0)->hd), typeof(((X##L*)0)->hd))) \
{					\
	assert(sizeof(l->hd) == sizeof(void*));	\
	return (X##L*)listsort((PtrL*)l, (int(*)(void*, void*))cmp); \
}					\
					\
X##L*				\
uniq##X##L(X##L *l)	\
{					\
	X##L *l1;			\
	l1 = l;			\
	while(l){			\
		while(l->tl && l->hd == l->tl->hd) \
			l->tl = l->tl->tl; \
		l = l->tl;		\
	}				\
	return l1;			\
}					\

#define LISTIMPL(X) _LISTIMPL(X, X*)
