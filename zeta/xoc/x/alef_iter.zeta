// Alef-style iterator.
//
//	a[i=0::10] = 0;
//	a[0::10] = 0;
//
// both turn into
//
//	for(i=0; i<10; i++)
//		a[i] = 0;

from xoc import *;

extend grammar C99
{
	%right AlefIter;
	%priority Shift > AlefIter > Relational;

	expr: expr "::" expr [AlefIter];
}

struct Iterator
{
	var: C.expr;
	lo: C.expr;
	hi: C.expr;
};

extend attribute
type(term: C.expr): Type
{
	switch(term){
	case `{\a=\b ::\c}:
		if(a.type.isinteger
		&& b.type.isinteger
		&& c.type.isinteger)
			return a.type;
		errorast(term, "non-integer iterator");
	case `{\b ::\c}:
		//print b;
		if(b.type.isinteger
		&& c.type.isinteger)
			return `Type{int};
		errorast(term, "non-integer iterator");
	}
	return default(term);
}

attribute
iterator(term: C.expr): Iterator
{
	switch(term){
	case `{\a=\b ::\c}:
		return Iterator(a, b, c);
	case `{\b ::\c}:
		sym := Sym.Var("i", term, `Type{int}, ...);
		sym.islocal = true;
		return Iterator(`C.expr{\sym}, b, c);
	}
	return nil;
}

attribute
iterators(term: C): list Iterator
{
	k := astsplit(term);
	out: list Iterator;
	for(i:=length(k)-1; i>=0; i--)
		out = k[i].iterators + out;
	if(term <: C.expr && term.iterator)
		out = term.iterator :: out;
	return out;
}

attribute
without_iterators(term: C): C
{
	if(term.iterators == nil)
		return term;
	if(term <: C.expr && term.iterator)
		return term.iterator.var;
	k := astsplit(term);
	for(i:=0; i<length(k); i++)
		k[i] = k[i].without_iterators;
	term = astjoin(term, k);
	return term;
}

extend attribute
compiled(term: C.stmt): COutput.stmt
{
	if(term.iterators == nil)
		return default(term);

	body1 := term.without_iterators;
	assert body1 <: C.stmt;
	body := body1;
	for(i in term.iterators)
		body = `C.stmt{
			for(\(i.var) = \(i.lo); \(i.var) < \(i.hi); \(i.var)++)
				\body
		};
	return body.compiled;
}
