import sparse_grammar;
from xoc import *;
import sys;
import util;

attribute
address_space(t: C.type): int
{
	for(a in t.attrs){
		if(a ~ `C.attr{address_space(\e)}){
			if(e.constvalue ~ Constant.Integer(i))
				return i;
			errorast(t, "address space is not constant integer");
			return 0;
		}
	}
	return 0;
}

attribute
has_address_space(t: C.type): bool
{
	for(a in t.attrs)
		if(a ~ `C.attr{address_space(\_)})
			return true;
	return false;
}

fn
no_address_space(a: C.attr): bool
{
	return a !~ `{address_space(\_)};
}

attribute
without_address_space(t: C.type): C.type
{
	tt := astcopy(t);
	tt.attrs = util.filter(no_address_space, t.attrs);
	return tt;
}

extend fn
canconvert(old: C.type, new: C.type): bool
{
	if(old ~ `{\told*} && new ~ `{\tnew*}){
		if(told.address_space != tnew.address_space){
			sys.werrstr("address space mismatch");
			return false;
		}
		if(told.has_address_space)
			return default(`C.type{\(told.without_address_space)*}, new);
	}
	return default(old, new);
}

extend fn
cancast(old: C.type, new: C.type): C.type
{
	if(old ~ `{\told*} && new ~ `{\tnew*}){
		if(told.address_space != tnew.address_space){
			sys.werrstr("address space mismatch");
			return nil;
		}
	}
	return default(old, new);
}

