// Alef channel operations

from xoc import *;
import alef_chan;

extend grammar C99
{
	stmt: "alloc" expr[,]+ ";";
	expr: "<-" expr
	    | expr "<-=" expr;
}

// Type check channel allocation statement.
extend attribute
typecheck(term: C.stmt)
{
	if(term ~ `{alloc\exprs;}){
		for(e in exprs)
			if(e.type && e.type !~ `{chan(\_)})
				errorast(term, "alloc of non-channel");
		return;
	}
	default(term);
}



extend attribute
type(term: C.expr): C.type
{
	if(
	switch(term){
	case `{<-\e}:
		if(e.type){
			if(e.type ~ `{chan(\t)})
				return t;
			errorast(term, "recv from non-channel");
		}
		return nil;

	case `{\e <-= \v}:
		if(e.type && v.type){
			if(e.type ~ `{chan(\t)}){
				if(canconvert(v.type, t))
					return t;
				errorast(term, "cannot send "+string(v.type)+" on chan of "+string(t));
			}
			errorast(term, "send on non-channel");
		}
		return nil;
	}

	return default(term);
}

