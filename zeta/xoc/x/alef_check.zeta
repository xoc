// Alef-style check expression.
//
//	check expr;
//	check expr, string;
//
// turn into
//
//	if(!expr) ALEF_check("file:line", "string");

grammar XAlefCheck extends C99
{
	stmt:	"check" expr ";"
	|	"check" expr "," string ";"
	;
}

fn
rewrite(expr: ptr C.expr, str: ptr C.string): ptr C.stmt
{
	where := expr.line.file + ":" + string(expr.line.line);
	check := `C.expr{ALEF_check}@globalctx;
	return `C.stmt{if(!\expr) \check(\where, \str);};
}

extend attribute
forward(term: ptr C.stmt): ptr C.stmt
{
	switch(term){
	case ~stmt{check \e;}:
		return rewrite(e, `C.string{\(string(e))});
	case ~stmt{check \e, \s;}:
		return rewrite(e, s);
	}
	return default(term);
}

