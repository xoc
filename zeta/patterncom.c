#include "a.h"
#include "type.h"
#include "runtime.h"
#include "grammar.h"
#include "compile.h"
#include "grammarcom.h"
#include "patterncom.h"
#include "listimpl.h"

int patterndebug;

typedef struct SlotChoice SlotChoice;

LIST(RgTokenL)
LIST(SlotChoice)
LIST(SlotChoiceL)

typedef enum SlotConvert {
	ConvertDirect = 1,
	ConvertToQuestion,
	ConvertListToStar,
	ConvertListToPlus,
	ConvertExternal,
} SlotConvert;

struct SlotChoice
{
	CfgSym *sym;
	Type *type;
	SlotConvert cvt;
	Var *var;
};


static void patternmerge(CfgSym*, ParseValue*, ParseValue*);
static RgTokenL *lexlist(RgParse*, CfgGram*, void*);
static int parsepattern(Typecheck*, ParsePattern*, Type*, int);
static int isambiguous(Ast*);
static Ast* disambiguate(Ast*, int);

static Hash *astchoices;

int
typecheckpexpr(Typecheck *tc, Expr *expr, int kind)
{
	if(expr->op != ExprBacktick && expr->op != ExprBacktick2)
		panic("typecheckpexpr");

	noassign(expr, kind);
	nosideeffect(expr, kind);

	if(parsepattern(tc, expr->pattern, nil, expr->op==ExprBacktick) < 0)
		return -1;
	expr->type = expr->pattern->type;
	return 0;
}

int
typecheckpmatch(Typecheck *tc, Expr *expr, Type *matchtype)
{
	if(expr->op != ExprBacktick && expr->op != ExprBacktick2)
		panic("typecheckpmatch");
	if(parsepattern(tc, expr->pattern, matchtype, expr->op==ExprBacktick) < 0)
		return -1;
	expr->realtype = expr->pattern->type;
	return 0;
}

static SlotChoiceL*slotchoices(Typecheck*, Type *tg, int destructure, Type *type, Name symname);
static SlotChoiceL*filterchoices(SlotChoiceL *l, CfgSym *sym);

static AstL *findslots(Ast*);

// Parse and type-check the parsing pattern p.
// This is one of the most complicated, subtle, and important
// functions in zeta.

extern Type *parseactiontype;

static int
parsepattern(Typecheck *tc, ParsePattern *p, Type *matchtype, int canonical)
{
	Ast *ast;
	AstL *al;
	CfgSym *start;
	CfgSym *sym;
	RgParse *glrp;
	RgToken *gt;
	RgTokenL *gl;
	RgTokenLL *gll;
	Gram *gram;
	ParseValue *pv;
	PatToken *pt;
	PatTokenL *l;
	SlotChoice *c;
	SlotChoiceL *choicel, *cl;
	SlotChoiceLL *cll;
	Type *t;
	char *s;
	int nslot;

	// Figure out the type of the pattern.
	if(matchtype){
		if(matchtype->op != TypeGram){
			error(p->pos, "cannot match parse pattern against %T", matchtype);
			return -1;
		}
		// This is inside a match expression.  Matchtype contains the
		// context type, and the source can contain an abbreviation relative
		// to that type, like `{} or `expr{} instead of `C.expr{}.
		if(p->typex->op == TypexEmpty)
			p->type = matchtype;
		else if(p->typex->op == TypexName && (t = typegramdot(matchtype, p->typex->name)) != nil)
			p->type = t;
		else if((p->type = typechecktype(tc, p->typex)) == nil)
			return -1;
	}else{
		// This is a restructuring; it must contain the full type.
		if((p->type = typechecktype(tc, p->typex)) == nil)
			return -1;
	}
	
	if(p->type->op != TypeGram){
		error(p->pos, "non-grammar type %T in parsing pattern", p->type);
		return -1;
	}
	if(p->type->sym == nil){
		error(p->pos, "can't use %T in pattern - need symbol", p->type);
		return -1;
	}

	gram = p->type->gram;
	start = p->type->sym;
	
	// Lex the pattern to create a token list.
	// Lexing is complicated enough that we just batch it all right here
	// rather than try to shoehorn it into a GLR callback function.
	// Then the GLR callback "lexer" actually just pulls responses off
	// a list one by one.  
	//
	// Each GLR lex response is a list of GLR token (a RgTokenL), so the
	// big list of all the lex responses is a list of RgTokenL (a RgTokenLL).
	//
	// As usual, the RgTokenLL gets built up in reverse for linear runtime
	// and then reversed at the end.

	glrp = rgnewparse(gram->cfg);
	if(p->tokenl)
		glrp->line = pos2fileline(p->tokenl->hd->pos, &s);
	else
		glrp->line = pos2fileline(p->pos, &s);
	glrp->file = nameof(s);
	glrp->col = 1;
	glrp->merge = patternmerge;

	gll = nil;
	cll = nil;
	nslot = 0;
	for(l=p->tokenl; l; l=l->tl){
		pt = l->hd;
		switch(pt->op){
		case PatTokenText:
			// Parse the text, adding each response from lextext to gll.
			s = nametostr(pt->text);
			for(;;){
				gl = lextext(glrp, gram->cfg, &s);
				if(gl == nil){
					error(p->pos, "error lexing at: %.10s", s);
					return -1;
				}
				if(gl->hd->sym == gram->cfg->eofsym && s == nil)
					break;
				gll = mkRgTokenLL(gl, gll);
			}
			break;
		case PatTokenSlot:
			if(matchtype){
				// Destructuring
				if(pt->expr && typecheckexpr(tc, pt->expr, LValue) < 0)
					return -1;
			}else{
				// Restructuring
				// \x means \(x).
				if(pt->expr == nil){
					pt->expr = mkExpr(ExprName);
					pt->expr->pos = pt->pos;
					pt->expr->name = pt->text;
				}
				if(typecheckexpr(tc, pt->expr, Value) < 0)
					return -1;
			}

			// Make list of possible type choices compatible
			// with pt->expr->type and pt->symname.
			choicel = slotchoices(tc, p->type, matchtype != nil,
				pt->expr ? pt->expr->type : nil, pt->symname);
			if(choicel == nil){
				error(pt->pos, "cannot use \\(%T)::%s in pattern",
					pt->expr ? pt->expr->type : nil, pt->symname);
				return -1;
			}

			// Make list of slots for lexer to return.
			// Only one will eventually be used, but we don't know which.
			gl = nil;
			sym = nil;
			for(cl=choicel; cl; cl=cl->tl){
				c = cl->hd;
				// There can be multiple ways to get a particular
				// symbol.  Don't confuse disambiguation by using
				// a symbol multiple times.
				if(c->sym == sym)
					continue;
				sym = c->sym;
				ast = mkastslot(p->type, sym, nslot);
				pv = mkParseValue(ast);
				pv->line.line = pos2fileline(pt->pos, &s);
				pv->line.file = nameof(s);
				gt = rgnewtoken(glrp, sym);
				gt->val = pv;
				gl = mkRgTokenL(gt, gl);
			}
			gl = revRgTokenL(gl);
			gll = mkRgTokenLL(gl, gll);
			cll = mkSlotChoiceLL(choicel, cll);
			nslot++;
			break;
		}
	}
	gll = revRgTokenLL(gll);
	cll = revSlotChoiceLL(cll);
	p->nslot = nslot;

	// Great, we have a token list.  Now we can run the GLR parser
	// to actually parse the pattern.
//fprint(2, "--parse %#P\n", p->pos);
	parseactiontype = typegramsym(p->type, nil, nil);
	pv = rgparse(gram->cfg, start, glrp, lexlist, &gll);
	parseactiontype = nil;
	if(pv == nil){
		error(p->pos, "cannot parse pattern as %T",
			p->type);
		return -1;
	}
	
	// Disambiguate.
	ast = pv->ast;
	ast = disambiguate(ast, nslot);
	if(isambiguous(ast)){
		error(p->pos, "couldn't parse pattern as %s: ambiguous\n%l#A",
			start->name, ast);
		return -1;
	}

	if(canonical && matchtype)
		ast = astcanonicalize(ast);
	p->ast = ast;

	if(patterndebug){
		fprint(2, "PATTERN: %#P\n", p->pos);
		fprint(2, "\t%#A\n", p->ast);
	}

	// Select appropriate types based on slot symbols.
	l = p->tokenl;
	for(al=findslots(p->ast); al; al=al->tl, cll=cll->tl, l=l->tl){
		ast = al->hd;
		choicel = filterchoices(cll->hd, ast->sym);
		if(choicel == nil)
			panic("%L: no choices for %#A", p->pos, ast);
		c = choicel->hd;
		while(l->hd->op != PatTokenSlot)
			l = l->tl;
		pt = l->hd;
		
		if(matchtype && pt->expr == nil){	// Destructuring \x
			// Declare match variable using type of first choice.
			pt->expr = mkExpr(ExprName);
			pt->expr->pos = pt->pos;
			pt->expr->name = pt->text;
			typecheckmatch(tc, pt->expr, c->type);
			choicel->tl = nil;
		}
		
		// The combination of pt->expr->type and the chosen symbol
		// should have gotten us down to a single choice for the slot.
		if(choicel->tl != nil)
			choicel->tl = nil;

		// Remember this choice for later.
		if(astchoices == nil)
			astchoices = mkhash();
		hashput(astchoices, ast, c);
		if(patterndebug)
			fprint(2, "\tslot %d %s -> %T\n", ast->slotnum, c->sym->name, c->type);
	}
	
	return 0;
}

static SlotChoice*
mkSlotChoice(CfgSym *sym, Type *type, SlotConvert cvt)
{
	SlotChoice *c;
	
	c = emalloc(sizeof *c);
	c->sym = sym;
	c->type = type;
	c->cvt = cvt;
	return c;
}

// Return a list of choices for this slot.
//	tc is the current typechecking context
//	tg is the grammar type for the pattern as a whole
//	destructure says whether this is a destructuring or restructuring expr
//	type is the type of the slot variable, if known
//	symname is the annotation name on the slot, if any
static SlotChoiceL*
slotchoices(Typecheck *tc, Type *tg, int destructure, Type *type, Name symname)
{
	int i;
	CfgSym *sym, *onesym;
	SlotChoiceL *cl, *cl1;
	SlotChoice *c;
	char *s;
	char *sname;
	Type *t, *tin, *tout;
	Name prefix, fn;
	VarL *vl;
	Var *v;
	Gram *g;
	Type *tgs;
	
	g = tg->gram;
	tg = typegramsym(tg, nil, nil);
	assert(tg);

	if(type == nil && !destructure)
		panic("slotchoices restructure without type");
	
	// Create list of all converters, ignoring the type constraint for now.
	cl = nil;
	for(i=0; i<g->cfg->nsym; i++){
		// See what we can do with a slot using grammar symbol sym.
		sym = g->cfg->sym[i];
		sname = nametostr(sym->name);

		// That would make the AST have type tgs, before any conversions.
		tgs = typegramsym(tg, sym->name, sym);

		// Used by CFG implementation
		if(sym->name == N("EOF") || sname[0] == '$')
			continue;

		// Ignore literals and regular expressions
		if(sname[0] == '"' || sname[0] == '/')
			continue;
		
		// If there's an annotation, it has to match.
		if(symname && sym->name != symname)
			continue;

		if(!symname){
			vl = lookupvarl(tc->scope, nameprint("nodestructure(%s)", sym->name));
			if(vl)
				continue;
		}

		// First choice is external converters.
		fn = nameprint("%s(%s)",
			destructure ? "destructure" : "restructure",
			sym->name);
		vl = lookupvarl(tc->scope, fn);
		for(; vl; vl=vl->tl){
			v = vl->hd;
			if(destructure){
				// Converter accepts tin, will return (bool, tout).
				tin = v->type->left->typel->hd;
				tout = v->type->left->left->typel->tl->hd;

				// Converter must accept tgs, generate t.
				if(!canunify(tin, tgs, 1)){
//fprint(2, "Reject destructurer %T: %T <: %T\n", v->type->left, tgs, v->type->left->typel->hd);
					continue;
				}
				t = tout;
			}
			else{
				// Converter accepts tin, will return tout.
				tin = v->type->left->typel->hd;
				tout = v->type->left->left;
				
				// Converter must accept t, generate tgs.
				t = tin;
				if(!canunify(tgs, tout, 1)){
//fprint(2, "Reject restructurer %T: %T <: %T\n", v->type->left, v->type->left->left, tgs);
					continue;
				}
			}
			
			// Slot has type t, but grammar symbol sym.
			c = mkSlotChoice(sym, t, ConvertExternal);
			c->var = v;
			cl = mkSlotChoiceL(c, cl);
		}

		s = nametostr(sym->name);
		switch(s[strlen(s)-1]){
		case '?':
			// Can use possibly-nil "Ast.name" as "Ast.name?" (not a question)
			prefix = nameofn(s, strlen(s)-1);
			c = mkSlotChoice(sym, typegramsym(tg, prefix, nil),
				ConvertToQuestion);
			cl = mkSlotChoiceL(c, cl);
			break;
		case '*':
			// Can use "list Ast.name" as "Ast.name*"
			onesym = gramunlistsym(g, sym);
			c = mkSlotChoice(sym, typelist(typegramsym(tg, onesym->name, nil)),
				ConvertListToStar);
			cl = mkSlotChoiceL(c, cl);
			// Could add here: Ast.name+ => Ast.name*
			break;
		case '+':
			// Can use "list Ast.name" as "Ast.name+"
			onesym = gramunlistsym(g, sym);
			c = mkSlotChoice(sym, typelist(typegramsym(tg, onesym->name, nil)),
				ConvertListToPlus);
			cl = mkSlotChoiceL(c, cl);
			// Could add here: Ast.name* => Ast.name+
			break;
		}

		// Last resort: use Ast.sym directly
		c = mkSlotChoice(sym, typegramsym(tg, sym->name, nil),
			ConvertDirect);
		cl = mkSlotChoiceL(c, cl);
	}
	cl = revSlotChoiceL(cl);

	// Apply type constraint.
	if(type){
		cl1 = nil;
		for(; cl; cl=cl->tl){
			c = cl->hd;
			if(destructure){
				// Check that can assign c->type to type.
				if(!canunify(type, c->type, 1))
					continue;
			}else{
				// Check that can assign type to c->type.
				// XXX Or should this be a dynamic check?
				if(!canunify(c->type, type, 1))
					continue;
			}
			cl1 = mkSlotChoiceL(c, cl1);
		}
		cl = revSlotChoiceL(cl1);
	}
	
	return cl;
}

static SlotChoiceL*
filterchoices(SlotChoiceL *l, CfgSym *sym)
{
	SlotChoiceL *f;

	if(l == nil)
		return nil;
	f = filterchoices(l->tl, sym);
	if(l->hd->sym == sym)
		f = mkSlotChoiceL(l->hd, f);
	return f;
}

static void
patternmerge(CfgSym *sym, ParseValue *a, ParseValue *b)
{
	// We could use a more aggressive merge function here
	// rather than save the trimming for after the parse,
	// but the original implementation in tinyrscc does it this way.
	// TODO:
	// (It has code to do it the aggressive way, but that is 
	// commented out, and I have no idea why.  It is certainly
	// worth trying to figure this out at some point in the future.
	// It might have to do with the first pass of disambiguate 
	// below.)
	glrmerge(sym, a, b);
}

static RgTokenL*
lexlist(RgParse *parse, CfgGram *cfg, void *v)
{
	RgTokenLL **lp, *l;
	RgTokenL *out, *tl;
	char *empty;
	
	lp = v;
	if(*lp == nil){
		// EOF
//fprint(2, "eof\n");
		empty = "";
		return lextext(parse, cfg, &empty);
	}
	
	// Pop off list.
	l = *lp;
	out = l->hd;
	*lp = l->tl;
	free(l);
	
	// If this is a slot, the type might have been refined since lexing.
	// Update the RgToken.
	for(tl=out; tl; tl=tl->tl)
		tl->hd->sym = tl->hd->val->ast->sym;

//fprint(2, "%s\n", out->hd->sym->name);
	return out;
}

enum { Incomparable = 1000 };

static void
_slotdepth(Ast *ast, int *slotd, int d)
{
	int i;

	if(ast == nil)
		return;
	switch(ast->op){
	case AstSlot:
		slotd[ast->slotnum] = d;
	case AstRule:
		for(i=0; i<ast->nright; i++)
			_slotdepth(ast->right[i], slotd, d+1);
		break;
	case AstMerge:
		for(i=0; i<ast->nright; i++)
			_slotdepth(ast->right[i], slotd, d);
		break;
	default:
		break;
	}
}

static void
slotdepth(Ast *ast, int *slotd, int nslot)
{
	int i;
	
	for(i=0; i<nslot; i++)
		slotd[i] = 1000000;
	_slotdepth(ast, slotd, 0);
}

static int
depthcmp(int *sa, int *sb, int nslot)
{
	int i, ret;
	
	ret = 0;
	for(i=0; i<nslot; i++){
		if(sa[i] == sb[i])
			continue;
		if(sa[i] < sb[i]){
			if(ret <= 0)
				ret = -1;
			else
				ret = Incomparable;
		}
		if(sa[i] > sb[i]){
			if(ret >= 0)
				ret = 1;
			else
				ret = Incomparable;
		}
	}
	return ret;
}
			

static Ast*
disambiguate(Ast *ast, int nslot)
{
	int *sa, *sb;
	Ast *a;
	Ast **right;
	int i, j, nright;
	
	if(ast == nil)
		return nil;
	
	switch(ast->op){
	default:
		break;
	
	case AstRule:
		for(i=0; i<ast->nright; i++){
			a = astdup(ast->right[i]);
			ast->right[i] = disambiguate(a, nslot);
			astfree(a);
		}
		break;
	
	case AstMerge:
		// Disambiguate the children.
		for(i=0; i<ast->nright; i++){
			a = astdup(ast->right[i]);
			ast->right[i] = disambiguate(a, nslot);
			astfree(a);
		}
		
		// Copy the child pointers for later - much easier than getting the
		// ref count manipulations right.
		right = emalloc(ast->nright*sizeof right[0]);
		nright = ast->nright;
		
		// Now choose the alternative that places the slots highest in the tree.
		sa = emalloc(nslot*sizeof sa[0]);
		sb = emalloc(nslot*sizeof sb[0]);
		for(i=0; i<ast->nright; i++){
			slotdepth(ast->right[i], sa, nslot);
			for(j=0; j<i; j++){
				if(ast->right[j] == nil)
					continue;
				slotdepth(ast->right[j], sb, nslot);
				switch(depthcmp(sa, sb, nslot)){
				case -1:	// right[i] is better
					ast->right[j] = nil;
					break;
				case 0:	// neither is better
				case Incomparable:
					break;
				case 1:	// right[j] is better
					ast->right[i] = nil;
					goto break2;
				}
			}
		break2:;
		}

		ast->nright = 0;
		for(i=0; i<nright; i++)
			if(ast->right[i])
				ast->right[ast->nright++] = ast->right[i];
		assert(ast->nright > 0);

		// Now that we've culled down, fix the ref counts.
		for(i=0; i<ast->nright; i++)
			astdup(ast->right[i]);
		for(i=0; i<nright; i++)
			astfree(right[i]);
		free(right);

		if(ast->nright == 1)
			return astdup(ast->right[0]);

/*
fprint(2, "ambiguous:\n");
for(i=0; i<ast->nright; i++){
	slotdepth(ast->right[i], sa, nslot);
	for(j=0; j<nslot; j++)
		fprint(2, " %d", sa[j]);
	fprint(2, " %#A\n", ast->right[i]);
}
*/

		break;
	}
	return ast;
}

static int
isambiguous(Ast *ast)
{
	int i;

	if(ast == nil)
		return 0;
	switch(ast->op){
	default:
		return 0;
	case AstRule:
		for(i=0; i<ast->nright; i++)
			if(isambiguous(ast->right[i]))
				return 1;
		return 0;
	case AstMerge:
		return 1;
	}
}

// Return a list of all the slots, in the right order.
static void _findslots(Ast*, AstL**);
static AstL*
findslots(Ast *a)
{
	AstL *l;

	l = nil;
	_findslots(a, &l);
	return revAstL(l);
}

static void
_findslots(Ast *a, AstL **l)
{
	int i;

	switch(a->op){
	case AstString:
		return;
	case AstMerge:
		panic("merge in findslots");
	case AstRule:
		for(i=0; i<a->nright; i++)
			_findslots(a->right[i], l);
		break;
	case AstSlot:
		if(a->slotnum != lenAstL(*l))
			panic("bad slotnum %d != %d in findslots", a->slotnum, lenAstL(*l));
		*l = mkAstL(a, *l);
		break;
	}
}


static Name
cgenmkast(Cbuf *cb, Pos pos, Ast *ast, ExprL *el)
{
	int i;
	SlotChoice *c;
	Name right;
	Name n, n1, tmp;
	Expr *e;

	switch(ast->op){
	case AstMerge:
		break;
	case AstSlot:
		e = nthExprL(el, ast->slotnum);
		n = cbtmp(cb);
		cbprint(cb, pos, "%hT %s = %s;", e->type, n, cgenexpr(cb, e));
		tmp = cbtmp(cb);
		cbprint(cb, pos, "Ast *%s;", tmp);
		c = hashget(astchoices, ast);
		if(c == nil)
			panic("cgenmatchast %#A", ast);
		switch(c->cvt){
		case ConvertDirect:
			cbprint(cb, pos, "%s = %s;", tmp, n);
			break;
		case ConvertToQuestion:
			cbprint(cb, pos, "%s = asttoquestion(%s, GS(%N));",
				tmp, n, ast->sym->name);
			break;
		case ConvertListToStar:
			cbprint(cb, pos, "%s = astlisttostar(%s, GS(%N));",
				tmp, n, ast->sym->name);
			break;
		case ConvertListToPlus:
			cbprint(cb, pos, "%s = astlisttoplus(%s, GS(%N));",
				tmp, n, ast->sym->name);
			break;
		case ConvertExternal:
			n1 = cbtmp(cb);
			cbprint(cb, pos, "Zfn *%s = %s;", n1, c->var->cname);
			cbprint(cb, pos, "%s = ((Ast*(*)(void*, Zpoly))%s->fn)(%s->escf, P(%s));", tmp, n1, n1, n);
			break;
		}
		cbprint(cb, pos, "if(%s == 0) "
			"panic(\"%%s: conversion failed in restructure\","
			"%N);",
			tmp, nameprint("%P", pos));
		cbprint(cb, pos, "if(%s->sym->name != %N) "
			"panic(\"%%s: bad type %%s != %%s in restructure\","
			"%N, %s->sym->name, %N);",
			tmp, ast->sym->name,
			nameprint("%P", pos),
			tmp, ast->sym->name);
		return tmp;
	case AstString:
		return nameprint("mkaststring(GS(%N), %N)",
			ast->sym->name, ast->text);
	case AstRule:
		right = cbtmp(cb);
		cbprint(cb, pos, "Ast **%s = emalloc(%d*sizeof %s[0]);",
			right, ast->nright, right);
		for(i=0; i<ast->nright; i++)
			cbprint(cb, pos, "%s[%d] = %s;", right, i,
				cgenmkast(cb, pos, ast->right[i], el));
		return nameprint("mkastrule(GR(%N), %s)",
			ast->rule->fmtname, right);
	}
	panic("cgenast");
	return nil;
}

Name
cgenpexpr(Cbuf *cb, Expr *expr)
{
	Name nval;
	PatTokenL *l;
	PatToken *pt;
	ExprL *el;

	if(expr->op != ExprBacktick && expr->op != ExprBacktick2)
		panic("cgenpexpr");

	el = nil;
	for(l=expr->pattern->tokenl; l; l=l->tl){
		pt = l->hd;
		if(pt->op == PatTokenSlot)
			el = mkExprL(pt->expr, el);
	}
	el = revExprL(el);
	nval = cbtmp(cb);
	cbprint(cb, expr->pos, "Ast *%s;", nval);
	cbprint(cb, expr->pos, "{ Type *__tgram__ = %#T;",
		expr->pattern->type);
	cbprint(cb, expr->pos, "%s = %s;",
		nval, cgenmkast(cb, expr->pos, expr->pattern->ast, el));
	cbprint(cb, expr->pos, "}");
	if(expr->op == ExprBacktick)
		cbprint(cb, expr->pos, "%s = astcanonicalize(%s);", nval, nval);
	cbprint(cb, expr->pos, "%s = astleak(%s);", nval, nval);
	return nval;
}

static void
cgenmatchast(Cbuf *cb, Pos pos, Ast *ast, Name val, ExprL *slotl, int lfail)
{
	int i;
	Expr *e;
	Name n, n1, n2, tmp;
	SlotChoice *c;

	// Make a copy of the value.
	tmp = cbtmp(cb);
	cbprint(cb, pos, "Ast *%s = %s;", tmp, val);
	val = tmp;
	
	// The type system is supposed to make sure the symbols match.
	cbprint(cb, pos, "if(%s->sym->name != %N) panic(\"%%s matchast\", %N);",
		val, ast->sym->name, nameprint("%P", pos));
	switch(ast->op){
	case AstMerge:
		panic("merge in cgenmatchast");
	case AstSlot:
		c = hashget(astchoices, ast);
		e = nthExprL(slotl, ast->slotnum);
		if(c == nil)
			panic("cgenmatchast %#A", ast);
		switch(c->cvt){
		case ConvertDirect:
			break;
		case ConvertToQuestion:
			val = nameprint("astfromquestion(%s, GS(%N))", val, ast->sym->name);
			break;
		case ConvertListToStar:
			cbprint(cb, pos, "if(astslotted(%s)) goto L%d;", val, lfail);
			n1 = cbtmp(cb);
			cbprint(cb, pos, "Zlist *%s;", n1);
			cbprint(cb, pos, "if(aststartolist(%s, GS(%N), &%s) < 0) goto L%d;",
				val, ast->sym->name, n1, lfail);
			val = n1;
			break;
		case ConvertListToPlus:
			cbprint(cb, pos, "if(astslotted(%s)) goto L%d;", val, lfail);
			n1 = cbtmp(cb);
			cbprint(cb, pos, "Zlist *%s;", n1);
			cbprint(cb, pos, "if(astplustolist(%s, GS(%N), &%s) < 0) goto L%d;",
				val, ast->sym->name, n1, lfail);
			val = n1;
			break;
		case ConvertExternal:
			n1 = cbtmp(cb);
			n2 = cbtmp(cb);
			cbprint(cb, pos, "Zfn *%s = %s;", n1, c->var->cname);
			cbprint(cb, pos, "Zpoly *%s = ((Zpoly*(*)(void*, Ast*))%s->fn)(%s->escf, %s);", n2, n1, n1, val);
			cbprint(cb, pos, "if(!%s[0]) goto L%d;", n2, lfail);
			val = nameprint("(%hT)(%s[1])", e->type, n2);
			break;
		}
		cgenassign(cb, e, val);
		break;
	case AstString:
		cbprint(cb, pos, "if(%s->op != AstString) goto L%d;", val, lfail);
		cbprint(cb, pos, "if(%s->text != %N) goto L%d;", val, ast->text, lfail);
		break;
	case AstRule:
		cbprint(cb, pos, "if(%s->op != AstRule) goto L%d;", val, lfail);
		cbprint(cb, pos, "if(%s->rule->fmtname != %N) goto L%d;", val, ast->rule->fmtname, lfail);
		for(i=0; i<ast->nright; i++){
			n = cbtmp(cb);
			cbprint(cb, pos, "Ast *%s = astright(%s, %d);", n, val, i);
			cgenmatchast(cb, pos, ast->right[i], n, slotl, lfail);
		}
		break;
	}
}

void
cgenpmatch(Cbuf *cb, Expr *expr, Name val, int lfail)
{
	Name n;
	ExprL *el;
	PatTokenL *l;
	Ast *ast;

	if(expr->op != ExprBacktick && expr->op != ExprBacktick2)
		panic("cgenpmatch");

	el = nil;
	for(l=expr->pattern->tokenl; l; l=l->tl)
		if(l->hd->op == PatTokenSlot)
			el = mkExprL(l->hd->expr, el);
	el = revExprL(el);

	n = cbtmp(cb);
	ast = expr->pattern->ast;
	if(expr->op == ExprBacktick)
		ast = astcanonicalize(ast);

	cbprint(cb, expr->pos, "{ Type *__tgram__ = %#T;",
		expr->pattern->type);
	cbprint(cb, expr->pos, "Ast *%s = %s;", n, val);
	cbprint(cb, expr->pos, "if(%s == 0) goto L%d;", n, lfail);
	cbprint(cb, expr->pos, "if(!issubtype(%s->tag, %#T)) goto L%d;", n, expr->pattern->type, lfail);
	cgenmatchast(cb, expr->pos, ast, n, el, lfail);
	cbprint(cb, expr->pos, "}");
}


PatToken*
mkPatToken(PatTokenOp op)
{
	PatToken *t;
	
	t = emalloc(sizeof *t);
	t->op = op;
	t->pos = yylastpos;
	return t;
}

ParsePattern*
mkParsePattern(void)
{
	ParsePattern *p;
	
	p = emalloc(sizeof *p);
	p->pos = yylastpos;
	return p;
}

LISTIMPL(PatToken)
LISTIMPL(RgTokenL)
LISTIMPL(SlotChoice)
LISTIMPL(SlotChoiceL)
