#include "utf.h"
#include "fmt.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <unistd.h>

typedef unsigned int uint32;
typedef unsigned long long uint64;
typedef int int32;
typedef long long int64;
typedef unsigned char uchar;
typedef unsigned long uintptr;

#define _LIST(X, Z) \
	typedef struct X##L X##L; \
	struct X##L { \
		Z hd; \
		X##L *tl; \
	}; \
	X##L *mk##X##L(Z, X##L*); \
	X##L *copy##X##L(X##L*); \
	X##L *rev##X##L(X##L*); \
	void free##X##L(X##L*); \
	int len##X##L(X##L*); \
	X##L *add##X##L(X##L*, X##L*); \
	Z nth##X##L(X##L*, int); \
	X##L *sort##X##L(X##L*, int(*)(Z, Z)); \
	X##L *uniq##X##L(X##L*); \

#define LIST(X) _LIST(X, X*)
_LIST(Ptr, void*)
PtrL*	listsort(PtrL*, int(*)(void*, void*));

void *erealloc(void*, int);
void *erealloczero(void*, int, int);
char *estrdup(char*);
void *emalloc(int);
void *emallocnz(int);
char *readfile(char*);
void panic(char*, ...);
int	recursefmt(Fmt*);

extern char errbuf[256];
void werrstr(char*, ...);

#define nil ((void*)0)
#define nelem(x) (sizeof(x)/sizeof((x)[0]))
#define USED(x) ((void)(x))

// Names
typedef struct _name_* Name;		// char* in disguise
Name nameofn(char*, int);
Name nameof(char*);
char *nametostr(Name);
#define nametostr(x) ((char*)(x))
Name nameprint(char*, ...);
#define N(x) ({static Name _; if(!_) _ = nameof(x); _; })
_LIST(Name, Name)

// Map
typedef struct Map Map;
typedef struct MapElem MapElem;
struct Map
{
	int (*cmp)(void*, void*);
	MapElem *root;
};

struct MapElem
{
	int color;
	MapElem *left;
	void *key;
	void *value;
	MapElem *right;
};

void *mapget(Map*, void*);
void mapput(Map*, void*, void*);
void mapputelem(Map*, void*, void*, MapElem*);

// Hash
typedef struct Hash Hash;

Hash *mkhash(void);
void *hashput(Hash*, void*, void*);
void *hashget(Hash*, void*);
void **hashgetp(Hash*, void*);

/* Iterators */
typedef struct Hashkv Hashkv;
typedef struct Hashiter Hashiter;

struct Hashkv
{
	void *key;
	void *value;
};

struct Hashiter
{
	int i, j;
	Hash *h;
};

int hashiterstart(Hashiter*, Hash*);
int hashiternextkey(Hashiter*, void**);
int hashiternextkv(Hashiter*, Hashkv*);
void hashiterend(Hashiter*);

typedef struct Fmap Fmap;
struct Fmap
{
	int color;
	Fmap *left;
	void *key;
	void *value;
	Fmap *right;
};

void* fmaplookup(Fmap*, void*);
Fmap* fmapinsert(Fmap*, void*, void*);

typedef struct Fmapkv Fmapkv;
typedef struct Fmapiter Fmapiter;

struct Fmapkv
{
	void *key;
	void *value;
};

struct Fmapiter
{
	Fmap **stack;
	int nstack;
	int mstack;
};
int fmapiterstart(Fmapiter*, Fmap*);
int fmapiternextkey(Fmapiter*, void**);
int fmapiternextkv(Fmapiter*, Fmapkv*);
void fmapiterend(Fmapiter*);
