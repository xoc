#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "a.h"
#include "type.h"
#include "runtime.h"
#include "compile.h"
#include "grammar.h"
#include "grammarcom.h"
#include "patterncom.h"
#include "parse.tab.h"

#define EOF (-1)

typedef struct Input Input;
typedef struct Lexsym Lexsym;
typedef struct Range Range;

int yylexdebug;
int ingrammar;
int intermname;
int inbacktick;

struct Input {
	char *bp;
	char *rp;
	char *ep;
};

struct Lexsym {
	char *str;
	int token;
};

struct Range {
	int p0;		// Input bytes [p0, p1)
	int p1;
	int line;	// start at line in file.
	char *file;
};

static Input input;

static Range *range;
static int nrange;

IncludeL *includel;

static int
countlines(char *p, char *ep)
{
	int n;
	
	n = 0;
	while((p = memchr(p, '\n', ep - p)) != nil){
		n++;
		p++;
	}
	return n;
}

int
pos2fileline(Pos pos, char **file)
{
	char *p;
	int i;
	int line;

	for(i=0; i<nrange; i++)
		if(range[i].p0 <= pos.start && pos.start < range[i].p1)
			break;
	if(i == nrange || (pos.start == 0 && pos.end == 0)){
		*file = "???";
		return 0;
	}
	p = input.bp + range[i].p0;
	line = range[i].line + countlines(p, input.bp + pos.start);
	*file = range[i].file;
	return line;
}

int
posfmt(Fmt *f)
{
	int line;
	char *p, *ep, *file;
	Pos pos;
	
	pos = va_arg(f->args, Pos);
	if(f->flags&FmtSharp){
		p = input.bp + pos.start;
		ep = input.bp + pos.end;
		if(ep < p)
			ep = p;
		fmtprint(f, "%.*s", utfnlen(p, ep - p), p);
		return 0;
	}
	line = pos2fileline(pos, &file);
	if(f->flags&FmtLong)
		fmtprint(f, "%d \"%s\"", line, file);
	else
		fmtprint(f, "%s:%d", file, line);
	return 0;
}

// Insert str, from name, at input.rp.
static void
pushinput(char *name, char *str)
{
	int i, p0, p1, nstr;
	char *obp;
	Range r;

	nstr = strlen(str);

	// Make room.
	obp = input.bp;
	input.bp = erealloc(input.bp, input.ep - input.bp + nstr);
	input.rp += input.bp - obp;
	input.ep += input.bp - obp;
	if(input.rp > input.ep)
		input.rp = input.ep;
	
	// Insert the new text, sliding old text up.
	p0 = input.rp - input.bp;
	p1 = p0 + nstr;
	memmove(input.bp+p1, input.rp, input.ep-input.rp);
	memmove(input.bp+p0, str, nstr);
	input.ep += nstr;

	// Update the range map.
	r.p0 = p0;
	r.p1 = p1;
	r.file = estrdup(name);
	r.line = 1;
	range = erealloc(range, (nrange+2)*sizeof range[0]);
	for(i=0; i<nrange; i++)
		if(p0 < range[i].p1)
			break;
	if(i == nrange || p0 == range[i].p0){
		// Insert before i.
		memmove(range+i+1, range+i, (nrange-i)*sizeof range[0]);
		nrange++;
		range[i] = r;
		for(i++; i<nrange; i++){
			range[i].p0 += nstr;
			range[i].p1 += nstr;
		}
	}else{
		// Split i into i and i+2.
		// Insert as i+1.
		assert(range[i].p0 < p0);
		memmove(range+i+2, range+i, (nrange-i)*sizeof range[0]);
		nrange += 2;
		range[i].p1 = p0;
		range[i+1] = r;
		range[i+2].p0 = p1;
		range[i+2].p1 += nstr;
		range[i+2].line += countlines(input.bp+range[i].p0, input.bp+range[i].p1);
		for(i+=3; i<nrange; i++){
			range[i].p0 += nstr;
			range[i].p1 += nstr;
		}
	}
}

void
pushinputstring(char *str)
{
	pushinput("<string>", str);
}

static char*
nextelem(char **pp)
{
	char *p, *q;
	
	p = *pp;
	while(*p == '/')
		p++;
	if(*p == 0)
		return nil;
	for(q=p; *q != '/' && *q != 0; q++)
		;
	if(*q == '/')
		*q++ = 0;
	*pp = q;
	return p;
}

static char*
prevelem(char *s, char *p)
{
	if(p == s+2 && memcmp(s, "..", 2) == 0)
		return nil;
	if(p >= s+3 && memcmp(p-3, "/..", 3) == 0)
		return nil;
	while(p > s && *--p != '/')
		;
	return p;
}

char*
cleanname(char *s)
{
	char *e, *r, *w;
	int rooted, n;
	
	if(*s == '/'){
		rooted = 1;
		s++;
	}
	r = w = s;
	while(*r){
		e = nextelem(&r);
		if(strcmp(e, ".") == 0)
			continue;
		if(strcmp(e, "..") == 0){
			w = prevelem(s, w);
			if(w)
				continue;
			if(rooted){
				w = s;
				continue;
			}
			e = "..";
			n = 2;
		}
		if(w > s)
			*w++ = '/';
		n = strlen(e);
		memmove(w, e, n);
		w += n;
	}
	*w = 0;
	if(rooted)
		s--;
	if(w == s)
		strcpy(s, ".");
	return s;
}

Name
fullpath(char *file)
{
	char wd[1024];
	char *s;
	Name name;
	
	if(file[0] == '/')
		s = estrdup(file);
	else{
		if(getcwd(wd, sizeof wd) == nil)
			panic("getcwd: %r");
		s = smprint("%s/%s", wd, file);
	}
	name = nameof(cleanname(s));
	free(s);
	return name;
}

void
pushinputfile(char *file)
{
	char *text;
	struct stat st;
	Include *inc;

	text = readfile(file);
	if(text == nil)
		panic("read %s: %r", file);
	if(stat(file, &st) < 0)
		panic("stat %s: %r", file);
	inc = emalloc(sizeof *inc);
	inc->file = fullpath(file);
	inc->mtime = st.st_mtime;
	inc->size = st.st_size;
	includel = mkIncludeL(inc, includel);
	pushinput(file, text);
	free(text);
}

Lexsym symlist[] = 
{
#include "y.syms"
};

static int
isident(uchar c)
{
	return c >= 0x80 || isalnum(c) || c == '_';
}

static int
getc(void)
{
	int c;

	if(input.rp >= input.ep){
		input.rp++;		// shut up, ANSI pinheads
		return EOF;
	}
	c = *input.rp++;
	return c;
}

static void
ungetc(void)
{
	assert(input.rp > input.bp);
	--input.rp;
}

static int
peekc(void)
{
	int c;
	
	c = getc();
	ungetc();
	return c;
}

static Lexsym*
symsearch(char *str, int n)
{
	int i, lo, hi, m;

	lo = 0;
	hi = nelem(symlist);
	while(lo < hi){
		m = (lo+hi)/2;
		i = strncmp(str, symlist[m].str, n);
		if(i == 0 && symlist[m].str != 0)
			i = -1;
		if(i < 0)
			hi = m;
		else if(i > 0)
			lo = m+1;
		else
			return &symlist[m];
	}
	if(lo < nelem(symlist) && strncmp(symlist[lo].str, str, n) == 0 && symlist[lo].str[n] == 0)
		return &symlist[lo];
	return nil;
}

static int yylexsym(Yystype*);
static int yylexname(Yystype*);
static int yylexstring(Yystype*);
static int yylexcomment(void);
static int yylexregexp(Yystype*);
static int yylextermname(Yystype*);
static int yylexbacktick(Yystype*);

int
_yylex(Yystype *yylval, Pos *yylloc)
{
	char *p;
	int c, c1, n;
	int sol;	// start of line
	long double f;
	uint64 i;
	
	sol = 0;
	if(input.rp == input.bp)
		sol = 1;
	while((c = getc()) == ' ' || c == '\t' || c == '\n')
		if(c == '\n')
			sol = 1;
	USED(sol);
	
	if(inbacktick && c != EOF && (!sol || c != '#')){
		ungetc();
		return yylexbacktick(yylval);
	}

	yylloc->start = input.rp-1 - input.bp;
	switch(c){
	case EOF:
		ungetc();
		return Eof;

	// Either a number or a symbol: need to look ahead.
	case '.':
		if(isdigit(peekc()))
			goto Number;
		goto Default;
	
	// Numeric literal
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
	Number:
		// parse [0-9.]([0-9.eE+-]|ident)*
		p = input.rp-1;
		while((c = getc()) != EOF){
			if(isdigit(c) || c == '.' || isident(c))
				continue;
			if((c == '+' || c == '-'))
			if((c1 = peekc()) == 'p' || c1 == 'e' || c1 == 'P' || c1 == 'E')
				continue;
			ungetc();
			break;
		}
		n = input.rp - p;
		yylval->name = nameofn(p, n);
		yylval->len = n;
		if(parsefloat(nametostr(yylval->name), &f) >= 0){
			yylval->f = f;
			return LFloat;
		}else if(parseinteger(nametostr(yylval->name), &i) >= 0){
			yylval->i = i;
			return LInteger;
		}
		yylloc->end = input.rp - input.bp;
		error(*yylloc, "malformed numeric constant %s", yylval->name);
		yylval->i = 42;
		return LInteger;

	// Type variable
	case '\'':
		ungetc();
		return yylexname(yylval);

	// Quoted string
	case '"':
		ungetc();
		return yylexstring(yylval);

	// Grammar directive if in grammar.
	case '%':
		if(ingrammar){
			ungetc();
			return yylexname(yylval);
		}
		goto Default;

	// Comment
	case '/':
		if(yylexcomment())
			return _yylex(yylval, yylloc);
		// Regexp if in grammar.
		if(ingrammar){
			ungetc();
			return yylexregexp(yylval);
		}
		goto Default;

	default:
	Default:
		ungetc();
		if(!isident(c))
			return yylexsym(yylval);
		if(intermname || ingrammar)
			return yylextermname(yylval);
		return yylexname(yylval);
	}		
}

int
yylex(Yystype *yylval, Pos *yylloc)
{
	int y;
	
	yylloc->start = input.rp - input.bp;
	y = _yylex(yylval, yylloc);
	yylloc->end = input.rp - input.bp;
	yylastpos = *yylloc;
	if(yylexdebug)
		fprint(2, "Lex %d\n", y);
	return y;
}

// Lex a symbol, using table to determine when to stop.
static int
yylexsym(Yystype *yylval)
{
	int c;
	char *p;
	Lexsym *sym, *nsym;
	
	p = input.rp;
	sym = nil;
	while((c = peekc()) != EOF && !isident(c)){
		if((nsym = symsearch(p, input.rp+1 - p)) == nil)
			break;
		sym = nsym;
		getc();
	}
	if(sym == nil){
		fprint(2, "Unexpected symbol: %.*s\n",
			utfnlen(p, input.rp+1 - p), p);
		return Error;
	}
	yylval->name = nameofn(p, input.rp - p);
	return sym->token;
}

// Lex a name.
static int
yylexname(Yystype *yylval)
{
	int c, n;
	char *p;
	Lexsym *sym;
	
	p = input.rp;
	getc();	// first char can be non-ident; caller vetted it
	while((c = peekc()) != EOF && isident(c))
		getc();
	n = input.rp - p;
	yylval->name = nameofn(p, n);
	if((sym = symsearch(p, n)) != nil)
		return sym->token;
	if(p[0] == '\'')
		return LTypeVar;
	return LName;
}

// Lex a quoted string.
static int
yylexstring(Yystype *yylval)
{
	int c, quote;
	char *p, *ep;
	Pos pos;
	
	p = input.rp;
	pos.start = input.rp - input.bp;
	quote = getc();
	while((c = getc()) != EOF && c != quote){
		if(c == '\n'){
			ungetc();
			pos.end = input.rp - input.bp;
			error(pos, "newline in string");
			return Error;
		}
		if(c == '\\'){
			if(getc() == EOF){
				ungetc();
				pos.end = input.rp - input.bp;
				error(pos, "eof in string");
				return Eof;
			}
		}
	}
	ep = input.rp;
	
	p++;	// cut out quotes
	ep--;
	yylval->name = nameofn(p, ep - p);
	return LString;
}

// Lex a regular expression between slashes.
static int
yylexregexp(Yystype *yylval)
{
	int tok;

	// Treat beginning like a string.
	if((tok = yylexstring(yylval)) != LString)
		return tok;
	
	// Then flag characters.
	yylval->flags = 0;
	for(;;){
		if(peekc() == 'a'){
			yylval->flags |= RegexpAmbiguous;
			getc();
		}else if(peekc() == 's'){
			yylval->flags |= RegexpShortest;
			getc();
		}else
			break;
	}
	return LRegexp;
}


// Lex a comment.  Leading / has been read.
static int
yylexcomment(void)
{
	int c;

	// Try C++-style comment.
	if(peekc() == '/'){
		while((c = getc()) != EOF && c != '\n')
			;
		if(c == '\n')
			ungetc();
		return 1;
	}
	
	// Try C-style comment.
	if(peekc() == '*'){
		getc();
		for(;;){
			while((c = getc()) != EOF && c != '*')
				;
			while(c == '*')
				c = getc();
			if(c == EOF || c == '/')
				return 1;
		}
	}
	
	// Not a comment.
	return 0;
}

// Lex a grammar term name.
static int 
yylextermname(Yystype *yylval)
{
	int c, tok;
	char *p;

	// Clumsy hack: term if we're looking for a term name
	// and see brace or paren, then pretend we saw the
	// empty term name (we kind of did).
	// XXX let's see if we can do without this
	
	// Other punctuation should be treated normally.
	if(!isident(peekc()))
		return yylexsym(yylval);

	p = input.rp;
	tok = yylexname(yylval);
	if(tok == Error)
		return Error;
	
	// Look for suffixes;
	c = peekc();
	if(c == '?' || c == '*' || c == '+')
		getc();
	else if(c == '['){
		// [sep]* or [sep]+
		while((c = getc()) != ']'){
			if(c == EOF){
				error(yylastpos, "eof during term name");
				return Error;
			}
		}
		c = getc();
		if(c != '+' && c != '*'){
			error(yylastpos, "no * + after term[sep]");
			return Error;
		}
	}
	
	yylval->name = nameofn(p, input.rp - p);
	return LGTermName;		
}

// Lex following a backtick.
// Picks up gobs of text uninterpreted except:
//	\x is a slot
//	\x::type is also a slot
//	\( is an escape but don't pick up the (
//	count braces and stop when the closing brace is found
// Have to parse quoted strings and comments so that
// braces inside them are not miscounted.

static int
yylexbacktick(Yystype *yylval)
{
	int c;
	char *p;
	Pos pos;
	static int nbrace;
	PatToken *t;
	Name name, symname;

	if(peekc() == '}' && nbrace == 0){
		getc();
		return '}';
	}
	
	pos.start = input.rp - input.bp;
	if(peekc() == '\\'){
		getc();
		if(peekc() == '('){
			pos.end = input.rp - input.bp;
			t = mkPatToken(PatTokenSlot);
			t->pos = pos;
			yylval->pattoken = t;
			return LPatternEscape;
		}
		if(isident((uchar)peekc())){
			if(yylexname(yylval) != LName)
				return Error;
			name = yylval->name;
			symname = nil;
			if(peekc() == ':'){
				getc();
				if(peekc() != ':')
					ungetc();
				else{
					getc();
					if(yylextermname(yylval) != LGTermName)
						return Error;
					symname = yylval->name;
				}
			}
			pos.end = input.rp - input.bp;
			t = mkPatToken(PatTokenSlot);
			t->text = name;
			t->symname = symname;
			t->pos = pos;
			yylval->pattoken = t;
			return LPatternSlot;
		}
		pos.end = input.rp - input.bp;
		warn(pos, "bad slot?  weird backslash");
	}

	// Read a chunk of text, matching braces
	// and stopping before an unmatched closing brace.
	// Have to parse comments and strings to be sure
	// not to count their contents as braces.  Sigh.
	p = input.rp;
	while((c = peekc()) != EOF && c != '\\' && (c != '}' || nbrace > 0)){
		getc();
		if(c == '{')
			nbrace++;
		if(c == '}')
			nbrace--;
		if(c == '/')
			yylexcomment();
		if(c == '"' || c == '\'')
			if(yylexstring(yylval) != LString)
				return Error;
	}
	pos.end = input.rp - input.bp;
	t = mkPatToken(PatTokenText);
	t->text = nameofn(p, input.rp - p);
	t->pos = pos;
	yylval->pattoken = t;
	return LPatternText;
}
