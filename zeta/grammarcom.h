// Compile-time for grammars

typedef struct GRule GRule;
typedef struct GStmt GStmt;
typedef struct GTop GTop;
typedef struct GTerm GTerm;

LIST(GStmt)
LIST(GRule)
LIST(GTerm)

struct GTop
{
	Pos pos;
	Name xflag;
	Name name;
	NameL *extendl;
	GStmtL *stmtl;
	Gram *gram;
	Type *tgram;
};
GTop *mkGTop(Name, Name, NameL*, GStmtL*);

struct GRule
{
	Pos pos;
	GTermL *terml;
	Name name;
	Name prec;
};
GRule *mkGRule(void);

typedef enum GStmtOp {
	GStmtRules = 1,
	GStmtAssoc,
	GStmtPriority,
	GStmtIgnore,
	GStmtNoCopy,
} GStmtOp;
struct GStmt
{
	GStmtOp op;
	Pos pos;
	Name name;
	GRuleL *rulel;
	GTerm *term;
	int assoc;
	NameL *namel;
	int prefer;
	GTermL *terml;
};
GStmt *mkGStmt(GStmtOp);

typedef enum GTermOp {
	GTermString = 1,
	GTermName,
	GTermRegexp,
} GTermOp;
struct GTerm
{
	GTermOp op;
	Pos pos;
	Name text;
	Name str;
	int flags;
};
GTerm *mkGTerm(GTermOp);

extern int ingrammar;
extern int intermname;

void glrmerge(CfgSym*, ParseValue*, ParseValue*);
RgTokenL *lextext(RgParse*, CfgGram*, void*);
