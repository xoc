// Type inference.
//
// Although this code could be used for general type inference a la ML,
// the fact that zeta requires type annotations on function arguments
// and return types avoids the unbridled power and mystery of
// ML-style inference.

#include "a.h"
#include "type.h"
#include "listimpl.h"

/////////////////////////////////////////////////////////////////////////////
// Mechanics of variable names, variable numbers, and variable instances.

Type*
typevarname(Name n)
{
	static Hash *h;
	Type *t;

	if(h == nil)
		h = mkhash();
	t = hashget(h, n);
	if(t == nil){
		t = mkType(TypeVarName, 1);
		t->name = n;
		hashput(h, n, t);
	}
	return t;
}

// Collect all the var names in t.
static void
dovarnames(Type *t, TypeL **lp)
{
	TypeL *tl;
	TypeL *l, **ll;

	if(t == nil)
		return;
	switch(t->op){
	case TypeVarInst:
		if(t->constraint->min != nil || t->constraint->max != nil)
			panic("not unified: %T", t);
	case TypeVarName:
	case TypeVarNum:
		for(ll=&*lp; (l=*ll); ll=&l->tl)
			if(l->hd == t)
				return;
		*ll = mkTypeL(t, nil);
		break;

	default:
		dovarnames(t->left, lp);
		dovarnames(t->right, lp);
		if(t->op != TypeStruct)
			for(tl=t->typel; tl; tl=tl->tl)
				dovarnames(tl->hd, lp);
		break;
	}
}

static TypeL*
varnames(Type *t)
{
	TypeL *l;
	
	l = nil;
	dovarnames(t, &l);
	return l;
}

static int
findname(TypeL *l, Type *t)
{
	int i;
	
	for(i=0; l; l=l->tl, i++)
		if(l->hd == t)
			return i;
	return -1;
}

static TypeL* replacevarnamesl(TypeL*, TypeL*);

static Type*
replacevarnames(TypeL *names, Type *t)
{
	int i;
	TypeL *l;

	if(t == nil)
		return nil;
	switch(t->op){
	case TypeVarName:
	case TypeVarNum:
	case TypeVarInst:
		i = findname(names, t);
		if(i < 0)
			panic("replacevarnames - %T not found", t);
		return typevarnum(i);

	case TypeModule:
	case TypeType:
	case TypeBool:
	case TypeInt:
	case TypeFloat:
	case TypeString:
	case TypeStruct:
	case TypeWrapper:
	case TypeNil:
	case TypeVarFrozen:
	case TypeAny:
	case TypeGram:
		return t;

	case TypeTuple:
		t = typetuple(l=replacevarnamesl(names, t->typel));
		freeTypeL(l);
		return t;
		
	case TypeList:
		return typelist(replacevarnames(names, t->left));
	
	case TypeArray:
		return typearray(replacevarnames(names, t->left));
	
	case TypeMap:
		return typemap(replacevarnames(names, t->left),
			replacevarnames(names, t->right));
	
	case TypeFmap:
		return typefmap(replacevarnames(names, t->left),
			replacevarnames(names, t->right));
	
	case TypeFn:
		t = typefn(l=replacevarnamesl(names, t->typel),
			replacevarnames(names, t->left));
		freeTypeL(l);
		return t;
	}
	panic("replacevarnames1 %T", t);
	return nil;
}

static TypeL*
replacevarnamesl(TypeL *names, TypeL *l)
{
	if(l == nil)
		return nil;
	return mkTypeL(replacevarnames(names, l->hd),
		replacevarnamesl(names, l->tl));
}


// Given a type with TypeVarName variables, 
// generalize, replacing those with TypeVarNums.
GType*
typegeneralize(Type *t)
{
	static Hash *cache;
	GType *tt;
	TypeL *l;
	int n;

	if(t == nil)
		return nil;
	t = typeunified(t);

	if(cache == nil)
		cache = mkhash();
	tt = hashget(cache, t);
	if(tt == nil){
		l = varnames(t);
		n = lenTypeL(l);
		tt = emalloc(sizeof *tt);
		tt->n = n;
		tt->right = t;
		if(n > 0)
			t = replacevarnames(l, t);
		tt->left = t;
		tt->typevarl = l;
		hashput(cache, tt->right, tt);
	}
	return tt;
}

static Type*
typevarinst(void)
{
	Type *t;
	
	t = mkType(TypeVarInst, 0);
	t->constraint = emalloc(sizeof *t->constraint);
	return t;
}

static TypeL* replacevarnumsl(Type**, TypeL*);

static Type*
replacevarnums(Type **inst, Type *t)
{
	if(t == nil)
		return nil;
	switch(t->op){
	case TypeVarNum:
		return inst[t->n];

	case TypeNil:
		return t;

	case TypeVarName:
	case TypeVarInst:
		panic("replacevarnums");

	case TypeBool:
	case TypeInt:
	case TypeFloat:
	case TypeString:
	case TypeStruct:
	case TypeWrapper:
	case TypeModule:
	case TypeType:
	case TypeVarFrozen:
	case TypeAny:
	case TypeGram:
		return t;

	case TypeTuple:
		return typetuple(replacevarnumsl(inst, t->typel));
		
	case TypeList:
		return typelist(replacevarnums(inst, t->left));
	
	case TypeArray:
		return typearray(replacevarnums(inst, t->left));
	
	case TypeMap:
		return typemap(replacevarnums(inst, t->left),
			replacevarnums(inst, t->right));
	
	case TypeFmap:
		return typefmap(replacevarnums(inst, t->left),
			replacevarnums(inst, t->right));
	
	case TypeFn:
		return typefn(replacevarnumsl(inst, t->typel),
			replacevarnums(inst, t->left));
	}
	panic("replacevarnums");
	return nil;
}

static TypeL*
replacevarnumsl(Type **inst, TypeL *l)
{
	if(l == nil)
		return nil;
	return mkTypeL(replacevarnums(inst, l->hd),
		replacevarnumsl(inst, l->tl));
}

// Given a type with TypeVarNums, create a new instantiation
// ready for unification.
Type*
typeinstantiate(GType *g)
{
	int i;
	Type **inst;
	Type *t;

	if(g == nil)
		return nil;
	inst = emalloc(g->n * sizeof inst[0]);
	for(i=0; i<g->n; i++)
		inst[i] = typevarinst();
	
	t = replacevarnums(inst, g->left);
	free(inst);
	return t;
}

Type*
typevarfrozen(Type *left)
{
	Type *t;
	
	t = mkType(TypeVarFrozen, 0);
	t->left = left;
	return t;
}


// Given a type with TypeVarNums, create a new instantiation
// with all the variables frozen into their own types.
// When type-checking a polymorphic function, the argument
// types get frozen during type-checking of the body so that
// they become essentially monomorphic.
Type*
typefreeze(GType *g)
{
	int i;
	Type **inst;
	Type *t;
	TypeL *tl;

	if(g == nil)
		return nil;
	inst = emalloc(g->n * sizeof inst[0]);
	for(i=0, tl=g->typevarl; i<g->n; i++, tl=tl->tl)
		inst[i] = typevarfrozen(tl->hd);
	
	t = replacevarnums(inst, g->left);
	free(inst);
	return t;
}



// Given a type, construct the unified type.
static TypeL* typeunifiedl(TypeL*);

Type*
typeunified(Type *t)
{
	TypeL *l;
	TypeConstraint *c;

	if(t == nil)
		return nil;
	switch(t->op){
	case TypeVarInst:
		c = t->constraint;
		if(c->max && c->max == c->min)
			return typeunified(c->max);
		if(c->min && c->max)
			fprint(2, "warning: didn't really unify: range %T to %T\n", c->min, c->max);
		if(c->min)
			return typeunified(c->min);
		if(c->max)
			return typeunified(c->max);
		return t;

	case TypeGram:
	case TypeBool:
	case TypeInt:
	case TypeFloat:
	case TypeString:
	case TypeStruct:
	case TypeWrapper:
	case TypeVarName:
	case TypeVarNum:
	case TypeModule:
	case TypeType:
	case TypeNil:
	case TypeVarFrozen:
	case TypeAny:
		return t;

	case TypeTuple:
		t = typetuple(l = typeunifiedl(t->typel));
		freeTypeL(l);
		return t;
		
	case TypeList:
		return typelist(typeunified(t->left));
	
	case TypeArray:
		return typearray(typeunified(t->left));
	
	case TypeMap:
		return typemap(typeunified(t->left),
			typeunified(t->right));
	
	case TypeFmap:
		return typefmap(typeunified(t->left),
			typeunified(t->right));
	
	case TypeFn:
		t = typefn(l = typeunifiedl(t->typel),
			typeunified(t->left));
		freeTypeL(l);
		return t;
	}
	panic("typeunified");
	return nil;
}

static TypeL*
typeunifiedl(TypeL *l)
{
	if(l == nil)
		return nil;
	return mkTypeL(typeunified(l->hd),
		typeunifiedl(l->tl));
}


//////////////////////////////////////////////////
// Unification

// Unify t1 with t2, returning whether it can be done.
//	subtype > 0:	want t1 :> t2
//	subtype < 0:	want t1 <: t2
//	subtype == 0:	want t1 == t2 (both)
int
canunify(Type *t1, Type *t2, int subtype)
{
	TypeConstraint *c1, *c2;

	if(t1 == t2)
		return 1;

	if(t1->op == TypeVarInst){
		c1 = t1->constraint;
		if(t2->op == TypeVarInst){
			// True generality would require that we handle
			// subtype>0 and subtype<0, so that you could
			// have constraints like 'a <: 'b, C99 <: 'b,
			// but we impose a stricter approximation: 'a == 'b.
			c2 = t2->constraint;
			if(c1 == c2)
				return 1;
			if(typemax(c1->min, c2->min, &c1->min) < 0){
				werrstr("%T, %T <: ?", c1->min, c2->min);
				return 0;
			}
			if(typemin(c1->max, c2->max, &c1->max) < 0){
				werrstr("? <: %T, %T", c1->max, c2->max);
				return 0;
			}
			t2->constraint = c1;
		}else{
			// subtype <= 0:  t1 <: t2, might lower max
			if(subtype <= 0 && typemin(c1->max, t2, &c1->max) < 0){
				werrstr("? <: %T, %T", c1->max, t2);
				return 0;
			}

			// subtype >= 0:  t2 <: t1, might raise min
			if(subtype >= 0 && typemax(c1->min, t2, &c1->min) < 0){
				werrstr("%T, %T <: ?", c1->min, t2);
				return 0;
			}
			if(c1->min && c1->min == c1->max)
				return canunify(c1->min, t2, subtype);
		}
		if(c1->min && c1->max && !issubtype(c1->min, c1->max)){
			werrstr("%T <: ? <: %T", c1->min, c1->max);
			return 0;
		}
		return 1;
	}
	
	if(t2->op == TypeVarInst)
		return canunify(t2, t1, -subtype);
	
	if(t1->op != t2->op)
		goto Subtype;

	switch(t1->op){
	case TypeStruct:
	case TypeGram:
	Subtype:
		if(subtype <= 0 && !issubtype(t1, t2)){
			werrstr("%T <: %T", t1, t2);
			return 0;
		}
		if(subtype >= 0 && !issubtype(t2, t1)){
			werrstr("%T <: %T", t2, t1);
			return 0;
		}
		return 1;

	case TypeInt:		// should have hit t1 == t2 above
	case TypeBool:
	case TypeString:
	case TypeFloat:
	case TypeWrapper:
	case TypeModule:
	case TypeType:
	case TypeVarInst:	// handled above
	case TypeAny:		// handled above
	case TypeVarName:	// should not see
	case TypeVarNum:	// should not see
	case TypeNil:	// should not see
		panic("canunify2 %T, %T", t1, t2);
	
	case TypeVarFrozen:
		fprint(2, "canunify %p %T %p %T", t1, t1, t2, t2);
		// t1==t2 was picked off above
		werrstr("%T <: %T", t1, t2);
		return 0;

	case TypeFn:
		// a->b <: c->d === a :> c && b <: d
		return canunifyl(t1->typel, t2->typel, -subtype) &&
			canunify(t1->left, t2->left, subtype);

	case TypeMap:
		// map(a, b) <: map(c, d) === a :> c && b == d
		return canunify(t1->left, t2->left, -subtype) &&
			canunify(t1->right, t2->right, 0);

	case TypeFmap:
		// fmap(a, b) <: fmap(c, d) === a :> c && b <: d
		return canunify(t1->left, t2->left, -subtype) &&
			canunify(t1->right, t2->right, subtype);

	case TypeList:
		// list a <: list b === a <: b  [safe because lists are immutable]
		return canunify(t1->left, t2->left, subtype);
	
	case TypeTuple:
		// [safe because tuples are immutable]
		// Since we box tuples, we could allow t1->typel to
		// be longer than t2->typel.  But no one needs that.
		return canunifyl(t1->typel, t2->typel, subtype);

	case TypeArray:
		// array a <: array b iff a == b
		return canunify(t1->left, t2->left, 0);
	}
	panic("canunify3 %T %T", t1, t2);
	return 0;
}

int
canunifyl(TypeL *l1, TypeL *l2, int subtype)
{
	for(; l1 && l2; l1=l1->tl, l2=l2->tl)
		if(!canunify(l1->hd, l2->hd, subtype))
			return 0;
	if(l1 || l2){
		werrstr("length mismatch");
		return 0;
	}
	return 1;
}
