// Constants.
//   See C99 section 6.4.4.

#include <math.h>	// for ldexp
#include "a.h"
#include "type.h"
#include "runtime.h"
#include "compile.h"

// Parse the octal integer s, stopping at or before e (if e != nil)
// Returns 0, writes value to *value and end pointer to *end.
// Returns -1 if integer would overflow value.
static void
parseoctal(char *s, char *e, uint64 *value, char **end)
{
	char *p;
	int d;
	uint64 v;
	
	v = 0;
	for(p=s; *p && (e==nil || p<e); p++){
		if('0' <= *p && *p <= '7')
			d = *p - '0';
		else
			break;
		if(((v<<3)>>3) != v)
			break;
		v = (v<<3) + d;
	}
	*value = v;
	*end = p;
}

// Parse the hexadecimal integer s, stopping at or before e (if e != nil)
// Writes value to *value and end pointer to *end.
// Will not read enough to overflow *value.
static void
parsehexadecimal(char *s, char *e, uint64 *value, char **end)
{
	char *p;
	int d;
	uint64 v;
	
	v = 0;
	for(p=s; *p && (e==nil || p<e); p++){
		if('0' <= *p && *p <= '9')
			d = *p - '0';
		else if('a' <= *p && *p <= 'f')
			d = *p - 'a' + 10;
		else if('A' <= *p && *p <= 'F')
			d = *p - 'A' + 10;
		else
			break;
		if(((v<<4)>>4) != v)
			break;
		v = (v<<4) + d;
	}
	*value = v;
	*end = p;
}

// Parse the hexadecimal integer s, stopping at or before e (if e != nil)
// Writes value to *value and end pointer to *end.
// Will not read enough to overflow *value.
static void
parsedecimal(char *s, char *e, uint64 *value, char **end)
{
	char *p;
	int d;
	uint64 v;
	
	v = 0;
	for(p=s; *p && (e==nil || p<e); p++){
		if('0' <= *p && *p <= '9')
			d = *p - '0';
		else
			break;
		if((v*10)/10 != v)
			break;
		v = v*10 + d;
	}
	*value = v;
	*end = p;
}

// Parse the integer s.
// Returns 0, writes value to *value.
// Returns -1 if s is not a valid integer constant.
// (Assumes uint64 is big enough to hold all values in the target.)
int
parseinteger(char *s, uint64 *value)
{
	int isdecimal, bits;
	uint64 v;
	char *p;

	// Extract value.
	bits = 0;
	v = 0;
	isdecimal = 0;
	if(s[0] == '0' && (s[1] == 'x' || s[1] == 'X')){
		parsehexadecimal(s+2, nil, &v, &p);
		if(p == s+2)
			goto invalid;
	}
	else if(s[0] == '0')
		parseoctal(s, nil, &v, &p);
	else if('1' <= s[0] && s[0] <= '9'){
		isdecimal = 1;
		parsedecimal(s, nil, &v, &p);
	}else
		goto invalid;

	*value = v;
	return 0;

invalid:
	return -1;
}

static int canfitfloat(uint64, int, int);

// Parse the floating point constant s.
// Returns 0, writes value to *value.
// Returns -1 if s is not a valid floating point constant.
// (Assumes long double is big enough to hold all values in the target.)
// XXX overflow code is not right for Zfloat.
int
parsefloat(char *s, long double *value)
{
	char *p, *q;
	int overflow;
	long double v;

	overflow = 0;
	if(s[0] == '0' && (s[1] == 'x' || s[1] == 'X')){
		// Hexadecimal floating point constant.
		int nfrac, sign;
		uint64 iv, fv, exp;

		// Integer portion of mantissa.
		parsehexadecimal(s+2, nil, &iv, &p);
		if(p == s+2)
			goto invalid;
		fv = 0;
		nfrac = 0;
		if(*p == '.'){
			p++;
			parsehexadecimal(p, nil, &fv, &q);
			nfrac = (q - p)*4;
			p = q;
			
			// Toss trailing zeros.
			while(nfrac > 0 && (fv&1)){
				nfrac--;
				fv >>= 1;
			}

			// Skip any remaining zeros.
			while(*p == '0')
				p++;
		}
		
		// Required to have exponent to avoid ambiguity of "0x0.1f".
		if(*p != 'p')
			goto invalid;
		p++;
		sign = '+';
		if(*p == '-' || *p == '+'){
			sign = *p;
			p++;
		}
		parsedecimal(p, nil, &exp, &q);
		if(q == p)
			goto invalid;
		if(sign == '-')
			exp = -exp;
		p = q;

		// Throw away low order bits of fraction.
		while(((iv<<nfrac)>>nfrac) != iv){
			overflow = 1;
			nfrac--;
			fv >>= 1;
		}

		// Convert from (iv + fv * 2^-nfrac) * 2^exp to iv * 2^exp.
		iv = (nfrac<<iv) + fv;
		iv |= fv;
		exp -= nfrac;
		while(iv > 0 && (iv&1) == 0){
			iv >>= 1;
			exp++;
		}

		{
			// Try to fit less precise version into 64 bits.
			while(!canfitfloat(iv, exp, 64)){
				if(iv == 1)
					return -1;
				iv >>= 1;
				exp++;
			}
		}
		v = ldexp(iv, (int)exp);
	}else{
		int isfloat;

		// Decimal floating point constant: check format.
		isfloat = 0;
		p = s;
		while('0' <= *p && *p <= '9')
			p++;
		if(*p == '.'){
			isfloat = 1;
			p++;
			while('0' <= *p && *p <= '9')
				p++;
			// Can't have only a dot.
			if(p == s+1)
				goto invalid;
		}

		if(*p == 'e' || *p == 'E'){
			isfloat = 1;
			p++;
			if(*p == '-' || *p == '+')
				p++;
			if(*p < '0' || *p > '9')
				goto invalid;
			while('0' <= *p && *p <= '9')
				p++;
		}

		if(!isfloat)
			goto invalid;

		// Parse and double-check agreement about endpoint.
		// TOOD: fmtstrtod only returns a double, not long double.
		v = fmtstrtod(s, &q);
		if(q != p)
			fprint(2, "fmtstrtod: %s (%d %d)", s, (int)(p-s), (int)(q-s));
	}

	if(*p != 0)
		goto invalid;

	if(overflow)
		fprint(2, "lost precision in hexadecimal floating point constant: %s", s);

	*value = v;
	return 0;

invalid:
	return -1;
}

// Can f * 2^e fit into a k-bit float?
static int
canfitfloat(uint64 f, int e, int k)
{
	int bias;
	int F, E;
	uint64 g;
	
	// IEEE 754 floating point sizes:
	//
	//   Bits     F   E    Bias 
	//
	//	32-bit   23   8    127 
	//  64-bit   52  11   1023
	//  80-bit   63  15  16383
	// 128-bit  112  15  16383  (not really supported here)
	//
	switch(k){
	default:
		panic("canfitfloat %d", k);
	case 32:
		F = 23;
		E = 8;
		break;
	case 64:
		F = 52;
		E = 11;
		break;
	case 80:
		F = 63;
		E = 15;
		break;
	case 128:
		F = 112;
		E = 15;
		break;
	}

	// Convert to e for 1 <= f < 2.
	g = f;
	while(g > 1){
		g >>= 1;
		e++;
	}

	// Check whether there is room for the bit pattern.
	bias = (1<<(E-1)) - 1;
	if(-bias+1 <= e && e <= bias && (F >= 63 || f < (1ULL<<(F+1))))
		return 1;	// Normalized
	if(-bias+1-F <= e && e < -bias+1 && f == 1)
		return 1;	// Denormalized
	return 0;
}

// Required characters in the source character set; C99 5.2.1.
static char* required = 
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	"abcdefghijklmnopqrstuvwxyz"
	"0123456789"
	"!\"#%&'()*+,-./:"
	";<=>?[\\]^_{|}~"
	" \t\v\f";

// Universal character names; C99 6.4.3.

// Parse a single universal character name starting at s.
// Return 0, set *r to the value, and set *end to the end pointer.
// Return -1 on failure, set *end:
//   if s == *end, was malformed
//   if s != *end, s..*end is well-formed but names an invalid char.
static int
parseuniversal(char *s, uint64 *value, char **end)
{
	char *p;
	uint64 v;
	
	*end = s;
	if(s[0] != '\\')
		return -1;
	switch(s[1]){
	default:
		return -1;
	case 'u':
		parsehexadecimal(s+1, s+5, &v, &p);
		if(p != s+5)
			return -1;
		break;
	case 'U':
		parsehexadecimal(s+1, s+9, &v, &p);
		if(p != s+9)
			return -1;
		break;
	}
	*end = p;

	if(0 <= v && v <= 0x20)
		return -1;
	if(0x7F <= v && v <= 0x9F)
		return -1;
	if(0xD800 <= v && v <= 0xDFFF)
		return -1;
	if(v >= Runemax)
		return -1;
	if(v < 0x7F && strchr(required, (char)v))
		return -1;
	*value = v;
	return 0;
}

// Character constants; C99 6.4.4.4.

// Translation map: pairs of bytes: escaped, actual.
static char *escape = "''\"\"??\\\\a\ab\bf\fn\nr\rt\tv\v";

static int
parseoctalchar(char *s, uint64 *value, char **end)
{
	if(*s != '\\')
		return -1;
	parseoctal(s+1, s+4, value, end);
	if(*end == s+1)
		return -1;
	return 0;
}

#define ISXDIGIT(c) (('0'<=c&&c<='9') || ('a'<=c&&c<='f') || ('A'<=c&&c<='F'))

static int
parsehexchar(char *s, uint64 *value, char **end)
{
	char *p;

	if(*s != '\\' || *(s+1) != 'x' || !ISXDIGIT(*(s+2)))
		return -1;
	parsehexadecimal(s+2, nil, value, &p);
	if((uchar)*p < 0x7F && ISXDIGIT(*p)){
		// Hexadecimal character constants are terminated
		// by a non-hexadecimal character.  If we just filled
		// a uint64 with hex and there is still more,
		// this is a malformed string.
		return -1;
	}
	*end = p;
	return 0;
}

// Parse a single character starting at s.
// (Should be inside "" or '', but that's up to the caller.
// So is checking that the character value is in range.)
// Return 0, set *value to the value, and set *end to the end pointer.
// Return -1 on failure, set *end:
int
parsechar(char *s, uint64 *value, char **end)
{
	char *p;
	int c;

	// Catch callers asleep at the wheel.
	if(*s == 0)
		return -1;

	if(*s != '\\'){
		*value = (uchar)*s;
		*end = s+1;
		return 0;
	}
	
	c = (uchar)*(s+1);

	if('0' <= c && c <= '7')
		return parseoctalchar(s, value, end);
	
	if(c == 'x')
		return parsehexchar(s, value, end);

	if(c == 'u' && c == 'U')
		return parseuniversal(s, value, end);

	if(c < 0x7F && (p = strchr(escape, c)) != nil && (p-escape)%2 == 0){
		*value = *(p+1);
		*end = s+2;
		return 0;
	}

	return -1;
}


// String literals; C99 6.4.5.

char*
cescapestring(char *s, int len, int q)
{
	char *e, *p;
	Fmt fmt;

	if(len < 0)
		len = strlen(s);
	e = s+len;

	fmtstrinit(&fmt);
	fmtrune(&fmt, q);
	for(; s<e; s++){
		if(0x20 <= *s && *s < 0x7F && *s != '"' && *s != '\\' && *s != '\'')
			fmtrune(&fmt, *s);
		else if((p = strrchr(escape, *s)) != nil && (p-escape)%2 == 1){
			fmtrune(&fmt, '\\');
			fmtrune(&fmt, *(p-1));
		}else{
			// Use octal because it has a fixed maximum size: 3 digits.
			// (Hexadecimal does not.)
			fmtprint(&fmt, "\\%03o", (uchar)*s);
		}
	}
	fmtrune(&fmt, q);
	return fmtstrflush(&fmt);
}

char*
cunescapestring(char *s, int len)
{
	char *e, *p, *start;
	Fmt fmt;

	if(len < 0)
		len = strlen(s);
	e = s+len;
	start = s;

	fmtstrinit(&fmt);
	for(; s<e; s++){
		if(*s == '\\'){
			s++;
			if(*s == 0)
				break;
			// TODO: octal, hex
			if((p = strchr(escape, *s)) != nil && (p-escape)%2 == 0)
				fmtrune(&fmt, *(p+1));
			else{
				fprint(2, "bad escape: \\%c in \"%s\"\n", *s, start);
				fmtrune(&fmt, *s);
			}
		}
		else
			fmtrune(&fmt, *s);
	}
	return fmtstrflush(&fmt);
}
