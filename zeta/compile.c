#include <unistd.h>
#include <fcntl.h>
#include "a.h"
#include "type.h"
#include "runtime.h"
#include "grammar.h"
#include "compile.h"
#include "listimpl.h"
#include "patterncom.h"
#include "arg.h"

char*		argv0;
extern int	doabort;
extern int yylexdebug;

Scope*		topscope;

static Typecheck	tc;
static Frame*		topframe;
static StmtL*		topstmtl;

static Name		zeta;

void	cbuild(void);
extern NameL *sys_modpath;

void
usage(void)
{
	fprint(2, "usage: zetacom [-C file]... file.zeta\n");
	exit(1);
}

extern int gramdebug;

int
main(int argc, char **argv)
{
	fmtinstall('A', astfmt);
	fmtinstall('N', namefmt);
	fmtinstall('R', cfgrulefmt);
	fmtinstall('P', posfmt);
	fmtinstall('T', typefmt);
	fmtinstall('V', recursefmt);
	fmtinstall('@', slrstatefmt);

	typecheckinit();

	ARGBEGIN{
	case 'A':
		doabort = 1;
		break;
	case 'C':
		compiling = mkNameL(nameof(EARGF(usage())), compiling);
		break;
	case 'G':
		gramdebug++;
		break;
	case 'L':
		linenumbers = 0;
		break;
	case 'P':
		patterndebug++;
		break;
	case 'Y':
		yylexdebug++;
		break;
	case 'M':
		sys_modpath = mkNameL(nameof(EARGF(usage())), sys_modpath);
		break;
	default:
		usage();
	}ARGEND
	
	sys_modpath = revNameL(sys_modpath);

	if(argc != 1)
		usage();
	
	m_sys_init(argc, argv);
	tc.nlabel++;	// avoid L0, catch bugs

	// Set up for input.
	zeta = nameof(argv[0]);
	memset(&tc, 0, sizeof tc);
	tc.reachable = 1;
	topframe = pushframe(&tc);
	topscope = tc.scope;
	topframe->prefix = zeta2prefix(zeta);
	
	// Read, parse, typecheck (via topstmt).
	pushinputfile(argv[0]);	
	if(yyparse() != 0)
		panic("yyparse");
	if(nerrors > 0)
		exit(1);
	topstmtl = revStmtL(topstmtl);

	cbuild();
	return 0;
}

void
cbuild(void)
{
	char buf[1024];
	int fd;
	Cbuf cb;
	Name ctmp, zo;
	StmtL *l;
	VarL *vl;

	// Set up for writing C code.
	ctmp = nameprint("/tmp/%s.c", topframe->prefix);
	if((fd = creat(nametostr(ctmp), 0666)) < 0)
		panic("write %s: %r", ctmp);
	memset(&cb, 0, sizeof cb);
	fmtfdinit(&cb.topfmt, fd, buf, sizeof buf);
	fmtstrinit(&cb.runfmt);
	
	// Write equivalent C code.
	fmtprint(&cb.topfmt,
		"#include \"a.h\"\n"
		"#include \"type.h\"\n"
		"#include \"runtime.h\"\n"
		"#include \"grammar.h\"\n"
		"#include \"attribute.h\"\n"
		"\n"
		"static Type **__types__;\n"
		"\n");
	
	cgenstructdecls(&cb);
	cgenimportdecls(&cb);

	for(vl=topframe->varl; vl; vl=vl->tl){
		Var *v = vl->hd;
		if(v->scope == nil)	// imported variable
			continue;
		if(v->flags&VarRedeclare)	// redeclaration of some other variable
			continue;
		switch(v->type->left->op){
		case TypeModule:
			continue;
		case TypeType:
			continue;
		default:
			break;
		}
		// Explicitly initialize in hope of catching
		// when multiple modules define the same variables.
		fmtprint(&cb.topfmt, "%hT %s = 0;\n", v->type->left, v->cname);
		if(v->flags&VarAttribute)
			fmtprint(&cb.topfmt, "AttrInfo %s_info = { 0 };\n", v->cname);
	}

	fmtprint(&cb.topfmt,
		"static void\n"
		"__init__(void)\n"
		"{\n");
	// Generate grammar rule additions, then other types.
	cgentypes0(&cb);	// allocates __types__
	cgengraminit(&cb);
	cgentypes(&cb);
	// Initialize attribute info structures.
	for(vl=topframe->varl; vl; vl=vl->tl){
		Var *v = vl->hd;
		if(v->scope == nil)
			continue;
		if(v->flags&VarAttribute){
			fmtprint(&cb.topfmt, "\t%s_info.name = %N;\n", v->cname, v->name);
			fmtprint(&cb.topfmt, "\t%s_info.type = %#T;\n", v->cname, v->type->left);
			fmtprint(&cb.topfmt, "\t%s_info.fn = &%s;\n", v->cname, v->cname);
			fmtprint(&cb.topfmt, "\t%s_info.pos = %N;\n", v->cname, nameprint("%P", v->pos));
		}
	}
	fmtprint(&cb.topfmt,
		"}\n"
		"\n");
	
	cb.fmt = &cb.runfmt;
	for(l=topstmtl; l; l=l->tl)
		cgenstmt(&cb, l->hd);

	fmtprint(&cb.topfmt,
		"\n"
		"static void\n"
		"__run__(void)\n"
		"{\n"
		"%s"
		"}\n",
		fmtstrflush(&cb.runfmt));

	// dlfcn.h is fcn with us.
	// If we make __types__ an extern, then the references to __types__
	// in future loaded modules will get resolved to the wrong module!
	// We have to make __types__ static to get correct resolutions.
	// Then we can make __modinfo__ extern and get at __types__ through it.
	// This doesn't have the same problem because no dynamically loaded
	// code contains references to variables named __modinfo__.
	fmtprint(&cb.topfmt,
		"\n"
		"Modinfo __modinfo__ = { %#x, __init__, __run__, &__types__ };\n"
		"\n",
		ModinfoMagic);
	fmtfdflush(&cb.topfmt);
	close(fd);
	
	if(nerrors > 0)
		exit(1);
	
	zo = zeta2zo(zeta);
	snprint(buf, sizeof buf, 
		"gcc -m32 -ggdb -c -shared -Wall -Werror -Wno-unused"
		" -Wno-uninitialized"
		" -fno-optimize-sibling-calls -finline"
		" -I'%s' -I'%s/lib' -I'%s/obj'"
		" -o %so %s",
		PWD, PWD, PWD, zo, ctmp);
	if(system(buf) != 0)
		panic("gcc %s failed", ctmp);
	snprint(buf, sizeof buf,
		"gcc -m32 -ggdb -shared -o %s %so", zo, zo);
	if(system(buf) !=0)
		panic("gcc %so failed", zo);
	snprint(buf, sizeof buf, "%so", zo);
	unlink(buf);
	writemodule(zeta);
}

void
topstmt(Stmt *s)
{
	typecheckstmt(&tc, s);
	topstmtl = mkStmtL(s, topstmtl);
}

int nerrors;

Fnarg*
mkFnarg(Name name, Typex *type)
{
	Fnarg *a;
	
	a = emalloc(sizeof *a);
	a->name = name;
	a->typex = type;
	a->pos = yylastpos;
	return a;
}

Fn*
mkFn(Name name, FnargL *args, Typex *returntype, StmtL *body)
{
	Fn *fn;

	fn = emalloc(sizeof *fn);
	fn->name = name;
	fn->args = args;
	fn->xreturntype = returntype;
	fn->body = body;
	fn->pos = yylastpos;
	return fn;
}

Typex*
mkTypex(TypexOp op)
{
	Typex *x;
	
	x = emalloc(sizeof *x);
	x->op = op;
	x->pos = yylastpos;
	return x;
}

Typex*
mkTypex1(Type *t)
{
	Typex *x;
	
	x = mkTypex(TypexType);
	x->type = t;
	return x;
}

Structelem*
mkStructelem(void)
{
	Structelem *e;
	
	e = emalloc(sizeof(Structelem));
	e->pos = yylastpos;
	return e;
}

void
warn(Pos pos, char *fmt, ...)
{
	va_list arg;
	
	va_start(arg, fmt);
	fprint(2, "%P: warning: %V\n", pos, fmt, arg);
	va_end(arg);
}

void
error(Pos pos, char *fmt, ...)
{
	va_list arg;
	
	va_start(arg, fmt);
	fprint(2, "%P: error: %V\n", pos, fmt, arg);
	va_end(arg);
	nerrors++;
}

Scope*
pushscope(Typecheck *tc)
{
	Scope *s;
	
	s = emalloc(sizeof *s);
	s->outer = tc->scope;
	if(s->outer)
		s->frame = s->outer->frame;
	tc->scope = s;
	return s;
}

// Create a copy of the base scope.
Scope*
copyscope(Scope *base)
{
	Scope *s;
	
	s = emalloc(sizeof *s);
	s->outer = base->outer;
	s->frame = base->frame;
	s->varl = base->varl;
	return s;
}

int
gtypemax(GType *t1, GType *t2, GType **max)
{
	Type *t;
	
	if(typemax(typeinstantiate(t1), typeinstantiate(t2), &t) < 0)
		return -1;
	*max = typegeneralize(t);
	return 0;
}

// Add any new entries common to s1 and s2 to base.
void
mergescopes(Scope *s1, Scope *s2, Scope *base)
{
	VarL *vl1, *vl2, *new;
	Var *v1, *v2;
	GType *t;

	new = nil;
	for(vl1=s1->varl; vl1; vl1=vl1->tl){
		v1 = vl1->hd;
		for(vl2=s2->varl; vl2; vl2=vl2->tl){
			v2 = vl2->hd;
			if(v1->name == v2->name){
				if(v1 != v2){	// XXX how can v1 == v2 happen?  it does
					if(gtypemax(v1->type, v2->type, &t) < 0){
						error(v1->pos, "cannot join types %T and %T for %s",
							v1->type->left, v2->type->left, v1->name);
						continue;
					}
					v1->type = t;
					v2->type = t;
					v2->redirect = v1;
					v2->cname = nil;	// catch unredirected references
					// possible to handle, but i doubt it can happen
					if(v2->escape)
						panic("mergescopes");
				}
				v1->scope = base;
				new = mkVarL(v1, new);
				break;
			}
		}
	}

	// Append new variables, last first.
	for(; new; new=new->tl){
		v1 = new->hd;
		base->varl = mkVarL(v1, base->varl);
	}
}


void
popscope(Typecheck *tc, Scope *s)
{
	assert(tc->scope == s);
	tc->scope = s->outer;
}

Frame*
pushframe(Typecheck *tc)
{
	Frame *f;
	Scope *s;
	
	f = emalloc(sizeof *f);
	s = pushscope(tc);
	s->frame = f;
	if(s->outer && s->outer->frame)
		f->fn = s->outer->frame->fn;
	return f;
}

void
popframe(Typecheck *tc, Frame *f)
{
	Scope *s;
	
	s = tc->scope;
	assert(s);
	assert(s->frame == f);
	assert(s->outer == nil || s->outer->frame != f);
	popscope(tc, s);
}

void
pushfn(Typecheck *tc, Fn *fn)
{
	tc->fnl = mkFnL(tc->fn, tc->fnl);
	tc->fn = fn;
}

void
popfn(Typecheck *tc, Fn *fn)
{
	assert(tc->fn == fn);
	tc->fn = tc->fnl->hd;
	tc->fnl = tc->fnl->tl;
}


Expr*
mkExpr(ExprOp op)
{
	Expr *e;
	
	e = emalloc(sizeof *e);
	e->op = op;
	e->pos = yylastpos;
	e->l1 = -1;
	e->l2 = -1;
	return e;
}

Stmt*
mkStmt(StmtOp op)
{
	Stmt *s;
	
	s = emalloc(sizeof *s);
	s->op = op;
	s->pos = yylastpos;
	s->lcontinue = -1;
	s->lbreak = -1;
	s->label = -1;
	s->lfalse = -1;
	return s;
}

Name
csanitize(Name n)
{
	char *s, *p;
	
	s = estrdup(nametostr(n));
	for(p=s; *p; p++)
		if((*p < 'A' || *p > 'Z') &&
		   (*p < 'a' || *p > 'z') &&
		   (*p < '0' || *p > '9'))
			*p = '_';
	n = nameof(s);
	free(s);
	return n;
}

Name
findinclude(Pos pos, Name n)
{
	char *s, *p;
	
	pos2fileline(pos, &s);
	s = estrdup(s);
	if((p = strrchr(s, '/')) != nil){
		*(p+1) = 0;
		n = nameprint("%s%s", s, n);
	}
	free(s);
	if(access(nametostr(n), 0) < 0){
		error(pos, "%s does not exist", n);
		exit(1);
	}
	return n;
}

LISTIMPL(Expr)
LISTIMPL(Fn)
LISTIMPL(Fnarg)
LISTIMPL(Include)
LISTIMPL(Stmt)
LISTIMPL(Var)
LISTIMPL(Typex)
LISTIMPL(Structelem)
