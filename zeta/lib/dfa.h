// Caller must typedef DKey and DKeyL.

typedef struct DFA DFA;
typedef struct DState DState;
typedef struct DScratch DScratch;

struct DScratch;
struct DState;

struct DFA
{
	NFA *nfa;
	DScratch *tmp;
	DState *dead;
	DState *start;
	DState **hash;
	int nhash;
	int nstate;
	int dirty;
};

struct DScratch
{
	NState **nstate;
	int nnstate;
};

enum
{
	Eol = 256,
	Bol = 257
};
struct DState
{
	DState *next[257];
	int id;
	DState *hash;
	NState **nstate;
	int nnstate;
	int match;
	DKeyL *val;
};

DKeyL *rundfa(DFA*, char*, char**);
int	addtodfa(DFA*, char*, int, int, DKey*);
DFA *mkdfa(void);

