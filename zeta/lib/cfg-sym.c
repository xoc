#include "a.h"
typedef struct CfgValue CfgValue;
#include "cfg.h"
#include "bitset.h"
#include "listimpl.h"

static uint32
hash(char *str)
{
	/* http://www.cs.yorku.ca/~oz/hash.html */
	uint32 h;
	int c;

	h = 5381;
	for(; (c=(uchar)*str) != 0; str++)
		h = ((h << 5) + h) ^ c; /* hash * 33 ^ c */
	return h;
}

CfgSym*
cfgnewsym(CfgGram *g, Name name)
{
	int i, sz;
	CfgSym *s;
	
	if(name){
		for(i=0; i<g->nsym; i++){
			s = g->sym[i];
			if(s->name == name)
				return s;
		}
	}

	s = (CfgSym*)emalloc(sizeof *s);
	s->gram = g;
	s->n = g->nsym;
	if(name){
		s->name = name;
		s->code = hash(nametostr(name));
	}

	if(g->nsym%32 == 0){
		sz = sizeof g->sym[0];
		g->sym = (CfgSym**)erealloczero(g->sym, g->nsym*sz, (g->nsym+32)*sz);
	}
	g->sym[g->nsym++] = s;
	return s;
}

CfgSym*
cfglooksym(CfgGram *g, Name name)
{
	int i;

	if(name == nil)
		return nil;

	for(i=0; i<g->nsym; i++)
		if(g->sym[i]->name == name)
			return g->sym[i];
//	werrstr("unknown symbol %s", name);
//	if(g->debug)
//		fprint(2, "%r\n");
	return nil;
}
