#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include "a.h"

char*
readfile(char *s)
{
	struct stat st;
	char *text;
	int fd, n, total;
	
	if((fd = open(s, O_RDONLY)) < 0)
		return nil;
	if(fstat(fd, &st) < 0){
		close(fd);
		return nil;
	}
	text = emalloc(st.st_size+1);
	for(total=0; total<st.st_size; total+=n){
		if((n = read(fd, text+total, st.st_size-total)) <= 0){
			close(fd);
			free(text);
			return nil;
		}
	}
	close(fd);
	text[st.st_size] = 0;
	return text;
}

