#include "a.h"
#include "error.h"

Error estack[100];
Error *estackp = estack;

void
kaboom(char *fmt, ...)
{
	char buf[256];
	va_list arg;
	
	if((char*)estackp >= (char*)estack+sizeof estack){
		fprint(2, "error stack overflow\n");
		abort();
	}
	va_start(arg, fmt);
	vsnprint(buf, sizeof buf, fmt, arg);
	va_end(arg);
	fprint(2, "kaboom: %s\n", buf);
	nexterror();
}

void
nexterror(void)
{
	if(estackp <= estack){
		fprint(2, "error stack underflow\n");
		abort();
	}
	estackp--;
	longjmp(estackp->jb, 1);
}

void
poperror(void)
{
	if(estackp <= estack){
		fprint(2, "error stack underflow\n");
		abort();
	}
	estackp--;
}
