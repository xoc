
typedef struct Bitset Bitset;

struct Bitset
{
	int n;
	uint a[1];		/* really a[n] */
};

int	addbit(Bitset*, int);
int	addbitset(Bitset*, Bitset*);
int	countbitset(Bitset*);
void	freebitset(Bitset*);
int	inbitset(Bitset*, int);
Bitset*	newbitset(uint);
int	nextbitinset(Bitset*, int);
void	clearbitset(Bitset*);

