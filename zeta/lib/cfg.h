struct Bitset;
struct Zfn;
typedef struct CfgSym CfgSym;
typedef struct CfgRule CfgRule;
typedef struct CfgPrec CfgPrec;
typedef struct CfgGram CfgGram;
LIST(CfgPrec)

/* associativity */
typedef enum CfgAssoc
{
	CfgAssocLeft    = 1,
	CfgAssocNone    = 0,
	CfgAssocRight   = -1
} CfgAssoc;

/* precedence resolution kinds */
typedef enum CfgPrecKind
{
	CfgPrecPriority,      /* operator priority style resolution */
	CfgPrecPrefer,        /* rule preference resolution (like SDF's 'prefer') */
} CfgPrecKind;

typedef int CfgAction(CfgGram*, CfgRule*, CfgValue**, CfgValue**);

// A CfgGram is a grammar whose value stack has type CfgValue*.
struct CfgGram
{
	int     debug;
	CfgSym  *eofsym;

	/* internal use only */
	int     dirty;          /* needs compilation */
	uint    seqnum;         /* incremented on compilation */
	CfgSym  **sym;          /* all symbols */
	int     nsym;
	CfgRule **rule;         /* all rules */
	int     nrule;
	CfgPrec **prec;         /* all precedence levels */
	int     nprec;
	int     *precorder;     /* precedence relationship (2d matrix) */
	
	uint	mtime;		/* last modification time */

	/* extra for SLR */
	struct SlrState **slrstate;     /* all states */
	int             nslrstate;
	struct SlrState *slrstatehash[128];
	void (*slrfreestate)(struct SlrState*);

	/* extra for GLR */
	struct GlrState **glrstate;
	int             nglrstate;
	struct GlrState *glrstatehash[128];
	void (*glrfreestate)(struct GlrState*);
	
	struct Gram *ggram;
};

struct Gram;
/* terminal or non-terminal in grammar */
enum
{
	CfgSymEmpty = 1<<1      /* reduces to empty string */
};
struct CfgSym
{
	CfgGram *gram;          /* parent grammar module */
	Name    name;           /* name of symbol */
	int     n;              /* symbol number */
	int     flags;          /* CfgSymReduces, etc. above */
	CfgRule *emptyrule;	/* the reason flag&CfgSymEmpty */
	int	nocopy;	/* don't copy Asts with this symbol */
	uint	mtime;	/* last modification time */

	CfgRule **rule;         /* rules with symbol on left */
	int     nrule;
	CfgRule *parseme;

	struct Bitset   *first;         /* first terms of derived strings */
	struct Bitset   *follow;        /* follow set */
	struct Bitset   *leadsto;       /* reduces to (A =>* B) */
	int	leadnum;
	uint32     code;
	int     slotted;                /* has slot rule been added */
	int     reflags;                /* hacky: for sym copying */

	struct Gram *ggram;
	struct Zfn *canonical;
	int	used_canonical;
};

/* grammar production */
struct CfgRule
{
	int        n;
	CfgPrec    *prec;
	CfgSym     *left;       /* left hand side */
	CfgSym     **right;     /* right hand side */
	int        nright;
	int	nsavedright;
	int	nnullright;	/* number of right-hand elements before completely nullable */
	int        toplevel;
	CfgAction  *fn;         /* reduce fn */
	uint64        code;
	Name       fmtname;     /* name from cfgrulefmt */
	Name       name;        /* user-specified rule name, defaults to "%R" */

	struct Gram *ggram;
	struct Type *tgram;
};

/* grammar precedence level (partial-order) */
struct CfgPrec
{
	int         n;
	CfgGram     *g;
	CfgPrecKind kind;
	CfgAssoc   assoc;       /* only for kind == CfgPrecPriority */
	Name        name;
	CfgPrecL        *aboveme;    /* immediately above only */
	CfgPrecL       *belowme;    /* immediately below only */
};

void     cfgcompile(CfgGram*);
void     cfgfreegram(CfgGram*);

CfgGram* cfgnewgram(void);
CfgSym*  cfgnewsym(CfgGram*, Name);
CfgPrec* cfgnewprec(CfgGram*, Name, CfgPrecKind);

CfgSym*  cfglooksym(CfgGram*, Name);
CfgRule* cfglookrule(CfgGram*, char*);
CfgPrec* cfglookprec(CfgGram*, Name);

int      cfglinkprec(CfgPrec*, CfgPrec*);
int      cfgcompareprec(CfgPrec*, CfgPrec*);

int      cfgrulefmt(Fmt*);

CfgRule* cfgrule(CfgGram*, CfgAction*, CfgPrec*, CfgSym*, CfgSym**, int);
CfgRule* vcfgrulestr(CfgGram*, CfgAction*, Name, Name, Name, va_list);
CfgRule* vcfgrulesym(CfgGram*, CfgAction*, CfgPrec*, CfgSym*, CfgSym*, va_list);
CfgRule *cfgrulesym(CfgGram*, CfgAction*, CfgPrec*, CfgSym*, CfgSym*, ...);
