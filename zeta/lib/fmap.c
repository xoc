// Functional (persistent, immutable) map structure, based on 
// Okasaki, Red Black Trees in a Functional Setting, JFP 1999

#include "a.h"

enum
{
	Red = 0,
	Black = 1
};

// Red-black trees are binary trees with this property:
//	1. No red node has a red parent.
//	2. Every path from the root to a leaf contains the
//		same number of black nodes.

static Fmap*
mkFmap(int color, Fmap *left, void *key, void *value, Fmap *right)
{
	Fmap *p;
	
	p = emallocnz(sizeof *p);
	p->color = color;
	p->left = left;
	p->key = key;
	p->value = value;
	p->right = right;
	return p;
}

void*
fmaplookup(Fmap *p, void *k)
{
	for(;;){
		if(p == nil)
			return nil;
		if(p->key == k)
			return p->value;
		if(p->key < k)
			p = p->left;
		else
			p = p->right;
	}
}

static Fmap*
balance(int color, Fmap *left, void *key, void *value, Fmap *right)
{
	void *xk, *xv, *yk, *yv, *zk, *zv;
	Fmap *a, *b, *c, *d;

	// Okasaki notation: (T is mkFmap, B is Black, R is Red, x, y, z are key-value.
	//
	// balance B (T R (T R a x b) y c) z d 
	// balance B (T R a x (T R b y c)) z d
	// balance B a x (T R (T R b y c) z d)
	// balance B a x (T R b y (T R c z d))
	//
	//     = T R (T B a x b) y (T B c z d)
	
	if(color == Black){
		if(left && left->color == Red){
			if(left->left && left->left->color == Red){
				a = left->left->left;
				xk = left->left->key;
				xv = left->left->value;
				b = left->left->right;
				yk = left->key;
				yv = left->value;
				c = left->right;
				zk = key;
				zv = value;
				d = right;
				goto hard;
			}else if(left->right && left->right->color == Red){
				a = left->left;
				xk = left->key;
				xv = left->value;
				b = left->right->left;
				yk = left->right->key;
				yv = left->right->value;
				c = left->right->right;
				zk = key;
				zv = value;
				d = right;
				goto hard;
			}
		}else if(right && right->color == Red){
			if(right->left && right->left->color == Red){
				a = left;
				xk = key;
				xv = value;
				b = right->left->left;
				yk = right->left->key;
				yv = right->left->value;
				c = right->left->right;
				zk = right->key;
				zv = right->value;
				d = right->right;
				goto hard;
			}else if(right->right && right->right->color == Red){
				a = left;
				xk = key;
				xv = value;
				b = right->left;
				yk = right->key;
				yv = right->value;
				c = right->right->left;
				zk = right->right->key;
				zv = right->right->value;
				d = right->right->right;
				goto hard;
			}
		}
	}
	return mkFmap(color, left, key, value, right);

hard:
	return mkFmap(Red, mkFmap(Black, a, xk, xv, b), yk, yv, mkFmap(Black, c, zk, zv, d));
}

static Fmap*
ins(Fmap *p, void *k, void *v)
{
	if(p == nil)
		return mkFmap(Red, nil, k, v, nil);
	if(p->key == k)
		return mkFmap(p->color, p->left, k, v, p->right);
	if(p->key < k)
		return balance(p->color, ins(p->left, k, v), p->key, p->value, p->right);
	else
		return balance(p->color, p->left, p->key, p->value, ins(p->right, k, v));
}

Fmap*
fmapinsert(Fmap *p, void *k, void *v)
{
	p = ins(p, k, v);
	p->color = Black;	/* okay because p is new */
	return p;
}

static void
pushleft(Fmapiter *it, Fmap *p)
{
	for(; p; p=p->left){
		if(it->nstack == it->mstack){
			if(it->mstack == 0)
				it->mstack = 8;
			else
				it->mstack *= 2;
			it->stack = erealloc(it->stack, it->mstack*sizeof it->stack[0]);
		}
		it->stack[it->nstack++] = p;
	}
}

int
fmapiterstart(Fmapiter *it, Fmap *p)
{
	it->stack = nil;
	it->nstack = 0;
	it->mstack = 0;
	if(p == nil)
		return 0;
	pushleft(it, p);
	return 1;
}

int
fmapiternextkv(Fmapiter *it, Fmapkv *kv)
{
	Fmap *p;
	
	if(it->nstack == 0)
		return 0;
	p = it->stack[--it->nstack];
	pushleft(it, p->right);
	kv->key = p->key;
	kv->value = p->value;
	return 1;
}

int
fmapiternextkey(Fmapiter *it, void **kp)
{
	Fmapkv kv;
	
	if(!fmapiternextkv(it, &kv))
		return 0;
	*kp = kv.key;
	return 1;
}

void
fmapiterend(Fmapiter *it)
{
	free(it->stack);
	it->stack = nil;
}
