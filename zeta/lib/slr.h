/*
 * SLR parsing
 */

typedef struct SlrRulePos SlrRulePos;
typedef struct SlrState SlrState;
typedef struct SlrStack SlrStack;
typedef struct SlrParse SlrParse;
typedef struct SlrAction SlrAction;
typedef struct SlrReduce SlrReduce;

/* single position in a rule */
struct SlrRulePos
{
	CfgRule	*r;
	int	i;
};

/*
 * Simple LR
 */
struct SlrReduce
{
	CfgRule *rule;
	int nright;
};
struct SlrAction
{
	CfgSym *sym;
	SlrState *shiftstate;
	SlrReduce *reduce;
	int nreduce;
};

/* a single SLR state */
struct SlrState
{
	int	n;		/* state number */
	CfgGram	*g;		/* grammar */
	SlrRulePos	*pos;		/* list of positions */
	int	npos;
	int	compiled;	/* is action filled out? */
	SlrAction	*action;
	int	naction;

	SlrState	*next;		/* in hash table */
	SlrState	*newer;		/* newer version of same state */
	
	uint	synctime;	/* last sync time */
};

struct SlrStack
{
	SlrState	*slrstate;
	CfgSym	*sym;
	CfgValue	**val;
};

struct SlrParse
{
	CfgGram	*g;				/* grammar */
	uint		seqnum;			/* grammar sequence number */
	SlrStack	*stack;
	SlrStack	*estack;
	SlrStack	*sp;
	CfgSym	*tok;				/* lookahead token */
	CfgValue	*tokval;
	CfgValue	*tmpval;

	void	*aux;				/* for caller use */
};
typedef CfgSym*	SlrLexer(CfgGram*, CfgValue**, SlrLexerArg*);

SlrParse*	slrnewparse(void);
CfgValue*		slrparse(CfgGram *g, CfgSym *start, SlrParse *p0, SlrLexer *slrlex, SlrLexerArg *arg);
// XXX slrfreeparse
int		slrstatefmt(Fmt *fmt);

// Internal use only.
SlrState*	slrcanonstate(CfgGram *g, SlrState *z);
void		slrcheckstate(SlrState *z);
int		slrcompile(CfgGram *g);
int		slrcompilestate(CfgGram *g, SlrState *z);
int		slrcompilestates(CfgGram *g);
SlrState*	slrcomputeshift(CfgGram *g, SlrState *z, CfgSym *s);
void		slrdumptables(CfgGram *g);
void		slrerror(CfgGram *g, SlrParse *p, char *msg);
void		slrfreestate(SlrState *z);
SlrState*	slrinitstate(CfgGram *g, CfgSym *start);
int		slrstatecmp(SlrState *z, SlrState *y);
SlrState*	slrsyncstate(CfgGram *g, SlrState *z);
SlrAction*	slraction(SlrState*, CfgSym*);
