/*
 * CFG CfgGram management.
 */

#include "a.h"
typedef struct CfgValue CfgValue;
#include "cfg.h"
#include "bitset.h"
#include "listimpl.h"

int cfgrules = 0;

static uint64
hash64(uint64 key)
{
	/* derived from http://www.concentric.net/~Ttwang/tech/inthash.htm */
	key += ~(key << 32);
	key ^= (key >> 22);
	key += ~(key << 13);
	key ^= (key >> 8);
	key += (key << 3);
	key ^= (key >> 15);
	key += ~(key << 27);
	key ^= (key >> 31);
	return key;
}
static uint64
hash2(uint32 h, uint32 i)
{
	return hash64(((uint64)h<<32)|i);
}

/*
 * Add a rule left -> right0 ...
 * The right hand side symbol list must be terminated by a null pointer.
 * Return the rule.
 */
CfgRule*
vcfgrulesym(CfgGram *g, CfgAction *fn, CfgPrec *prec,
            CfgSym *left, CfgSym *right0, va_list arg)
{
	int i, n;
	CfgSym **right;
	CfgRule *r;

	if(right0 == nil)
		n = 0;
	else{
		va_list tmp;
		va_copy(tmp, arg);
		for(n=1;; n++)
			if(va_arg(tmp, CfgSym*) == nil)
				break;
		va_end(tmp);
	}
	right = (CfgSym**)emalloc((n+1)*sizeof right[0]);
	right[0] = right0;
	for(i=1; i<n; i++)
		right[i] = va_arg(arg, CfgSym*);
	r = cfgrule(g, fn, prec, left, right, n);
	free(right);
	return r;
}

/*
 * Add a rule leftstr -> right0 ...
 * The right hand side string list must be terminated by a null pointer.
 * Return the rule.
 */
CfgRule*
cfgrulestr(CfgGram *g, CfgAction *fn, Name precstr, Name leftstr, Name right0, ...)
{
	Name p;
	int i, n;
	va_list arg;
	CfgSym *left;
	CfgSym **right;
	CfgPrec *prec;
	CfgRule *r;

	hash2(0, 0);
	
	left = cfglooksym(g, leftstr);
	if(left == nil)
		return nil;
	if(right0 == nil)
		n = 0;
	else{
		va_start(arg, right0);
		for(n=1;; n++)
			if(va_arg(arg, Name) == nil)
				break;
		va_end(arg);
	}
	if(precstr){
		prec = cfglookprec(g, precstr);
		fprint(2, "bad prec %s\n", precstr);
		if(prec == nil)
			return nil;
	}else
		prec = nil;
	right = (CfgSym**)emalloc((n+1)*sizeof right[0]);
	if(right0 == nil)
		right[0] = nil;
	else{
		right[0] = cfglooksym(g, right0);
		if(right[0] == nil){
			fprint(2, "bad right0 %s\n", right0);
		Free:
			free(right);
			return nil;
		}
		va_start(arg, right0);
		for(i=1; i<n; i++){
			right[i] = cfglooksym(g, p=va_arg(arg, Name));
			if(right[i] == nil){
				fprint(2, "bad right%d: %s\n", i, p);
				goto Free;
			}
		}
		va_end(arg);
		right[i] = nil;
	}
	r = cfgrule(g, fn, prec, left, right, n);
	free(right);
	return r;
}

/*
 * Add a new rule left -> right[0] ... right[nright-1].
 * Return the rule.
 * The precedence and associativity of the new rule are taken from prec,
 * if given, or else from the first sym in the right[] array that has
 * a precedence value.
 */
CfgRule*
cfgrule(CfgGram *g, CfgAction *fn, CfgPrec *prec,
        CfgSym *left, CfgSym **right, int nright)
{
	int i, n, sz;
	CfgRule *r, *oldr;

	r = (CfgRule*)emalloc(sizeof *r + nright*sizeof right[0]);
	r->n = g->nrule;
	r->fn = fn;
	r->left = left;
	r->right = (CfgSym**)(r+1);
	memmove(r->right, right, nright*sizeof right[0]);
	r->nright = nright;
	r->prec = prec;
	r->code = left->code;

	n = 0;
	for(i=0; i<nright; i++){
		if(nametostr(right[i]->name)[0] != '"')
			n++;
		r->code = hash2(r->code, right[i]->code);
	}
	r->nsavedright = n;

	// Ignore duplicate rules.
	r->fmtname = nameprint("%R", r);
	r->name = r->fmtname;
	if((oldr = cfglookrule(g, nametostr(r->fmtname))) != nil){
		free(r);
		return oldr;
	}

	if(g->nrule%32 == 0){
		sz = sizeof g->rule[0];
		g->rule = (CfgRule**)erealloczero(g->rule, g->nrule*sz, (g->nrule+32)*sz);
	}
	if(left->nrule%32 == 0){
		sz = sizeof left->rule[0];
		left->rule = (CfgRule**)erealloczero(left->rule, left->nrule*sz, (left->nrule+32)*sz);
	}
	g->rule[g->nrule++] = r;
	left->rule[left->nrule++] = r;
	g->dirty = 1;
	left->mtime = ++g->mtime;
	
	if(cfgrules)
		fprint(2, "Rule: %R\n", r);
	return r;
}

/*
 * Print a rule for formatting.  
 * If invoked with a precision (e.g., %.4R),
 * prints a dot after that item in the rule.
 */
int
cfgrulefmt(Fmt *fmt)
{
	int i, dot;
	CfgRule *r;

	dot = -1;
	if(fmt->flags&FmtPrec){
		dot = fmt->prec;
		fmt->flags &= ~FmtPrec;
		fmt->prec = 0;
	}
	r = va_arg(fmt->args, CfgRule*);
	if(r == nil)
		return fmtprint(fmt, "<nil>");
	if(dot == -1 && r->fmtname){
		fmtstrcpy(fmt, nametostr(r->fmtname));
		return 0;
	}
	fmtprint(fmt, "%s =>", r->left->name);
	for(i=0; i<r->nright; i++){
		if(i == dot)
			fmtprint(fmt, " .");
		fmtprint(fmt, " %s", r->right[i]->name);
	}
	if(i == dot)
		fmtprint(fmt, " .");
	return 0;
}

/* Lookup a rule by fmtname */
CfgRule*
cfglookrule(CfgGram *g, char *fmtname)
{
	int i;
	Name x;

	if(!fmtname)
		return nil;
	x = nameof(fmtname);

	for(i=0; i<g->nrule; i++){
		if(g->rule[i]->fmtname == x)
			return g->rule[i];
	}
	return nil;
}

/* Lookup a precedence level by name */
CfgPrec*
cfglookprec(CfgGram *g, Name name)
{
	int i;

	if(!name)
		return nil;
	for(i=0; i<g->nprec; i++){
		if(g->prec[i]->name == name)
			return g->prec[i];
	}
	return nil;
}

/* Add a new precedence level */
CfgPrec*
cfgnewprec(CfgGram *g, Name name, CfgPrecKind kind)
{
	CfgPrec *p;
	int sz, old, neww, i, *pp;

	if(name == nil)
		return nil;
	if((p = cfglookprec(g, name)) != nil){
		if(p->kind != kind)
			return nil;
		return p;
	}
	p = (CfgPrec*)emalloc(sizeof *p);
	p->n = g->nprec;
	p->g = g;
	p->kind = kind;
	p->name = name;

	if(g->nprec%32 == 0){
		sz = sizeof g->prec[0];
		g->prec = (CfgPrec**)erealloczero(g->prec, g->nprec*sz, (g->nprec+32)*sz);
		// Need to copy prec table manually, since it's a 2x2 matrix
		sz = sizeof g->precorder[0];
		old = g->nprec;
		neww = g->nprec + 32;
		pp = g->precorder;
		g->precorder = (int*)emalloc(neww*neww*sz);
		for(i=0; i < old; i++)
			memmove(&g->precorder[neww*i], &pp[old*i], old*sz);
		free(pp);
	}
	g->prec[g->nprec++] = p;
	return p;
}

/*
 * Update the transitive closure of the precedence ordering.
 * Return zero if there is a conflict.
 */
static int
cfgupdateprec1(CfgGram *g, int hi, int lo)
{
	CfgPrec *hiprec, *loprec;
	int cur, w;

	// An entry in the predecence table [a:row,b:col] means:
	//  -1  : a < b
	//   0  : unknown
	//   1  : a > b
	// The table is kept symmetric.  There is no way to specify
	// 'equal' vs 'unknown' (currently that doesn't matter).
	w = g->nprec + 32 - g->nprec%32;
	// Check for a cycle.
	cur = g->precorder[hi*w + lo];
	if(cur < 0){
		hiprec = g->prec[hi];
		loprec = g->prec[lo];
		fprint(2, "cfg precedence cycle at '%s' < '%s'\n", hiprec->name, loprec->name);
		return 0;
	}
	// Update this cell.
	g->precorder[hi*w + lo] = 1;
	g->precorder[lo*w + hi] = -1;
	return 1;
}

static int
cfgupdateprec_closure(CfgGram *g, int hi, int lo)
{
	int w, i, k, n;
	int a, b;

	// Add a new link.
	cfgupdateprec1(g, hi, lo);
	// Recompute the transitive closure.
	w = g->nprec + 32 - g->nprec%32;
	for(n=0; n < g->nprec; n++){
		for(i=0; i < g->nprec; i++){
			for(k=i+1; k < g->nprec; k++){
				a = g->precorder[i*w + n];  // (i > n)?
				b = g->precorder[n*w + k];  // (n > k)?
				if(a == b && a > 0){
					if(!cfgupdateprec1(g, i, k))
						return 0;
				}
				if(a == b && a < 0){
					if(!cfgupdateprec1(g, k, i))
						return 0;
				}
			}
		}
	}
	return 1;
}

static int
findCfgPrecL(CfgPrecL *l, CfgPrec *a)
{
	for(; l; l=l->tl)
		if(l->hd == a)
			return 1;
	return 0;
}

/*
 * Add a new link in the grammar's partial-order precedence
 * Recompute the precedence table, and return zero if there
 * is a conflict.
 */
int
cfglinkprec(CfgPrec *hi, CfgPrec *lo)
{
	if(!hi || !lo)
		return 1;
	assert(hi->g    == lo->g);
	assert(hi->kind == lo->kind);

	if(findCfgPrecL(lo->aboveme, hi))
		assert(findCfgPrecL(hi->belowme, lo));
	else{
		lo->aboveme = mkCfgPrecL(hi, lo->aboveme);
		hi->belowme = mkCfgPrecL(lo, hi->belowme);
	}
	if(!cfgupdateprec_closure(hi->g, hi->n, lo->n))
		return 0;
	return 1;
}

/*
 * Determine if 'hi > lo'.
 * Return 1 for yes, 0 for unknown/equal, and -1 if 'lo < hi'.
 */
int
cfgcompareprec(CfgPrec *hi, CfgPrec *lo)
{
	CfgGram *g;
	int w;

	if(!hi || !lo || hi == lo)
		return 0;
	assert(hi->g    == lo->g);
	assert(hi->kind == lo->kind);
	g = hi->g;
	w = g->nprec + 32 - g->nprec%32;
	return g->precorder[hi->n * w + lo->n];
}

CfgRule*
cfgrulesym(CfgGram *g, CfgAction *action, CfgPrec *prec,  CfgSym *left, CfgSym *right0, ...)
{
	va_list arg;
	
	va_start(arg, right0);
	CfgRule *r = vcfgrulesym(g, action, prec, left, right0, arg);
	va_end(arg);
	return r;
}
