// Mutable map structure, but still based on 
// Okasaki, Red Black Trees in a Functional Setting, JFP 1999,
// which is a lot easier than the traditional red-black
// and plenty fast enough for me.  (Also I could copy
// and edit fmap.c.)

#include "a.h"

enum
{
	Red = 0,
	Black = 1
};


// Red-black trees are binary trees with this property:
//	1. No red node has a red parent.
//	2. Every path from the root to a leaf contains the
//		same number of black nodes.

static MapElem*
rwMapElem(MapElem *p, int color, MapElem *left, void *key, void *value, MapElem *right)
{
	if(p == nil)
		p = emallocnz(sizeof *p);
	p->color = color;
	p->left = left;
	p->key = key;
	p->value = value;
	p->right = right;
	return p;
}

static MapElem*
balance(MapElem *m0)
{
	void *xk, *xv, *yk, *yv, *zk, *zv;
	MapElem *a, *b, *c, *d;
	MapElem *m1, *m2;
	int color;
	MapElem *left, *right;
	void *key, *value;
	
	color = m0->color;
	left = m0->left;
	key = m0->key;
	value = m0->value;
	right = m0->right;

	// Okasaki notation: (T is mkMapElem, B is Black, R is Red, x, y, z are key-value.
	//
	// balance B (T R (T R a x b) y c) z d 
	// balance B (T R a x (T R b y c)) z d
	// balance B a x (T R (T R b y c) z d)
	// balance B a x (T R b y (T R c z d))
	//
	//     = T R (T B a x b) y (T B c z d)
	
	if(color == Black){
		if(left && left->color == Red){
			if(left->left && left->left->color == Red){
				a = left->left->left;
				xk = left->left->key;
				xv = left->left->value;
				b = left->left->right;
				yk = left->key;
				yv = left->value;
				c = left->right;
				zk = key;
				zv = value;
				d = right;
				m1 = left;
				m2 = left->left;
				goto hard;
			}else if(left->right && left->right->color == Red){
				a = left->left;
				xk = left->key;
				xv = left->value;
				b = left->right->left;
				yk = left->right->key;
				yv = left->right->value;
				c = left->right->right;
				zk = key;
				zv = value;
				d = right;
				m1 = left;
				m2 = left->right;
				goto hard;
			}
		}else if(right && right->color == Red){
			if(right->left && right->left->color == Red){
				a = left;
				xk = key;
				xv = value;
				b = right->left->left;
				yk = right->left->key;
				yv = right->left->value;
				c = right->left->right;
				zk = right->key;
				zv = right->value;
				d = right->right;
				m1 = right;
				m2 = right->left;
				goto hard;
			}else if(right->right && right->right->color == Red){
				a = left;
				xk = key;
				xv = value;
				b = right->left;
				yk = right->key;
				yv = right->value;
				c = right->right->left;
				zk = right->right->key;
				zv = right->right->value;
				d = right->right->right;
				m1 = right;
				m2 = right->right;
				goto hard;
			}
		}
	}
	return rwMapElem(m0, color, left, key, value, right);

hard:
	return rwMapElem(m0, Red, rwMapElem(m1, Black, a, xk, xv, b), 
		yk, yv, rwMapElem(m2, Black, c, zk, zv, d));
}

static MapElem*
ins0(MapElem *p, void *k, void *v, MapElem *rw)
{
	if(p == nil)
		return rwMapElem(rw, Red, nil, k, v, nil);
	if(p->key == k){
		if(rw)
			return rwMapElem(rw, p->color, p->left, k, v, p->right);
		p->value = v;
		return p;
	}
	if(p->key < k)
		p->left = ins0(p->left, k, v, rw);
	else
		p->right = ins0(p->right, k, v, rw);
	return balance(p);
}

static MapElem*
ins1(Map *m, MapElem *p, void *k, void *v, MapElem *rw)
{
	int i;

	if(p == nil)
		return rwMapElem(rw, Red, nil, k, v, nil);
	i = m->cmp(p->key, k);
	if(i == 0){
		if(rw)
			return rwMapElem(rw, p->color, p->left, k, v, p->right);
		p->value = v;
		return p;
	}
	if(i < 0)
		p->left = ins1(m, p->left, k, v, rw);
	else
		p->right = ins1(m, p->right, k, v, rw);
	return balance(p);
}

void
mapputelem(Map *m, void *key, void *val, MapElem *rw)
{
	if(m->cmp)
		m->root = ins1(m, m->root, key, val, rw);
	else
		m->root = ins0(m->root, key, val, rw);
}

void
mapput(Map *m, void *key, void *val)
{
	mapputelem(m, key, val, nil);
}

void*
mapget(Map *m, void *key)
{
	int i;
	MapElem *p;

	p = m->root;
	if(m->cmp){
		for(;;){
			if(p == nil)
				return nil;
			i = m->cmp(p->key, key);
			if(i < 0)
				p = p->left;
			else if(i > 0)
				p = p->right;
			else
				return p->value;
		}
	}else{
		for(;;){
			if(p == nil)
				return nil;
			if(p->key == key)
				return p->value;
			if(p->key < key)
				p = p->left;
			else
				p = p->right;
		}
	}
}
