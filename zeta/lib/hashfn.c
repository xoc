#include "u.h"
#include "hashfn.h"

static uint64
llhash(uint64 key)
{
	/* derived from http://www.concentric.net/~Ttwang/tech/inthash.htm */
	key += ~(key << 32);
	key ^= (key >> 22);
	key += ~(key << 13);
	key ^= (key >> 8);
	key += (key << 3);
	key ^= (key >> 15);
	key += ~(key << 27);
	key ^= (key >> 31);
	return key;
}

uint32
hash32(uint32 h, uint32 i)
{
	return llhash(((uint64)h<<32)|i);
}
