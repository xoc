#include <setjmp.h>

typedef struct Error Error;

struct Error
{
	jmp_buf jb;
};

#define waserror() setjmp((estackp++)->jb)
void nexterror(void);
void poperror(void);

extern Error estack[100];
extern Error *estackp;
void	kaboom(char*, ...);

