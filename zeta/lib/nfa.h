typedef struct Arrow Arrow;
typedef struct NFA NFA;
typedef struct NState NState;

enum
{
	ANone,
	ABol,
	AEol,
	AEmpty,
	ARune,
	AByte,
	ANever
};
struct Arrow
{
	int op;
	NState *z;
	int lo, hi;
};

struct NFA
{
	NState *start;
	NState *end;
	NFA *next;
	int gen;
	uint nnstate;
};

struct NState
{
	int id;
	int gen;
	int flags;
	Arrow *arrow;
	int narrow;
	Arrow *rarrow;
	int nrarrow;
	NState *in1;
	DKey *val;
	int valprec;
};

enum
{
	RegexpEgrep,
	RegexpLiteral,
	
	RegexpShortest = 0x10,
	RegexpAmbiguous = 0x20,
};

void freenfa(NFA*);
NFA	*parseregexp(char *regexp, int mode);
NFA	*unionnfa(NFA *n1, NFA *n2);

