#include "a.h"
#include "bitset.h"

enum {
	BitW = 8*sizeof(uint)
};

/* Allocate a new set capable of holding values 0..n-1 */
Bitset*
newbitset(uint n)
{
	Bitset *s;
	int nw;

	nw = (n+BitW-1)/BitW;
	s = (Bitset*)emalloc(sizeof(Bitset)+(nw-1)*sizeof(uint));
	s->n = nw;
	return s;
}

void
clearbitset(Bitset *s)
{
	if(s)
		memset(s->a, 0, s->n*sizeof s->a[0]);
}

/* Free a set. */
void
freebitset(Bitset *s)
{
	free(s);
}

/* Count bits in b. */
static int
bits(uint b)
{
	uint n;

	/* overkill */
	n = b;
	n = (n&0x55555555)+((n&0xAAAAAAAA)>>1);
	n = (n&0x33333333)+((n&0xCCCCCCCC)>>2);
	n = (n&0x0F0F0F0F)+((n&0xF0F0F0F0)>>4);
	n = (n&0x00FF00FF)+((n&0xFF00FF00)>>8);
	n = (n&0x0000FFFF)+((n&0xFFFF0000)>>16);
	return n;
}

/* Count bits in set. */
int
countbitset(Bitset *s)
{
	int i, n;

	assert(s != nil);
	n = 0;
	for(i=0; i<s->n; i++)
		n += bits(s->a[i]);
	return n;
}

/* Add the bits from t to s.  Returns whether any bits were new. */
int
addbitset(Bitset *s, Bitset *t)
{
	int i, did;

	assert(s != nil);
	assert(t != nil);
	assert(t->n <= s->n);
	did = 0;
	for(i=0; i<t->n; i++){
		if(~s->a[i] & t->a[i])
			did = 1;
		s->a[i] |= t->a[i];
	}
	return did;
}

/* Check whether x is in the bit set. */
int
inbitset(Bitset *s, int x)
{
	assert(s != nil);
	if(x < 0 || x >= s->n*BitW)
		return 0;
	return s->a[x/BitW] & (1<<(x%BitW));
}

/* Add x to the bit set.  Returns whether bit was new. */
int
addbit(Bitset *s, int x)
{
	int b;
	
	assert(s != nil);
	assert(0 <= x && x < s->n*BitW);
	b = 1<<(x%BitW);
	if(s->a[x/BitW] & b)
		return 0;
	s->a[x/BitW] |= b;
	return 1;
}

/* Return the next bit >= i in the set. */
int
nextbitinset(Bitset *s, int i)
{
	if(i>=0)
		for(; i<s->n*BitW; i++)
			if(s->a[i/BitW]&(1<<(i%BitW)))
				return i;
	return -1;
}

