#include "a.h"
typedef struct CfgValue CfgValue;
#include "cfg.h"
typedef struct SlrLexerArg SlrLexerArg;
#include "slr.h"
#include "bitset.h"

enum {
	BigHammer = 0	// Whether to clear entire SLR state at grammar change
};

int
slrcompile(CfgGram *g)
{
	int i;

	if(g->dirty == 0)
		return 0;

	cfgcompile(g);

	if(BigHammer){
		for(i=0; i<g->nslrstate; i++)
			slrfreestate(g->slrstate[i]);
		free(g->slrstate);
		g->slrfreestate = slrfreestate;
		g->slrstate = nil;
		g->nslrstate = 0;
		memset(g->slrstatehash, 0, sizeof g->slrstatehash);
	}

	g->seqnum++;
	g->dirty = 0;
	return 1;
}

int
slrcompilestates(CfgGram *g)
{
	int i;
	SlrState *z;
	
	slrcompile(g);

	if(slrinitstate(g, nil) == nil)
		return -1;
	for(i=0; i<g->nslrstate; i++){
		z = g->slrstate[i];
		if(!z->compiled)
			slrcompilestate(g, z);
	}
	return 0;
}

extern int addtostate(SlrState*, CfgRule*, int);

int
slrcompilestate(CfgGram *g, SlrState *z)
{
	int i, j, k, si, n;
	SlrRulePos *p, *q;
	CfgSym *sym;
	SlrRulePos **shift, **reduce;
	int nshift, nreduce;
	SlrAction *action, *a;
	SlrState *y;
	SlrReduce *reduceblock;

assert(!z->compiled);
//	g->stats.slrcompilestate++;
	if(z->action){
		free(z->action);
		z->action = nil;
		z->naction = 0;
	}

	nreduce = 0;
	for(i=0; i<z->npos; i++){
		p = &z->pos[i];
		if(p->i >= p->r->nnullright)
			nreduce++;
	}
	n = g->nsym+1;

	/* Go through each symbol, building shift/reduce action for that symbol. */
	shift = emalloc(z->npos*sizeof shift[0]);
	reduce = emalloc(z->npos*sizeof reduce[0]);
	action = emalloc(n*sizeof action[0]+n*nreduce*sizeof reduceblock[0]);
	reduceblock = (void*)(action+n);
	si = 0;
	for(k=0; k<n; k++){
		if(k == g->nsym)
			sym = nil;
		else
			sym = g->sym[k];

		nshift = 0;
		nreduce = 0;
		for(i=0; i<z->npos; i++){
			p = &z->pos[i];
			if(p->i < p->r->nright
			&& p->r->right[p->i] == sym)
				shift[nshift++] = p;
			if(p->i >= p->r->nnullright
			&& (sym == nil || inbitset(p->r->left->follow, sym->n)))
				reduce[nreduce++] = p;
		}

		/* Apply precedence to eliminate some possibilities. */
		for(i=0; i<nshift; i++)
		for(j=0; j<nreduce; j++){
			p = shift[i];
			q = reduce[j];
			if(p && q
			&& p->r->prec
			&& q->r->prec
			&& p->r->prec->kind == CfgPrecPriority
			&& q->r->prec->kind == CfgPrecPriority){
				switch(cfgcompareprec(p->r->prec, q->r->prec)){
				case -1:	// q wins
					shift[i] = nil;
					break;
					
				case 1:	// p wins
					reduce[j] = nil;
					break;
	
				case 0:
					if(p->r->prec == q->r->prec){
						switch(p->r->prec->assoc){
						case CfgAssocLeft:
							// reduce wins
							shift[i] = nil;
							break;
						case CfgAssocRight:
							// shift wins
							reduce[j] = nil;
							break;
						case CfgAssocNone:
							// no one wins
							// TODO: should we disable shift *and* reduce?
							// shift[i] = nil;
							// reduce[j] = nil;
							break;
						}
					}
					break;
				}
			}
		}
		
		/* Get rid of the nils now in the arrays */
		for(i=0; i<nshift; ){
			if(shift[i] == nil)
				shift[i] = shift[--nshift];
			else
				i++;
		}
		for(i=0; i<nreduce; ){
			if(reduce[i] == nil)
				reduce[i] = reduce[--nreduce];
			else
				i++;
		}
		
		a = &action[k];
		a->sym = sym;
		if(nshift > 0){
			y = emalloc(sizeof *y);
			y->g = z->g;
			for(i=0; i<nshift; i++){
				p = shift[i];
				if(addtostate(y, p->r, p->i+1) < 0)
					panic("addtostate");
			}
			y = slrcanonstate(g, y);
			a->shiftstate = y;
		}
		if(nreduce > 0){
			a->reduce = reduceblock;
			reduceblock += nreduce;
			a->nreduce = nreduce;
			for(i=0; i<nreduce; i++){
				a->reduce[i].rule = reduce[i]->r;
				a->reduce[i].nright = reduce[i]->i;
			}
		}
	}
	free(shift);
	free(reduce);
	z->action = action;
	z->naction = n;
	z->compiled = 1;
	return 0;
}

SlrAction*
slraction(SlrState *z, CfgSym *sym)
{
	if(!z->compiled)
		slrcompilestate(z->g, z);
	if(sym)
		return &z->action[sym->n];
	return &z->action[z->naction-1];
}

void
mkallparseme(CfgGram *g)
{
	int i;
	CfgSym *s, *s0;
	CfgRule *r;

	for(i=0; i<g->nsym; i++){
		s = g->sym[i];
		if(s->parseme == nil && nametostr(s->name)[0] != '$'){
			s0 = cfgnewsym(g, nameprint("$%s", s->name));
			r = cfgrulesym(g, nil, nil, s0, s, cfglooksym(g, N("EOF")), nil);
			assert(r);
			assert(s0->rule);
			assert(r == s0->rule[0]);
			s->parseme = r;
			s->parseme->toplevel = 1;
		}
	}
}

/*
 * on-the-fly compilation
 */
SlrState*
slrinitstate(CfgGram *g, CfgSym *start)
{
	SlrState *z;
	CfgSym *s, *s0;
	CfgRule *r;
	
	if(start){
		s = start;
	}else{
		if(g->nrule == 0){
		//	werrstr("no rules");
			return nil;
		}
		s = g->rule[0]->left;
	}
	
	if(s->parseme == nil){
		s0 = cfgnewsym(g, nameprint("$%s", s->name));
		r = cfgrulesym(g, nil, nil, s0, s, cfglooksym(g, N("EOF")), nil);
		assert(r);
		s->parseme = r;
		s->parseme->toplevel = 1;
		mkallparseme(g);	// avoid lots of extra compiling
		slrcompile(g);
	}
	
	z = (SlrState*)emalloc(sizeof *z);
	z->g = g;
	z->pos = (SlrRulePos*)emalloc(32*sizeof z->pos[0]);
	z->pos[0].r = s->parseme;
	z->npos = 1;
//	fprint(2, "state0 %s(%d; %d) %lY\n", s->name, s->nrule, z->npos, z);
	z = slrcanonstate(g, z);
//	fprint(2, "canonstate %lY\n", z);
	return z;
}
