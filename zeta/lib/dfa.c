/*
 * Regular expression search.  Reimplementation of Plan 9 grep.
 * Same basic ideas, completely new code.
 *
 * Build NFA and run cached subset construction on the fly
 * during execution.  Also translate UTF-8 expressions into
 * corresponding byte sequences so that machine can run
 * byte-at-a-time.
 *
 * Not as pretty as I would like.
 *
 * Russ Cox, February 2006
 */

#include "a.h"

typedef struct DKey DKey;
LIST(DKey)

#include "nfa.h"
#include "dfa.h"
#include "listimpl.h"

LISTIMPL(DKey)

enum
{
	FlagMatch = 1,
	FlagStop = 2,
};

static DScratch*
mkscratch(NFA *m)
{
	DScratch *s;
	
//fprint(2, "mkscratch %d\n", m->nnstate);
	s = (DScratch*)emalloc(sizeof *s + m->nnstate*sizeof s->nstate[0]);
	s->nstate = (NState**)(s+1);
	s->nnstate = 0;
	return s;
}

static void
walk0(DScratch *tmp, uint gen, NState *z, int also)
{
	int i;

	if(z->gen == gen)
		return;
	z->gen = gen;
	tmp->nstate[tmp->nnstate++] = z;
	for(i=0; i<z->narrow; i++)
		if(z->arrow[i].op == AEmpty || (also && z->arrow[i].op == also))
			walk0(tmp, gen, z->arrow[i].z, also);
}

static void
walk(DScratch *tmp, NFA *m, NState **z, int nz, int also)
{
	int i;
	
	memset(tmp, 0, sizeof *tmp);
	tmp->nstate = (NState**)(tmp+1);
	m->gen++;
	for(i=0; i<nz; i++)
		walk0(tmp, m->gen, z[i], also);
}

static void
walkbyte(DScratch *tmp, NFA *m, NState **z, int nz, int x)
{
	int i, j;
	
	memset(tmp, 0, sizeof *tmp);
	tmp->nstate = (NState**)(tmp+1);
	x = (uchar)x;
	m->gen++;
	for(i=0; i<nz; i++)
		for(j=0; j<z[i]->narrow; j++)
			if(z[i]->arrow[j].op == AByte && z[i]->arrow[j].lo <= x && x <= z[i]->arrow[j].hi)
				walk0(tmp, m->gen, z[i]->arrow[j].z, x=='\n' ? AEol : 0);
}

static uint
canonhash(DScratch *tmp)
{
	int i;
	uint p;
	
	p = 10007;
	for(i=0; i<tmp->nnstate; i++)
		p = (p*127+tmp->nstate[i]->id)%100000007;
	return p;
}

static DState*
canonical(DFA *d, DScratch *tmp)
{
	int i, h, size;
	DState *s;

	if(tmp->nnstate == 0)
		return d->dead;
// TODO: Shouldn't tmp->nstate get sorted before we go much further?
	h = canonhash(tmp)%d->nhash;
	size = tmp->nnstate*sizeof tmp->nstate[0];
	for(s=d->hash[h]; s; s=s->hash)
		if(s->nnstate == tmp->nnstate && memcmp(s->nstate, tmp->nstate, size) == 0)
			return s;
	s = (DState*)emalloc(size+sizeof *s);
	s->id = ++d->nstate;
	s->nstate = (NState**)(s+1);
	memmove(s->nstate, tmp->nstate, size);
	memset(s->next, 0, sizeof s->next);
	s->nnstate = tmp->nnstate;
	s->match = 0;
	s->val = nil;

	for(i=0; i<s->nnstate; i++){
		if(s->nstate[i] == d->nfa->end || s->nstate[i]->val)
			s->match |= FlagMatch;
		if(s->nstate[i]->flags&RegexpShortest)
			s->match |= FlagStop;
	}
	s->hash = d->hash[h];
	d->hash[h] = s;
	return s;
}

static void
dfainit(DFA *d)
{
	DState *s;
	
	if(d->dead == nil){
		s = (DState*)emalloc(sizeof *s);
		memset(s, 0, sizeof *s);
		s->id = -1;
		d->dead = s;
	}
	free(d->tmp);
	d->tmp = nil;
	d->tmp = mkscratch(d->nfa);
	walk(d->tmp, d->nfa, &d->nfa->start, 1, 0);
	if(d->tmp->nnstate > d->nfa->nnstate)
		abort();
	d->start = canonical(d, d->tmp);
}

static void
dfareset(DFA *d)
{
	int i;
	DState *s, *next;

	d->dirty = 0;
	for(i=0; i<d->nhash; i++){
		for(s=d->hash[i]; s; s=next){
			next = s->hash;
			free(s);
		}
		d->hash[i] = nil;
	}
	d->start = nil;
	d->nstate = 0;
	dfainit(d);
}

static DState*
walkstate(DFA *d, DState *s, int c)
{
	if(c == Eol)
		walk(d->tmp, d->nfa, s->nstate, s->nnstate, AEol);
	else if(c == Bol)
		walk(d->tmp, d->nfa, s->nstate, s->nnstate, ABol);
	else
		walkbyte(d->tmp, d->nfa, s->nstate, s->nnstate, c);
	if(d->tmp->nnstate > d->nfa->nnstate)
		abort();
	return canonical(d, d->tmp);
}


/*
 * Public interface for lexer.
 */
DFA*
mkdfa(void)
{
	DFA *d;

	d = (DFA*)emalloc(sizeof *d+256*sizeof d->hash[0]);
	memset(d, 0, sizeof *d+256*sizeof d->hash[0]);
	d->hash = (DState**)(d+1);
	d->nhash = 256;
	d->dirty = 1;
	return d;
}

int
addtodfa(DFA *d, char *re, int mode, int prec, DKey *val)
{
	NFA *m;
	
	d->dirty = 1;
	if((m = parseregexp(re, mode)) == nil)
		return -1;
	m->end->val = val;
	m->end->valprec = prec;
	d->nfa = unionnfa(d->nfa, m);
	return 0;
}

DKeyL*
rundfa(DFA *d, char *p, char **pp)
{
	int ambig, prec;
	DState *q, *nq, *qmatch;
	NState *z;
	uchar *t, *match;
	int i, c;
	DKeyL *val;

	if(d->dirty)
		dfareset(d);
	
	match = nil;
	qmatch = nil;
	q = d->start;
	/* q = walkstate(d, q, Bol); */
	for(t=(uchar*)p; (c=*t); t++){
		if((nq = q->next[c]) == nil){
			if(q->id == -1)
				break;
			nq = walkstate(d, q, c);
			q->next[c] = nq;
		}
		q = nq;
		if(q->match & FlagMatch){
			qmatch = q;
			match = t+1;
			if(q->match & FlagStop)
				break;
		}
	}
	if(match == nil)
		return nil;
	*pp = (char*)match;
	ambig = 1;
	// Not sure why ambig exists -- it could be implemented
	// by putting all the ambiguous ones at minimum precedence.
	prec = 0;
	if(qmatch->val == nil){
		val = nil;
		for(i=0; i<qmatch->nnstate; i++){
			z = qmatch->nstate[i];
			if(z->val){
				if(z->flags&RegexpAmbiguous){
					if(ambig)
						val = mkDKeyL(z->val, val);
				}else{
					if(ambig || prec < z->valprec){
						ambig = 0;
						prec = z->valprec;
						freeDKeyL(val);
						val = mkDKeyL(z->val, nil);
					}else if(prec == z->valprec)
						val = mkDKeyL(z->val, val);
				}
			}
		}
		qmatch->val = revDKeyL(val);
	}
	return qmatch->val;
}

