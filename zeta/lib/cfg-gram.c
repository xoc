/*
 * CFG CfgGram management.
 */

#include "a.h"
typedef struct CfgValue CfgValue;
#include "cfg.h"
#include "bitset.h"
#include "listimpl.h"

/*
 * Allocate, initialize a new grammar.
 */
CfgGram*
cfgnewgram(void)
{
	CfgGram *g;

	g = (CfgGram*)emalloc(sizeof *g);
	g->eofsym = cfgnewsym(g, N("EOF"));
	return g;
}

/*
 * Free a grammar.
 */
void
cfgfreegram(CfgGram *g)
{
	int i;
	CfgRule *r;
	CfgSym *s;

	if(g->slrfreestate){
		for(i=0; i<g->nslrstate; i++)
			g->slrfreestate(g->slrstate[i]);
		free(g->slrstate);
	}
	if(g->glrfreestate){
		for(i=0; i<g->nglrstate; i++)
			g->glrfreestate(g->glrstate[i]);
		free(g->glrstate);
	}

	for(i=0; i<g->nsym; i++){
		s = g->sym[i];
		freebitset(s->first);
		free(s->name);
		free(s->rule);
		free(s);
	}
	free(g->sym);
	
	for(i=0; i<g->nrule; i++){
		r = g->rule[i];
		free(r->right);
		free(r);
	}
	free(g->rule);

	free(g);
}

