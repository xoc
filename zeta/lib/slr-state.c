#include <sys/time.h>
#include "a.h"
typedef struct CfgValue CfgValue;
#include "cfg.h"
typedef struct SlrLexerArg SlrLexerArg;
#include "slr.h"
#include "bitset.h"

static uint usec()
{
	struct timeval tv;
	gettimeofday(&tv, 0);
	return tv.tv_sec*1000000 + tv.tv_usec;
}

uint closuretime;
int nclosure;
int nsync;
int nslowsync;

/*
static void closuretimeinfo() {
	print("%d closures; %d sync; %d slow sync\n", nclosure, nsync, nslowsync);
	print("%d us closure time\n", closuretime);
}
*/

/*
 * state management
 *
 * pos lists are sorted, first decreasing by i, then increasing by r->n.
 */
void
slrfreestate(SlrState *z)
{	
	free(z->action);
	free(z->pos);
	free(z);
}

int
slrstatecmp(SlrState *z, SlrState *y)
{
	int i;

	if(z->npos != y->npos)
		return 1;
	for(i=0; i<z->npos; i++)
		if(z->pos[i].i != y->pos[i].i || z->pos[i].r != y->pos[i].r)
			return 1;
	return 0;
}

static uint
hashstate(SlrState *z)
{
	int i;
	uint n;
	
	n = 0;
	for(i=0; i<z->npos && (i == 0 || z->pos[i].i > 0); i++)
		n += 37*z->pos[i].r->n+101*z->pos[i].i;
	return n%nelem(z->g->slrstatehash);
}

void
slrcheckstate(SlrState *z)
{
	int i;
	
	for(i=0; i<z->npos-1; i++){
		if(z->pos[i].i < z->pos[i+1].i
		|| z->pos[i].r->n > z->pos[i+1].r->n){
			print("bad sort\n");
			abort();
		}
	}
}

enum
{
	BeSlow = 0
};

int
addtostate(SlrState *z, CfgRule *r, int i)
{
	int j;
	SlrRulePos *p;

	if(BeSlow){
		for(j=z->npos-1; j>=0; j--){
			p = &z->pos[j];
			if(p->r == r && p->i == i)
				return 0;
			if(p->i > i || (p->i==i && p->r < r))
				break;
		}
	}else{
		int lo, hi, m;
		static int speedhack;
		if(speedhack+1 < z->npos){
			// Lookups tend to go sequentially.
			// This speeds up parsing by 40%
			p = &z->pos[speedhack+1];
			if(p->r == r && p->i == i){
				speedhack++;
				return 0;
			}
		}
	
		// Binary search for insertion position.
		lo = 0;
		hi = z->npos;
		while(lo != hi){
			m = (lo+hi)/2;
			p = &z->pos[m];
			if(p->r == r && p->i == i){
				speedhack = m;
				return 0;
			}
			if(p->i > i || (p->i==i && p->r < r))
				lo = m+1;
			else
				hi = m;
		}
		j = hi-1;
	}
	if(j >= 0){
		p = &z->pos[j];
		assert(p->i > i || (p->i==i && p->r < r));
		if(j < z->npos-1){
			p++;
			assert(!(p->i > i || (p->i==i && p->r < r)));
		}
	}

	if(z->npos%32==0)
		z->pos = (SlrRulePos*)realloc(z->pos, (z->npos+32)*sizeof(z->pos[0]));
	p = &z->pos[++j];
	if(z->npos != j)
		memmove(p+1, p, (z->npos-j)*sizeof p[0]);
	z->npos++;
	p->r = r;
	p->i = i;
	return 1;
}

static void
closure(CfgGram *g, SlrState *z)
{
	int did;
	SlrRulePos *p;
	CfgSym *s;
	int i, j;
	static Bitset *b;
	static int nb;

nclosure++;
	if(nb < g->nsym){
		freebitset(b);
		b = newbitset(g->nsym);
		nb = g->nsym;
	}

	clearbitset(b);
	do{
		did = 0;
		for(i=0; i<z->npos; i++){
			p = &z->pos[i];
			if(p->i == p->r->nright)
				continue;
			s = p->r->right[p->i];
			if(!addbit(b, s->n))
				continue;
			for(j=0; j<s->nrule; j++)
				did += addtostate(z, s->rule[j], 0);
		}
	}while(did);
	z->synctime = g->mtime;
	z->compiled = 0;
}

static int
slrdelcanon(CfgGram *g, SlrState *z)
{
	SlrState **l;
	uint h;

	h = hashstate(z);
	for(l = &g->slrstatehash[h]; *l; l=&(*l)->next){
		if(*l == z){
			*l = z->next;
			z->next = nil;
			return 1;
		}
	}
	return 0;
}

SlrState*
_slrcanonstate(CfgGram *g, SlrState *z, int canfree)
{
	uint h;
	SlrState *y, **l, **a;

	/* checksort(z); */
	/* checksort(z); */
	g = z->g;
	uint t0 = usec();
	closure(g, z);
	uint t1 = usec();
	h = hashstate(z);
	for(l=&g->slrstatehash[h]; (y=*l); l=&y->next){
		if(slrstatecmp(z, y) == 0){
			// if(t1-t0 > 0 && closuretime == 0)
			// 	atexit(closuretimeinfo);
			closuretime += t1 - t0;
			if(canfree)
				slrfreestate(z);
			else
				z->newer = y;
			if(l != &g->slrstatehash[h]){
				*l = y->next;
				y->next = g->slrstatehash[h];
				g->slrstatehash[h] = y;
			}
			return y;
		}
//		// g->stats.hashwalk++;
	}

	if(g->nslrstate%32 == 0){
		a = (SlrState**)realloc(g->slrstate, (g->nslrstate+32)*sizeof g->slrstate[0]);
		if(a == nil){
			slrfreestate(z);
			return nil;
		}
		g->slrstate = a;
	}

	z->next = g->slrstatehash[h];
	g->slrstatehash[h] = z;

	z->n = g->nslrstate;
	g->slrstate[g->nslrstate++] = z;
	return z;
}

SlrState*
slrcanonstate(CfgGram *g, SlrState *z)
{
	return _slrcanonstate(g, z, 1);
}

static int
quicksync(CfgGram *g, SlrState *z)
{
	int i;
	SlrAction *a;

	if(!z->compiled)
		return 0;
	for(i=0; i<z->naction; i++){
		a = &z->action[i];
		if(a->sym && a->sym->mtime > z->synctime)
			return 0;
	}
	if(z->naction != g->nsym+1)
		return 0;
	return 1;
}

SlrState*
slrsyncstate(CfgGram *g, SlrState *z)
{
	SlrState *z0;

	z0 = z;
	while(z->newer)
		z = z->newer;
nsync++;
	if(z->synctime >= g->mtime || quicksync(g, z))
		return z;
nslowsync++;
	slrdelcanon(g, z);
	z = _slrcanonstate(g, z, 0);
	if(z0 != z)
		z0->newer = z;
	return z;
}

int
slrstatefmt(Fmt *fmt)
{
	int i, islong;
	SlrState *s;
	SlrRulePos *p;

	s = va_arg(fmt->args, SlrState*);
	if(s == nil)
		return fmtprint(fmt, "<nil>");

	islong = (fmt->flags&FmtLong);
	for(i=0; i<s->npos; i++){
		p = &s->pos[i];
		if(i > 0){
			if(p->i == 0)
				break;
			fmtprint(fmt, " ||\n\t");
		}
		fmtprint(fmt, "%.*R", p->i, p->r);
	}
	return 0;
}

SlrState*
slrcomputeshift(CfgGram *g, SlrState *z, CfgSym *s)
{
	int i, didshift;
	SlrRulePos *p;
	SlrState *y;

//	g->stats.slrcomputeshift++;
	y = (SlrState*)emalloc(sizeof *y);
	y->g = z->g;
	didshift = 0;
	for(i=0; i<z->npos; i++){
		p = &z->pos[i];
		if(p->i < p->r->nright && p->r->right[p->i]==s){
			didshift = 1;
			if(addtostate(y, p->r, p->i+1) < 0){
				slrfreestate(y);
				return nil;
			}
		}
	}
	return slrcanonstate(g, y);
}
