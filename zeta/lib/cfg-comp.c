/*
 * CFG computations.
 */
#include "a.h"
typedef struct CfgValue CfgValue;
#include "cfg.h"
#include "bitset.h"
#include "listimpl.h"

/*
 * Compute whether each symbol can produce an empty string.
 */
static void
empty(CfgGram *g)
{
	int i, j, k, did, flag;
	CfgRule *r;
	CfgSym *s, *ss;
	
	/* Step 1: clear CfgSymEmpty flags */
	for(i=0; i<g->nsym; i++){
		s = g->sym[i];
		s->flags &= ~CfgSymEmpty;
		for(j=0; j<s->nrule; j++){
			r = s->rule[j];
			r->nnullright = r->nright;
		}
	}

	/* Step 2: loop looking for symbols with RHS that can all empty */
	/* could keep a queue for speed, but not a bottleneck */
	do{
		did = 0;
		for(i=0; i<g->nsym; i++){
			s = g->sym[i];
			for(j=0; j<s->nrule; j++){
				r = s->rule[j];
				flag = CfgSymEmpty;
				for(k=r->nright-1; k>=0; k--){
					ss = r->right[k];
					if(ss->flags&CfgSymEmpty)
						r->nnullright = k;
					else{
						flag = 0;
						break;
					}
				}
				if(flag && !(s->flags & CfgSymEmpty)){
					s->emptyrule = r;
					s->flags |= flag;
					did = 1;
				}
				if(r->nright == 0)
					s->emptyrule = r;
			}
		}
	}while(did);
}

/*
 * Compute first set for each symbol s:
 * symbols that can start a sentence derived from s.
 * Must include non-terminals too, in case lexer is handing back
 * non-terminals.
 *
 * Caller must have called empty already.
 */
static void
first(CfgGram *g)
{
	int did, i, j, k;
	CfgRule *r;
	CfgSym *s, *ss;

	/* allocate sets, initialize all symbols with themselves */
	for(i=0; i<g->nsym; i++){
		s = g->sym[i];
		freebitset(s->first);
		s->first = newbitset(g->nsym);
		addbit(s->first, s->n);
	}

	/* iterate to find first sets for right hand sides */
	do{
		did = 0;
		for(i=0; i<g->nsym; i++){
			s = g->sym[i];
			for(j=0; j<s->nrule; j++){
				r = s->rule[j];
				for(k=0; k<r->nright; k++){
					ss = r->right[k];
					did += addbitset(s->first, ss->first);
					if(!(ss->flags&CfgSymEmpty))
						break;
				}
			}
		}
	}while(did);
}

/*
 * Compute follow set for each symbol s:
 * symbols that can follow an s in a valid sentence of any type.
 */
static void
follow(CfgGram *g)
{
	int i, did, k, lastnonempty, l;
	CfgRule *r;
	CfgSym **rhs, *s;

	for(i=0; i<g->nsym; i++){
		s = g->sym[i];
		freebitset(s->follow);
		s->follow = newbitset(g->nsym);
	}

	do{
		did = 0;
		for(i=0; i<g->nrule; i++){
			r = g->rule[i];
			s = r->left;
			rhs = r->right;
			lastnonempty = 0;
			for(k=0; k<r->nright; k++){
				/* X => ... Y Z ..., so follow(Y) += first(Z) */
				for(l=lastnonempty; l<k; l++)
					did += addbitset(rhs[l]->follow, rhs[k]->first);
				if(!(rhs[k]->flags&CfgSymEmpty))
					lastnonempty = k;
			}
			/* X => ... Y, so follow(Y) += follow(X) */
			for(l=lastnonempty; l<r->nright; l++)
				did += addbitset(rhs[l]->follow, s->follow);
		}
	}while(did);
}

/*
 * Compute `leads to' set for each symbol.
 * A leads to B if A => ... => B
 */
static void
leadsto(CfgGram *g)
{
	int did, i, j, k, lastnonempty, nnonempty;
	CfgSym *s;
	CfgRule *r;

	for(i=0; i<g->nsym; i++){
		s = g->sym[i];
		freebitset(s->leadsto);
		s->leadsto = newbitset(g->nsym);
		addbit(s->leadsto, s->n);
	}
	
	do{
		did = 0;
		for(i=0; i<g->nsym; i++){
			s = g->sym[i];
			for(j=0; j<s->nrule; j++){
				r = s->rule[j];
				nnonempty = 0;
				lastnonempty = 0;
				for(k=0; k<r->nright; k++){
					if(!(r->right[k]->flags&CfgSymEmpty)){
						nnonempty++;
						lastnonempty = k;
					}
				}
				switch(nnonempty){
				case 0:
					for(k=0; k<r->nright; k++)
						did += addbitset(s->leadsto, r->right[k]->leadsto);
					break;
				case 1:
					did += addbitset(s->leadsto, r->right[lastnonempty]->leadsto);
					break;
				}
			}
		}
	}while(did);	
}

/*
 * Assign an ordering to symbols that respects leadsto,
 * so that if A leads to B, then B appears before A in the list.
 */
static void leadstonumber1(CfgGram*, CfgSym*, int*);
static void
leadstonumber(CfgGram *g)
{
	int seq;
	int i;
	CfgSym *s;

	for(i=0; i<g->nsym; i++){
		s = g->sym[i];
		s->leadnum = 0;
	}
	seq = 0;
	for(i=0; i<g->nsym; i++)
		leadstonumber1(g, g->sym[i], &seq);
}

static void
leadstonumber1(CfgGram *g, CfgSym *s, int *seq)
{
	int i;
	CfgSym *t;

	if(s->leadnum != 0)
		return;
	
	s->leadnum = -1;  // break cycles
	for(i=0; i<g->nsym; i++){
		t = g->sym[i];
		if(inbitset(s->leadsto, t->n))
			leadstonumber1(g, t, seq);
	}
	s->leadnum = ++(*seq);
}

void
cfgcompile(CfgGram *g)
{
	empty(g);
	first(g);
	follow(g);
	leadsto(g);
	leadstonumber(g);
}

LISTIMPL(CfgPrec)
