#include "a.h"
typedef struct CfgValue CfgValue;
#include "cfg.h"
typedef struct SlrLexerArg SlrLexerArg;
#include "slr.h"
#include "bitset.h"

static void
die(char *fmt, ...)
{
	va_list arg;
	
	fprint(2, "fatal: ");
	va_start(arg, fmt);
	vfprint(2, fmt, arg);
	fprint(2, "\n");
	exit(1);
}

SlrParse*
slrnewparse(void)
{
	return (SlrParse*)emalloc(sizeof(SlrParse));
}

static SlrParse*
slrparseinit(CfgGram *g, SlrParse *p, CfgSym *start)
{	
	CfgValue **val;
	int i;

	if(p == nil)
		p = slrnewparse();
	p->stack = emalloc(100*sizeof p->stack[0]);
	p->estack = p->stack+100;
	p->sp = p->stack;
	p->tok = nil;
	
	val = emalloc(100*sizeof val[0]);
	for(i=0; i<100; i++)
		p->stack[i].val = &val[i];
	
	if(slrinitstate(g, start) == nil)
		return nil;
	slrcompile(g);
	
	p->sp->slrstate = slrinitstate(g, start);
	p->sp++;
	p->seqnum = g->seqnum;

//fprint(2, "initstate: %l@\n", p->sp[-1].slrstate);
	return p;
}

/*
 * Figure out what to do next, given current parse state and lookahead token.
 */
static int
action(CfgGram *g, SlrParse *p, SlrState *z, SlrState **nz, CfgRule **r, CfgSym *tok)
{
	int i;
	SlrAction *a;

	*nz = nil;
	*r = nil;
	if(!z->compiled)
		slrcompilestate(g, z);
	if(tok == nil && (z->naction < 1 || z->action[0].sym))
		return 0;
	for(a=z->action, i=0; i<z->naction && a->sym != tok && a->sym; i++, a++)
		;
	assert(i < z->naction);
	if(a->shiftstate)
		*nz = a->shiftstate;
	for(i=0; i<a->nreduce; i++){
		if(a->reduce[i].nright == a->reduce[i].rule->nright){
			*r = a->reduce[i].rule;
			break;
		}
	}
	return 1;
}

/*
 * Recompute the parse states on the stack by re-shifting
 * if the grammar has been changed.
 */
static void
reshift(CfgGram *g, CfgSym *start, SlrParse *p)
{
	SlrStack *sp;
	
	if(!g->dirty && g->seqnum == p->seqnum)
		return;
fprint(2, "dirty %d seqnum %d %d\n", g->dirty, g->seqnum, p->seqnum);
	slrcompile(g);
	p->stack->slrstate = slrinitstate(g, start);
	for(sp=p->stack+1; sp<p->sp; sp++){
		sp->slrstate = slraction((sp-1)->slrstate, sp->sym)->shiftstate;
		if(!sp->slrstate)
			die("lost parse in reshift at %s", sp->sym->name);
	}
	p->seqnum = g->seqnum;
}

/*
 * Parse
 */
CfgValue*
slrparse(CfgGram *g, CfgSym *start, SlrParse *p0, SlrLexer *slrlex, SlrLexerArg *arg)
{
	SlrParse *p;
	SlrStack *sp;
	SlrState *nextstate;
	CfgRule *rule;
	
//	fprint(2, "slrparse: start %s; valsize %d\n", start, g->valsize);
	p = slrparseinit(g, p0, start);
	if(p == nil)
		return nil;

	for(;;){
		if(p->sp >= p->estack)
			die("stack overflow");
//		fprint(2, "slrparse: [%s] %@\n", p->tok ? p->tok->name : "", p->sp[-1].slrstate);
		if(!action(g, p, p->sp[-1].slrstate, &nextstate, &rule, p->tok)){
			p->tok = slrlex(g, &p->tokval, arg);
			reshift(g, start, p);
//			fprint(2, "slrparse: [%s] %@\n", p->tok ? p->tok->name : "", p->sp[-1].slrstate);
			action(g, p, p->sp[-1].slrstate, &nextstate, &rule, p->tok);
		}
		die("slrparse depends on obsolete rule->prec");
		if(nextstate && (!rule /*|| p->tok->prec >= rule->prec*/)){
			/* shift */
			sp = p->sp++;
			if(p->sp >= p->estack)
				die("stack overflow");
			sp->sym = p->tok;
			sp->slrstate = nextstate;
			// *(CfgSym**)p->tokval = p->tok;
			*sp->val = p->tokval;
			p->tok = nil;
		}else if(rule){
			/* reduce */
			if(rule->toplevel){
				p->sp -= 2;	/* start EOF */
				return *p->sp->val;
			}
			p->sp -= rule->nright;
			p->tmpval = *p->sp->val;
			if(rule->fn){
				rule->fn(g, rule, &p->tmpval, p->sp->val);
				reshift(g, start, p);
			}
			// *(CfgSym**)p->tmpval = rule->left;
			*p->sp->val = p->tmpval;
			p->sp->sym = rule->left;
			p->sp->slrstate = slraction((p->sp-1)->slrstate, p->sp->sym)->shiftstate;
			if(!p->sp->slrstate)
				die("lost parse");
			p->sp++;
		}else{
			for(sp=p->stack+1; sp<p->sp; sp++)
				print("%s ", sp->sym->name);
			if(p->tok)
				print("%s\n", p->tok->name);
			else
				print("\n");
			die("parse error");
		}
	}	
}

void
slrerror(CfgGram *g, SlrParse *p, char *msg)
{
	panic("slrerror: %s", msg);
}
