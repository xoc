#include "a.h"
#include <errno.h>

char errbuf[256];

void*
emalloc(int n)
{
	void *v;
	
	v = calloc(n, 1);
	if(v == nil)
		panic("out of memory");
	return v;
}

void*
emallocnz(int n)
{
	void *v;
	
	v = malloc(n);
	if(v == nil)
		panic("out of memory");
	return v;
}

char*
estrdup(char *s)
{
	s = strdup(s);
	if(s == nil)
		panic("out of memory");
	return s;
}

void*
erealloc(void *v, int n)
{
	v = realloc(v, n);
	if(v == nil)
		panic("out of memory");
	return v;
}

void*
erealloczero(void *v, int on, int n)
{
	v = erealloc(v, n);
	if(on < n)
		memset((char*)v+on, 0, n-on);
	return v;
}

void
werrstr(char *fmt, ...)
{
	va_list arg;
	
	va_start(arg, fmt);
	vsnprint(errbuf, sizeof errbuf, fmt, arg);
	va_end(arg);
	errno = 99999;
}
