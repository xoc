/*
 * Generalized LR.
 */
typedef struct GlrStack GlrStack;
typedef struct GlrToken GlrToken;
typedef struct GlrPath GlrPath;
typedef struct GlrEdge GlrEdge;
typedef struct GlrParse GlrParse;

struct GlrToken
{
	CfgSym	*sym;
	CfgValue	*val;
};
LIST(GlrToken)

struct GlrEdge
{
	GlrEdge	*allsib;
	GlrEdge	*sib;
	GlrStack	*down;
	CfgSym	*sym;
	CfgValue	*val;
	int		nullval;
};

struct GlrParse
{
	CfgGram	*g;
	uint	seqnum;
	uint	ipos;
	GlrStack	**top;
	uint	ntop;
	uint	mtop;
	GlrTokenL	*toks;
	CfgValue	**tmpval;
	GlrPath	**heap;
	uint	nheap;
	uint	mheap;
	Name file;
	int	line;
	int	col;
	void	*aux;
	void	(*merge)(CfgSym*, CfgValue**, CfgValue*);
	void	(*dup)(CfgSym*, CfgValue**, CfgValue*);
	void	(*free)(CfgSym*, CfgValue*);
	void (*touch)(CfgValue*, int);
	GlrToken *freetoks;
	GlrEdge *freeedges;
	GlrStack *freestacks;
};

struct GlrPath 
{
	int	leftpos;
	CfgRule	*rule;
	GlrStack	*sp;
	int nedge;
	GlrEdge	*edge[1];
};

struct GlrStack
{
	int		ipos;
	SlrState	*state;
	GlrEdge	*edge[37];
	GlrEdge	*allsib;
	int	ref;
	int	ntouch;
	int	seq;
	int	nlinear;
};

typedef void GlrTokenArg;
typedef GlrTokenL *GlrLexer(GlrParse*, CfgGram*, GlrTokenArg*);

GlrParse*	glrnewparse(CfgGram*);
CfgValue*		glrparse(CfgGram *g, CfgSym *start, GlrParse *p0, GlrLexer *yylex, GlrTokenArg *arg);
void		glrprintstate(GlrParse *p);
GlrToken*	glrnewtoken(GlrParse*, CfgSym*);
void		glrfreetoken(GlrParse*, GlrToken*);
