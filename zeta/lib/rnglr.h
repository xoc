/* Right-nulled GLR parser */

typedef struct RgEdge RgEdge;
typedef struct RgParse RgParse;
typedef struct RgReduce RgReduce;
typedef struct RgReduceSet RgReduceSet;
typedef struct RgShift RgShift;
typedef struct RgShiftSet RgShiftSet;
typedef struct RgStack RgStack;
typedef struct RgToken RgToken;
LIST(RgToken);

typedef void RgTokenArg;
typedef RgTokenL *RgLexer(RgParse*, CfgGram*, RgTokenArg*);

/* Graph-structured stack: nodes and edges */
struct RgStack
{
	int ref;
	SlrState *state;		/* underlying SLR state */
	Map edgemap;		/* indexed by target state */
	RgEdge *edges;	/* linked list of all edges */
	MapElem me;
};

struct RgEdge
{
	RgEdge *next;	/* in parent edges list */
	RgStack *sp;		/* target of edge */
	CfgSym *sym;
	CfgValue *val;
	MapElem me;
};


/* Input tokens */
struct RgToken
{
	CfgSym *sym;
	CfgValue *val;
};


/* Pending reduce actions */
struct RgReduce
{
	RgStack *sp;	/* where the reduction starts in the stack graph */
	CfgRule *rule;	/* the rule being applied */
	int pathlen;	/* the number of values to pull from the graph (the rest are null strings) */
	RgEdge *edge;	/* the edge that led to sp; is # pathlen-1 */
	RgStack *edgeptr;	/* the sp pointing at edge, to keep it from being freed */
};

struct RgReduceSet
{
	RgReduce *e;	/* elements in set */
	int n;			/* number of elements in set */
	int m;		/* max (allocated) n */
};


/* Pending input shift actions */
struct RgShift
{
	RgStack *sp;	/* where the shift starts */
};

struct RgShiftSet
{
	RgShift *e;
	int n;
	int m;
};


/* Parse state */
struct RgParse
{
	CfgGram *g;

	RgStack **top;		/* array of stack tops */
	int ntop;
	int mtop;
	Map topmap;
	
	Name file;			/* current input position */
	int line;
	int col;
	
	CfgValue **val;		/* working space for reducer */
	int nval;
	
	RgToken *freetoks;
	RgStack *freestacks;
	RgEdge *freeedges;
	
	CfgSym *toksym;	/* next token symbol, if unambiguous */
	
	RgReduceSet *toreduce;
	RgShiftSet *toshift;
	RgShiftSet *toshift1;
	
	/* client-supplied callbacks */
	void *aux;
	void (*merge)(CfgSym*, CfgValue*, CfgValue*);
	void (*dup)(CfgSym*, CfgValue**, CfgValue*);
	void (*free)(CfgSym*, CfgValue*);
};

CfgValue *rgparse(CfgGram*, CfgSym*, RgParse*, RgLexer*, RgTokenArg*);
RgParse *rgnewparse(CfgGram*);
void rgfreeparse(RgParse*);
RgToken *rgnewtoken(RgParse*, CfgSym*);
