/*
 * Right-nulled GLR
 * See Scott and Johnstone, "Right Nulled GLR Parsers", ASPLOS 2006
 * (although I like to think that my code is a lot easier to read than theirs).
 *
 * S & J assume that the lexer has produced an unambiguous input sequence.
 * I've adapted things here and there to accomodate the possibility of multiple
 * values for a particular input token.
 */

#include "a.h"
typedef struct CfgValue CfgValue;
#include "cfg.h"
typedef struct SlrLexerArg SlrLexerArg;
#include "slr.h"
#include "rnglr.h"
#include "listimpl.h"

static int	applyrule(RgParse*, CfgRule*, CfgValue**, CfgValue**);
static RgEdge*	findedge(RgStack*, RgStack*, CfgSym*);
static RgStack*	findtop(RgParse*, SlrState*);
static int	mkemptyval(RgParse*, CfgSym*, CfgValue**);
static RgEdge*	newedge(RgParse*, RgStack*, RgStack*, CfgSym*);
static RgStack*	newtop(RgParse*, SlrState*);
static void	pushvalue(RgParse*, CfgSym*, CfgValue*, RgStack*, SlrState*, int);
static void	queuereduce(RgParse*, RgStack*, CfgRule*, int, RgEdge*, RgStack*);
static void	queueshift(RgParse*, RgStack*);
static void	reducepath(RgParse*, RgReduce*, RgStack*, CfgValue**);
static void	reducer(RgParse*, RgReduce*);
static void	shifter(RgParse*, RgTokenL*);
static void	walkpaths(RgParse*, RgReduce*, RgStack*, int, CfgValue**);
static void printstate(RgParse*, char*);
static inline void freestack(RgParse*, RgStack*);
static void freeedge(RgParse*, RgEdge*);
static void _freestack(RgParse*, RgStack*);
static void freetokens(RgParse*, RgTokenL*);

/*
 * Parse using the RNGLR parsing algorithm.
 *
 * TODO: Debugging prints.
 */
CfgValue*
rgparse(CfgGram *g, CfgSym *start, RgParse *p0, RgLexer *yylex, void *arg)
{
	int i, first;
	RgParse *p;
	CfgSym *eofsym;
	RgReduce r;
	SlrState *state0;
	CfgValue *out;
	RgTokenL *toks, *newtoks;;
	
	/*
	 * Initialize g, p if needed.
	 */
	slrcompile(g);
	if((state0 = slrinitstate(g, start)) == nil)
		return nil;
	if((p = p0) == nil)
		p = rgnewparse(g);
	
	out = nil;
	eofsym = cfglooksym(g, N("EOF"));
	first = 1;
	newtoks = nil;
	
	/* Read first input symbol to get the ball rolling. */
	if((toks = yylex(p, g, arg)) == nil || toks->hd == nil){
		fprint(2, "rgparse: unexpected nil from yylex\n");
		goto out;
	}
	p->toksym = nil;
	if(!toks->tl)
		p->toksym = toks->hd->sym;
	
	/* Special case: empty input */
	if(toks->hd->sym == eofsym){
		if(start->flags&CfgSymEmpty)
			mkemptyval(p, start, &out);
		goto out;
	}

	/* Now that we have input, set up the parse stack. */
	pushvalue(p, nil, nil, nil, state0, 0);
	if(g->debug > 1)
		printstate(p, "initial state");

	while(p->ntop > 0){
		/* Apply reductions */
		while(p->toreduce->n > 0){
			r = p->toreduce->e[--p->toreduce->n];
			reducer(p, &r);
			freestack(p, r.sp);
			if(r.edgeptr)
				freestack(p, r.edgeptr);
		}
		if(g->debug > 1)
			printstate(p, "after reductions");

		/* Clear stack tops (shifter will create new ones). */
		for(i=0; i<p->ntop; i++)
			freestack(p, p->top[i]);
		p->ntop = 0;
		p->topmap.root = nil;

		/* Get upcoming tokens */
		if((newtoks = yylex(p, g, arg)) == nil || newtoks->hd == nil){
			fprint(2, "rgparse: unexpected nil from yylex\n");
			break;
		}
		p->toksym = nil;
		if(!newtoks->tl)
			p->toksym = newtoks->hd->sym;

		if(g->debug > 0)
			print("=== shift %s [lookahead %s]\n",
				toks->tl ? "many" : nametostr(toks->hd->sym->name),
				p->toksym ? nametostr(p->toksym->name) : "many");

		/* Shift input tokens. */
		shifter(p, toks);
		if(g->debug > 1)
			printstate(p, "after shifting");
		
		if(toks->hd->sym == eofsym){
			if(p->ntop == 0){
				fprint(2, "rgparse: lost parse at EOF\n");
				break;
			}
			if(p->ntop > 1){
				fprint(2, "rgparse: ambiguous top-level parse!\n");
				break;
			}
			if(g->debug > 1)
				printstate(p, "success");
			out = p->top[0]->edges->sp->edges->val;
			if(p->dup)
				p->dup(p->top[0]->edges->sp->edges->sym, &out, out);
			break;
		}
		
		if(toks)
			freetokens(p, toks);
		toks = newtoks;
		newtoks = nil;
	}
	
out:
	if(toks)
		freetokens(p, toks);
	if(newtoks)
		freetokens(p, newtoks);
	if(p != p0)
		rgfreeparse(p);
	return out;
}


/*
 * Apply the queued reduction along all possible matching paths.
 */
static void
reducer(RgParse *p, RgReduce *r)
{
	int i;
	CfgValue **val;

	// TODO: Cache val in p.
	if(p->nval < r->rule->nright){
		p->nval = r->rule->nright;
		p->val = erealloc(p->val, p->nval*sizeof p->val[0]);
	}
	val = p->val;
	if(r->pathlen > 0)
		val[r->pathlen-1] = r->edge->val;
		
	/* Create the empty values that we're not going to parse directly */
	for(i=r->pathlen; i<r->rule->nright; i++){
		if(mkemptyval(p, r->rule->right[i], &val[i]) < 0){
			if(p->free)
				for(i--; i>=r->pathlen; i--)
					p->free(r->rule->right[i], val[i]);
			free(val);
			return;
		}
	}

	/* Let walkpaths fill in the remaining values and call reducepath. */
	walkpaths(p, r, r->sp, r->pathlen-1, val);

	if(p->free)
		for(i=r->pathlen; i<r->rule->nright; i++)
			p->free(r->rule->right[i], val[i]);
}

/*
 * Walk over the graph, starting at sp, to find paths matching r,
 * accumulating values in val.  The last n+1 values are still needed.
 * Once val is complete, call reducepath to perform the reduction.
 */
static void
walkpaths(RgParse *p, RgReduce *r, RgStack *sp, int n, CfgValue **val)
{
	RgEdge *e;
	CfgSym *sym;
	
	if(n <= 0){
		reducepath(p, r, sp, val);
		return;
	}
	sym = r->rule->right[n-1];
	for(e=sp->edges; e; e=e->next){
		if(e->sym == sym){
			val[n-1] = e->val;
			walkpaths(p, r, e->sp, n-1, val);
		}
	}
}

/*
 * Apply the reduction r at the endpoint sp, using the values in val.
 */
static void
reducepath(RgParse *p, RgReduce *r, RgStack *sp, CfgValue **val)
{
	CfgSym *sym;
	CfgValue *newval;
	SlrState *nextstate;
	
	sym = r->rule->left;

	if(p->g->debug > 0)
		print("reduce %R in %@\n", r->rule, sp->state);

	nextstate = slraction(sp->state, sym)->shiftstate;
	if(nextstate == nil)
		return;

	if(applyrule(p, r->rule, val, &newval) < 0)
		return;

	pushvalue(p, sym, newval, sp, nextstate, r->pathlen > 0);
	
	/* If value was saved, it has been dup'ed; free our copy. */
	if(p->free)
		p->free(sym, newval);
}

/*
 * Create a ParseValue representing the empty string.
 * Only finds one way, which is not quite correct:
 * the GLR parser should return all the ways.  Also,
 * if we create multiple empty vals at the same input
 * location, they will not be correctly crosslinked
 * (meaning we'll waste a little bit more memory than
 * we need to).  Too bad.
 */
static int
mkemptyval(RgParse *p, CfgSym *sym, CfgValue **outp)
{
	int i;
	CfgRule *r;
	CfgValue **val;
	CfgValue *newval;
	
	r = sym->emptyrule;	/* the rule to make an empty sym */

	/* Fill in children for rule application. */
	val = emalloc(r->nright*sizeof val[0]);
	for(i=0; i<r->nright; i++){
		if(mkemptyval(p, r->right[i], &val[i]) < 0){
		error:
			if(p->free)
				for(i--; i>=0; i--)
					p->free(r->right[i], val[i]);
			free(val);
			return -1;
		}
	}
	
	/* Create new value */
	if(applyrule(p, r, val, &newval) < 0)
		goto error;
	
	*outp = newval;
	if(p->free)
		for(i=0; i<r->nright; i++)
			p->free(r->right[i], val[i]);
	free(val);
	return 0;
}

/*
 * Apply a rule's constructor function to val to create a new value.
 */
static int
applyrule(RgParse *p, CfgRule *rule, CfgValue **val, CfgValue **newvalp)
{
	CfgValue *newval;
	
	/* Create new value */
	if(rule->fn){
		if(rule->fn(p->g, rule, &newval, val) < 0)
			return -1;
	}else if(rule->nright > 0){
		newval = val[0];
		if(p->dup)
			p->dup(rule->left, &newval, val[0]);
	}else
		newval = nil;
	*newvalp = newval;
	return 0;
}


/*
 * Given a list of possible input tokens, apply all the queued
 * shifts as applicable.  The shifts were filtered as we queued
 * them, but due to uncertainty about the next input token,
 * there might still be entries that don't apply to a particular token.
 */
static void
shifter(RgParse *p, RgTokenL *tokens)
{
	RgShift sh;
	RgShiftSet *set;
	SlrAction *a;
	RgTokenL *l;
	RgToken *tok;

	set = p->toshift;
	p->toshift = p->toshift1;
	p->toshift1 = set;
	assert(p->toshift->n == 0);
	while(set->n > 0){
		sh = set->e[--set->n];
		for(l=tokens; l; l=l->tl){
			tok = l->hd;
			a = slraction(sh.sp->state, tok->sym);
			if(a->shiftstate){
				if(p->g->debug > 1)
					print("--- shift %s\n", tok->sym->name);
				pushvalue(p, tok->sym, tok->val, sh.sp, a->shiftstate, 1);
			}
		}
		freestack(p, sh.sp);
	}
}


/*
 * Work sets (order doesn't matter, so they're stacks not queues, despite the name).
 */
 
static void
queuereduce(RgParse *p, RgStack *sp, CfgRule *rule, int pathlen, RgEdge *edge, RgStack *edgeptr)
{
	RgReduce *r;
	RgReduceSet *set;
	
	/*
	 * The reduce set has items added and removed concurrently,
	 * so there is no point to looking for duplicates.  The algorithm
	 * itself will make sure not to put anything in twice.
	 */
	set = p->toreduce;
	if(set->n >= set->m){
		if(set->m < 16)
			set->m = 16;
		else
			set->m *= 2;
		set->e = erealloc(set->e, set->m*sizeof set->e[0]);
	}

	if(p->g->debug > 0)
		print("queue reduce %d/%R in %@\n", pathlen, rule, sp->state);
	r = &set->e[set->n++];
	r->sp = sp;
	sp->ref++;
	r->rule = rule;
	r->pathlen = pathlen;
	r->edge = edge;
	r->edgeptr = edgeptr;
	if(edgeptr)
		edgeptr->ref++;
}

static void
queueshift(RgParse *p, RgStack *sp)
{
	int i;
	RgShift *sh;
	RgShiftSet *set;
	
	/*
	 * The shift set is an actual set - we have to 
	 * remove duplicates to ensure termination.
	 * I don't believe these sets get big enough
	 * to worry about the O(n) lookup right here.
	 */
	set = p->toshift;
	for(i=0; i<set->n; i++){
		sh = &set->e[i];
		if(sh->sp == sp)
			return;
	}

	set = p->toshift;
	if(set->n >= set->m){
		if(set->m < 16)
			set->m = 16;
		else
			set->m *= 2;
		set->e = erealloc(set->e, set->m*sizeof set->e[0]);
	}
	
	sh = &set->e[set->n++];
	sh->sp = sp;
	sp->ref++;
}


/*
 * We have a new value to add to the graph-structured stack. 
 * 
 * 	p - the parse state
 *	sym - the symbol for the value
 *	newval - the value itself
 *	sp - where value's edge should point
 *	nextstate - slraction(sp->state, sym)->shiftstate
 *		(where to hook the edge in at the top)
 *	queueempty - whether to queue empty reductions
 *		when creating a new edge
 *
 * Special case: sym == nil means that we're initializing the
 * stack for the first time; stop after creating the new top.
 */
static void
pushvalue(RgParse *p, CfgSym *sym, CfgValue *newval,
	RgStack *sp, SlrState *nextstate, int queuevalreduce)
{
	int i;
	RgEdge *e;
	RgStack *top;
	SlrAction *a;
	SlrReduce *sr;

	/* Create new top, edge to hold value */
	a = slraction(nextstate, p->toksym);
	if((top = findtop(p, nextstate)) == nil){
		top = newtop(p, nextstate);
		if(p->g->debug > 0)
			print("newtop %@\n", nextstate);
		if(a->shiftstate || p->toksym == nil)
			queueshift(p, top);
		for(i=0; i<a->nreduce; i++){
			sr = &a->reduce[i];
			if(sr->nright != 0)
				continue;
			queuereduce(p, top, sr->rule, 0, nil, nil);
		}
	}

	if(sym == nil)
		return;

	if((e = findedge(top, sp, sym)) == nil){
		e = newedge(p, top, sp, sym);
		if(p->g->debug > 0)
			print("newedge %@ -> %@ [%s] %p %p\n", top->state, sp->state, sym->name, top, sp);
		if(queuevalreduce){
			/* Queue reductions involving the newval */
			for(i=0; i<a->nreduce; i++){
				sr = &a->reduce[i];
				if(sr->nright == 0)
					continue;
				queuereduce(p, sp, sr->rule, sr->nright, e, top);
			}
		}
	}
	
	/* Add value to edge.  e->val == nil if edge was just allocated */
	if(e->val == nil){
		e->val = newval;
		if(p->dup)
			p->dup(sym, &e->val, newval);
	}else{
		if(p->merge)
			p->merge(sym, e->val, newval);
	}
}


/*
 * Manipulate graph-structured stack.
 */
static RgStack*
findtop(RgParse *p, SlrState *state)
{
	return mapget(&p->topmap, state);
}

static RgStack*
newtop(RgParse *p, SlrState *state)
{
	RgStack *sp;

	if(p->ntop >= p->mtop){
		if(p->mtop == 0)
			p->mtop = 8;
		else
			p->mtop *= 2;
		p->top = erealloc(p->top, p->mtop*sizeof p->top[0]);
	}
	if((sp = p->freestacks) != nil)
		p->freestacks = *(RgStack**)sp;
	else	
		sp = emallocnz(sizeof *sp);
	sp->ref = 1;
	sp->state = state;
	sp->edgemap.cmp = nil;
	sp->edgemap.root = nil;
	sp->edges = nil;
	p->top[p->ntop++] = sp;
	mapputelem(&p->topmap, state, sp, &sp->me);
	return sp;
}

static inline void
freestack(RgParse *p, RgStack *sp)
{
	if(sp == nil || --sp->ref > 0)
		return;
	_freestack(p, sp);
}

static void
_freestack(RgParse *p, RgStack *sp)
{
	assert(sp->ref == 0);
	if(sp->edges)
		freeedge(p, sp->edges);
	*(RgStack**)sp = p->freestacks;
	p->freestacks = sp;
}

static RgEdge*
findedge(RgStack *src, RgStack *dst, CfgSym *sym)
{
	RgEdge *e;

	e = mapget(&src->edgemap, dst);
	if(e){
		/*
		 * Apparently for a given (src, dst) pair, there can
		 * only be one possible sym for the edge connecting
		 * those.  I haven't worked out why this is, so 
		 * I'm asserting it in case the paper is wrong.
		 */
	 	assert(e->sym == sym);
	 	return e;
	 }
	 return nil;
}

static RgEdge*
newedge(RgParse *p, RgStack *src, RgStack *dst, CfgSym *sym)
{
	RgEdge *e;
	
	if((e = p->freeedges) != nil)
		p->freeedges = *(RgEdge**)e;
	else
		e = emallocnz(sizeof *e);
	e->next = src->edges;
	src->edges = e;
	e->sp = dst;
	e->sp->ref++;
	e->sym = sym;
	e->val = nil;
	mapputelem(&src->edgemap, dst, e, &e->me);
	return e;
}

static void
freeedge(RgParse *p, RgEdge *e)
{
	if(e == nil)
		return;
	freeedge(p, e->next);
	freestack(p, e->sp);
	if(p->free)
		p->free(e->sym, e->val);
	*(RgEdge**)e = p->freeedges;
	p->freeedges = e;
}

/*
 * Memory allocation
 */
RgParse*
rgnewparse(CfgGram *g)
{
	RgParse *p;
	
	p = emalloc(sizeof *p);
	p->g = g;
	p->toshift = emalloc(sizeof *p->toshift);
	p->toshift1 = emalloc(sizeof *p->toshift1);
	p->toreduce = emalloc(sizeof *p->toreduce);
	return p;
}

RgToken*
rgnewtoken(RgParse *p, CfgSym *sym)
{
	RgToken *t;
	
	if((t = p->freetoks) != nil)
		p->freetoks = *(RgToken**)t;
	else
		t = emallocnz(sizeof *t);
	t->sym = sym;
	t->val = nil;
	return t;
}

static void
freetoken(RgParse *p, RgToken *t)
{
	*(RgToken**)t = p->freetoks;
	p->freetoks = t;
}

static void
freetokens(RgParse *p, RgTokenL *l1)
{
	RgTokenL *l;
	
	for(l=l1; l; l=l->tl)
		freetoken(p, l->hd);
	freeRgTokenL(l);
}

void
rgfreeparse(RgParse *p)
{
	int i;
	RgReduce *r;
	RgShift *sh;
	RgToken *t;
	RgStack *sp;
	RgEdge *e;

	if(p == nil)
		return;

	free(p->val);
	free(p->top);
	for(i=0; i<p->toreduce->n; i++){
		r = &p->toreduce->e[i];
		freestack(p, r->sp);
		if(r->edgeptr)
			freestack(p, r->edgeptr);
	}
	free(p->toreduce);
	for(i=0; i<p->toshift->n; i++){
		sh = &p->toshift->e[i];
		freestack(p, sh->sp);
	}
	free(p->toshift);
	for(i=0; i<p->toshift1->n; i++){
		sh = &p->toshift1->e[i];
		freestack(p, sh->sp);
	}
	free(p->toshift1);
	
	while((t = p->freetoks) != nil){
		p->freetoks = *(RgToken**)t;
		free(t);
	}
	while((sp = p->freestacks) != nil){
		p->freestacks = *(RgStack**)sp;
		free(sp);
	}
	while((e = p->freeedges) != nil){
		p->freeedges = *(RgEdge**)e;
		free(e);
	}

	free(p);
}


/*
 * Debugging
 */
static void
printstate(RgParse *p, char *note)
{
	int i;
	RgReduce *r;
	RgShift *sh;

	print("=== %s (rg state)\n", note);
	for(i=0; i<p->ntop; i++)
		print("%d.%p. %l@\n", p->top[i]->state->n, p->top[i]->state, p->top[i]->state);
	
	if(p->toreduce->n > 0){
		print("--- queued reductions\n");
		for(i=0; i<p->toreduce->n; i++){
			r = &p->toreduce->e[i];
			print("state %d / rule %R pathlen=%d / edge=%p\n",
				r->sp->state->n, r->rule, r->pathlen, r->edge);
		}
	}

	if(p->toshift->n > 0){
		print("--- queued shifts\n");
		for(i=0; i<p->toshift->n; i++){
			sh = &p->toshift->e[i];
			print("shift %d\n", sh->sp->state->n);
		}
	}
}

LISTIMPL(RgToken);

