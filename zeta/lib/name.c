#include "a.h"

enum
{
	Stringtabs = 1<<10,
	Stringchunk = 1<<20,
};

/*
 *	Custom allocators to avoid malloc overheads on small objects.
 * 	We never free these.  (See below.)
 */
typedef struct Stringtab	Stringtab;
struct Stringtab {
	Stringtab *link;
	char *str;
	int len;
};
static Stringtab*
taballoc(void)
{
	static Stringtab *t;
	static uint nt;

	if(nt == 0){
		t = (Stringtab*)emalloc(Stringtabs*sizeof(Stringtab));
		nt = Stringtabs;
	}
	nt--;
	return t++;
}

static char*
xstrndup(char *s, int len)
{
	char *r;
	static char *t;
	static int nt;

	len++;
	if(len >= Stringchunk){
		r = emalloc(len);
		strncpy(r, s, len-1);
		return r;
	}

	if(nt < len){
		t = (char*)emalloc(Stringchunk);
		nt = Stringchunk;
	}
	r = t;
	t += len;
	nt -= len;
	strncpy(r, s, len-1);
	return r;
}

/*
 *	Return a uniquely allocated copy of a string.
 *	Don't free these -- they stay in the table for the 
 *	next caller who wants that particular string.
 *	String comparison can be done with pointer comparison 
 *	if you know both strings are names.
 */
static Stringtab *stab[8192];

static uint
hashn(void *s, int len)
{
	uint h;
	uchar *p;

	h = 0;
	for(p=(uchar*)s; len; p++, len--)
		h = h*37 + *p;
	return h;
}

static Name empty = { (Name)"" };
static char one[256][2];

Name
nameofn(char *str, int len)
{
	uint h;
	Stringtab *tab;
	
	if(str == nil)
		return nil;

	/*
	 * Handle empty and one-char strings quickly.
	 */
	if(len == 0)
		return empty;
	if(len == 1){
		h = str[0];
		one[h][0] = h;
		return (Name)one[h];
	}
	
	h = hashn(str, len) % nelem(stab);
	for(tab=stab[h]; tab; tab=tab->link)
		if(tab->str[0] == str[0] && tab->len == len && memcmp(str, tab->str, len) == 0)
			return (Name)tab->str;

	tab = taballoc();
	tab->str = xstrndup(str, len);
	tab->link = stab[h];
	tab->len = len;
	stab[h] = tab;
	return (Name)tab->str;
}

Name
nameof(char *str)
{
	return nameofn(str, strlen(str));
}

Name
nameprint(char *fmt, ...)
{
	va_list arg;
	char *s;
	Name x;

	va_start(arg, fmt);
	s = vsmprint(fmt, arg);
	va_end(arg);
	x = nameof(s);
	free(s);
	return x;
}

#undef nametostr
char*
nametostr(Name n)
{
	return (char*)n;
}

