#include "a.h"

int doabort;

void
panic(char *fmt, ...)
{
	va_list arg;
	
	va_start(arg, fmt);
	fprint(2, "panic: %V\n", fmt, arg);
	va_end(arg);
	if(doabort)
		abort();
	exit(1);
}

int
recursefmt(Fmt *f)
{
	char *fmt;
	va_list arg;

	fmt = va_arg(f->args, char*);
	arg = va_arg(f->args, va_list);
	fmtvprint(f, fmt, arg);
	return 0;
}

