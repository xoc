#include "a.h"
typedef struct CfgValue CfgValue;
#include "cfg.h"
typedef struct SlrLexerArg SlrLexerArg;
#include "slr.h"
#include "bitset.h"

void
slrdumptables(CfgGram *g)
{
	int i, j, k;
	SlrState *z;
	CfgSym *s;
	char *sep;
	SlrAction *a;
	
	print("\n================================\n");

	for(i=0; i<g->nsym; i++){
		s = g->sym[i];
		print("%s:%s\n", s->name, 
			(s->flags&CfgSymEmpty) ? " canempty" : "");
		print("\tfirst:");
		for(j=0; j<g->nsym; j++)
			if(inbitset(s->first, g->sym[j]->n))
				print(" %s", g->sym[j]->name);
		print("\n");
		print("\tfollow:");
		for(j=0; j<g->nsym; j++)
			if(inbitset(s->follow, g->sym[j]->n))
				print(" %s", g->sym[j]->name);
		print("\n");
	}
	print("\n");

/*	compileall(g); */
	for(i=0; i<g->nslrstate; i++){
		z = g->slrstate[i];
		print("#state %d: %d pos, %d action\n",
			z->n, z->npos, z->naction);
		print("%d: %lY\n", i, z);
		for(j=0; j<z->naction; j++){
			print("\ton %s:");
			sep = " ";
			a = &z->action[j];
			if(a->shiftstate){
				print("%sshift %d", sep, a->shiftstate->n);
				sep = ", ";
			}
			for(k=0; k<a->nreduce; k++){
				print("%sreduce %R", sep, a->reduce[k]);
				sep = ", ";
			}
			print("\n");
		}
	}
}
