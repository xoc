/*
 * GLR parser. 
 *
 * Thanks to Scott McPeak for writing UCB TR CSD-2-1214,
 * though he's not responsible for this crappy code.
 */
#include "a.h"
typedef struct CfgValue CfgValue;
#include "cfg.h"
typedef struct SlrLexerArg SlrLexerArg;
#include "slr.h"
#include "glr.h"
#include "bitset.h"
#include "listimpl.h"

enum {
	Freelists = 1
};

enum {
	MaxLinear = 20
};
static void reducelinear(CfgGram *g, GlrParse *p, CfgRule *r, int, CfgSym*);


typedef struct GlrPathelem GlrPathelem;
struct GlrPathelem
{
	GlrEdge *edge;
	GlrPathelem *next;
};

int nedge, nstack, npath;
int nmkedge, nfreeedge;
int nmkstack, nfreestack;
int nmkpath, nfreepath;
int ndoshift, nlrshift;
int ndoreduce, nlrreduce;
extern int nmkast, nfreeast;

/*
 * Walk the graph-structured stack, calling p->touch
 * on each CfgValue, for memory allocation debugging.
 */
static void touchglrstack(GlrParse*, GlrStack*, int);
static int ntouch;
static void
touchgraph(GlrParse *p)
{
	int i, j;

	if(p->touch == nil)
		return;
	for(j=0; j<3; j++){
		++ntouch;
		for(i=0; i<p->ntop; i++)
			touchglrstack(p, p->top[i], j);
	}
}

static void
touchglrstack(GlrParse *p, GlrStack *sp, int pass)
{
	GlrEdge *e;
	
	if(sp->ntouch == ntouch)
		return;
	sp->ntouch = ntouch;
	for(e=sp->allsib; e; e=e->allsib){
		p->touch(e->val, pass);
		touchglrstack(p, e->down, pass);
	}
}

static GlrEdge*
mkedge(GlrParse *p, CfgGram *g, CfgSym *sym, CfgValue *val, GlrStack *down, GlrEdge *sib, GlrEdge *allsib)
{
	GlrEdge *e;
	
	if((e = p->freeedges) != nil)
		p->freeedges = e->sib;
	else
		e = (GlrEdge*)emalloc(sizeof *e);
	e->sib = sib;
	e->down = down;
	down->ref++;
	e->sym = sym;
	e->val = val;
	e->allsib = allsib;
	e->nullval = 0;
	if(p->dup)
		p->dup(sym, &e->val, val);
	nmkedge++;
	return e;
}

static void freestack(GlrParse*, GlrStack*);

static void
freeedge(GlrParse *p, GlrEdge *e)
{
	GlrEdge *next;
	
	for(; e; e=next){
		next = e->allsib;
		nfreeedge++;
		freestack(p, e->down);
		if(p->free)
			p->free(e->sym, e->val);
		if(Freelists){
			e->sib = p->freeedges;
			p->freeedges = e;
		}else
			free(e);
	}
}

static GlrStack*
mkstack(GlrParse *p, SlrState *state)
{
	GlrStack *s;
	
	if((s = p->freestacks) != nil){
		p->freestacks = (GlrStack*)s->state;
		memset(s->edge, 0, sizeof s->edge);
	}else
		s = (GlrStack*)emalloc(sizeof *s);
	s->ipos = p->ipos;
	s->state = state;
	s->ref = 1;
	s->seq = nmkstack++;
	return s;
}

static inline int
shash(GlrStack *sp)
{
	return sp->seq % nelem(sp->edge);
}

static void
freestack(GlrParse *p, GlrStack *s)
{
	if(--s->ref > 0)
		return;
	nfreestack++;
	if(s->ref < 0)
		abort();
	freeedge(p, s->allsib);
	if(Freelists){
		s->allsib = nil;
		s->state = (void*)p->freestacks;
		p->freestacks = s;
	}else
		free(s);
}

static GlrPath *freepaths[10];

static GlrPath*
mkpath(GlrParse *p, CfgRule *r, GlrStack *sp, GlrPathelem *pe)
{
	int i;
	GlrPath *path;
	
	nmkpath++;
	i = r->nright;
	if((path = freepaths[i]) != nil)
		freepaths[i] = (void*)path->sp;
	else
		path = (GlrPath*)emalloc(sizeof *path+i*sizeof path->edge[0]);
	path->sp = sp;
	path->rule = r;
	for(i=0; i<r->nright && pe; i++){
		path->edge[i] = pe->edge;
		pe = pe->next;
	}
	path->nedge = i;
	if(path->nedge >0)
		path->leftpos = path->edge[0]->down->ipos;
	else
		path->leftpos = p->ipos;
	npath++;
	return path;
}

static void
freepath(GlrPath *path)
{
	int i;
	
	nfreepath++;
	i = path->rule->nright;
	if(i < nelem(freepaths)){
		path->sp = (void*)freepaths[i];
		freepaths[i] = path;
	}else
		free(path);
}

GlrParse*
glrnewparse(CfgGram *g)
{
	GlrParse *p;
	
	p = (GlrParse*)emalloc(sizeof(GlrParse));
	p->g = g;
	return p;
}

static GlrParse*
glrparseinit(CfgGram *g, GlrParse *p, CfgSym *start)
{
	CfgValue **val;
	SlrState* init;
	
	slrcompile(g);
	init = slrinitstate(g, start);
	if(!init)
		return nil;

	if(p == nil)
		p = glrnewparse(g);

	p->mtop = 32;
	p->top = (GlrStack**)emalloc(p->mtop*sizeof p->top[0]);
	p->top[0] = mkstack(p, init);
	p->ntop = 1;
	val = emalloc(20*sizeof val[0]);
	p->tmpval = val;
	p->toks = nil;
	p->seqnum = g->seqnum;
	return p;
}

void
glrtokenlistfree(GlrParse *p, GlrTokenL *l)
{
	GlrTokenL *l1;
	
	for(l1=l; l1; l1=l1->tl)
		glrfreetoken(p, l1->hd);
	freeGlrTokenL(l);
}

void
glrfreeparse(GlrParse *p)
{
	GlrToken *t, *tnext;
	GlrEdge *e, *enext;
	GlrStack *s, *snext;

	// XXX free paths inside p?
	glrtokenlistfree(p, p->toks);
	for(t=p->freetoks; t; t=tnext){
		tnext = (GlrToken*)t->val;
		free(t);
	}
	for(e=p->freeedges; e; e=enext){
		enext = e->sib;
		free(e);
	}
	for(s=p->freestacks; s; s=snext){
		snext = (GlrStack*)s->state;
		free(s);
	}
	free(p);
}

static GlrEdge*
edge1(GlrStack *sp)
{
	return sp->allsib;
}

static void
printinput(GlrStack *sp)
{
	GlrEdge *e;
	
	if((e = edge1(sp)) != nil){
		printinput(e->down);
		print(" %s", e->sym->name);
	}
}

/*
 * Parsing work queue (heap).
 * Sorted by:
 *	Rule 1: reductions which span fewer tokens com first.
 *	Rule 2: If two reductions A -> alpha and B -> beta span
 *		the same tokens, then A -> alpha comes first if B ->+ A.
 * See section 4.2 of McPeak's report.
 */
static int
leadsto(CfgSym *a, CfgSym *b)
{
	return inbitset(a->leadsto, b->n);
}
static int
ypathcmp(GlrPath *a, GlrPath *b)
{
	if(a->leftpos != b->leftpos)
		return b->leftpos - a->leftpos;
	if(a->rule->left == b->rule->left)
		return 0;
	if(leadsto(b->rule->left, a->rule->left))
		return -1;
	if(leadsto(a->rule->left, b->rule->left))
		return 1;
	return 0;
}

static void
addpath(GlrParse *p, GlrPath *path)
{
	int i, up;
	
	if(p->nheap >= p->mheap){
		assert(p->nheap == p->mheap);
		p->mheap += 100;
		p->heap = (GlrPath**)erealloc(p->heap, p->mheap*sizeof p->heap[0]);
	}
	p->heap[i=p->nheap++] = path;
	for(; i>0; i=up){
		up = (i-1)/2;
		if(ypathcmp(path, p->heap[up]) >= 0)
			break;
		p->heap[i] = p->heap[up];
		p->heap[up] = path;
	}
}

static GlrPath*
nextpath(GlrParse *p)
{
	int i, down;
	GlrPath *ret;
	GlrPath *path;
	
	if(p->nheap == 0)
		return nil;
	ret = p->heap[0];
	--p->nheap;
	if(p->nheap > 0){
		path = p->heap[p->nheap];
		p->heap[0] = path;
		for(i=0; i<p->nheap; i=down){
			down = i*2+1;
			if(down >= p->nheap)	/* no kids */
				break;
			/* determine smaller kid */
			if(down+1 < p->nheap && ypathcmp(p->heap[down], p->heap[down+1]) > 0)
				down++;
			if(ypathcmp(path, p->heap[down]) < 0)
				break;
			p->heap[i] = p->heap[down];
			p->heap[down] = path;
		}
	}
	return ret;
}

/*
 * Parse
 */
static void walkpaths(GlrPathelem*, GlrParse*, GlrStack*, uint, CfgRule*);
static void reducepath(CfgGram*, GlrParse*, GlrPath*, CfgSym*);

static void
doreduce(CfgGram *g, GlrParse *p, GlrTokenL *toks)
{
	int i, j;
	CfgRule *r;
	GlrStack *sp;
	GlrPath *path;
	CfgSym *sym;
	SlrAction *a;

	touchgraph(p);

	if(toks && !toks->tl)
		sym = toks->hd->sym;
	else
		sym = nil;

	/*
	 * LR optimization
ndoreduce++;
	if(p->ntop == 1 && (!toks || !toks->tl)){
		sym = nil;
		if(toks)
			sym = toks->hd->sym;
		a = slraction(p->top[0]->state, sym);
		if(a->nreduce == 0){
			nlrreduce++;
			return;
		}
		if(a->nreduce == 1){
			r = a->reduce[0];
			if(r->nright > p->top[0]->nlinear || r->nright > MaxLinear)
				goto hard;
			reducelinear(g, p, r, a->reduce[0].nnull, sym);
			if(p->nheap > 0)
				goto finish;
			nlrreduce++;
			return;
		}
	}
	 */

hard:
	for(i=0; i<p->ntop; i++){
		sp = p->top[i];
		a = slraction(sp->state, sym);
		for(j=0; j<a->nreduce; j++){
			r = a->reduce[j].rule;
			walkpaths(nil, p, sp, a->reduce[j].nright, r);
		}
	}

	touchgraph(p);

finish:
	while((path = nextpath(p)) != nil){
		reducepath(g, p, path, sym);
		freepath(path);
	}

	touchgraph(p);
}

static void
qreduce1(CfgGram *g, GlrParse *p, GlrStack *sp, GlrEdge *e, CfgSym *sym)
{
	int i, n;
	CfgRule *r;
	GlrPathelem pe;
	SlrAction *a;

	if(e->nullval)
		return;

	/*
	 * Add all possible reductions that use the (newly added) edge e
	 * to the work queue.
	 */
	if(!sp->state->compiled)
		slrcompilestate(g, sp->state);
	a = slraction(sp->state, sym);
	for(i=0; i<a->nreduce; i++){
		r = a->reduce[i].rule;
		n = a->reduce[i].nright;
		if(n == 0){
			addpath(p, mkpath(p, r, sp, nil));
			continue;
		}
		pe.edge = e;
		pe.next = nil;
		walkpaths(&pe, p, e->down, n-1, r);
	}
}

/*
 * Explore paths and queue them.
 */
static void
walkpaths(GlrPathelem *pep, GlrParse *p, GlrStack *sp, uint n, CfgRule *r)
{
	GlrEdge *e;
	GlrPathelem pe;

	if(n == 0){
		addpath(p, mkpath(p, r, sp, pep));
		return;
	}
	
	for(e=sp->allsib; e; e=e->allsib){
		if(pep == nil && e->nullval)
			continue;
		pe.edge = e;
		pe.next = pep;
		walkpaths(&pe, p, e->down, n-1, r);
	}
}

/*
 * Find the stack top ending in state.
 */
static GlrStack*
findtop(GlrParse *p, SlrState *state)
{
	int i;
	GlrStack *sp;
	
	for(i=0; i<p->ntop; i++){
		sp = p->top[i];
		if(sp->state == state)
			return sp;
	}
	return nil;
}

/*
 * Create a new stack top for state.
 */
static GlrStack*
newtop(GlrParse *p, SlrState *state)
{
	GlrStack *sp;

	sp = mkstack(p, state);
	if(p->ntop >= p->mtop){
		p->mtop += 40;
		p->top = (GlrStack**)erealloc(p->top, p->mtop*sizeof p->top[0]);
	}
	p->top[p->ntop++] = sp;
	return sp;
}

/*
 * Find the edge coming out of sp that leads to down.
 */
static GlrEdge*
findedge(GlrStack *sp, GlrStack *down)
{
	GlrEdge *e;
	
	for(e=sp->edge[shash(down)]; e; e=e->sib)
		if(e->down == down)
			return e;
	return nil;
}

static void
reducelinear(CfgGram *g, GlrParse *p, CfgRule *r, int nnull, CfgSym *sym)
{
	int i;
	struct {
		GlrPath p;
		GlrEdge e[MaxLinear];
	} s;
	GlrPath *path;
	GlrStack *sp;

	path = &s.p;
	sp = p->top[0];
	path->rule = r;

	for(i=0; i<r->nright - nnull; i++){
		path->edge[r->nright-1 - i] = sp->allsib;
		sp = sp->allsib->down;
	}
	path->sp = sp;
	path->nedge = r->nright - nnull;
	if(path->nedge > 0)
		path->leftpos = path->edge[0]->down->ipos;
	else
		path->leftpos = p->ipos;

	reducepath(g, p, path, sym);
}

/*
 * Create a ParseValue representing the empty string.
 * Only finds one way, which is not quite correct:
 * the GLR parser should return all the ways.  Too bad.
 */
static int
mkemptyval(CfgGram *g, GlrParse *p, CfgSym *sym, CfgValue **out)
{
	int i;
	CfgRule *r;
	CfgValue **v;
	
	assert(sym->emptyrule);
	r = sym->emptyrule;
	v = emalloc((1+r->nright)*sizeof v[0]);
	for(i=0; i<r->nright; i++){
		if(mkemptyval(g, p, r->right[i], &v[1+i]) < 0){
			if(p->free)
				for(i--; i>=0; i--)
					p->free(r->right[i], v[1+i]);
			free(v);
			return -1;
		}
	}
	if(r->nright > 0)
		v[0] = v[1];
	else
		v[0] = nil;
	if(r->fn){
		if(r->fn(g, r, v, v+1) < 0){
			if(p->free)
				for(i=0; i<r->nright; i++)
					p->free(r->right[i], v[1+i]);
			free(v);
			return -1;
		}
	}else if(p->dup)
		p->dup(r->left, &v[0], v[1]);
	if(p->free){
		for(i=0; i<r->nright; i++)
			p->free(r->right[i], v[1+i]);
	}
	*out = v[0];
	free(v);
	return 0;
}

/*
 * Apply a reduction along a given path.
 */
static void
reducepath(CfgGram *g, GlrParse *p, GlrPath *path, CfgSym *sym)
{
	int i, h;
	GlrStack *sp;
	GlrEdge *e;
	CfgRule *rule;
	SlrState *nextstate;

if(g->debug) print("reduce %R\n", path->rule);

	touchgraph(p);

	rule = path->rule;

	nextstate = slraction(path->sp->state, rule->left)->shiftstate;
	if(nextstate == nil)	// Should never happen.
		panic("reducepath %d %d %R", rule->nright, path->nedge, rule);
	
	// XXX check for overflow in p->tmpval? (unlikely)
	for(i=0; i<rule->nright; i++){
		if(i < path->nedge)
			p->tmpval[1+i] = path->edge[i]->val;
		else{
			if(mkemptyval(g, p, rule->right[i], &p->tmpval[1+i]) < 0){
				if(p->free)
					for(i--; i>=path->nedge; i--)
						p->free(rule->right[i], p->tmpval[1+i]);
				return;
			}
		}			
		/* no dup - the action rule must dup if it wants to keep it */
	}
	p->tmpval[0] = p->tmpval[1];
	if(rule->fn){
		touchgraph(p);
		if(rule->fn(g, rule, p->tmpval, p->tmpval+1) < 0){
			if(p->free){
				for(i=path->nedge; i < rule->nright; i++)
					p->free(rule->right[i], p->tmpval[1+i]);
			}
			return;
		}
		touchgraph(p);
	//	reshift(g, start, p);
	}else if(p->dup)
		p->dup(rule->left, &p->tmpval[0], p->tmpval[1]);
	if(p->free){
		for(i=path->nedge; i < rule->nright; i++)
			p->free(rule->right[i], p->tmpval[1+i]);
	}
	touchgraph(p);

	if((sp = findtop(p, nextstate)) == nil)
		sp = newtop(p, nextstate);

	if((e = findedge(sp, path->sp)) == nil){
		h = shash(path->sp);
		if(sp->allsib)
			sp->nlinear = 0;	// XXX need to find people pointing at sp
		else
			sp->nlinear = 1 + path->sp->nlinear;
		e = mkedge(p, g, rule->left, p->tmpval[0], path->sp, sp->edge[h], sp->allsib);
		if(path->nedge == 0)
			e->nullval = 1;
		sp->allsib = sp->edge[h] = e;
		/* McPeak skips qreduce if we called newtop ? */
		qreduce1(g, p, sp, sp->edge[h], sym);
	}else{
		e->nullval = 0;
		if(p->merge)
			p->merge(rule->left, &e->val, p->tmpval[0]);
	}
	touchgraph(p);
	if(p->free)
		p->free(rule->left, p->tmpval[0]);
	touchgraph(p);
}

/*
 * Apply all possible shifts.
 */
static void doshift1(CfgGram*, GlrParse*, GlrStack**, int, CfgSym*, CfgValue*);
static void
doshift(CfgGram *g, GlrParse *p, GlrTokenL *toks)
{
	int i, ntop;
	GlrTokenL *l;
	GlrStack **top;
	GlrToken *tok;
	CfgSym *sym;
	SlrAction *a;

	touchgraph(p);
	
	/*
	 * LR optimization.
	 */
ndoshift++;
if(0)	if(p->ntop == 1 && (toks==nil || toks->tl == nil)){
		nlrshift++;
		sym = nil;
		if(toks)
			sym = toks->hd->sym;
		a = slraction(p->top[0]->state, sym);
		if(a->shiftstate == nil)
			return;
	}

	/*
	 * Copy old stack tops - we use them many times.
	 */
	ntop = p->ntop;
	top = p->top;
	p->top = nil;
	p->mtop = 0;
	p->ntop = 0;
	
	/*
	 * Try to shift each possible symbol.
	 */
	for(l=toks; l; l=l->tl){
		tok = l->hd;
		doshift1(g, p, top, ntop, tok->sym, tok->val);
	}

	touchgraph(p);

	/*
	 * Drop refs to old stack tops.
	 */
	for(i=0; i<ntop; i++)
		freestack(p, top[i]);
	free(top);

	touchgraph(p);
}

static void
doshift1(CfgGram *g, GlrParse *p, GlrStack **top, int ntop, CfgSym *sym, CfgValue *val)
{
	int i, h;
	GlrStack *sp, *nsp;
	SlrState *nstate;

	for(i=0; i<ntop; i++){
		sp = top[i];
		nstate = slraction(sp->state, sym)->shiftstate;
		if(nstate == nil)
			continue;
		if((nsp = findtop(p, nstate)) == nil)
			nsp = newtop(p, nstate);
		h = shash(sp);
		if(nsp->allsib)
			nsp->nlinear = 0;	// XXX update guys pointing at nsp
		else
			nsp->nlinear = 1 + sp->nlinear;
		nsp->edge[h] = nsp->allsib = mkedge(p, g, sym, val, sp, nsp->edge[h], nsp->allsib);
	}
}

void
glrprintstate(GlrParse *p)
{
	int i;
	
	for(i=0; i<p->ntop; i++)
		print("%d.%p. %l@\n", i, p->top[i]->state, p->top[i]->state);
}

#define H ((void*)-1)

void
glrfreetoken(GlrParse *p, GlrToken *t)
{
	if(t == nil)
		return;
	if(p->free)
		p->free(t->sym, t->val);
	t->val = nil;
	assert(t->sym != H);
	t->sym = (CfgSym*)H;
	if(Freelists){
		t->val = (void*)p->freetoks;
		p->freetoks = t;
	}else
		free(t);
}

GlrToken*
glrnewtoken(GlrParse *p, CfgSym *sym)
{
	GlrToken *t;

	if((t = p->freetoks) != nil)
		p->freetoks = (void*)t->val;
	else
		t = emalloc(sizeof *t);
	t->sym = sym;
	t->val = nil;
	return t;
}

void
glrstats(void)
{
	fprint(2, "shift: %d/%d, reduce: %d/%d\n",
		ndoshift, nlrshift, ndoreduce, nlrreduce);
	fprint(2, "mk: edge %d/%d stack %d/%d path %d/%d ast %d/%d\n",
		nmkedge, nfreeedge, nmkstack, nfreestack, nmkpath, nfreepath, nmkast, nfreeast);
}

CfgValue*
glrparse(CfgGram *g, CfgSym *start, GlrParse *p0, GlrLexer *yylex, void *arg)
{
	int i;
	void *rv;
	GlrParse *p;
	CfgSym *eofsym;
	GlrToken *t;
	GlrTokenL *l;
	CfgValue *v;

static int first = 1;
if(first){
	atexit(glrstats);
	first = 0;
}

	p = glrparseinit(g, p0, start);
	if(p == nil)
		return nil;

	rv = nil;
	eofsym = cfglooksym(g, N("EOF"));
	if(g->debug>1)
		glrprintstate(p);
	for(;;){
		glrtokenlistfree(p, p->toks);
		if((p->toks = yylex(p, g, arg)) == nil){
			fprint(2, "weird: got unexpected nil from yylex\n");
			break;
		}
		p->ipos++;
		if(g->debug){
			print("yylex");
			for(l=p->toks; l; l=l->tl){
				t = l->hd;
				print(" %s", t->sym->name);
			}
			print("\n");
			if(g->debug>1)
				print("reduce\n");
		}
		doreduce(g, p, p->toks);
		if(g->debug>1)
			glrprintstate(p);
		if(g->debug>1)
			print("shift\n");
		doshift(g, p, p->toks);
		if(p->ntop == 0){
			if(g->debug){
				fprint(2, "lost parse near %s\n", ((CfgSym*)p->toks->hd)->name);
				glrprintstate(p);
			}
			break;
		}
		if(g->debug>1)
			glrprintstate(p);
		t = (GlrToken*)p->toks->hd;
		if(t->sym == eofsym){
			if(g->debug)
				fprint(2, "success\n");
			if(p->ntop > 1){
				fprint(2, "ambiguous parse -- too many stack tops\n");
				break;
			}
			v = edge1(edge1(p->top[0])->down)->val;
			if(p->dup)
				p->dup(p->toks->hd->sym, &v, v);
			rv = v;
			break;
		}
	}

	for(i=0; i<p->ntop; i++)
		freestack(p, p->top[i]);
	free(p->top);
	p->top = nil;
	p->ntop = 0;
	p->mtop = 0;
	glrtokenlistfree(p, p->toks);
	p->toks = nil;
//	if(p != p0)
//		yyfreeparse(p);
	return rv;
}

LISTIMPL(GlrToken)
