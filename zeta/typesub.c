#include "a.h"
#include "type.h"
#include "runtime.h"
#include "grammar.h"

// Can nil be used as this type?
// It's safe for nil to be used as any type--zero is the initial
// value for all variables--but we want to avoid weirdness
// like using nil as a bool or number.
int
nillable(TypeOp op)
{
	return op == TypeString ||
		op == TypeTuple ||
		op == TypeList ||
		op == TypeArray ||
		op == TypeMap ||
		op == TypeFmap ||
		op == TypeFn ||
		op == TypeStruct ||
		op == TypeWrapper ||
		op == TypeNil ||
		op == TypeGram;
}

// Is t1 a subtype of t2?
// Remember that nil (didn't type check) is different from typenil().
int
issubtype(Type *t1, Type *t2)
{
	NameL *l1, *l2;

	if(t1 == nil || t2 == nil)
		return 0;

	if(t1 == t2)
		return 1;
	// TypeAny is used when there is no instance of an actual type.
	// For example, list [] has type list TypeAny.  Essentially TypeAny
	// is a giant wildcard, taking on any type imaginable at any time.
	if(t1->op == TypeAny || t2->op == TypeAny)
		return 1;
	if(t1->op == TypeNil)
		return nillable(t2->op);
	if(t1->op != t2->op)
		return 0;
	if(t1->op == TypeStruct){
		for(; t1; t1=t1->super)
			if(t1 == t2)
				return 1;
		return 0;
	}
	if(t1->op == TypeGram){
		if(t2->gram == nil)
			return 1;
		if(t1->gram != t2->gram)
			return 0;
		if(t2->sym && t1->sym != t2->sym)
			return 0;
		while(t1->sym)
			t1 = t1->left;
		while(t2->sym)
			t2 = t2->left;
		// t2 must have all of t1's names,
		// unless it is gram+?
		// The lists are in sorted order.
		if(t2->gramx && !t2->gramx->tl && t2->gramx->hd == N("?"))
			return 1;
		l1 = t1->gramx;
		for(l2=t2->gramx; l2; l2=l2->tl)
			if(l1 && l1->hd == l2->hd)
				l1 = l1->tl;
		if(l1)
			return 0;
		return 1;
	}
	return 0;
}

// Take minimum of two types.
// Remember that nil (no constraint) is different from typenil().
// Lattice and type theory weenies call this the meet or infimum.
int
typemin(Type *t1, Type *t2, Type **min)
{
	if(t1 == nil){
		*min = t2;
		return 0;
	}
	if(t2 == nil){
		*min = t1;
		return 0;
	}
	
	// A constraint t1 says that the desired type needs to be
	// t1 or a descendant (subtype) of t1 in the type hiearchy.  
	// Since the type hierarchy is a tree, the only way to satisfy
	// constraints t1 and t2 is if t1 is a subtype of t2 or vice versa.
	//
	// Actually the hierarchy is not a tree--the expression "nil"
	// can be a string or a list or an array, which makes it a dag--
	// but since nil is itself a minimum type, that doesn't break anything.
	//
	// XXX Do we have to treat the grammar types specially in typemin?
	
	if(issubtype(t1, t2)){
		*min = t1;
		return 0;
	}
	if(issubtype(t2, t1)){
		*min = t2;
		return 0;
	}
	return -1;
}

static int
isquest(Type *t)
{
	return t->gramx && t->gramx->tl == nil && t->gramx->hd == N("?");
}

// Take maximum of two types.  t1==nil or t2==nil means no constraint.
// Remember that nil is different from typenil().
// Lattice and type theory weenies call this the join or supremum.
int
_typemax(Type *t1, Type *t2, Type **max)
{
	Type *t;
	CfgSym *sym;
	
	// max(nil, x) = x for x = nillable type.
	if(t2->op == TypeNil){
		t = t2;
		t2 = t1;
		t1 = t;
	}
	if(t1->op == TypeNil){
		if(nillable(t2->op)){
			*max = t2;
			return 0;
		}
	}

	// Many axes for grammar types.
	if(t1->op == TypeGram){
		if(t2->op != TypeGram)
			return -1;
		if(t1->gram == nil || t2->gram == nil || t1->gram != t2->gram){
			*max = typegram1(nil, nil);
			return 0;
		}
		sym = nil;
		if(t1->sym == t2->sym)
			sym = t1->sym;
		t1 = typegramsym(t1, nil, nil);
		t2 = typegramsym(t2, nil, nil);
		if(t1 == t2){
			assert(sym == nil);
			*max = t1;
			return 0;
		}
		if(isquest(t1))
			t = t1;
		else if(isquest(t2))
			t = t2;
		else
			t = typegramx(t1, addNameL(copyNameL(t1->gramx), t2->gramx));
		if(sym)
			t = typegramsym(t, sym->name, sym);
		*max = t;
		return 0;
	}
	
	if(t1->op == TypeStruct){
		if(t2->op != TypeStruct)
			return -1;
		for(; t1; t1=t1->super)
			for(t=t2; t; t=t->super)
				if(t == t1){
					*max = t1;
					return 0;
				}
		return -1;
	}
			
	// TODO: More typemax cases go here, as needed.
fprint(2, "no max %T %T\n", t1, t2);

	return -1;
}

int
typemax(Type *t1, Type *t2, Type **max)
{
	static Hash *h1;
	Hash *h2;
	Type *t;

	if(t1 == nil){
		*max = t2;
		return 0;
	}
	if(t2 == nil){
		*max = t1;
		return 0;
	}	
	if(t1 == t2){
		*max = t1;
		return 0;
	}
	if(t1->op == TypeGram && t2->op == TypeGram){
		if(t1->sym != t2->sym){
			while(t1->sym)
				t1 = t1->left;
			while(t2->sym)
				t2 = t2->left;
		}
		if(t1 == t2){
			*max = t1;
			return 0;
		}
	}

	if(h1 == nil)
		h1 = mkhash();
	if((h2 = hashget(h1, t1)) == nil)
		hashput(h1, t1, h2 = mkhash());
	t = hashget(h2, t2);
	if(t){
		*max = t;
		return 0;
	}
	if(_typemax(t1, t2, max) >= 0){
		hashput(h2, t2, *max);
		return 0;
	}
	return -1;
}

Type*
typegramadd(Type *t1, Type *t2)
{
	Type *t;

	if(t1 == nil)
		return t2;
	if(t2 == nil)
		return t1;
	if(t1->op != TypeGram || t2->op != TypeGram){
	NoAdd:
		werrstr("cannot add %T and %T", t1, t2);
		return nil;
	}
	if(t1->gram == nil || t2->gram == nil || t1->gram != t2->gram)
		goto NoAdd;
	t = nil;
	if(typemax(t1, t2, &t) < 0)
		goto NoAdd;
	return t;
}

Type*
typegramquest(Type *t1)
{
	return typegramxl(t1, N("?"), nil);
}
