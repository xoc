// Run-time for grammars

typedef struct ParseValue ParseValue;
typedef struct Gram Gram;
typedef struct GramPrec GramPrec;
typedef struct Ast Ast;
typedef struct Line Line;

// CFG values
typedef ParseValue CfgValue;
typedef void SlrLexerArg;

#include "cfg.h"
#include "slr.h"
#ifdef GLR
#include "glr.h"
#define RgTokenL GlrTokenL
#define RgToken GlrToken
#define RgParse GlrParse
#define mkRgTokenL mkGlrTokenL
#define revRgTokenL revGlrTokenL
#define RgTokenLL GlrTokenLL
#define mkRgTokenLL mkGlrTokenLL
#define revRgTokenLL revGlrTokenLL
#define rgnewparse glrnewparse
#define rgfreeparse (void)
#define rgnewtoken glrnewtoken
#define rgparse glrparse
#else
#include "rnglr.h"
#endif

LIST(CfgSym)
LIST(Gram)
LIST(Ast)

// DFA values
typedef CfgSym DKey;
typedef CfgSymL DKeyL;

#include "nfa.h"
#include "dfa.h"

struct Gram
{
	Name name;
	Type *type;
	CfgGram *cfg;
	Hash *syms;
	DFA *dfa;
	int dfaprec;
	Hash *precs;
	Hash *ignores;
	Hash *rules;
	CfgSym *ignore;
	CfgSym *any;
	TypeL *gramx1s;
};

struct GramPrec
{
	CfgPrec *cfgprec;
};

Gram*	mkGram(Name name);
void	gramaddrule(Type*, GramPrec*, CfgSym*, CfgSymL*);
CfgSym*	gramsym(Type*, Name);
CfgSym*	gramliteral(Type*, Name);
CfgSym*	gramregexp(Type*, Name, int regexpflags, int canfail);
GramPrec*	gramprec(Gram*, Name, CfgAssoc, CfgPrecKind, int creat);
CfgSym*	gramlooksym(Gram*, Name);
CfgRule*	gramlookrule(Gram*, Name);
CfgSym*	gramunlistsym(Gram*, CfgSym*);

int	gramignoring(Gram*, CfgSym*);
CfgSym*	gramignore(Gram*, Name, int, int);
int		grampriority(Gram*, Name, Name, int);

// Parsing

struct Line
{
	Name file;
	int line;
};

// TODO: Replace ParseValue with Ast.
struct ParseValue
{
	int ref;	// reference count during parsing
	int refcheck;
	Ast *ast;
	Line line;
};
ParseValue *mkParseValue(Ast*);
void pvfree(CfgSym*, CfgValue*);
void pvdup(CfgSym*, CfgValue**, CfgValue*);

Ast *parsetext(Type*, Name);
int astfmt(Fmt*);

// Abstract syntax trees
#define NoParentYet	((Ast*)-1)

typedef struct Attr Attr;
typedef enum AstOp {
	AstString = 1,
	AstRule,
	AstMerge,
	AstSlot,
} AstOp;
struct Ast
{
	uchar op;
	uchar slotnum;
	uchar ref;
	uchar nright;
//	int refcheck;
	Ast *canonical;
	Ast *copiedfrom;
	Type *tag;
	Line line;
	Ast *parent;
	Map attrmap;

	CfgSym *sym;
	CfgRule *rule;
	Name text;
	Ast **right;
};
Ast *mkastrule(CfgRule*, Ast**);
Ast *mkaststring(CfgSym*, Name);
Ast *mkastmerge(Ast*, Ast*);
Ast *mkastslot(Type*, CfgSym*, int);
Ast *mkAst(AstOp, CfgSym*);

Ast *asttoquestion(Ast*, CfgSym*);	/* sym is ? type */
Ast *astfromquestion(Ast*, CfgSym*);	/* sym is ? type */
Ast *astlisttostar(Zlist*, CfgSym*);	/* sym is * type */
Ast *astlisttoplus(Zlist*, CfgSym*);	/* sym is + type */
int aststartolist(Ast*, CfgSym*, Zlist**);	/* sym is * type */
int astplustolist(Ast*, CfgSym*, Zlist**);	/* sym is + type */
Zlist *asttolist(Ast*);
Zarray *astsplit(Ast*);
void	astremovecopiedfrom(Ast*);
Ast *astjoin(Ast*, Zarray*);
Ast *astcopy(Ast*);
Ast *astlastkid(Ast*);
Name	astlisp(Ast*);
Name	astlong(Ast*);
Name	asttostring(Ast*);
int	astmerged(Ast*);
int	astslotted(Ast*);
Ast*	astdup(Ast*);
void	astfree(Ast*);
void	astrefcheck(Ast*, int);
Ast *astleak(Ast*);
Ast	*astright(Ast*, int);
Zpoly*	astline(Ast*);
Ast	*astcanonicalize(Ast*);

Ast	*astparent(Ast*, Type*);
Ast	*astcvtslot(Ast*, Type*);

void	astswap(Ast*, Ast*);

int	astsame(Ast*, Ast*);

#define astparent_attr(a) astparent(a, 0)
Ast	*astprev_attr(Ast*);
Ast	*astnext_attr(Ast*);
Ast	*astcopiedfrom_attr(Ast*);
int	parseaction(CfgGram*, CfgRule*, ParseValue**, ParseValue**);

// Attributes on ASTs

typedef struct AttrInfo AttrInfo;

struct AttrInfo
{
	Name name;
	Type *type;
	Zfn **fn;
	Name pos;
	int registered;
	int ncompute;
};

typedef enum AttrState
{
	AttrUninit = 0,
	AttrComputing,
	AttrInit
} AttrState;
struct Attr
{
	AttrState	state;
	Zpoly	data;
	MapElem	me;
};
void regattr(AttrInfo*);
void attrstats(void);
void attrloop(AttrInfo*);

void	set_attribute(Ast*, AttrInfo*, Zpoly);
Attr *get_attribute(Ast*, AttrInfo*);
//Zpoly	attribute(Ast*, AttrInfo*);

typedef struct AttrLink AttrLink;
struct AttrLink
{
	AttrInfo *info;
	Ast *ast;
	AttrLink *next;
};

extern AttrLink *attrlinks;


extern int nmkast;
extern int nfreeast;
extern int nleakast;

extern int nattr;
extern AttrInfo **attrinfo;

#define GS(_name_) \
	({ \
		static CfgSym *sym; \
		if(!sym){ \
			Name x = _name_; \
			sym = gramlooksym(__tgram__->gram, x); \
			if(sym == nil) \
				panic("no symbol %s in %s", x, __tgram__->name); \
		} \
		sym; \
	})

#define GR(_name_) \
	({ \
		static CfgRule *rule; \
		if(!rule){ \
			Name x = _name_; \
			rule = gramlookrule(__tgram__->gram, x); \
			if(rule == nil) \
				panic("no rule %s in %s", x, __tgram__->name); \
		} \
		rule; \
	})


enum
{
	LeftRecursive = 1
};
