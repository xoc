// Compile-time for parsing patterns

typedef struct PatToken PatToken;
typedef struct ParsePattern ParsePattern;
struct Ast;
struct AstL;

typedef enum {
	PatTokenText = 1,
	PatTokenSlot,
} PatTokenOp;
struct PatToken
{
	Pos pos;
	PatTokenOp op;
	Name text;
	Expr *expr;
	Name symname;
};
PatToken *mkPatToken(PatTokenOp);

LIST(PatToken)


struct ParsePattern
{
	Pos pos;
	Typex *typex;
	PatTokenL *tokenl;
	Type *type;
	struct Ast *ast;
	struct AstL *slotl;
	int nslot;
};
ParsePattern *mkParsePattern(void);

extern int patterndebug;
