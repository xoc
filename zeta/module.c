// Zeta module loader
//
// A module is the compiled form of a zeta source file.
// The source a.zeta compiles into two files: metadata a.zm 
// and a shared object a.zo.  
// 
// The metadata records the name, size, and modification time
// of every input file that was read during compilation.
// These make it possible to check whether the compiled object
// needs to be rebuilt.  The metadata also lists all the 
// othe rmodules imported by the module.  The .zo for those
// other modules have to be loaded before the .zo for this one.
//
// The module loader invokes the compiler on demand, as needed.
//
// The shared object .zo contains three special symbols: 
// "__init__", a void function which initializes the module,
// "__run__", a void function which runs top-level statements
// in the module, and "__types__", an array of all the Type* needed
// by the module.
//
// The init function sets up the T array -- it is far easier
// to create those Types using C code than to try to shoehorn
// them back into parsable zeta code.
//
// The .zm lists all the top-level variables declared by the
// module, along with their types as indices into T.

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <dlfcn.h>
#include "a.h"
#include "type.h"
#include "runtime.h"
#include "compile.h"
#include "listimpl.h"

static Hash *mcache;
static Module *readmodule(Name, int);
static int compilemodule(Name);

ulong zetacomtime;

NameL *compiling;
NameL *modpath;
ModuleL *importl;

#define Loading ((Module*)-1)

void
registermodule(Name name, Module *m)
{
	if(mcache == nil)
		mcache = mkhash();
	m->internal = 1;
	hashput(mcache, name, m);
	importl = mkModuleL(m, importl);
}

Module*
builtinmodule(Name name)
{
	Module *m;
	
	m = hashget(mcache, name);
	if(m && m->internal)
		return m;
	return nil;
}

Name
modulename(Name file)
{
	char *s, *p;
	Name n;
	
	s = estrdup(nametostr(file));
	p = strrchr(s, '/');
	if(p)
		p++;
	else
		p = s;
	if(strcmp(p+strlen(p)-5, ".zeta") == 0)
		p[strlen(p)-5] = 0;
	n = nameof(p);
	free(s);
	return n;
}

extern NameL *sys_modpath;

Module*
loadmodule(Name file, Name pname, int run)
{
	Name name;
	Module *m;
	NameL *nl;
	
	if(mcache == nil)
		mcache = mkhash();
	if(access(nametostr(file), 0) < 0){
		if(pname == nil)
			return nil;
		for(nl=sys_modpath; nl; nl=nl->tl){
			file = nameprint("%s/%s.zeta", nl->hd, pname);
			if(access(nametostr(file), 0) >= 0)
				break;
			file = nil;
		}
		if(file == nil)
			return nil;
	}
	name = modulename(file);
	m = hashget(mcache, name);
	if(m){
		if(m == Loading){
			fprint(2, "module loop: %s\n", file);
			werrstr("module loop");
			return nil;
		}
		return m;
	}
	hashput(mcache, name, Loading);

	if((m = readmodule(file, run)) == nil){
		if(compilemodule(file) < 0){
fprint(2, "compile %s failed: %r\n", file);
			hashput(mcache, name, nil);
			return nil;
		}
		if((m = readmodule(file, run)) == nil){
fprint(2, "read %s failed after compile: %r\n", file);
			hashput(mcache, name, nil);
			return nil;
		}
	}
	hashput(mcache, name, m);
	importl = mkModuleL(m, importl);
	return m;
}

static int
split(char *s, char **f, int mf)
{
	int i;
	
	i = 0;
	if(mf <= 0)
		return 0;
	for(;;){
		f[i++] = s;
		if(i >= mf)
			return i;
		while(*s != 0 && *s != '\t')
			s++;
		if(*s == 0)
			return i;
		*s++ = 0;
	}
}

static char*
unsplit(char **f, int nf)
{
	int i;

	for(i=0; i<nf-1; i++)
		f[i][strlen(f[i])] = '\t';
	return f[0];
}

static Module*
readmodule(Name zeta, int run)
{
	char *odata, *data, *line;
	char *f[10];
	int nf;
	Name zm, zo;
	Module *m, *mm;
	struct stat st;
	MVarL *vl;
	MVar *v;

	if(zetacomtime == 0){
		if(stat(PWD "/zetacom", &st) < 0)
			panic("stat %s: %r", PWD "/zetacom");
		zetacomtime = st.st_mtime;
	}

	zm = zeta2zm(zeta);
	odata = data = readfile(nametostr(zm));
	if(data == nil){
		werrstr("%s: %r", zm);
		return nil;
	}
	werrstr("no error yet");
	m = nil;
	if(memcmp(data, "zeta7\n", 6) != 0){
		fprint(2, "bad header: %s\n", data);
		werrstr("bad header");
		goto error;
	}
	data += 6;
	vl = nil;
	m = emalloc(sizeof *m);
	m->zeta = zeta;
	while(*data){
		line = data;
		data = strchr(line, '\n');
		if(data)
			*data++ = 0;
		else
			data = line+strlen(line);

		nf = split(line, f, nelem(f));
		if(nf == 0)
			continue;
		if(strcmp(f[0], "mtime") == 0 && nf == 2){
			m->mtime = strtoul(f[1], 0, 0);
			continue;
		}
		if(strcmp(f[0], "include") == 0 && nf == 4){
			if(stat(f[1], &st) < 0 || st.st_size != strtoul(f[2], 0, 0) ||
					st.st_mtime != strtoul(f[3], 0, 0)){
				werrstr("include %s has changed", f[1]);
				goto error;
			}
			continue;
		}
		if(strcmp(f[0], "import") == 0 && nf == 3){
			mm = loadmodule(nameof(f[1]), nil, run);
			if(mm == nil){
				werrstr("import %s: %r", f[1]);
				goto error;
			}
			if(mm->mtime != strtoul(f[2], 0, 0)){
				werrstr("import %s has changed %lud != %s", f[1], (ulong)mm->mtime, f[2]);
				goto error;
			}
			continue;
		}
		if(strcmp(f[0], "var") == 0 && nf == 6){
			v = emalloc(sizeof *v);
			v->name = nameof(f[1]);
			v->cname = nameof(f[2]);
			v->typeid = atoi(f[3]);
			v->realtypeid = atoi(f[4]);
			v->flags = atoi(f[5]);
			vl = mkMVarL(v, vl);
			continue;
		}
		fprint(2, "%s: unexpected line[%d]: %s\n", zm, nf, unsplit(f, nf));
		werrstr("syntax error");
		goto error;
	}

	if(m->mtime == 0){
		fprint(2, "%s: missing mtime\n", zm);
		werrstr("missing mtime");
		goto error;
	}
	if(m->mtime < zetacomtime){
		werrstr("module mtime older than compiler");
		goto error;
	}
	zo = zeta2zo(zeta);
	if(stat(nametostr(zo), &st) < 0 || st.st_mtime != m->mtime){
		fprint(2, "stat %s: %r [%lu %lu]\n", zo, st.st_mtime, m->mtime);
		werrstr("stat %s: %r", nametostr(zo));
		goto error;
	}

	// Install the variables.
	m->mvarl = revMVarL(vl);
	
	// Point of no return: we're going to load it now.
	free(odata);
	return loadzo(zo, m, run);

error:
	free(odata);
	if(m){
		for(vl=m->mvarl; vl; vl=vl->tl)
			free(vl->hd);
		freeMVarL(m->mvarl);
		free(m);
	}
	return nil;
}

Module*
loadzo(Name zo, Module *m, int run)
{
	MVarL *l;
	Modinfo *mi;
	Type ***typesp;
	void (*init)(void);

	// If no slash, dlopen starts looking all over.
	// If slash, dlopen just opens the named file.
	if(strchr(nametostr(zo), '/') == nil)
		zo = nameprint("./%s", zo);
	m->dl = dlopen(nametostr(zo), RTLD_NOW|RTLD_GLOBAL);
	if(m->dl == nil)
		panic("dlopen %s: %s", zo, dlerror());
	mi = dlsym(m->dl, "__modinfo__");
	if(mi == nil)
		panic("loadmodule %s: no __modinfo__");
	if(mi->magic != ModinfoMagic)
		panic("loadmodule %s: bad modinfo magic %#ux", mi->magic);
	init = mi->init;
	if(init == nil)
		panic("loadomdule %s: no init");
	m->run = mi->run;
	if(m->run == nil)
		panic("loadmodule %s: no run");
	typesp = mi->typesp;
	if(typesp == nil)
		panic("loadmodule %s: no __types__");
	
	// Run the init function: fills in *typesp.
	init();
	if(*typesp == nil)
		panic("loadmodule %s: nil __types__");

	// Finally, resolve the types.
	for(l=m->mvarl; l; l=l->tl){
		l->hd->type = typegeneralize((*typesp)[l->hd->typeid]);
		if(l->hd->realtypeid >= 0)
			l->hd->realtype = (*typesp)[l->hd->realtypeid];
	}

	// If we're running, run.
	if(run)
		m->run();
	return m;
}

static int
compilemodule(Name file)
{
	char **argv;
	int fd, argc, nargv, pid, status;
	NameL *l;
	
	fprint(2, "zetacom %s\n", file);

	for(l=compiling; l; l=l->tl){
		if(l->hd == file){
			fprint(2, "module loop at %s\n", file);
			for(l=compiling; l; l=l->tl)
				fprint(2, "\t%s\n", l->hd);
			panic("module loop");
		}
	}

	pid = fork();
	if(pid < 0)
		panic("fork: %r");

	if(pid > 0){
		if(waitpid(pid, &status, 0) < 0)
			panic("waitpid: %r");
		if(!WIFEXITED(status) || WEXITSTATUS(status) != 0){
			if(WIFEXITED(status))
				panic("zetacom exit %d", WEXITSTATUS(status));
			if(WIFSIGNALED(status))
				panic("zetacom signal %d%s", WTERMSIG(status),
					WCOREDUMP(status) ? " (core dumped)" : "");
			return -1;
		}
		return 0;
	}

	compiling = mkNameL(file, compiling);
	nargv = 10 + lenNameL(compiling) + lenNameL(sys_modpath);
	argv = emalloc(nargv*sizeof argv[0]);
	argc = 0;
	argv[argc++] = PWD "/zetacom";
	for(l=revNameL(compiling); l; l=l->tl)
		argv[argc++] = smprint("-C%s", l->hd);
	for(l=sys_modpath; l; l=l->tl)
		argv[argc++] = smprint("-M%s", l->hd);
	argv[argc++] = nametostr(file);
	argv[argc++] = nil;
	
	// Don't need to see output caused by the compiler
	// loading modules with print statements.
	close(1);
	if((fd = open("/dev/null", O_WRONLY)) >= 0 && fd != 1){
		dup2(fd, 1);
		close(fd);
	}
	execvp(argv[0], argv);
	fprint(2, "exec %s: %r\n", argv[0]);
	_exit(1);
	for(;;);
}

Name
zeta2prefix(Name zeta)
{
	char *t;
	char *w, *s, *ns;
	int n;
	Rune r;
	Name x;

	s = estrdup(nametostr(zeta));

	if(strlen(s) > 5 && strcmp(s+strlen(s)-5, ".zeta") == 0)
		s[strlen(s)-5] = 0;
	t = strrchr(s, '/');
	if(t)
		s = t+1;

	ns = w = emalloc(strlen(s)*4+1);
	for(t=s; *t; t+=n){
		n = chartorune(&r, t);
		if(r >= 0x80 || (!isalpha(r) && !isdigit(r)))
			snprint(w, 6, "_%04x", r);
		else
			*w++ = r;
	}
	*w = 0;
	x = nameprint("z_%s__", ns);
	free(ns);
	return x;
}

Name
zeta2zm(Name zeta)
{
	char *s;
	Name x;

	s = emalloc(strlen(nametostr(zeta)) + 10);
	strcpy(s, nametostr(zeta));
	if(strlen(s) > 5 && strcmp(s+strlen(s)-5, ".zeta") == 0)
		s[strlen(s)-5] = 0;
	strcat(s, ".zm");
	x = nameof(s);
	free(s);
	return x;
}

// Convert file.zeta into file.zo.
// N.B. We used to use a .zo extension "zeta object"
// but to make various tools (i.e., Google's pprof profiler)
// notice the loaded objects, it needs to end with .so.
// So now we use .zo.so.
Name
zeta2zo(Name zeta)
{
	char *s;
	Name x;

	s = emalloc(strlen(nametostr(zeta)) + 10);
	strcpy(s, nametostr(zeta));
	if(strlen(s) > 5 && strcmp(s+strlen(s)-5, ".zeta") == 0)
		s[strlen(s)-5] = 0;
	strcat(s, ".zo.so");
	x = nameof(s);
	free(s);
	return x;
}


LISTIMPL(Module)
LISTIMPL(MVar)
_LISTIMPL(Name, Name)
