// Maintenance of grammars.

#include "a.h"
#include "type.h"
#include "runtime.h"
#include "grammar.h"
#include "listimpl.h"

int gramdebug;
GramL *graml;

Gram*
mkGram(Name name)
{
	Gram *g;
	
	g = emalloc(sizeof *g);
	g->name = name;
	g->cfg = cfgnewgram();
	g->cfg->debug = gramdebug;
	g->cfg->eofsym->ggram = g;
	g->cfg->ggram = g;
	g->syms = mkhash();
	hashput(g->syms, g->cfg->eofsym->name, g->cfg->eofsym);
	g->dfa = mkdfa();
	g->dfaprec = 0;
	g->precs = mkhash();
	g->rules = mkhash();
	graml = mkGramL(g, graml);	// fool valgrind leak detector
	return g;
}

CfgSym*
gramlooksym(Gram *g, Name name)
{
	return hashget(g->syms, name);
}

CfgRule*
gramlookrule(Gram *g, Name name)
{
	return hashget(g->rules, name);
}

int
gramignoring(Gram *g, CfgSym *sym)
{
	return sym == g->ignore;
}

void
gramaddrule(Type *tg, GramPrec *p, CfgSym *left, CfgSymL *right)
{
	CfgSymL *l;
	CfgSym **cright;
	int i, n;
	CfgRule *r;
	Name prefix;
	Gram *g;
	
	g = tg->gram;

	assert(left->ggram == g);
	
	n = lenCfgSymL(right);
	i = 0;
	cright = emalloc(n*sizeof cright[0]);
	for(l=right; l; l=l->tl){
		assert(l->hd->ggram == g);
		cright[i++] = l->hd;
	}
	r = cfgrule(g->cfg, parseaction, p ? p->cfgprec : nil, left, cright, n);
	free(cright);
	r->ggram = g;
	r->tgram = tg;
	prefix = N("");
	if(tg->gramx)
		prefix = nameprint("%T.", tg);
	r->fmtname = nameprint("%s%R", prefix, r);
	hashput(g->rules, r->fmtname, r);
}

static Name
parsesep(Name *prefixp)
{
	char *p;
	char *prefix;
	Name sep;
	
	prefix = nametostr(*prefixp);
	if(prefix[strlen(prefix)-1] != ']')
		return nil;
	for(p = prefix+strlen(prefix); p > prefix && *p != '['; p--)
		;
	if(*p != '[')
		panic("bad name: %s", prefix);
	sep = nameofn(p+1, strlen(p+1)-1);
	*prefixp = nameofn(prefix, p - prefix);
	return sep;
}

CfgSym*
gramunlistsym(Gram *g, CfgSym *sym)
{
	Name prefix;
	char *s;
	
	s = nametostr(sym->name);
	switch(s[strlen(s)-1]){
	case '*':
	case '+':
		prefix = nameofn(s, strlen(s)-1);
		parsesep(&prefix);
		return gramlooksym(g, prefix);
	default:
		return nil;
	}
}

static Ast*
nop_canonical_(void *escf, Ast *ast)
{
	return ast;
}

Zfn nop_canonical = { nop_canonical_, nil };


CfgSym*
gramsym(Type *tg, Name n)
{
	CfgSym *s;
	char *strname;
	Name prefix, sep;
	int suffix;
	CfgSymL *sl;
	Gram *g;
	
	g = tg->gram;

	s = hashget(g->syms, n);
	if(s)
		return s;
	s = cfgnewsym(g->cfg, n);
	s->ggram = g;
	s->canonical = &nop_canonical;
	hashput(g->syms, n, s);
	
	// Create implicit rules for this symbol.
	strname = nametostr(n);
	switch(strname[0]){
	case '"':	// literal string
	case '/':	// regular expression
		break;

	default:
		suffix = strname[strlen(strname)-1];
		prefix = nameofn(strname, strlen(strname)-1);
		
		// Keep these in sync with astfromquestion, etc. in ast.c.
		switch(suffix){
		case '?':
			// name?:
			// name?: name
			gramaddrule(tg, nil, s, nil);
			gramaddrule(tg, nil, s, sl=mkCfgSymL(gramsym(tg, prefix), nil));
			freeCfgSymL(sl);
			break;
		
		case '*':
			// name*:
			// name*: name+
			gramaddrule(tg, nil, s, nil);
			gramaddrule(tg, nil, s, sl=mkCfgSymL(gramsym(tg, nameprint("%s+", prefix)), nil));
			freeCfgSymL(sl);
			break;
		
		case '+':
			// name+: name
			sep = parsesep(&prefix);
			gramaddrule(tg, nil, s, sl=mkCfgSymL(gramsym(tg, prefix), nil));
			freeCfgSymL(sl);
			if(LeftRecursive){
				// name+: name+ sep name
				sl = mkCfgSymL(gramsym(tg, prefix), nil);
				if(sep)
					sl = mkCfgSymL(gramliteral(tg, sep), sl);
				sl = mkCfgSymL(s, sl);
			}else{
				// name+: name sep name+
				sl = mkCfgSymL(s, nil);
				if(sep)
					sl = mkCfgSymL(gramliteral(tg, sep), sl);
				sl = mkCfgSymL(gramsym(tg, prefix), sl);
			}
			gramaddrule(tg, nil, s, sl);
			freeCfgSymL(sl);
			break;
		}
		break;
	}

	return s;
}

CfgSym*
gramliteral(Type *tg, Name n)
{
	CfgSym *s;
	Name n1;
	Gram *g;
	
	g = tg->gram;
	
	n1 = nameprint("\"%s\"", n);
	s = hashget(g->syms, n1);
	if(s)
		return s;
	s = gramsym(tg, n1);
	s->nocopy = 1;
	if(addtodfa(g->dfa, nametostr(n), RegexpLiteral, --g->dfaprec, s) < 0)
		panic("gramliteral: addtodfa failed");
	return s;
}

CfgSym*
gramignore(Gram *g, Name n, int flags, int canfail)
{
	if(g->ignore == nil)
		g->ignore = gramsym(typegram1(g->name, g), N(".Ignore"));
	if(addtodfa(g->dfa, nametostr(n), flags, --g->dfaprec, g->ignore) < 0){
		if(!canfail)
			panic("gramignore: addtodfa failed");
		return nil;
	}
	return g->ignore;
}

CfgSym*
gramregexp(Type *tg, Name n, int flags, int canfail)
{
	CfgSym *s;
	Name n1;
	Gram *g;
	
	g = tg->gram;
	n1 = nameprint("/%s/", n);
	s = hashget(g->syms, n1);
	if(s)
		return s;
	s = gramsym(tg, n1);
	s->nocopy = 1;
	if(addtodfa(g->dfa, nametostr(n), RegexpEgrep|flags, --g->dfaprec, s) < 0){
		if(!canfail)
			panic("gramregexp: addtodfa failed");
		hashput(g->syms, n1, nil);
		return nil;
	}
	return s;
}

GramPrec*
gramprec(Gram *g, Name n, CfgAssoc assoc, CfgPrecKind kind, int creat)
{
	GramPrec *p;

	// Convenience for caller.
	if(n == nil)
		return nil;

	p = hashget(g->precs, n);
	if(p){
		if(creat){
			if(p->cfgprec->assoc != assoc){
				fprint(2, "grammar precedence assoc mismatch\n");
				return nil;
			}
			if(p->cfgprec->kind != kind){
				fprint(2, "grammar precedence kind mismatch\n");
				return nil;
			}
		}
		return p;
	}
	if(!creat){
fprint(2, "no %s\n", n);
		return nil;
}
	p = emalloc(sizeof *p);
	p->cfgprec = cfgnewprec(g->cfg, n, kind);
	p->cfgprec->assoc = assoc;
	hashput(g->precs, n, p);
	return p;
}

int
grampriority(Gram *gram, Name xhi, Name xlo, int prefer)
{
	GramPrec* lo, *hi;
	int pkind;
	
	pkind = CfgPrecPriority;
	if(prefer)	
		pkind = CfgPrecPrefer;
	lo = hashget(gram->precs, xlo);
	if(lo == nil)
		lo = gramprec(gram, xlo, CfgAssocNone, pkind, 1);
	else{
		if(lo->cfgprec->kind != pkind){
			werrstr("kind mismatch for %s", xlo);
			return -1;
		}
	}
	hi = hashget(gram->precs, xhi);
	if(hi == nil)
		hi = gramprec(gram, xhi, CfgAssocNone, pkind, 1);
	else{
		if(hi->cfgprec->kind != pkind){
			werrstr("kind mismatch for %s", xhi);
			return -1;
		}
	}
	if(!cfglinkprec(hi->cfgprec, lo->cfgprec)){
		werrstr("circular precedence directive");
		return -1;
	}
	return 0;
}


LISTIMPL(Gram)
LISTIMPL(CfgSym)
