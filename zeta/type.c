#include "a.h"
#include "type.h"
#include "runtime.h"
#include "grammar.h"
#include "listimpl.h"

Type **alltype;
int nalltype;

Type*
mkType(TypeOp op, int reg)
{
	Type *t;
	
	t = emalloc(sizeof *t);
	t->op = op;
	if(reg){
		t->id = nalltype;
		alltype = erealloc(alltype, (nalltype+1)*sizeof alltype[0]);
		alltype[nalltype++] = t;
	}else{
		t->id = -1;
	}
	return t;
}

// Type cache
Type*
typebool(void)
{
	static Type *t;
	if(t == nil)
		t = mkType(TypeBool, 1);
	return t;
}

Type*
typemodule(void)
{
	static Type *t;
	if(t == nil)
		t = mkType(TypeModule, 1);
	return t;
}

Type*
typenil(void)
{
	static Type *t;
	if(t == nil)
		t = mkType(TypeNil, 1);
	return t;
}

Type*
typeint(void)
{
	static Type *t;
	if(t == nil)
		t = mkType(TypeInt, 1);
	return t;
}

Type*
typefloat(void)
{
	static Type *t;
	if(t == nil)
		t = mkType(TypeFloat, 1);
	return t;
}

Type*
typestring(void)
{
	static Type *t;
	if(t == nil)
		t = mkType(TypeString, 1);
	return t;
}

Type*
typeany(void)
{
	static Type *t;
	if(t == nil)
		t = mkType(TypeAny, 1);
	return t;
}

Type*
typetype(void)
{
	static Type *t;
	if(t == nil)
		t = mkType(TypeType, 1);
	return t;
}

Type*
typevoid(void)
{
	return typetuple(nil);
}

static int
cmptypel(void *a, void *b)
{
	TypeL *la=a, *lb=b;
	
	for(; la && lb; la=la->tl, lb=lb->tl){
		if(la->hd < lb->hd)
			return -1;
		if(la->hd > lb->hd)
			return 1;
	}
	if(la)
		return 1;
	if(lb)
		return -1;
	return 0;
}

Type*
typetuple(TypeL *types)
{
	static Map m = { cmptypel };
	Type *t;
	int reg;
	TypeL *l;
	
	// Singleton tuple is just the original type.
	if(types && !types->tl)
		return types->hd;

	t = mapget(&m, types);
	if(t)
		return t;
	reg = 1;
	for(l=types; l && reg; l=l->tl)
		if(l->hd->id < 0)
			reg = 0;
	t = mkType(TypeTuple, reg);
	t->typel = copyTypeL(types);
	mapput(&m, t->typel, t);
	return t;
}

Type*
typetuplel(Type *t, ...)
{
	TypeL *l;
	va_list arg;

	va_start(arg, t);
	l = nil;
	while(t != nil){
		l = mkTypeL(t, l);
		t = va_arg(arg, Type*);
	}
	va_end(arg);
	l = revTypeL(l);
	t = typetuple(l);
	freeTypeL(l);
	return t;
}

Type*
typelist(Type *t)
{
	static Hash *h;
	Type *tt;
	
	if(h == nil)
		h = mkhash();
	tt = hashget(h, t);
	if(tt)
		return tt;
	tt = mkType(TypeList, t->id>=0);
	tt->left = t;
	hashput(h, t, tt);
	return tt;
}

Type*
typegram1(Name name, struct Gram *g)
{
	static Hash *h;
	Type *tt;

	if(name == nil && g == nil){
		static Type *t;
		if(t == nil){
			t = mkType(TypeGram, 1);
			t->gram = nil;
			t->name = nil;
		}
		return t;
	}

	if(name == nil)
		panic("typegram1 no name");
	if(strchr(nametostr(name), '+'))
		panic("typegram1 %s", name);
	if(h == nil)
		h = mkhash();
	tt = hashget(h, name);
	if(tt)
		return tt;
	if(g == nil)
		panic("typegram1 no gram");
	tt = mkType(TypeGram, 1);
	tt->gram = g;
	tt->name = name;
	hashput(h, name, tt);
	return tt;
}

static int
namecmp(Name a, Name b)
{
	if(a == b)
		return 0;
	if(a == nil)
		return -1;
	if(b == nil)
		return 1;
	return strcmp(nametostr(a), nametostr(b));
}

static int
cmpnamel(void *a, void *b)
{
	NameL *la=a, *lb=b;
	
	for(; la && lb; la=la->tl, lb=lb->tl){
		if(la->hd < lb->hd)
			return -1;
		if(la->hd > lb->hd)
			return 1;
	}
	if(la)
		return 1;
	if(lb)
		return -1;
	return 0;
}

Type*
typegramx(Type *tg, NameL *nl)
{
	static Hash *h;
	Map *m;
	Type *t;
	struct Gram *g;
	Fmt fmt;
	NameL *l;
	
	g = tg->gram;
	assert(g);

	nl = uniqNameL(sortNameL(copyNameL(nl), namecmp));

	while(tg->gramx || tg->sym)
		tg = tg->left;

	if(nl == nil)
		return tg;

	if(h == nil)
		h = mkhash();
	if((m=hashget(h, g)) == nil){
		m = emalloc(sizeof *m);
		m->cmp = cmpnamel;
		hashput(h, g, m);
	}
	t = mapget(m, nl);
	if(t == nil){
		t = mkType(TypeGram, 1);
		t->left = tg;
		t->gram = g;
		t->gramx = nl;
		fmtstrinit(&fmt);
		fmtprint(&fmt, "%s", tg->name);
		for(l=nl; l; l=l->tl)
			fmtprint(&fmt, "+%s", l->hd);
		t->name = nameof(fmtstrflush(&fmt));
		mapput(m, nl, t);
	}

	return t;		
}

Type*
typegramxl(Type *tg, Name x, ...)
{
	NameL *nl;
	va_list arg;
	
	nl = nil;
	va_start(arg, x);
	while(x != nil){
		nl = mkNameL(x, nl);
		x = va_arg(arg, Name);
	}
	va_end(arg);
	return typegramx(tg, nl);
}

Type*
typeast(void)
{
	return typegram1(nil, nil);
}

Type*
typegramsym(Type *t, Name n, struct CfgSym *sym)
{
	static Hash *h;
	Type *tt;
	Hash *hh;

	// If have sym, walk up to sym-less version.
	if(t->sym)
		t = t->left;
	
	// No name means drop sym if any.
	if(n == nil)
		return t;

	if(n && nametostr(n)[0] == '$')
		return nil;

	// Otherwise look for cached.
	if(h == nil)
		h = mkhash();
	if((hh=hashget(h, t)) == nil)
		hashput(h, t, hh = mkhash());
	tt = hashget(hh, n);
	if(tt == nil){
		assert(t->gram);
		if(sym == nil){
			sym = gramlooksym(t->gram, n);
			if(sym == nil)
				panic("typegramsym %s no sym", n);
		}
		tt = mkType(TypeGram, 1);
		tt->gram = t->gram;
		tt->left = t;
		tt->name = t->name;
		tt->symname = n;
		tt->sym = sym;
		hashput(hh, n, tt);
	}
	return tt;
}

Type*
typearray(Type *t)
{
	static Hash *h;
	Type *tt;
	
	if(h == nil)
		h = mkhash();
	tt = hashget(h, t);
	if(tt)
		return tt;
	tt = mkType(TypeArray, t->id>=0);
	tt->left = t;
	hashput(h, t, tt);
	return tt;
}

Type*
typefn(TypeL *ins, Type *out)
{
	TypeL *l;
	Type *t;
	static Map m = { cmptypel };
	int reg;

	l = mkTypeL(out, ins);
	t = mapget(&m, l);
	free(l);
	if(t)
		return t;
	reg = out->id >= 0;
	for(l=ins; l && reg; l=l->tl)
		if(l->hd->id < 0)
			reg = 0;
	t = mkType(TypeFn, reg);
	t->left = out;
	t->typel = copyTypeL(ins);
	l = mkTypeL(out, t->typel);
	mapput(&m, l, t);
	return t;
}

Type*
typefnl(Type *t, ...)
{
	TypeL *l;
	Type *out;

	if(t == nil)
		return typefn(nil, nil);

	l = nil;
	out = t;
	va_list arg;
	va_start(arg, t);
	while((t = va_arg(arg, Type*)) != nil){
		l = mkTypeL(out, l);
		out = t;
	}
	va_end(arg);
	l = revTypeL(l);
	t = typefn(l, out);
	freeTypeL(l);
	return t;
}

Type*
typewrapper(Name name)
{
	Type *t;
	static Hash *h;
	
	if(h == nil)
		h = mkhash();
	t = hashget(h, name);
	if(t)
		return t;
	t = mkType(TypeWrapper, 1);
	t->name = name;
	hashput(h, name, t);
	return t;
}

Type*
typemap(Type *left, Type *right)
{
	static Hash *h;
	Type *t;
	Hash *hh;

	if(h == nil)
		h = mkhash();
	if((hh=hashget(h, left)) == nil)
		hashput(h, left, hh = mkhash());
	t = hashget(hh, right);
	if(t == nil){
		t = mkType(TypeMap, left->id>=0 && right->id>=0);
		t->left = left;
		t->right = right;
		hashput(hh, right, t);
	}
	return t;
}

Type*
typefmap(Type *left, Type *right)
{
	static Hash *h;
	Type *t;
	Hash *hh;

	if(h == nil)
		h = mkhash();
	if((hh=hashget(h, left)) == nil)
		hashput(h, left, hh = mkhash());
	t = hashget(hh, right);
	if(t == nil){
		t = mkType(TypeFmap, left->id>=0 && right->id>=0);
		t->left = left;
		t->right = right;
		hashput(hh, right, t);
	}
	return t;
}

Type*
typevarnum(int i)
{
	Type *t;
	static Hash *h;
	void *k;
	
	if(h == nil)
		h = mkhash();

	k = (void*)i;
	t = hashget(h, k);
	if(t == nil){
		t = mkType(TypeVarNum, 1);
		t->n = i;
		hashput(h, k, t);
	}
	return t;
}

static int
typeconstructfmt(Fmt *f, Type *t)
{
	TypeL *l;
	NameL *nl;

	switch(t->op){
	case TypeBool:
	case TypeInt:
	case TypeFloat:
	case TypeString:
	case TypeModule:
	case TypeNil:
	case TypeAny:
	case TypeType:
		fmtprint(f, "type%T()", t);
		break;
	case TypeList:
		fmtprint(f, "typelist(%#T)", t->left);
		break;
	case TypeArray:
		fmtprint(f, "typearray(%#T)", t->left);
		break;
	case TypeMap:
		fmtprint(f, "typemap(%#T, %#T)", t->left, t->right);
		break;
	case TypeFmap:
		fmtprint(f, "typefmap(%#T, %#T)", t->left, t->right);
		break;
	case TypeTuple:
		fmtprint(f, "typetuplel(");
		for(l=t->typel; l; l=l->tl)
			fmtprint(f, "%#T, ", l->hd);
		fmtprint(f, "nil)");
		break;
	case TypeFn:
		fmtprint(f, "typefnl(");
		for(l=t->typel; l; l=l->tl)
			fmtprint(f, "%#T, ", l->hd);
		fmtprint(f, "%#T, ", t->left);
		fmtprint(f, "nil)");
		break;
	case TypeWrapper:
		fmtprint(f, "typewrapper(%N)", t->name);
		break;
	case TypeVarName:
		fmtprint(f, "typevarname(%N)", t->name);
		break;
	case TypeVarNum:
		fmtprint(f, "typevarnum(%d)", t->n);
		break;
	case TypeStruct:
		fmtprint(f, "typestruct(%N, %N, %#T, %d, %#T)",
			t->name, t->cname, t->super, t->extensible,
			t->extends);
		break;
	case TypeVarInst:
	case TypeVarFrozen:
		fmtprint(f, "/* %T */ nil", t);
		break;
	case TypeGram:
		if(t->sym)
			fmtprint(f, "typegramsym(%#T, %N, nil)", t->left, t->symname);
		else if(t->gramx){
			fmtprint(f, "typegramxl(%#T, ", t->left);
			for(nl=t->gramx; nl; nl=nl->tl)
				fmtprint(f, "%N, ", nl->hd);
			fmtprint(f, "nil)");
		}
		else if(t->gram)
			fmtprint(f, "typegram1(%N, nil)", t->name);
		else
			fmtprint(f, "typegram1(nil, nil)");
		break;
	}
	return 0;
}

static int
ctypefmt(Fmt *f, Type *t)
{
	switch(t->op){
	case TypeBool:
	case TypeInt:
	case TypeFloat:
	case TypeString:
		fmtprint(f, "Z%T", t);
		break;
	case TypeList:
		fmtprint(f, "Zlist*");
		break;
	case TypeArray:
		fmtprint(f, "Zarray*");
		break;
	case TypeMap:
		fmtprint(f, "Zmap*");
		break;
	case TypeFmap:
		fmtprint(f, "Zfmap*");
		break;
	case TypeTuple:
		fmtprint(f, "Zpoly*");
		break;
	case TypeFn:
		fmtprint(f, "Zfn*");
		break;
	case TypeWrapper:
		fmtprint(f, "%s*", t->name);
		break;
	case TypeStruct:
		fmtprint(f, "%s*", t->cname);
		break;
	case TypeVarInst:
	case TypeVarNum:
	case TypeVarName:
	case TypeModule:
	case TypeNil:
	case TypeVarFrozen:
	case TypeAny:
	case TypeType:
		fmtprint(f, "/* %T */ Zpoly", t);
		break;
	case TypeGram:
		fmtprint(f, "Ast*");
		break;
	}
	return 0;
}

int
typefmt(Fmt *f)
{
	Type *t;
	TypeL *l;

	t = va_arg(f->args, Type*);
	if(t == nil){
		fmtprint(f, "(0)");
		return 0;
	}

	if(f->flags&FmtSharp){
		if(f->flags&FmtLong)
			return typeconstructfmt(f, t);
		if(t->id < 0)
			panic("typefmt no id - %T", t);
		fmtprint(f, "__types__[%d]", t->id);
		return 0;
	}
	if(f->flags&FmtShort)
		return ctypefmt(f, t);

	switch(t->op){
	case TypeBool:
		fmtprint(f, "bool");
		break;
	case TypeInt:
		fmtprint(f, "int");
		break;
	case TypeFloat:
		fmtprint(f, "float");
		break;
	case TypeString:
		fmtprint(f, "string");
		break;
	case TypeModule:
		fmtprint(f, "module");
		break;
	case TypeNil:
		fmtprint(f, "nil");
		break;
	case TypeAny:
		fmtprint(f, "any");
		break;
	case TypeType:
		fmtprint(f, "type");
		break;
	case TypeList:
		fmtprint(f, "list %T", t->left);
		break;
	case TypeArray:
		fmtprint(f, "array %T", t->left);
		break;
	case TypeMap:
		fmtprint(f, "map (%T, %T)", t->left, t->right);
		break;
	case TypeFmap:
		fmtprint(f, "fmap (%T, %T)", t->left, t->right);
		break;
	case TypeTuple:
		fmtprint(f, "(");
		for(l=t->typel; l; l=l->tl){
			fmtprint(f, "%T", l->hd);
			if(l->tl)
				fmtprint(f, ", ");
		}
		fmtprint(f, ")");
		break;
	case TypeFn:
		fmtprint(f, "fn(");
		for(l=t->typel; l; l=l->tl){
			fmtprint(f, "%T", l->hd);
			if(l->tl)
				fmtprint(f, ", ");
		}
		fmtprint(f, "): %T", t->left);
		break;
	case TypeStruct:
		if(t->super)
			fmtprint(f, "%T.", t->super);
		fmtprint(f, "%s", t->name);
		break;
	case TypeWrapper:
		fmtprint(f, "«%s»", t->name);
		break;
	case TypeVarName:
		fmtprint(f, "%s", t->name);
		break;
	case TypeVarNum:
		fmtprint(f, "'_%d", t->n);
		break;
	case TypeVarInst:
		if(t->constraint->min && t->constraint->min == t->constraint->max)
			fmtprint(f, "%T", t->constraint->min);
		else if(t->constraint->min && t->constraint->max)
			fmtprint(f, "'(%T<:%T)", t->constraint->min, t->constraint->max);
		else if(t->constraint->max)
			fmtprint(f, "'(<:%T)", t->constraint->max);
		else if(t->constraint->min)
			fmtprint(f, "'(%T<:)", t->constraint->min);
		else
			fmtprint(f, "'?");
		break;
	case TypeVarFrozen:
		fmtprint(f, "%T", t->left);
		break;
	case TypeGram:
		if(t->name){
			if(t->symname && strchr(nametostr(t->name), '+'))
				fmtprint(f, "(%s)", t->name);
			else
				fmtprint(f, "%s", t->name);
		}else
			fmtprint(f, "Ast");
		if(t->symname)
			fmtprint(f, ".%s", t->symname);
		break;
	}
	return 0;
}

Type*
typestruct(Name name, Name cname, Type *super, 
	int extensible, Type *extends)
{
	Type *t;
	static Hash *h;
	
	if(super){
		assert(super->op == TypeStruct);
		cname = nameprint("%s__%s", super->cname, name);
		extensible = super->extensible;
	}

	if(h == nil)
		h = mkhash();
	t = hashget(h, cname);
	if(t)
		return t;
	
	t = mkType(TypeStruct, 1);
	t->name = name;
	t->cname = cname;
	t->super = super;
	t->extends = extends;
	t->extensible = extensible;
	
	if(super)
		super->tags = addTypeL(super->tags, mkTypeL(t, nil));
	if(extends){
		assert(extends->op == TypeStruct);
		extends->xstructs = addTypeL(extends->xstructs, mkTypeL(t, nil));
	}
	
	hashput(h, cname, t);
	
	return t;
}

void
typestructmember(Type *t, Name name, Type *t1)
{
	assert(t->op == TypeStruct);
	t->namel = addNameL(t->namel, mkNameL(name, nil));
	t->typel = addTypeL(t->typel, mkTypeL(t1, nil));
}

static void
_structmktypes(Type *t, TypeL **tl, NameL **nl)
{
	TypeL *tl1;
	NameL *nl1;
	
	if(t->super)
		_structmktypes(t->super, tl, nl);
	for(tl1=t->typel, nl1=t->namel; tl1; tl1=tl1->tl, nl1=nl1->tl){
		*tl = mkTypeL(tl1->hd, *tl);
		*nl = mkNameL(nl1->hd, *nl);
	}
}

TypeL*
structmktypes(Type *t, NameL **nl, int deep)
{
	TypeL *tl;

	assert(t->op == TypeStruct);
	if(!deep){
		*nl = t->namel;
		return t->typel;
	}
	tl = nil;
	*nl = nil;
	_structmktypes(t, &tl, nl);
	tl = revTypeL(tl);
	*nl = revNameL(*nl);
	return tl;
}

Type*
typestructdot(Type *t, Name name)
{
	TypeL *tl;

	if(t == nil || t->op != TypeStruct)
		return nil;
	for(tl=t->tags; tl; tl=tl->tl)
		if(tl->hd->name == name)
			return tl->hd;
	return nil;
}

Type*
structdot(Type *t, Name name, Type **xtype)
{
	NameL *nl;
	Type *tt;
	TypeL *tl;
	
	assert(t->op == TypeStruct);

	for(tl=t->xstructs; tl; tl=tl->tl){
		if((tt = structdot(tl->hd, name, xtype)) != nil){
			*xtype = tl->hd;
			return tt;
		}
	}
	*xtype = nil;
	for(nl=t->namel, tl=t->typel; nl; nl=nl->tl, tl=tl->tl)
		if(nl->hd == name)
			return tl->hd;
	if(t->super)
		return structdot(t->super, name, xtype);
	return nil;
}

int
istagtype(Type *t)
{
	if(t->op == TypeGram)
		return 1;
	if(t->op == TypeStruct && (t->super || t->tags))
		return 1;
	return 0;
}

LISTIMPL(Type)
