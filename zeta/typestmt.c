#include "a.h"
#include "type.h"
#include "runtime.h"
#include "compile.h"

static StmtL* mkswitch(Stmt*);
static Stmt* expandcases(StmtL*);
static void structdecl(Typecheck *tc, int tagsokay, Type *tstruct, StructelemL *l);
int typecheckfntype(Typecheck *tc, Fn *fn);
Name attributename(Name, Type*);
static Type *destructuretype(Type*);
static Type *restructuretype(Type*);
static Type *nodestructuretype(Type*);

void
typecheckstmt(Typecheck *tc, Stmt *stmt)
{
	StmtL *l;
	int reach;
	Scope *scope, *truescope, *falsescope;
	Scope *oldscope;
	Module *m;
	Var *v;
	Type *t, *tt;
	Expr *e;
	NameL *nl;
	Name name;
	char *file;

	if(stmt == nil)
		return;

	oldscope = tc->scope;

	if(!tc->reachable && stmt->op != StmtLabel)
		warn(stmt->pos, "unreachable code %d", stmt->op);
	tc->reachable = 1;

	switch(stmt->op){
	case StmtBlock:
		scope = pushscope(tc);
		for(l=stmt->stmtl; l; l=l->tl)
			typecheckstmt(tc, l->hd);
		popscope(tc, scope);
		break;
	case StmtExpr:
		if(stmt->e1)
			typecheckexpr(tc, stmt->e1, 0);
		break;
	case StmtPrint:
		typecheckexpr(tc, stmt->e1, Value);
		break;
	case StmtType:
		if(typecheckexpr(tc, stmt->e1, Value) >= 0)
			warn(stmt->pos, "%#P has type %T", stmt->e1->pos, stmt->e1->type);
		break;

	case StmtAssert:
		falsescope = pushscope(tc);
		popscope(tc, falsescope);
		typecheckcond(tc, stmt->e1, tc->scope, falsescope);
		stmt->e1 = tobool(tc, stmt->e1);
		break;
	
	case StmtIf:
		stmt->lfalse = tc->nlabel++;
		if(stmt->s2)
			stmt->label = tc->nlabel++;
		else
			stmt->label = stmt->lfalse;

		// XXX clean up
		truescope = pushscope(tc);
		popscope(tc, truescope);
		falsescope = pushscope(tc);
		popscope(tc, falsescope);

		typecheckcond(tc, stmt->e1, truescope, falsescope);
		tc->scope = truescope;
		typecheckstmt(tc, stmt->s1);
		tc->scope = truescope->outer;
		reach = tc->reachable;
		// XXX if(!tc->reachable) push falsescope onto tc->scope?
		if(stmt->s2){
			tc->reachable = 1;
			tc->scope = falsescope;
			typecheckstmt(tc, stmt->s2);
			tc->scope = falsescope->outer;
			reach |= tc->reachable;
		}else
			reach = 1;
		tc->reachable = reach;
		break;
	
	case StmtWhile:
		stmt->lcontinue = tc->nlabel++;
		stmt->lbreak = tc->nlabel++;

		// XXX clean up
		truescope = pushscope(tc);
		popscope(tc, truescope);

		typecheckcond(tc, stmt->e1, truescope, tc->scope);
		if(stmt->e1->type)
			stmt->e1 = tobool(tc, stmt->e1);
		tc->scope = truescope;
		tc->breakl = mkStmtL(stmt, tc->breakl);
		tc->continuel = mkStmtL(stmt, tc->continuel);
		typecheckstmt(tc, stmt->s1);
		tc->scope = truescope->outer;
		tc->breakl = tc->breakl->tl;
		tc->continuel = tc->continuel->tl;

		// If bottom not reachable, why use a while statement?
		if(!tc->reachable && !stmt->continued)
			warn(stmt->pos, "loop never loops");

		// Assume condition can be false: can get past while statement.
		tc->reachable = 1;
		break;
	
	case StmtFor:
		scope = pushscope(tc);
		stmt->lcontinue = tc->nlabel++;
		stmt->lbreak = tc->nlabel++;
		stmt->label = tc->nlabel++;
		if(stmt->e1)
			typecheckexpr(tc, stmt->e1, 0);
		if(stmt->e2){
			// XXX clean up
			truescope = pushscope(tc);
			popscope(tc, truescope);
			typecheckcond(tc, stmt->e2, truescope, tc->scope);
			if(stmt->e2->type)
				stmt->e2 = tobool(tc, stmt->e2);
		}
		if(stmt->e3)
			typecheckexpr(tc, stmt->e3, 0);
		tc->breakl = mkStmtL(stmt, tc->breakl);
		tc->continuel = mkStmtL(stmt, tc->continuel);
		typecheckstmt(tc, stmt->s1);
		tc->breakl = tc->breakl->tl;
		tc->continuel = tc->continuel->tl;
		tc->reachable = stmt->e2 != nil || stmt->broke;
		popscope(tc, scope);
		break;
	
	case StmtForIn:
		scope = pushscope(tc);
		stmt->label = tc->nlabel++;
		stmt->lcontinue = tc->nlabel++;
		stmt->lbreak = tc->nlabel++;
		if(typecheckexpr(tc, stmt->e2, Value) < 0){
			popscope(tc, scope);
			return;
		}
		switch(stmt->e2->type->op){
		default:
			error(stmt->pos, "cannot iterate %T", stmt->e2->type);
			popscope(tc, scope);
			return;
		case TypeList:
		case TypeArray:
			t = stmt->e2->type->left;
			break;
		case TypeMap:
			t = typetuplel(stmt->e2->type->left, stmt->e2->type->right, nil);
			break;
		case TypeFmap:
			t = typetuplel(stmt->e2->type->left, stmt->e2->type->right, nil);
			break;
		}
		declaretype(tc->scope, stmt->e1, t);
		tc->breakl = mkStmtL(stmt, tc->breakl);
		tc->continuel = mkStmtL(stmt, tc->continuel);
		typecheckstmt(tc, stmt->s1);
		tc->breakl = tc->breakl->tl;
		tc->continuel = tc->continuel->tl;
		popscope(tc, scope);
		tc->reachable = 1;
		break;

	case StmtDoWhile:
		stmt->label = tc->nlabel++;
		stmt->lcontinue = tc->nlabel++;
		stmt->lbreak = tc->nlabel++;
		tc->breakl = mkStmtL(stmt, tc->breakl);
		tc->continuel = mkStmtL(stmt, tc->continuel);
		typecheckstmt(tc, stmt->s1);
		tc->breakl = tc->breakl->tl;
		tc->continuel = tc->continuel->tl;
		if(typecheckexpr(tc, stmt->e1, Value) >= 0)
			stmt->e1 = tobool(tc, stmt->e1);
	
		// Assume condition can be false: can get past while statement.
		tc->reachable = 1;
		break;

	case StmtBreak:
		if(!tc->breakl){
			error(stmt->pos, "nothing to break");
			return;
		}
		stmt->label = tc->breakl->hd->lbreak;
		tc->breakl->hd->broke++;
		tc->reachable = 0;
		break;

	case StmtContinue:
		if(!tc->continuel){
			error(stmt->pos, "nothing to continue");
			return;
		}
		stmt->label = tc->continuel->hd->lcontinue;
		tc->breakl->hd->continued++;
		tc->reachable = 0;
		break;

	case StmtAttrDecl:
		if((t = typechecktype(tc, stmt->typex)) == nil)
			return;
		if((tt = attributeargtype(t)) == nil){
			error(stmt->pos, "bad attribute type %T", t);
			return;
		}
		name = attributename(stmt->name, tt);
		if((v = lookupvar(tc->scope, name)) == nil){
			v = declarevar(stmt->pos, tc->scope, name, t);
			v->flags |= VarAttribute|VarExtensible;
		}else{
			if(v->type != typegeneralize(t)){
				error(stmt->pos, "redeclaring attribute: %T => %T",
					v->type, t);
				return;
			}
		}
		break;

	case StmtFn:
		v = nil;
		if(stmt->fn->attribute){
			if(typecheckfntype(tc, stmt->fn) < 0)
				return;
			if((t = attributeargtype(stmt->fn->type->left)) == nil){
				error(stmt->pos, "bad attribute type %T", stmt->fn->type);
				return;
			}
			v = lookupattribute(tc->scope, stmt->fn->name, t);
			if(v == nil){
				name = attributename(stmt->fn->name, t);
				if(name == nil)
					panic("attributename");	// can't fail if attributeargtype succeeded
				stmt->fn->name = name;
			}else
				stmt->fn->var = v;
		}
		if(stmt->fn->extend){
			Expr *e1, *e2, *e3, *e4, *e5;
			Stmt *s1;
			Fn *fn;

			fn = stmt->fn;
			if(v == nil)
				v = lookupvar(tc->scope, fn->name);
			if(v == nil){
				error(fn->pos, "cannot find %s to extend", fn->name);
				return;
			}else if(!v->flags&VarExtensible){
				error(fn->pos, "%s is not extensible", v->name);
				return;
			}
			
			// Rewrite to be an anonymous function
			// and then create the following code:
			//
			//	fn(default: [v->type]) {
			//		name = fn { ... }
			//	}();
			//
		
			// name = fn() { ... }
			fn->extend = 0;
			fn->name = nil;
		
			e1 = mkExpr(ExprName);
			e1->name = v->name;
			e1->var = v;

			e2 = mkExpr(ExprFn);
			e2->fn = fn;
			
			s1 = mkStmt(StmtExpr);
			s1->e1 = mkExpr(ExprAssign);
			s1->e1->e1 = e1;
			s1->e1->e2 = e2;
			
			// fn(default: [v->type]) { s1 }
			fn = mkFn(nil, mkFnargL(mkFnarg(N("default"), mkTypex1(v->type->left)), nil),
				mkTypex1(typevoid()), mkStmtL(s1, nil));
			e3 = mkExpr(ExprFn);
			e3->fn = fn;

			// fn(){}(name)
			e5 = mkExpr(ExprName);
			e5->name = v->name;
			e5->var = v;

			e4 = mkExpr(ExprCall);
			e4->e1 = e3;
			e4->el = mkExprL(e5, nil);
			
			stmt->e1 = e4;
			stmt->op = StmtExpr;
			typecheckstmt(tc, stmt);
			break;
		}
		if(stmt->fn->name == N("destructure") || stmt->fn->name == N("restructure") || stmt->fn->name == N("nodestructure")){
			// These can't happen unless we toss the special syntax.
			if(stmt->fn->extend || stmt->fn->attribute){
				error(stmt->pos, "cannot extend/attribute %s", stmt->fn->name);
				return;
			}
			if(typecheckfntype(tc, stmt->fn) < 0)
				return;
			if(stmt->fn->name == N("destructure"))
				t = destructuretype(stmt->fn->type->left);
			else if(stmt->fn->name == N("nodestructure"))
				t = nodestructuretype(stmt->fn->type->left);
			else
				t = restructuretype(stmt->fn->type->left);
			if(t == nil){
				error(stmt->pos, "bad %s function type %T", stmt->fn->name, stmt->fn->type->left);
				return;
			}
			stmt->fn->name = nameprint("%s(%s)", stmt->fn->name, t->symname);
			stmt->fn->var = declarevarmany(stmt->fn->pos, tc->scope, stmt->fn->name, stmt->fn->type->left);
		}
		typecheckfn(tc, stmt->fn);
		tc->reachable = 1;
		e = mkExpr(ExprName);
		e->name = stmt->fn->name;
		e->type = typeinstantiate(stmt->fn->type);
		e->var = stmt->fn->var;
		stmt->e1 = e;
		break;
	
	case StmtReturn:
		if(tc->fn == nil){
			error(stmt->pos, "return outside function");
			return;
		}
		if(stmt->e1){
			if(typecheckexpr(tc, stmt->e1, Value) < 0)
				return;
			if(!canunify(tc->fn->returntype, stmt->e1->type, 1)){
				error(stmt->pos, "cannot return %T from %T function [%r]",
					stmt->e1->type, tc->fn->returntype);
				return;
			}
			stmt->fn = tc->fn;
		}else{
			if(tc->fn->returntype != typevoid()){
				error(stmt->pos, "should return %T", tc->fn->returntype);
				return;
			}
		}
		tc->reachable = 0;
		break;
	
	case StmtImport:
		if((m = builtinmodule(stmt->name)) == nil){
			pos2fileline(stmt->pos, &file);
			name = fullpath(smprint("%s/../%s.zeta", file, stmt->name));
			m = loadmodule(name, stmt->name, 1);
			if(m == nil){
				error(stmt->pos, "cannot load %s: %r", stmt->name);
				return;
			}
		}
		if(stmt->namel){
			if(stmt->namel->tl == nil && stmt->namel->hd == N("*")){
				MVar *mv;
				MVarL *l;
				
				for(l=m->mvarl; l; l=l->tl){
					mv = l->hd;
					v = _declarevar(stmt->pos, tc->scope, mv->name, mv->type->left, 1, mv->cname);
					v->cname = mv->cname;
					v->realtype = mv->realtype;
					v->scope = nil;
					v->flags = mv->flags;
				}
				break;
			}
			for(nl=stmt->namel; nl; nl=nl->tl){
				Var *mv;

				mv = mlookupvar(m, nl->hd);
				if(mv == nil){
					error(stmt->pos, "%s.%s not found", stmt->name, nl->hd);
					continue;
				}
				v = declarevar(stmt->pos, tc->scope, nl->hd, mv->type->left);
				v->cname = mv->cname;
				v->realtype = mv->realtype;
				v->scope = nil;
				v->flags = mv->flags;
			}
		}else{
			v = declarevar(stmt->pos, tc->scope, stmt->name, typemodule());
			v->module = m;
		}
		break;
	
	case StmtLabel:
		for(l=tc->jumpl; l; l=l->tl){
			if(l->hd->op == StmtLabel && l->hd->name == stmt->name){
				error(stmt->pos, "redeclaration of label %s from %P", stmt->name, l->hd->pos);
				return;
			}
		}
		stmt->label = tc->nlabel++;
		for(l=tc->jumpl; l; l=l->tl)
			if(l->hd->op == StmtGoto && l->hd->name == stmt->name)
				l->hd->label = stmt->label;
		tc->jumpl = mkStmtL(stmt, tc->jumpl);
		break;
	
	case StmtGoto:
		stmt->label = -1;
		for(l=tc->jumpl; l; l=l->tl){
			if(l->hd->op == StmtLabel && l->hd->name == stmt->name){
				stmt->label = l->hd->label;
				break;
			}
		}
		if(stmt->label == -1)
			tc->jumpl = mkStmtL(stmt, tc->jumpl);
		break;
	
	case StmtTypedef:
		if((t = typechecktype(tc, stmt->typex)) == nil)
			return;
		v = declarevar(stmt->pos, tc->scope, stmt->name, typetype());
		v->realtype = t;
		break;
	
	case StmtCase:
		panic("typestmt stmtcase");

	case StmtSwitch:
		l = mkswitch(stmt);
		stmt->stmtl = l;
		stmt->op = StmtBlock;
//		stmt->lbreak = tc->nlabel++;
//		tc->breakl = mkStmtL(stmt, tc->breakl);
		typecheckstmt(tc, stmt);
//		tc->breakl = tc->breakl->tl;
		break;
	
	case StmtStruct:
		if(stmt->xflag == N("extend")){
			Type *tx;
			static int nx;
			Name cname;

			v = lookupvar(tc->scope, stmt->name);
			if(v == nil || v->type->left->op != TypeType || v->realtype->op != TypeStruct || !v->realtype->extensible){
				error(stmt->pos, "%s is not an extensible struct", stmt->name);
				return;
			}
			cname = nameprint("%s_x_%d", tc->scope->frame->prefix, nx++);
			tx = typestruct(nameprint("%s.X", v->name), cname, nil, 0, v->realtype);
			structdecl(tc, 0, tx, stmt->sel);
			break;
		}
		v = lookupvar(tc->scope, stmt->name);
		if(v){
			if(v->realtype == nil || v->realtype->op != TypeStruct || (v->realtype->namel && stmt->xflag != N("extend"))){
				error(stmt->pos, "redefining %s", stmt->name);
				return;
			}
			if(v->realtype->extensible != (stmt->xflag==N("extensible"))){
				error(stmt->pos, "extensible mismatch");
				return;
			}
		}else{
			v = declarevar(stmt->pos, tc->scope, stmt->name, typetype());
			v->realtype = typestruct(v->name, v->cname, nil, stmt->xflag == N("extensible"), nil);
		}
		t = v->realtype;
		if(stmt->structinit){
			if(t->structinit){
				error(stmt->pos, "redefining struct %s members", stmt->name);
				return;
			}
			structdecl(tc, 1, t, stmt->sel);
		}
		break;
	
	case StmtGrammar:
		typecheckgram(tc, stmt->gtop);
		break;
	}
	
	assert(tc->scope == oldscope);
}

// Return grammar type for destructure function.
static Type*
destructuretype(Type *t)
{
	if(t->op != TypeFn
	|| t->left->op != TypeTuple || lenTypeL(t->left->typel) != 2
	|| t->left->typel->hd != typebool()
	|| lenTypeL(t->typel) != 1
	|| t->typel->hd->op != TypeGram
	|| t->typel->hd->sym == nil)
		return nil;
	return t->typel->hd;
}

// Return grammar type for nodestructure function.
static Type*
nodestructuretype(Type *t)
{
	if(t->op != TypeFn
	|| t->left->op != TypeTuple || lenTypeL(t->left->typel) != 0
	|| lenTypeL(t->typel) != 1
	|| t->typel->hd->op != TypeGram
	|| t->typel->hd->sym == nil)
		return nil;
	return t->typel->hd;
}

// Return grammar type for restructure function.
static Type*
restructuretype(Type *t)
{
	if(t->op != TypeFn
	|| t->left->op != TypeGram || t->left->sym == nil
	|| lenTypeL(t->typel) != 1)
		return nil;
	return t->left;
}

static void
structdecl(Typecheck *tc, int tagsokay, Type *tstruct, StructelemL *l)
{
	Structelem *se;
	Type *t;

	tstruct->structinit = 1;
	for(; l; l=l->tl){
		se = l->hd;
		if(se->tag){
			if(!tagsokay){
				error(se->pos, "tags in %s", tstruct->name);
				continue;
			}
			t = typestruct(se->name, nil, tstruct, 0, nil);
			structdecl(tc, 1, t, se->sel);
		}else{
			t = typechecktype(tc, se->typex);
			if(t == nil)
				continue;
			typestructmember(tstruct, se->name, t);
		}
	}
}
			

static StmtL*
mkswitch(Stmt *stmt)
{
	Stmt *s1;
	Stmt *s2;
	Expr *e;
	
	yylastpos = stmt->pos;
	e = mkExpr(ExprDeclAssign);
	e->e1 = mkExpr(ExprName);
	e->e1->name = N("__it__");
	e->e2 = stmt->e1;

	s1 = mkStmt(StmtExpr);
	s1->e1 = e;

	s2 = expandcases(stmt->stmtl);

	return mkStmtL(s1, mkStmtL(s2, nil));
}

static Stmt*
expandcases(StmtL *cases)
{
	Stmt *s, *cas;
	Expr *e;
	
	if(cases == nil)
		return mkStmt(StmtBlock);
	
	cas = cases->hd;
	assert(cas->op == StmtCase);
	e = mkExpr(ExprMatch);
	e->e1 = mkExpr(ExprName);
	e->e1->name = N("__it__");
	e->e2 = cas->e1;
	
	s = mkStmt(StmtIf);
	s->e1 = e;
	s->s1 = mkStmt(StmtBlock);
	s->s1->stmtl = cas->stmtl;
	s->s2 = expandcases(cases->tl);
	
	return s;
}

static TypeL*
argtypes(Typecheck *tc, FnargL *fnargs)
{
	Type *t;
	TypeL *types;
	FnargL *l;
	
	types = nil;
	for(l=fnargs; l; l=l->tl){
		t = typechecktype(tc, l->hd->typex);
		if(t == nil)
			return nil;
		types = mkTypeL(t, types);
	}
	return revTypeL(types);
}

int
typecheckfntype(Typecheck *tc, Fn *fn)
{
	TypeL *tl;
	Type *t;

	tl = argtypes(tc, fn->args);
	t = typechecktype(tc, fn->xreturntype);
	if((tl == nil && fn->args != nil) || t == nil)
		return -1;
	fn->type = typegeneralize(typefn(tl, t));
	return 0;
}

int
typecheckfn(Typecheck *tc, Fn *fn)
{
	Type *ft;
	FnargL *l;
	Fnarg *a;
	StmtL *sl;
	TypeL *tl;
	Var *v;
	StmtL *ojumpl;
	
	ojumpl = tc->jumpl;
	tc->jumpl = nil;

	if(typecheckfntype(tc, fn) < 0)
		return -1;
	if(fn->name && !fn->var){
		v = lookupvar(tc->scope, fn->name);
		if(v == nil){
			v = declarevar(fn->pos, tc->scope, fn->name, fn->type->left);
			if(fn->extensible)
				v->flags |= VarExtensible;
			if(fn->attribute)
				v->flags |= VarAttribute|VarExtensible;
		}else{
			if(v->type != fn->type)
				error(fn->pos, "redeclaration of %s: %T became %T",
					fn->name, v->type->left, fn->type->left);
		}
		fn->var = v;
	}
	
	fn->outer = tc->fn;
	pushfn(tc, fn);
	fn->argframe = pushframe(tc);
	tc->scope->frame->fn = fn;
	fn->argframe->prefix = N("a_");
	ft = typefreeze(fn->type);
	fn->returntype = ft->left;
	for(l=fn->args, tl=ft->typel; l; l=l->tl, tl=tl->tl){
		a = l->hd;
		a->var = declarevar(a->pos, tc->scope, a->name, tl->hd);
	}

	assert(tl == nil);
	fn->localframe = pushframe(tc);
	fn->localframe->prefix = N("l_");
	for(sl=fn->body; sl; sl=sl->tl)
		typecheckstmt(tc, sl->hd);
	if(tc->reachable && fn->returntype != typevoid())
		error(fn->pos, "missing return at end of fn");
	popframe(tc, fn->localframe);
	popframe(tc, fn->argframe);
	popfn(tc, fn);
	tc->jumpl = ojumpl;
	tc->reachable = 1;
	return 0;
}

Type*
attributeargtype(Type *t)
{
	if(t->op != TypeFn)
		return nil;
	if(t->typel == nil || t->typel->tl != nil)
		return nil;
	if(t->typel->hd->op != TypeGram)
		return nil;
	return t->typel->hd;
}

Name
attributename(Name name, Type *t)
{
	return nameprint("attribute %s(%T)", name, t);
}

