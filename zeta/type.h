typedef struct Type Type;
typedef struct TypeConstraint TypeConstraint;
typedef struct GType GType;	// generalized type aka type scheme aka polytype
LIST(Type)

struct Gram;
struct CfgSym;

typedef enum TypeOp {
	TypeBool,
	TypeInt,
	TypeFloat,
	TypeString,
	TypeTuple,
	TypeList,
	TypeArray,
	TypeMap,
	TypeFmap,
	TypeFn,
	TypeStruct,
	TypeWrapper,
	TypeModule,
	TypeNil,
	TypeAny,
	TypeType,
	TypeGram,

	TypeVarFrozen,
	TypeVarName,
	TypeVarNum,
	TypeVarInst,
} TypeOp;

struct Type
{
	TypeOp op;
	int id;
	Type *left;
	Type *right;
	TypeL *typel;
	Name name;

	int n;		// TypeVarNum

	// TypeVarInst
	TypeConstraint *constraint;
	
	// TypeStruct
	int structinit;
	Name cname;
	NameL *namel;
	int extensible;
	Type *extends;
	Type *super;
	TypeL *tags;
	TypeL *xstructs;
	
	// TypeGram
	struct Gram *gram;
	NameL *gramx;
	Name symname;
	struct CfgSym *sym;
};

struct TypeConstraint
{
	Type *min;
	Type *max;
};

struct GType
{
	Type *left;
	Type *right;
	TypeL *typevarl;
	int n;
};

Type*		mkType(TypeOp, int);
Type*		typearray(Type *t);
Type*		typebool(void);
Type*		typefloat(void);
Type*		typefn(TypeL *ins, Type *out);
Type*		typefnl(Type *t, ...);
Type*		typeint(void);
Type*		typelist(Type *t);
Type*		typestring(void);
Type*		typetuple(TypeL *types);
Type*		typetuplel(Type *t, ...);
Type*		typevoid(void);
Type*		typewrapper(Name name);
Type*		typemap(Type*, Type*);
Type*		typefmap(Type*, Type*);
int		issubtype(Type*, Type*);
int		typemin(Type*, Type*, Type**);
int		typemax(Type*, Type*, Type**);
Type*		typemodule(void);
Type*		typevarnum(int);
Type*		typenil(void);
Type*		typefreeze(GType*);
Type*		typeany(void);
Type*		typetype(void);
Type*		typestruct(Name, Name, Type*, int, Type*);
void			typestructmember(Type*, Name, Type*);
TypeL*		structmktypes(Type*, NameL**, int);
Type*		typestructdot(Type*, Name);
Type*		structdot(Type*, Name, Type**);
Type*		typegram1(Name, struct Gram*);
Type*		typeast(void);
Type*		typegramx(Type*, NameL*);
Type*		typegramxl(Type*, Name, ...);
Type*		typegramsym(Type*, Name, struct CfgSym*);
Type*		typegramadd(Type*, Type*);
Type*		typegramquest(Type*);
int		istagtype(Type*);

int		typefmt(Fmt *f);

extern Type	**alltype;
extern int	nalltype;


// Type inference (unification only happens during compilation,
// 	but the types get recreated at runtime.).

int		canunify(Type*, Type*, int);
int		canunifyl(TypeL*, TypeL*, int);

GType*		typegeneralize(Type*);
Type*		typeinstantiate(GType*);
Type*		typevarname(Name name);
Type*		typeunified(Type*);
