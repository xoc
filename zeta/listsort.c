#include "a.h"
#include "listimpl.h"

static PtrL*
listmerge(PtrL *l1, PtrL *l2, int (*cmp)(void*, void*))
{
	int i;
	PtrL lhead, *l;
	
	l = &lhead;
	while(l1 || l2){
		if(l1 == nil)
			i = 1;
		else if(l2 == nil)
			i = -1;
		else{
			if(cmp)
				i = (*cmp)(l1->hd, l2->hd);
			else if(l1->hd <= l2->hd)
				i = -1;
			else
				i = 1;
		}
		if(i <= 0){
			l->tl = l1;
			l1 = l1->tl;
		}else{
			l->tl = l2;
			l2 = l2->tl;
		}
		l = l->tl;
	}
	l->tl = nil;
	return lhead.tl;
}

PtrL*
listsort(PtrL *l, int (*cmp)(void*, void*))
{
	PtrL *l1, *l2, *l1head, *l2head;

	if(l == nil || l->tl == nil)
		return l;
	
	l1head = l1 = l;
	l2head = l2 = l->tl;
	l = l2->tl;
	while(l){
		l1->tl = l;
		l1 = l;
		if((l = l->tl) != nil){
			l2->tl = l;
			l2 = l;
			l = l->tl;
		}
	}
	l1->tl = nil;
	l2->tl = nil;
	
	l1 = listsort(l1head, cmp);
	l2 = listsort(l2head, cmp);
	return listmerge(l1, l2, cmp);
}

_LISTIMPL(Ptr, void*)
