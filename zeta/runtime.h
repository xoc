// Zeta runtime

typedef struct Modinfo Modinfo;
typedef struct Module Module;
typedef struct MVar MVar;

LIST(Module);
LIST(MVar);

// List of files *not* to try to compile, to avoid loops in loadmodule.
extern NameL *compiling;

// In-module (in shared object) representation of metadata.
enum {
	ModinfoMagic = 0x052608,
};

struct Modinfo
{
	int magic;
	void (*init)(void);
	void (*run)(void);
	Type ***typesp;
};

// In-memory representation of a loaded module.
struct Module
{
	Name zeta;
	uint32 mtime;		// mtime of .zo file
	void *dl;		// dlopen(zo)
	void (*run)(void);	// run function
	MVarL *mvarl;		// top-level variables
	int internal;		// is a built-in module
};

enum {
	VarExtensible = 1<<0,
	VarAttribute = 1<<1,
	VarRedeclare = 1<<2,
	VarBuiltin = 1<<3,
	VarCanonical = 1<<4,
};

struct MVar
{
	Name name;
	Name cname;
	int typeid;
	int realtypeid;
	GType *type;
	Type *realtype;
	int flags;
};

// Load a module from the named file and, optionally, run it.
Module*	loadmodule(Name, Name, int run);

// Load a module straight from a .zo file.  Internal use only.
Module*	loadzo(Name, Module*, int run);

// Register an already-loaded (built-in) module under a given name.
void registermodule(Name, Module*);

// Look for a built-in module.
Module*	builtinmodule(Name);

// Initialize the built-in "sys" module.
void m_sys_init(int, char**);


// Run-time representation of zeta objects.
// To make polymorphism simpler, all objects must have the same size.
// In this implementation, that size is pointer-sized.
// Tailoring for 32-bit machines, we assume that void*, int, and float
// are all the same size.  Could do 64-bit instead on 64-bit machines.
// Notice that Zlist, Zarray, and Zfn are represented by pointers to
// those structures, not the structures themselves.
// Notice also that Zfloat is represented by a void*.  It's actually the bits
// of a float, but passed around as a void* so that it is possible to cast
// to and from Zpoly without the compiler throwing a hissy fit.
// That means we have to use special access functions (macros) F2P and P2F
// to get at the float value.

typedef int	Zint;
typedef void*	Zpoly;
typedef int	Zbool;
typedef Name	Zstring;
typedef void*	Zfloat;	// surprise!

// Convert Zpoly -> Zfloat and Zfloat -> Zpoly
#define P2F(pp) ({ volatile Zunion u; u.p = pp; u.fl; })
#define F2P(ff) ({ volatile Zunion u; u.fl = ff; u.p; })

// Cast to Zpoly
#define P(x)	((Zpoly)(x))

// Convert name to zeta string.
// The only difference is that "" is always nil.
#define ZN(n) ({ Name __x__ = n; if(__x__ && nametostr(__x__)[0] == 0) __x__ = nil; __x__; })

int	lenZstring(Zstring);
int	intZstring(Zstring);
Zstring	addZstring(Zstring, Zstring);
int	cmpZstring(Zstring, Zstring);
Zstring	indexZstring(Zstring, int);
Zstring	sliceZstring(Zstring, int, int);

typedef struct Zarray	Zarray;
typedef struct Zfn	Zfn;
typedef struct Zlist	Zlist;
typedef struct Zmap	Zmap;
typedef union Zunion	Zunion;
typedef Fmap Zfmap;

#define NO_UPPER	2147483647

struct Zarray
{
	int n;
	Zpoly *a;
};
Zarray *mkZarray(int);
int	lenZarray(Zarray*);
Zpoly*	indexZarray(Zarray*, int);
Zarray*	sliceZarray(Zarray*, int, int);
Zlist*	listZarray(Zarray*);

struct Zlist
{
	Zpoly hd;
	Zlist *tl;
};
Zlist *mkZlist(Zpoly, Zlist*);
Zlist *revZlist(Zlist*);
int	lenZlist(Zlist*);
Zpoly	indexZlist(Zlist*, int);
Zlist*	sliceZlist(Zlist*, int, int);
Zlist*	addZlist(Zlist*, Zlist*);
Zpoly	assocZlist(Zlist*, Zpoly);
Zarray*	arrayZlist(Zlist*);

Zlist*	keysZmap(Zmap*);

struct Zfn
{
	void *fn;
	void *escf;
};
Zfn *mkZfn(void*, void*);

struct Zmap
{
	Hash *h;
};
Zmap*	mkZmap(void);
Zpoly*	indexZmap(Zmap*, Zpoly);
Zpoly	indexZtuple(Zpoly*, int);

#define insertZfmap fmapinsert
#define indexZfmap fmaplookup

union Zunion
{
	Zint	i;
	Zpoly	p;
	Zbool	b;
	Zstring	s;
	Zfloat	f;
	float		fl;
	Zarray*	a;
	Zfn*	fn;
	Zlist*	l;
	Zpoly*	t;	// tuple
	Zmap*	m;
};


// Run-time support functions.
void zprint(Type*, void*);
void*	xget(Hash**, Type*);

extern int sys_astchatty;
