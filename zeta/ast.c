// Abstract syntax trees

#include "a.h"
#include "type.h"
#include "runtime.h"
#include "grammar.h"
#include "listimpl.h"

int nmkast;
int nfreeast;
int nleakast;

enum {
	Leaked = 255,
	AstFreelist = 1,
};

static Ast *astfreed;

Ast*
mkAst(AstOp op, CfgSym *sym)
{
	Ast *a;
	static Ast *ma;
	static int nma;
	
	a = astfreed;
	if(a){
		astfreed = (Ast*)a->right;
		a->right = nil;
	}else{
		if(nma == 0){
			if(AstFreelist)
				nma = 4096;
			else
				nma = 1;
			ma = emallocnz(nma*sizeof *a);
		}
		a = ma++;
		nma--;
	}
	a->ref = 1;
	a->canonical = nil;
	a->copiedfrom = nil;
	a->op = op;
	a->tag = nil;
	a->line.file = nil;
	a->line.line = 0;
	a->sym = sym;
	a->rule = nil;
	a->right = nil;
	a->nright = 0;
	a->slotnum = 0;
	a->text = nil;
	a->parent = NoParentYet;
	a->attrmap.root = nil;
	a->attrmap.cmp = nil;
	nmkast++;
	return a;
}

void
astfree(Ast *a)
{
	int i;

	if(a == nil || a->ref == Leaked || --a->ref > 0)
		return;
	assert(a->ref == 0);

	for(i=0; i<a->nright; i++)
		astfree(a->right[i]);
	free(a->right);
	if(AstFreelist){
		a->right = (void*)astfreed;
		astfreed = a;
	}else
		free(a);
	nfreeast++;
}

// Swap the AST's data but not their ref counts or tree position.
void
astswap(Ast *a, Ast *b)
{
	Ast t;
	
	t = *a;
	*a = *b;
	*b = t;
	
	b->ref = a->ref;
	a->ref = t.ref;
	
	b->parent = a->parent;
	a->parent = t.parent;
}

Ast*
astdup(Ast *a)
{
	if(a && a->ref != Leaked)
		a->ref++;
	return a;
}

Ast*
astleak(Ast *ast)
{
	int i;

	if(ast == nil || ast->ref == Leaked)
		return ast;
	ast->ref = Leaked;
	nleakast++;
	for(i=0; i<ast->nright; i++)
		astleak(ast->right[i]);
	return ast;
}

#if 0
void
astrefcheck(Ast *ast, int pass)
{
	int i;

	switch(pass){
	case 0:
		ast->refcheck = 0;
		break;
	case 1:
		if(ast->refcheck++ == 0)
			for(i=0; i<ast->nright; i++)
				astrefcheck(ast->right[i], pass);
		break;
	case 2:
		if(ast->refcheck != -1){
			if(ast->ref < ast->refcheck){
				fprint(2, "%d < %d | %s | %#A\n", ast->ref, ast->refcheck, ast->sym->name, ast);
				abort();
			}
			for(i=0; i<ast->nright; i++)
				astrefcheck(ast->right[i], pass);
			ast->refcheck = -1;
		}
		break;
	}
}
#endif

Ast*
mkaststring(CfgSym *sym, Name name)
{
	Ast *a;

	a = mkAst(AstString, sym);
	a->text = name;
	a->tag = typegramsym(typegram1(sym->ggram->name, nil), sym->name, nil);
	assert(a->tag->sym == sym);
	return a;
}

Ast*
mkastmerge(Ast *a, Ast *b)
{
	int i, n;
	Ast *m;
	
	n = 0;
	if(a->op == AstMerge)
		n += a->nright;
	else
		n++;
	if(b->op == AstMerge)
		n += b->nright;
	else
		n++;

	m = mkAst(AstMerge, a->sym);
	m->right = emalloc(n*sizeof m->right[0]);
	m->nright = n;
	n = 0;
	if(a->op == AstMerge)
		for(i=0; i<a->nright; i++)
			m->right[n++] = astdup(a->right[i]);
	else
		m->right[n++] = astdup(a);
	if(b->op == AstMerge)
		for(i=0; i<b->nright; i++)
			m->right[n++] = astdup(b->right[i]);
	else
		m->right[n++] = astdup(b);
	assert(n == m->nright);
	if(typemax(a->tag, b->tag, &m->tag) < 0)
		panic("typemax %T %T in mkastmerge", a->tag, b->tag);
	assert(m->tag->sym == m->sym);
	m->line = m->right[0]->line;
	return m;
}

Ast*
mkastmerge1(Ast **right, int nright)
{
	Ast *m;
	Type *t;
	int i;
	
	if(nright <= 0)
		panic("mkastmerge %d", nright);

	if(nright == 1)
		return right[0];

	m = mkAst(AstMerge, right[0]->sym);
	m->right = right;
	m->nright = nright;
	t = right[0]->tag;
	for(i=1; i<nright; i++){
		if(typemax(t, right[i]->tag, &t) < 0)
			panic("typemax %T %T in mkastmerge1", t, right[i]->tag);
	}
	m->tag = t;
	assert(m->tag->sym = m->sym);
	m->line = m->right[0]->line;
	return m;
}

Ast*
mkastrule(CfgRule *r, Ast **right)
{
	Ast *a;
	Type *t, *tt;
	int i, j;

	a = mkAst(AstRule, r->left);
	a->rule = r;
	a->right = right;
	a->nright = r->nsavedright;
	for(i=j=0; i<a->nright; i++, j++){
		while(nametostr(r->right[j]->name)[0] == '"')
			j++;
	//	assert(right[i]->sym == r->right[j]);
	}
	t = r->tgram;
	if(t == nil)
		panic("nil r->tgram in mkastrule %s", r->name);
	for(i=0; i<a->nright; i++){
		if(typemax(t, right[i]->tag, &tt) < 0)
			panic("typemax %T %T in mkastrule", t, right[i]->tag);
		t = tt;
		astdup(right[i]);
	}
	for(i=0; i<a->nright; i++){
		if(right[i]->line.file){
			a->line = right[i]->line;
			break;
		}
	}
	a->tag = typegramsym(t, r->left->name, nil);
	assert(a->tag->sym == a->sym);
	return a;
}

Ast*
mkastslot(Type *t, CfgSym *sym, int slotnum)
{
	Ast *a;
	
	a = mkAst(AstSlot, sym);
	a->slotnum = slotnum;
	a->tag = typegramsym(t, sym->name, sym);
	assert(a->tag->sym == a->sym);
	return a;
}


//============================================================
// Printing

static void astfmtsexpr(Fmt*, Ast*, int, int);
static void astfmttext(Fmt*, Ast*);

// %A - format Ast as infix string (leaves separated by spaces)
// %#A - format Ast as one-line S-expression.
// %#lA - format Ast as multi-line S-expression.
int
astfmt(Fmt *fmt)
{
	Ast *ast;
	
	ast = va_arg(fmt->args, Ast*);
	if(ast == nil)
		fmtprint(fmt, "<nil ast>");
	else if(fmt->flags&FmtSharp)
		astfmtsexpr(fmt, ast, fmt->flags&FmtLong, 0);
	else
		astfmttext(fmt, ast);
	return 0;
}

static char*
dots(int n)
{
	int i;
	static int m = -1;
	static char *dots;
	
	if(n > m) {
		m = n + 128;
		free(dots);
		dots = emalloc(m+1);
		char *p = dots;
		for(i = 0; i < m; i++)
			*p++ = ". "[i%2];
		*p = 0;
	}
	return dots;
}

static void
astfmtsexpr(Fmt *fmt, Ast *ast, int newlines, int indent)
{
	int i;

	switch(ast->op){
	case AstSlot:
		fmtprint(fmt, " ?slot.%s", ast->sym->name);
		break;
	case AstString:
		fmtprint(fmt, " '%s", ast->text);
		break;
	case AstMerge:
		fmtprint(fmt, " (%s.merge", ast->sym->name);
		goto right;
	case AstRule:
		fmtprint(fmt, " (%R", ast->rule);
	right:
		for(i=0; i<ast->nright; i++){
			if(newlines)
				fmtprint(fmt, "\n%.*s", indent*2, dots(indent*2));
			astfmtsexpr(fmt, ast->right[i], newlines, indent+1);
		}
		fmtprint(fmt, ")");
		break;
	}
}

static void
astfmttext(Fmt *fmt, Ast *ast)
{
	int i, j;
	CfgSym *sym;
	char *s;

	switch(ast->op){
	case AstSlot:
		fmtprint(fmt, "\\%d ", ast->slotnum);
		break;
	case AstString:
		fmtprint(fmt, "%s ", ast->text);
		break;
	case AstRule:
		j = 0;
		for(i=0; i<ast->rule->nright; i++){
			sym = ast->rule->right[i];
			s = nametostr(sym->name);
			if(s[0] == '"')
				fmtprint(fmt, "%.*s ", utfnlen(s+1, strlen(s+1)-1), s+1);
			else
				astfmttext(fmt, ast->right[j++]);
		}
		assert(j == ast->nright);
		break;
	case AstMerge:
		fmtprint(fmt, "{M: %A}", ast->right[0]);
		break;
	}
}

//============================================================
// Zeta interface
// Must use astright, not ast->right, to preserve parent pointers.

Ast*
astparent(Ast *ast, Type *t)
{
	if(ast->parent == NoParentYet)
		ast->parent = nil;
	for(ast=ast->parent; ast; ast=ast->parent){
		if(t == nil || issubtype(ast->tag, t))
			return ast;
		if(ast->parent == NoParentYet)
			ast->parent = nil;
	}
	return nil;
}

Ast*
astprev_attr(Ast *ast)
{
	int i;
	Ast *p;

	p = astparent(ast, nil);
	if(p == nil)
		return nil;
	for(i=0; i<p->nright; i++){
		if(p->right[i] == ast){
			for(i--; i>=0; i--)
				if(!p->right[i]->sym->nocopy)
					return astright(p, i);
			return nil;
		}
	}
	fprint(2, "cannot find self in parent!");
	abort();
	return nil;
}

Ast*
astnext_attr(Ast *ast)
{
	int i;
	Ast *p;

	p = astparent(ast, nil);
	if(p == nil)
		return nil;
	for(i=0; i<p->nright; i++){
		if(p->right[i] == ast){
			for(i++; i<p->nright; i++)
				if(!p->right[i]->sym->nocopy)
					return astright(p, i);
			return nil;
		}
	}
	fprint(2, "cannot find self in parent!");
	abort();
	return nil;
}

Ast*
astcopiedfrom_attr(Ast *ast)
{
	if(ast == nil || ast->copiedfrom == nil)
		return nil;
	return ast->copiedfrom;
}

Ast*
astcvtslot(Ast *ast, Type *t)
{
	Ast *a;

	if(ast->op != AstSlot)
		abort();
	a = mkastslot(t, t->sym, ast->slotnum);
	return a;
}

Ast*
astright(Ast *ast, int i)
{
	Ast *a;
	
	assert(i >= 0 && i < ast->nright);
	a = ast->right[i];
	if(a->sym->nocopy)
		return a;
	if(a->parent == ast)
		return a;
	if(a->parent == NoParentYet){
		a->parent = ast;
		return a;
	}
	ast->right[i] = a = astcopy(a);
	a->parent = ast;
	return a;
}

Ast*
asttoquestion(Ast *ast, CfgSym *sym)
{
	Ast **right;

	// name?:
	// name?: name
	assert(sym);
	assert(sym->nrule == 2);
	assert(sym->rule[0]->nright == 0);
	assert(sym->rule[1]->nright == 1);
	assert(sym->rule[1]->nsavedright == 1);
	
	if(ast == nil)
		return mkastrule(sym->rule[0], nil);

	assert(ast->sym == sym->rule[1]->right[0]);
	right = emalloc(1*sizeof right[0]);
	right[0] = ast;
	return mkastrule(sym->rule[1], right);
}

Ast*
astfromquestion(Ast *ast, CfgSym *sym)
{
	// name?:
	// name?: name
	assert(sym);
	assert(sym->nrule == 2);
	assert(sym->rule[0]->nright == 0);
	assert(sym->rule[1]->nright == 1);
	assert(sym->rule[1]->nsavedright == 1);
	
	assert(ast->sym == sym);
	
	if(ast->nright == 0)
		return nil;
	return astright(ast, 0);
}

Ast*
astlisttostar(Zlist *zl, CfgSym *sym)
{
	Ast **right;

	// name*:
	// name*: name+
	assert(sym);
	assert(sym->nrule == 2);
	assert(sym->rule[0]->nright == 0);
	assert(sym->rule[1]->nright == 1);
	assert(sym->rule[1]->nsavedright == 1);
	
	if(zl == nil)
		return mkastrule(sym->rule[0], nil);
	
	right = emalloc(1*sizeof right[0]);
	right[0] = astlisttoplus(zl, sym->rule[1]->right[0]);
	if(right[0] == nil)
		return nil;
	return mkastrule(sym->rule[1], right);
}

int
aststartolist(Ast *ast, CfgSym *sym, Zlist **l)
{
	// name*:
	// name*: name+
	assert(sym);
	assert(sym->nrule == 2);
	assert(sym->rule[0]->nright == 0);
	assert(sym->rule[1]->nright == 1);
	assert(sym->rule[1]->nsavedright == 1);

	assert(ast->sym == sym);

	if(ast->op == AstSlot)
		return -1;

	if(ast->nright == 0){
		*l = nil;
		return 0;
	}
	return astplustolist(astright(ast, 0), sym->rule[1]->right[0], l);
}

Ast*
_astlisttoplus(Zlist *zl, CfgSym *sym)
{
	Ast **right;

	// name+: name
	// name+: name+ sep name
	assert(sym);
	assert(sym->nrule == 2);
	assert(sym->rule[0]->nright == 1);
	assert(sym->rule[1]->nright == 2 || sym->rule[1]->nright == 3);
	assert(sym->rule[1]->nsavedright == 2);
	if(LeftRecursive)
		assert(sym->rule[1]->right[0] == sym);
	else
		assert(sym->rule[1]->right[sym->rule[1]->nright-1] == sym);

	if(zl == nil){
		fprint(2, "convert nil list to %s", sym->name);
		abort();
	}
	
	assert(zl->hd);
	assert(((Ast*)zl->hd)->sym == sym->rule[0]->right[0]);
	
	if(zl->tl == nil){
		right = emalloc(1*sizeof right[0]);
		right[0] = zl->hd;
		return mkastrule(sym->rule[0], right);
	}
	
	right = emalloc(2*sizeof right[0]);
	if(LeftRecursive){
		right[0] = _astlisttoplus(zl->tl, sym);
		right[1] = zl->hd;
	}else{
		right[0] = zl->hd;
		right[1] = _astlisttoplus(zl->tl, sym);
	}
	return mkastrule(sym->rule[1], right);
}

Ast*
astlisttoplus(Zlist *zl, CfgSym *sym)
{
	if(LeftRecursive)
		zl = revZlist(zl);
	return _astlisttoplus(zl, sym);
}

int
astplustolist(Ast *ast, CfgSym *sym, Zlist **l)
{
	Zlist *zl;

	// name+: name
	// name+: name+ sep name
	assert(sym);
	assert(sym->nrule == 2);
	assert(sym->rule[0]->nright == 1);
	assert(sym->rule[1]->nright == 2 || sym->rule[1]->nright == 3);
	assert(sym->rule[1]->nsavedright == 2);
	if(LeftRecursive)
		assert(sym->rule[1]->right[0] == sym);
	else
		assert(sym->rule[1]->right[sym->rule[1]->nright-1] == sym);
	assert(ast->sym == sym);

	zl = nil;
	for(;;){
		assert(ast->sym == sym);
		if(ast->op == AstSlot)
			return -1;
		switch(ast->nright){
		default:
			panic("astplustolist");
		case 1:
			ast = astright(ast, 0);
			zl = mkZlist(ast, zl);
			goto out;
		case 2:
			if(LeftRecursive){
				Ast *last, *prefix;
				last = astright(ast, ast->nright-1);
				prefix = astright(ast, 0);
				zl = mkZlist(last, zl);
				ast = prefix;
			}else{
				Ast *first, *suffix;
				first = astright(ast, 0);
				suffix = astright(ast, ast->nright-1);
				zl = mkZlist(first, zl);
				ast = suffix;
			}
			break;
		}
	}
out:
	if(!LeftRecursive)
		zl = revZlist(zl);
	*l = zl;
	return 0;
}

Zlist*
asttolist(Ast *ast)
{
	char *s;
	Zlist *l;

	if(ast == nil)
		return nil;
	s = nametostr(ast->sym->name);
	switch(s[strlen(s)-1]){
	case '*':
		if(aststartolist(ast, ast->sym, &l) < 0)
			return nil;
		return l;
	case '+':
		if(astplustolist(ast, ast->sym, &l) < 0)
			return nil;
		return l;
	default:
		return nil;
	}
}

Zarray*
astsplit(Ast *ast)
{
	int i, n;
	Zarray *za;

	n = 0;
	for(i=0; i<ast->nright; i++)
		if(!ast->right[i]->sym->nocopy)
			n++;
	za = mkZarray(n);
	n = 0;
	for(i=0; i<ast->nright; i++)
		if(!ast->right[i]->sym->nocopy)
			za->a[n++] = astright(ast, i);
	assert(n == za->n);
	return za;
}

Ast*
astjoin(Ast *ast, Zarray *za)
{
	Ast **right;
	Ast *a;
	int i, n;

	right = emalloc(ast->nright*sizeof right[0]);
	n = 0;
	for(i=0; i<ast->nright; i++){
		if(ast->right[i]->sym->nocopy)
			right[i] = ast->right[i];
		else
			right[i] = za->a[n++];
	}
	assert(n == za->n);

	if(ast->op == AstMerge)
		a = mkastmerge1(right, za->n);
	else if(ast->op == AstRule){
		n = 0;
		for(i=0; i<ast->rule->nright; i++){
			if(nametostr(ast->rule->right[i]->name)[0] == '"')
				continue;
			if(right[n]->sym != ast->rule->right[i]){
				fprint(2, "bad right %d / %R: %s %s\n", i, ast->rule, right[i]->sym->name, ast->rule->right[i]->name);
				assert(right[n]->sym == ast->rule->right[i]);
			}
			n++;
		}
		assert(n == ast->nright);
		a = mkastrule(ast->rule, right);
	}
	else{
		panic("astjoin: %#A\n", ast);
		return nil;
	}
		
	a = astleak(a);
	a->copiedfrom = ast;	// XXX is this correct?
	a->line = ast->line;
	return a;
}

Ast*
astcopy(Ast *ast)
{
	int i;
	Ast *a;

	if(ast == nil)
		return nil;
	if(sys_astchatty > 0)
		fprint(2, "astcopy: %s\n", ast->sym->name);
	a = mkAst(ast->op, ast->sym);
	a->slotnum = ast->slotnum;
	a->tag = ast->tag;
	a->text = ast->text;
	a->rule = ast->rule;
	if(ast->nright > 0){
		a->right = emalloc(ast->nright*sizeof a->right[0]);
		for(i=0; i<ast->nright; i++)
			a->right[i] = astdup(ast->right[i]);
		a->nright = ast->nright;
	}
	a->copiedfrom = ast;
	a->line = ast->line;
//fprint(2, "astcopy: %p/%p -> %p\n", ast, ast->parent, a);
	return astleak(a);
}

Ast*
astlastkid(Ast *ast)
{
	int i;

	if(ast == nil)
		return nil;
	for(i=ast->nright-1; i>=0; i--)
		if(!ast->right[i]->sym->nocopy)
			return astright(ast, i);
	return nil;
}

int
astmerged(Ast *ast)
{
	return ast && ast->op == AstMerge;
}

int
astslotted(Ast *ast)
{
	return ast && ast->op == AstSlot;
}

Name
asttostring(Ast *ast)
{
	char *s, *t, *e;
	Name x;

	if(ast == nil)
		return nameof("");
	s = smprint("%A", ast);
	t = s;
	while(*t == ' ')
		t++;
	e = t+strlen(t);
	while(e > t && e[-1] == ' ')
		e--;
	x = nameofn(t, e-t);
	free(s);
	return x;

}

Name
astlisp(Ast *ast)
{
	char *s, *t, *e;
	Name x;

	if(ast == nil)
		return nameof("(nil)");
	s = smprint("%#A", ast);
	t = s;
	while(*t == ' ')
		t++;
	e = t+strlen(t);
	while(e > t && e[-1] == ' ')
		e--;
	x = nameofn(t, e-t);
	free(s);
	return x;

}

Name
astlong(Ast *ast)
{
	char *s, *t, *e;
	Name x;

	if(ast == nil)
		return nameof("(nil)");
	s = smprint("%#lA", ast);
	t = s;
	while(*t == ' ')
		t++;
	e = t+strlen(t);
	while(e > t && e[-1] == ' ')
		e--;
	x = nameofn(t, e-t);
	free(s);
	return x;

}

AttrLink *attrlinks;
int nattrinfo;
AttrInfo **attrinfo;

int
attrinfocmp(const void *va, const void *vb)
{
	AttrInfo *a, *b;
	
	a = *(AttrInfo**)va;
	b = *(AttrInfo**)vb;
	return b->ncompute - a->ncompute;
}

void
regattr(AttrInfo *info)
{
	if(info->registered)
		return;
	attrinfo = erealloc(attrinfo, (nattrinfo+1)*sizeof attrinfo[0]);
	attrinfo[nattrinfo++] = info;
	info->registered = 1;
}

void
attrstats(void)
{
	int i;
	AttrInfo *info;

	qsort(attrinfo, nattrinfo, sizeof attrinfo[0], attrinfocmp);
	for(i=0; i<nattrinfo; i++){
		info = attrinfo[i];
		print("%10d %s %T\n", info->ncompute, info->name, info->type);
	}
}

Attr*
get_attribute(Ast *ast, AttrInfo *info)
{
	Attr *a;
	static Attr *ma;
	static int nma;

	if(ast == nil){
		fprint(2, "%s called on nil\n", info->name);
		abort();
		// XXX maybe return 0?
	}
	if(!info->registered)
		regattr(info);
	if((a = mapget(&ast->attrmap, info)) == nil){
		if(nma == 0){
			nma = 100;
			ma = emallocnz(nma*sizeof ma[0]);
		}
		a = ma++;
		nma--;
		a->state = 0;
		a->data = nil;
		mapputelem(&ast->attrmap, info, a, &a->me);
	}
	return a;
}

void
set_attribute(Ast *ast, AttrInfo *info, Zpoly data)
{
	Attr *a;
	
	a = get_attribute(ast, info);
	if(a->state == AttrComputing)
		fprint(2, "warning: changing attribute %s during its computation\n",
			info->name);
	else if(a->state == AttrInit){
		fprint(2, "warning: changing attribute %s after its computation\n",
			info->name);
		abort();
	}
	a->state = AttrInit;
	a->data = data;
}

void
attrloop(AttrInfo *info)
{
	AttrLink *l;

	fprint(2, "attribute loop:\n");
	fprint(2, "\t%s: %s\n", info->pos, info->name);
	for(l=attrlinks; l; l=l->next)
		fprint(2, "\t%s: %s : %T\n", l->info->pos, l->info->name, l->ast->tag);
}


Zpoly*
astline(Ast *ast)
{
	Zpoly *p;
	
	p = emalloc(2*sizeof p[0]);
	p[0] = ast->line.file;
	p[1] = (Zpoly)ast->line.line;
	return p;
}

Ast*
astcanonicalize(Ast *ast)
{
	int i, changed;
	Ast **right, **oldright;
	Ast *a, *c;
	Zfn *f;

	if(ast->canonical)
		return ast->canonical;
	
	if(ast->op == AstSlot)
		return ast;

	right = emalloc(ast->nright*sizeof ast->right[0]);
	changed = 0;
	for(i=0; i<ast->nright; i++){
		right[i] = astcanonicalize(ast->right[i]);
		if(right[i] != ast->right[i])
			changed = 1;
	}
	
	if(changed){
		// Have to make a new AST to pass to the canonicalize function.
		// Easier to reuse astcopy, temporarily editing ast.
		oldright = ast->right;
		ast->right = right;
		a = astcopy(ast);
		ast->right = oldright;
	}else
		a = ast;
	
	if(ast->sym != ast->tag->sym)
		fprint(2, "oops: %s vs %s\n", ast->sym->name, ast->tag->sym->name);

	assert(ast->sym == ast->tag->sym);
	if(ast->op == AstRule){
		f = ast->sym->canonical;
		ast->sym->used_canonical = 1;
//extern Zfn nop_canonical;
//	if(f == &nop_canonical)
//		fprint(2, "no canonical for %T\n", ast->tag);
//	else
//		fprint(2, "canonical for %T\n", ast->tag);

		c = ((Ast*(*)(void*, Ast*))f->fn)(f->escf, astleak(a));

		if(c == nil){
			fprint(2, "astcanonicalize(%T) returned nil for %A\n",
				ast->tag, a);
			abort();
		}
	}else
		c = ast;

	ast->canonical = c;
	a->canonical = c;
	c->canonical = c;
	return c;
}

int
astsame(Ast *a, Ast *b)
{
	int i;

	if(a == nil && b == nil)
		return 1;
	if(a == nil || b == nil)
		return 0;
	if(a->op != b->op
	|| a->sym != b->sym
	|| a->rule != b->rule
	|| a->text != b->text)
		return 0;
	for(i=0; i<a->nright; i++)
		if(!astsame(a->right[i], b->right[i]))
			return 0;
	return 1;
}

void
astremovecopiedfrom(Ast *ast)
{
	int i;

	ast->copiedfrom = nil;
	ast->parent = NoParentYet;
	for(i=0; i<ast->nright; i++)
		astremovecopiedfrom(ast->right[i]);
}


LISTIMPL(Ast)
