#include "a.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "type.h"
#include "runtime.h"
#include "compile.h"
#include "grammar.h"
#include "listimpl.h"
#include "arg.h"

char*		argv0;
extern int	doabort;
extern int printtypes;

void
usage(void)
{
	fprint(2, "usage: zeta module\n");
	exit(1);
}

extern Zfloat sys_time_(void*);
extern int gramdebug;
extern NameL *sys_modpath;
extern void attrstats(void);

int
main(int argc, char **argv)
{
	char *file;
	extern ulong zetacomtime;

	fmtinstall('A', astfmt);
	fmtinstall('R', cfgrulefmt);
	fmtinstall('T', typefmt);
	fmtinstall('V', recursefmt);
	fmtinstall('@', slrstatefmt);

	assert(sizeof(Zunion) == sizeof(Zpoly));
	assert(sizeof(Zfloat) == sizeof(Zpoly));
	assert(sizeof(Zint) == sizeof(Zpoly));
	assert(sizeof(void*) == sizeof(Zpoly));

	/* Limit memory to avoid making entire system swap if we mess up */
	struct rlimit rlim;
	rlim.rlim_cur = 1000<<20;
	rlim.rlim_max = 1000<<20;
	setrlimit(RLIMIT_AS, &rlim);

	ARGBEGIN{
	case 'C':
		zetacomtime = 1;
		break;
	case 'A':
		doabort = 1;
		break;
	case 'G':
		gramdebug++;
		break;
	case 'M':
		sys_modpath = mkNameL(nameof(EARGF(usage())), sys_modpath);
		break;
	case 'a':
		atexit(attrstats);
		break;
	case 'T':
		printtypes = 1;
		break;
	default:
		usage();
	}ARGEND

	sys_modpath = revNameL(sys_modpath);

	if(argc < 1)
		usage();

	m_sys_init(argc, argv);

	// Nod to sloppiness: allow foo and foo.zeta.
	file = argv[0];
	if(access(file, 0) < 0){
		file = smprint("%s.zeta", file);
		if(access(file, 0) < 0)
			panic("cannot find %s", argv[0]);
	}

	if(loadmodule(nameof(file), nil, 1) == nil)
		panic("could not load %s", file);
	return 0;
}
