// Parsing using grammars.

#include "a.h"
#include "type.h"
#include "runtime.h"
#include "grammar.h"
#include "listimpl.h"

int FLAGS_parsemergedebug = 0;

static ParseValue *freepvs;

ParseValue*
mkParseValue(Ast *ast)
{
	ParseValue *pv;
	
	if((pv = freepvs) != nil)
		freepvs = (void*)pv->ast;
	else
		pv = emallocnz(sizeof(ParseValue));
	pv->ref = 1;
	pv->ast = ast;
	pv->line.file = 0;
	pv->line.line = 0;
	return pv;
}

void
pvfree(CfgSym *sym, ParseValue *pv)
{
	if(pv == nil || --pv->ref > 0)
		return;
	if(pv->ast)
		astfree(pv->ast);
	pv->ast = (void*)freepvs;
	freepvs = pv;
}

void
pvdup(CfgSym *sym, ParseValue **a, ParseValue *b)
{
	if(*a)
		(*a)->ref++;
}

#if 0
void
pvtouch(ParseValue *pv, int pass)
{
	if(pv == nil)
		return;

	assert(pv->ref > 0);
	if(pv->ast)
		assert(pv->ast->ref > 0);
	
	switch(pass){
	case 0:
		pv->refcheck = 0;
		if(pv->ast)
			astrefcheck(pv->ast, 0);
		break;
	case 1:
		if(pv->refcheck++ == 0)
			if(pv->ast)
				astrefcheck(pv->ast, 1);
		break;
	case 2:
		if(pv->refcheck != -1){
			if(pv->ref < pv->refcheck)
				fprint(2, "%d < %d | PV %p\n", pv->ref, pv->refcheck, pv);
			if(pv->ast)
				astrefcheck(pv->ast, 2);
			pv->refcheck = -1;
		}
		break;
	}
}
#endif

//============================================================
// Parsing action invoked during GLR parsing.

static Line lastline;
Type *parseactiontype;

int
parseaction(CfgGram *cfg, CfgRule *rule, ParseValue **outp, ParseValue **in)
{
	int i, n;
	Ast **right, *a;
	ParseValue *out;

	if(!issubtype(rule->tgram, parseactiontype)){
		//fprint(2, "rejecting: %R [%T <: %T]\n", rule, rule->tgram, parseactiontype);
		return -1;
	}

	right = emalloc(rule->nright*sizeof right[0]);
	n = 0;
	for(i=0; i<rule->nright; i++){
		a = in[i]->ast;
		
		// Only save the ASTs that aren't literal strings.
		// The literal strings can be inferred from the rule.
		// (They're the "concrete" syntax, kind of.)
		if(nametostr(rule->right[i]->name)[0] != '"')
			right[n++] = a;
	}
	
	if(n != rule->nsavedright)
		panic("parseaction - %R %d %d", rule, n, rule->nsavedright);
	
	*outp = out = mkParseValue(mkastrule(rule, right));
	for(i=0; i<rule->nright; i++){
		if(out->line.file == nil)
			out->line = in[i]->line;
	}
	out->ast->line = out->line;
	if(rule->nright == 0)
		out->line = lastline;
	lastline = out->line;
	return 1;
}


// ========================================================
// Parsing of regular text (not patterns)

static void lexsharp(RgParse*, char**);

// GLR lexer callback.  Should get the next token and
// return a list of RgToken* representing the various
// ways to interpret that token.  Each RgToken is a 
// CfgSym* paired with its corresponding Ast*.
// The state, in vstate, is a char** pointing at the
// current input location (itself a char*).
RgTokenL*
lextext(RgParse *parse, CfgGram *cfg, void *v)
{
	char **pp, *p, *op;
	Gram *gram;
	RgToken *t;
	CfgSymL *l;
	ParseValue *pv;
	int len, sol;
	char *s;
	CfgSym *sym;
	Name name;
	RgTokenL *tl;
	
	pp = v;
	gram = cfg->ggram;
	sol = parse->col == 1;

	// Loop looking for a non-ignorable token.
	do{
		p = *pp;
		// check EOF
		if(p == nil || *p == 0){
			*pp = nil;
			return mkRgTokenL(rgnewtoken(parse, cfg->eofsym), nil);
		}
		
		// process # line directives
		if(sol)
			lexsharp(parse, &p);
	
		// get next token (actually token set)
		op = p;
		l = rundfa(gram->dfa, p, &p);
		*pp = p;
		if(p == op || l == nil)
			return nil;
		
		// scan lexed text to update line, column
		sol = 0;
		for(s=op; s<p; s++){
			if(*s == '\n'){
				parse->line++;
				parse->col = 1;
				sol = 1;
			}else{
				parse->col++;
				if(sol && *s != ' ' && *s != '\t')
					sol = 0;
			}
		}
	}while(gramignoring(gram, l->hd));

	len = p - op;
	name = nameofn(op, len);
	tl = nil;
	for(; l; l=l->tl){
		sym = l->hd;
		t = rgnewtoken(parse, sym);
		pv = mkParseValue(mkaststring(sym, name));
		t->val = pv;
		pv->line.file = parse->file;
		pv->line.line = parse->line;
		tl = mkRgTokenL(t, tl);
	}
	return revRgTokenL(tl);
}

// Parse the cpp's "# ..." directives, assuming that the current
// position is "^".  This will eat all sol whitespace and record
// line number info (if present).  We could do some fancier
// parsing of pragmas, but we won't.
static void
lexsharp(RgParse *parse, char **pp)
{
	char *op, buf[512];
	int line, i;

	assert(pp);
	assert(*pp);
	op = *pp;
	while(**pp && (**pp == ' ' || **pp == '\t'))
		(*pp)++;
	if(**pp != '#')
		goto done;
	(*pp)++;
	// Line number
	while(**pp && (**pp == ' ' || **pp == '\t'))
		(*pp)++;
	line = 0;
	if(**pp < '0' || '9' < **pp)
		goto eatline;
	while(**pp && '0' <= **pp && **pp <= '9'){
		line = line*10 + (**pp - '0');
		(*pp)++;
	}
	parse->line = line - 1;  // the \n will add 1
	// File name
	while(**pp && (**pp == ' ' || **pp == '\t'))
		(*pp)++;
	if(**pp != '"')
		goto eatline;
	(*pp)++;
	for(i=0; **pp && **pp != '"'; i++){
		if(**pp == '\\'){
			(*pp)++;
			if(!**pp)
				break;
		}
		if(i >= (int)sizeof buf){
			buf[sizeof(buf)-1] = '\0';
			panic("file name too long near '%s'", buf);
		}
		buf[i] = **pp;
		(*pp)++;
	}
	buf[i] = '\0';
	parse->file = nameof(buf);

eatline:
	while(**pp && **pp != '\n')
		(*pp)++;
done:
	parse->col += *pp - op;
}

// Two different parses, a and b, have been found for 
// the same segment of text.  Choose which to use
// and update av in place.  It's worse than that, because
// the RNGLR parser can use an Ast before it has been
// fully merged.  Rather than fix that (a lot of work),
// we make glrmerge sneakily use astswap to make sure
// that av->ast updates in place.  This is only okay because
// all the ASTs manipulated were just created in the parser,
// so we know that no one else is pointing at them other
// than the values created during the parse.
void
glrmerge(CfgSym *sym, ParseValue *av, ParseValue *bv)
{
	Ast *a, *b, *aa, *m;
	int ta, tb, debug;
	CfgPrec *aprec, *bprec;

	a = av->ast;
	b = bv->ast;
	
	ta = -1;
	tb = -2;
	debug = 0;

	// Can get these when some productions can yield empty strings.
	// For example:
	// 	grammar {
	// 		S: A S "b";
	// 		S: "x";
	// 		A: ;
	// 	}
	// 	`S{xbbbbb};
	if(a->rule && a->rule == b->rule && a->rule->nright == 0)
		goto A_wins;

	aprec = a->rule ? a->rule->prec : nil;
	bprec = b->rule ? b->rule->prec : nil;

	// Rule preference resolution.
	if(aprec && bprec && aprec->kind == CfgPrecPrefer && bprec->kind == aprec->kind){
		int r = cfgcompareprec(aprec, bprec);
		if(r != 0)
			debug = 1;
		if(r > 0)
			goto A_wins;
		if(r < 0)
			goto B_wins;
	}

	// Rule priority resolution is done in the action rule
	// (because the merge-node parent actually does the resolution).
	// Associativity resolution is also done in the action rule,
	// so that it applies even if there is no ambiguity.

	// Ambiguous.
	if(FLAGS_parsemergedebug)
		fprint(2,"---- Merge [%p.%d/%p] ----\n%R\n%R\n", aprec, aprec ? aprec->assoc : 0, bprec, a->rule, b->rule);
		
	// First make a copy of a at a new location,
	// since the merged node is going to use a's
	// location, and we don't want it pointing at itself.
	aa = mkAst(a->op, a->sym);
	astswap(aa, a);
	
	// Now make the merge node.
	m = mkastmerge(aa, b);
	
	// Now move the merge node into a's location.
	astswap(a, m);
	
	// Free the scraps.
	astfree(m);
	astfree(aa);

	if(FLAGS_parsemergedebug)
		fprint(2, "%#lA\n----\n", av->ast);
	return;

A_wins:
	if(FLAGS_parsemergedebug){
		if(debug == 1)
			fprint(2,"---- A wins (prefer) ----\n%R\n%R\n", a->rule, b->rule);
		else if(debug == 2)
			fprint(2,"---- A wins (assoc) ----\n[%d] %#A\n[%d] %#A\n", ta, a, tb, b);
		else
			fprint(2,"---- A wins ----\n%R\n%R\n", a->rule, b->rule);
	}
	return;
B_wins:
	if(FLAGS_parsemergedebug){
		if(debug == 1)
			fprint(2,"---- B wins (prefer) ----\n%R\n%R\n", a->rule, b->rule);
		else if(debug == 2)
			fprint(2,"---- B wins (assoc) ----\n[%d] %#A\n[%d] %#A\n", ta, a, tb, b);
		else
			fprint(2,"---- B wins ----\n%R\n%R\n", a->rule, b->rule);
	}
	bv->ast = nil;	// now b is ours
	astswap(a, b);
	astfree(b);
	return;
}


Ast*
parsetext(Type *tg, Name text)
{
	Ast *ast;
	RgParse *p;
	char *t;
	ParseValue *pv;
	CfgSym *gsym;

	if(text == nil)
		text = N("");
	
	gsym = tg->sym;
	p = rgnewparse(gsym->gram);
	p->file = N("<string>");
	p->line = 1;
	p->col = 1;
	p->merge = glrmerge;
	p->free = pvfree;
	p->dup = pvdup;
//	p->touch = pvtouch;
	t = nametostr(text);
	parseactiontype = typegramsym(tg, nil, nil);
	pv = rgparse(gsym->gram, gsym, p, lextext, &t);
	parseactiontype = nil;
	if(pv == nil){
		// The lexer might have seen some # lines and 
		// updated p->file and p->line.
		fprint(2, "%s:%d: error parsing, near %.20s\n",
			p->file ? p->file : N("???"), p->line, t);
		return nil;
	}
	if(t != nil){
		fprint(2, "%s:%d: error parsing, didn't reach eof, near %.20s\n",
			p->file ? p->file : N("???"), p->line, t);
		return nil;
	}
	ast = astleak(pv->ast);
//	glrfreeparse(p);

	// XXX validateast ?

	return ast;
}

